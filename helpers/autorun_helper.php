<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function jalankan()
{

    $CI = get_instance();

    $CI->load->model('sistem/Tglbackup_model', '', true);

    $CI->load->helper('fungsi');

    //backup db---------------------------------------------------------------------------------------
    $kmrn = time() - (86400 * 2);
    $tgl_kmrn = date("Y-m-d", $kmrn);

    if ($CI->Tglbackup_model->check_backup($tgl_kmrn) == true) {
        $prefs = array(

            'format' => 'gzip', // gzip, zip, txt
            // File name - NEEDED ONLY WITH ZIP FILES
            'add_drop' => true, // Whether to add DROP TABLE statements to backup file
            'add_insert' => true, // Whether to add INSERT data to backup file
            'newline' => "\n", // Newline character used in backup file
        );
        $CI->load->dbutil();
        $backup = $CI->dbutil->backup($prefs);
        $CI->load->helper('file');
        $tgl = date("Y-m-d");
        write_file('./backup/project ' . $tgl . '.gzip', $backup);
        $tglxx = $CI->Tglbackup_model->update();
    }
    //backup db selesai---------------------------------------------------------------------------------------

    $tglxxx = tgl_hijriyah();

    $tgl_skr = $tglxxx[0];
    $tanggal_gaji = $CI->Tglbackup_model->ambil_gaji();

    if ($tanggal_gaji != $tgl_skr) {

        if ($tanggal_gaji > $tgl_skr) {
            $query = "gajian > '$tanggal_gaji' or gajian<='$tgl_skr'";
        } else {
            $query = "gajian > '$tanggal_gaji' and gajian<='$tgl_skr'";
        }

        $CI->load->model('sdm/Pegawai_model', '', true);
        $CI->load->model('personal/Whattodo_model', '', true);

        $hasil = $CI->Pegawai_model->get_gaji($query)->result();

        foreach ($hasil as $row) {

            $isi = $row->nama . " gajian tanggal " . $row->gajian;
            // Persiapan data

            $xx = who_auth("auth_reminder_gaji");

            foreach ($xx as $yy) {

                $whattodo = array(
                    'isi' => $isi,
                    'username' => $yy,
                );
                $CI->Whattodo_model->add($whattodo);
            }

//exit();

/*
$whattodo = array(
'isi'   => $isi,
'username'    => "admin"
);
$CI->Whattodo_model->add($whattodo);

 */
        }
        $tglxx = $CI->Tglbackup_model->update_gaji($tgl_skr);

    }

}
