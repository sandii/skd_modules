<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function cek_auth($halaman, $bypass = null)
{

    $CI = get_instance();

    $alamat = "login";

    isset($_SESSION['jabatan']) ? $jabatan = $_SESSION['jabatan'] : $jabatan = "";

    $CI->load->model('auth/Akses_model', '', true);

    $id_halaman = $CI->Akses_model->get_id_by_halaman($halaman)->id;

    $hasil = $CI->Akses_model->get_akses($jabatan, $id_halaman);

    $hasil > 0 ? $boleh = true : $boleh = "";

    if (empty($boleh) and $jabatan != "admin") {

        if ($bypass) {
            redirect($alamat);
        } else {
            return false;
        }

    } else {
        return true;
    }

}

function who_auth($halaman)
{

    $CI = get_instance();

    $alamat = "login";

    isset($_SESSION['jabatan']) ? $jabatan = $_SESSION['jabatan'] : $jabatan = "";

    $CI->load->model('auth/Akses_model', '', true);

    $id_halaman = $CI->Akses_model->get_id_by_halaman($halaman)->id;

    $hasil = $CI->Akses_model->get_akses_who($id_halaman);
    $orders = $hasil->result();
    $num_rows = $hasil->num_rows();

    $datax = array();

    if ($num_rows > 0) // Jika query menghasilkan data
    {

        foreach ($orders as $order) {

            //$hasil2=$CI->Akses_model->get_akses($order->nama,$halaman);

            //isset($hasil2->hak)?$boleh=$hasil2->hak:$boleh="";

            array_push($datax, $order->username);

        }
    }

    return $datax;

}

function hitung_sisa($akhir)
{

    $hari = floor($akhir / (24 * 60 * 60));
    $skr = (mktime(7, 0, 0, date("n"), date("j"), date("Y")) / (24 * 60 * 60));

    if (!empty($akhir)) {

        $hh = $hari - $skr;

        if ($hh == 0) {
            $hari2 = floor($akhir / (60 * 60));
            $skr2 = floor(time() / (60 * 60));
            $jamx = $hari2 - $skr2;

        }

        $class = "danger";
        $waktu = " hari";
        $lewat = "kurang ";
        $hhx = $hh;

        if ($hh > 4) {
            $class = "success";
        } else if ($hh > 2) {
            $class = "info";
        } else if ($hh > 0) {
            $class = "warning";
        } else if ($hh < 0) {$lewat = "lewat ";
            $class = "danger";
            $hhx = abs($hh);

        } else if ($hh == 0) {$waktu = " jam";
            $hhx = $jamx;

            if ($jamx < 0) {
                $lewat = "lewat ";
                $hhx = abs($jamx);
                $class = "danger";
            }

        }

//    return ' <div id=timeline class='.$class.'>'.$lewat.$hhx.$waktu.'</div>';
        return ' <span class="label label-' . $class . '" >' . $lewat . $hhx . $waktu . '</span>';

    }

}

function hitung_tanggal($xx)
{

    $hasil = array();
    if (!empty($xx)) {
        $yy = explode(" ", $xx);
        $zz = explode(":", $yy[1]);

        $hasil[0] = $yy[0];
        $hasil[1] = $zz[0];
    } else {
        $hasil[0] = "";
        $hasil[1] = "";
    }
    return $hasil;

}

function format_uang($angka)
{
    return "<div align=right>" . number_format($angka, 0, ',', '.') . "</div>";}

function bikin_nav($data_nav, $aktif = null)
{

    $hasil = "";

    foreach ($data_nav as $datax) {

        if ($datax['nama'] == $aktif) {
            $aktifx = " class=active ";
        } else {
            $aktifx = "";
        }

        if (!empty($datax['judul'])) {
            $judul = $datax['judul'];
        } else {
            $judul = $datax['nama'];
        }

        $hasil .= "<li " . $aktifx . "style='height:30px'>" . anchor($datax['link'], $judul) . '</li>';

    }

    return $hasil;

}

function ambil_var($var)
{

    $CI = get_instance();
    $CI->load->model('auth/Akses_model', '', true);

    return $CI->Akses_model->get_variable_sistem_by_id($var)->isi;

}

function tgl_hijriyah()
{

    $CI = get_instance();
    $CI->load->model('sistem/Hijriyah_model', '', true);
    $now = time();
    $query = $CI->Hijriyah_model->get_before_date();

    $day = strtotime($query->tgl_masehi);
    $month = bln_hijriyah($query->bulan);
    $year = $query->tahun_hijriyah;

    $diff   = $now - $day;
    $hijriyah = floor($diff / (60 * 60 * 24));
    $hijriyah += 1 ;

    if($hijriyah > 30){
        $hijriyah = "tanggal belum di set";
        $m = 0;
        $year = "";
    }

    return array(
         $hijriyah,
         $month,
         $year, $query->bulan
    );

}

function bln_hijriyah($bulan)
{

    $month = array(1 => "Muharram", "Shofar", "Robi'ul Awwal", "Robi'ul Akhir",
        "Jumadil Ula", "Jumadil Akhir", "Rojab", "Sya'ban",
        "Romadhon", "Syawal", "Dzulqo'dah", "Dzulhijjah");

    return $month[$bulan];

}
