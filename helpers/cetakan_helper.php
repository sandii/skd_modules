<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function hitung_kertas($jenis,$tebal,$panjang,$lebar,$jumlah)
{


if($panjang<$lebar)
{
$xx=$panjang;
$panjang=$lebar;
$lebar=$xx;
}

$panjang=$panjang+0.5;
$lebar=$lebar+1;

				   

$j=new jeni();
   
$k=new kertaspotong();
$i=0;
$k->group_start();
    foreach($j->where('name',$jenis)->get()->kerta->where('tebal',$tebal)->get() as $row)
   {
   
   if($i==0)
   $k->where_related('kertasukuran','id',$row->kertasukuran->get()->id);
   else
   $k->or_where_related('kertasukuran','id',$row->kertasukuran->get()->id);
  $i++;
   }

 $k->group_end()->where('panjang >=',$panjang)->where('lebar >=',$lebar);
  
$hasil['bahan']="";	
 
 
 
 foreach($k->order_by('luas','asc')->get(3,0)->all as $urutan => $rr)
 {
	

if($urutan==0)
{
$kk= new kerta();
$kk->where_related('kertasukuran','id',$rr->kertasukuran->get()->id)->get();
$hasil['harga']=$jumlah*$kk->harga/$rr->jadi;
}


$urutan++;

$nama=$rr->kertasukuran->get()->name;
$hasil['bahan'].="alternatif $urutan <br>ukuran plano:".$nama."<br>max potong ukuran: ".$rr->panjang."x".$rr->lebar."<br> 1 plano jd: ".$rr->jadi."<br><br>";

}



return $hasil;
}


function hitung_cetak($set,$cetak,$warna,$panjang,$lebar,$jumlah)
{
if($panjang<$lebar)
{
$xx=$panjang;
$panjang=$lebar;
$lebar=$xx;
}

    
$j=new mesin();
   
$j->where('cetakmaxpanjang >=',$panjang);
$j->where('cetakmaxlebar >=',$lebar);

$j->order_by('id','asc')->limit(1,0)->get();


$setcetak=$set;

// jika mesin GTO dan (cetak bolak balik atau set lebih dari 1)
if($j->nama=='GTO52' and ($cetak==3 or $set>1))
{
    
$kk=new kertasgto();

$kk->where('panjang >=',$panjang);
$kk->where('lebar >=',$lebar);
if($cetak==3)
$kk->where('jadi %2=0');



$kk->order_by('jadi','desc')->limit(5,0)->get();

echo$kk->jadi."<br>".$kk->panjang."<br>".$kk->lebar."<br>";
if($kk->jadi>=2)
{
    
    
}
else if ($cetak==2)
$jumlah=$jumlah*2;
elseif($cetak==3)
$setcetak*=2;  
} 
else if ($cetak==2)
$jumlah=$jumlah*2;
elseif($cetak==3)
$setcetak*=2;  



$harga=$j->harga_min*$warna;
if($jumlah>$j->cetak_min)
$harga+=$j->harga_druk*$warna*($jumlah-$j->cetak_min);


$harga=$harga*$setcetak;

$hasil['harga']=$harga; 

return $hasil;
}

function hitung_laminasi($finishing,$panjang,$lebar,$jumlah)
{

if($panjang<$lebar)
{
$xx=$panjang;
$panjang=$lebar;
$lebar=$xx;
}

$nama=array('','uv','uv','glossy','glossy','dof','dof','spotuv','spotuv');
$muka=array('',1,2,1,2,1,2,1,2);
    
$fin=new master();  
$fin->where('nama',$nama[$finishing])->get();
$harga=$fin->perdruk*$panjang*$lebar*$muka[$finishing];
if($harga<$fin->harga_min)
$harga=$fin->harga_min;


if($nama[$finishing]=='spotuv')
{
$fin=new master();  
$fin->where('nama','dof')->get();
$hargadof=$fin->perdruk*$panjang*$lebar*$muka[$finishing];
if($hargadof<$fin->harga_min)
$hargadof=$fin->harga_min;   
 
$harga+=$hargadof;    
}

$hasil['harga']=$harga;


return $hasil;
}


function insheet($jumlah)

{
    if($jumlah<=1000)
$insheet=100;
else
$insheet=50+ceil($jumlah/20);

return $jumlah+$insheet; 
    
}
