<?php

class Crud extends CI_Controller {


function __construct($params = array())
	{
		
		$this->CI =& get_instance(); 
		$this->session =& $this->CI->session;
		$this->table =& $this->CI->table;	
		$this->pagination =& $this->CI->pagination;	
    $this->input =& $this->CI->input;
    $this->form_validation =& $this->CI->form_validation;


  $this->data_utama = $params['data_utama'];
  $this->data_field = $params['data_field'];
  $this->fitur = $params['fitur'];

  $this->model = "Crud_model";



	}		
	function retrieve($data_input=null)
	{
		$datax = new $this->model();
	  $id_primary=$this->data_utama['primary'];         

$where=null;
$url_tambahan="";


if(!empty($this->fitur['search']))
{

	$data['url'] = site_url($this->data_utama['alamat']);

$data['custom_view']='crud_custom_view';



$ii=0;

	foreach ($this->fitur['search'] as $key => $xx)
	{





$nama=!empty($data_input[$ii])?$data_input[$ii]:'';


 if($this->input->post('cari'))
     $nama=$this->input->post($xx);



    

	if(empty($nama) or $nama=="semua")
	{$nama="semua"; }
    else 
{
if(empty($this->lookup[$key]))
    	$where[$xx]="$xx like '%$nama%'"; 
else
    	$where[$xx]="$xx = $nama "; 


}

         $data['data_field'][$xx]=$nama;

$url_tambahan.='/'.$nama;





if(empty($this->data_field[$xx]['lookup_table']))
 $data['form_fields'][$xx]=array('label'=>$xx);
    
else
 {  

$isi_field=$this->data_field[$xx]['lookup_table']['isi'];
$id_field=$this->data_field[$xx]['lookup_table']['id'];

$ddd=array('table'=>$this->data_field[$xx]['lookup_table']['nama_table']);


$hasil= $datax->get_all($ddd)->result();
$options[0]="semua";
foreach ($hasil as $row)
$options[$row->$id_field]=$row->$isi_field;


if(!empty($this->data_field[$xx]['judul']))
$labelx=$this->data_field[$xx]['judul'];
else
$labelx=$xx;


 $data['form_fields'][$xx]=
 array(
 	'label'=>$labelx,
 	'jenis'=>'lookup',
 	'options'=>$options,
 	);

}
$ii++;
	}
$nomor_offset=$ii;

}

if(!empty($this->data_utama['order_by']))
$orderxx=	$this->data_utama['order_by'];
else
$orderxx=	$id_primary;



$aas=array('table'=>$this->data_utama['nama_table'],
'order_by'=>$orderxx,'where'=>$where);

		$query=$datax->get_all($aas);

		$baris = $query->result();
		$num_rows = $query->num_rows();

			if ($num_rows > 0)
		{

	$i = 0;

if(!empty($this->fitur['pagination']))
{

$config['per_page'] = $this->fitur['pagination']['per_page'];
$config['uri_segment'] = $this->fitur['pagination']['uri_segment'];
$config['base_url'] = $this->fitur['pagination']['base_url'].$url_tambahan;


$config['total_rows'] = $num_rows;
	

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';


			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();


$aas['limit']=$this->fitur['pagination']['per_page'];
$i=$aas['offset']=isset($data_input[$nomor_offset])?$data_input[$nomor_offset]:0;


		$query=$datax->get_all($aas);

		$baris = $query->result();
}


			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			$this->table->set_empty("&nbsp;");
$header_tabel=array();

	foreach ($this->data_field as $key => $xx)
	{

if(empty($xx['judul']))
array_push($header_tabel,$key);
else
array_push($header_tabel,$xx['judul']);


}


      array_unshift($header_tabel,"No");
      array_push($header_tabel,"");

    	$this->table->set_heading($header_tabel);
		
			
			foreach ($baris as $row)
			{
			
			$dataxx=array();
	foreach ($this->data_field as $key => $xx)
	{


$jenisx=!empty($xx['jenis'])?$xx['jenis']:'';

	
        
if(!empty($xx['lookup_table']))
 {    


$isi_field=$xx['lookup_table']['isi'];
$id_field=$xx['lookup_table']['id'];
$nama_tablez=$xx['lookup_table']['nama_table'];


$nama_lookup=$datax->get_by_id($nama_tablez,$id_field,$row->$key)->nama;


      array_push($dataxx,$nama_lookup);

}
else if($jenisx=='gambar')
{

if(empty($row->$key))
$penampakan=anchor_popup($this->data_utama['alamat']."/gambar/".$row->$id_primary,'<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>',array("class"=>"btn btn-warning btn-xs","height"=>1000,"width"=>1000));
else
$penampakan=anchor_popup($this->data_utama['alamat']."/gambar/".$row->$id_primary,"<img src=".base_url().'gambar/'.$this->data_utama['nama_table'].'/'.$row->$key."_thumb.jpg >",array("height"=>700,"width"=>1000));
	



      array_push($dataxx,$penampakan);


}

else
      array_push($dataxx,$row->$key);
	 
	 }     
     
   
  $actions=anchor($this->data_utama['alamat'].'/edit/'.$row->$id_primary,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'));
  
  if(!empty($this->fitur['hapus']))
  $actions.=anchor($this->data_utama['alamat'].'/delete/'.$row->$id_primary,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"));
  
  
              
        array_unshift($dataxx,++$i);        
				array_push($dataxx, $actions);
				$this->table->add_row($dataxx);					
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data!';
		}				
		
    if(!empty($this->fitur['tambah']))
    $data['link'] = array('link_add' => anchor($this->data_utama['alamat'].'/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
		// Load view
		return $data;
	
	}			
	function update($id = -1)
	{
		// Create User Object
		$datax = new $this->model();		
  $id_primary=$this->data_utama['primary'];

		
		if($id == 'save')
		{
			// Try to save the user
					
			$id = $this->input->post('id');			
				

	foreach ($this->fitur['validation'] as $key => $xx)
{
	
$this->form_validation->set_rules($key, $key, $xx);

}


if ($this->form_validation->run() == TRUE)
{
if(empty($id))

{
unset($_POST['id']);	
$success=$datax->add($this->data_utama['nama_table'],$_POST);

}	

else

{
unset($_POST['id']);	

$success=$datax->update($this->data_utama['nama_table'],$_POST,$id_primary,$id);

}	

	$data_field=$datax->get_by_id($this->data_utama['nama_table'],$this->data_utama['primary'],$id);
						
			$isian=$_POST;
			

			// redirect on save
			if($success)
			{
				
				if($id < 1)
				{
					$this->session->set_flashdata('message', ' data  berhasil disimpan!');
				}
				else
				{		
					$this->session->set_flashdata('message', ' data  berhasil diupdate!');
				}
				redirect($this->data_utama['alamat']);
			}
}
		}

	$data_field=$datax->get_by_id($this->data_utama['nama_table'],$this->data_utama['primary'],$id);	

				
		foreach ($this->data_field as $key => $xx)
	{
		

if(!empty($xx['jenis']))
$data_jenis=$xx['jenis'];
else
$data_jenis="";





if(!empty($xx['judul']))
$labelx=$xx['judul'];
else
$labelx=$key;



if(!empty($xx['lookup_table']))

 {  

$isi_field=$xx['lookup_table']['isi'];
$id_field=$xx['lookup_table']['id'];
$sss=array('table'=>$xx['lookup_table']['nama_table']);


$hasil= $datax->get_all($sss)->result();
$options=array("pilih ".$labelx);


foreach ($hasil as $row)
$options[$row->$id_field]=$row->$isi_field;


 $data['form_fields'][$key]=
 array(
 	'label'=>$labelx,
 	'jenis'=>'lookup',
 	'options'=>$options,
 	);

}
else if(!empty($xx['lookup']))

 {  

$tambahan=array("pilih ".$labelx);


$options=array_merge($tambahan,$xx['lookup']);


 $data['form_fields'][$key]=
 array(
 	'label'=>$labelx,
 	'jenis'=>'lookup',
 	'options'=>$options,
 	);

}


else if($data_jenis!='gambar')
    $data['form_fields'][$key]=array('label'=>$labelx,'jenis'=>$data_jenis);


if(!empty($this->fitur['validation'][$key]))
	   $data['form_fields'][$key]['form_error']=true;

	 }     
		
		// Set up page text
		if($id > 0)
		{
			$title = 'Edit User';
			$data['url'] = site_url($this->data_utama['alamat'].'/edit/save');
		}
		else
		{
			$title = 'Add User';
			$data['url'] =site_url( $this->data_utama['alamat'].'/add/save');
		}
		$data["data_field"]=$data_field;

$data['primary']=$id_primary;
$data['custom_view'] = 'crud_form'; 
$data['link']= array(anchor($this->data_utama['alamat'],'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));



	return $data;
	}
			
	function delete($id = 0)
	{
		$datax = new $this->model();
	
	$datax->delete($this->data_utama['nama_table'],$this->data_utama['primary'],$id);
	
			$this->session->set_flashdata('message', '1 data berhasil dihapus');
			redirect($this->data_utama['alamat']);
		}	





function gambar($data_input=null)
	{
		$datax = new $this->model();
	  $id_primary=$this->data_utama['primary'];         
	  $data['custom_view']='crud_gambar';         
$data['url'] = site_url($this->data_utama['alamat'].'/upload_gambar');
$data['isi_primary'] = $data_input[0];





$data_field=$datax->get_by_id($this->data_utama['nama_table'],$this->data_utama['primary'],$data_input[0]);	

$data['h2_title']=$data_field->nama;



if(!empty($data_field->gambar))
$data['gambar']=$this->data_utama['nama_table'].'/'.$data_field->gambar;

 		
		// Load view
		return $data;
	
	}





function upload_gambar()
	{

	$id = $this->input->post('id');			
				

$alamat_gambar="./gambar/".$this->data_utama['nama_table'];




		$config['upload_path'] = $alamat_gambar ;
		$config['allowed_types'] = 'jpg';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';
		$config['file_name']  = $id.'.jpg';


$config['overwrite']=true;



$this->CI->load->library('upload', $config);


	if (!$this->CI->upload->do_upload())
		{



			$this->session->set_flashdata('message', 'gambar tidak berhasil diupload');
			redirect($this->data_utama['alamat'].'/gambar/'.$id);
	


		}
		else
		{





$this->CI->load->library('image_lib'); 





$config2['image_library'] = 'gd2';
$config2['source_image']	=$alamat_gambar."/".$id.".jpg";
$config2['create_thumb'] = true;
$config2['maintain_ratio'] = TRUE;
$config2['width']	= 110;
$config2['height']	= 80;


$this->CI->image_lib->initialize($config2); 

$this->CI->image_lib->resize();

$this->CI->image_lib->clear();

$xx=$this->CI->upload->data();
if($xx['image_width']>980 or $xx['image_height']>650)
{
$config3['image_library'] = 'gd2';
$config3['source_image']	=$alamat_gambar."/".$id.".jpg";
$config3['maintain_ratio'] = TRUE;
$config3['width']	= 980;
$config3['height']	= 650;
$config3['create_thumb'] = false;


$this->CI->image_lib->initialize($config3); 

$this->CI->image_lib->resize();
}


$datax=new $this->model();

$success=$datax->update($this->data_utama['nama_table'],array('gambar'=>$id),$this->data_utama['primary'],$id);


	
			$this->session->set_flashdata('message', 'gambar berhasil diupload!');
			redirect($this->data_utama['alamat'].'/gambar/'.$id);
	
	}


}





}



