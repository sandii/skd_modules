<?php
/**
 * Depan Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Sop extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'S O P';
	var $limit = 20;
  
  function Sop()
	{
		parent::__construct();
		$this->load->model('Sop_model', '', TRUE);
		
	
  
 	  $this->load->helper('fungsi'); 
  	
   $this->load->library('ckeditor');
   $this->load->library('ckfinder');
	 $this->ckeditor->basePath = base_url().'asset/ckeditor/';
   $this->ckeditor-> config['toolbar'] = 'Full';
   $this->ckeditor->config['language'] = 'en';	
	 $this->ckfinder->SetupCKEditor($this->ckeditor,'../../../../asset/ckfinder');

	
	 
	 
	 
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
 
	      $this->load->library('cekker');
   $this->cekker->cek($this->router->fetch_class());
  
  }

	function index($bagian,$offset = 0,$lihat="")
	{
		
		$data = $this->data;
		$data['h2_title'] = "SOP ".$bagian;
	
		
		// Offset
		$uri_segment = 4;
		$offset = $this->uri->segment($uri_segment);
		
		// Load data
		$depan = $this->Sop_model->get_all($bagian,$this->limit, $offset);
		$num_rows = $this->Sop_model->count_all($bagian);
		
		if ($num_rows > 0)
		{
			$config['base_url'] = site_url('sop/index/'.$bagian);
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
					$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';

			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

			$i = 0 + $offset;
			
			
			if($offset==0)
			$offset="0";
			
			foreach ($depan as $row)
			{
				$this->table->add_row(++$i.". ".
			           	anchor_popup('sop/detail/'.$bagian.'/'.$row->id_sop,$row->judul,array("class"=>"detail",'width'=>1000,'height'=>700))
													);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data '.$bagian;
		}		
		
		$data['link'] = "";
							
							
  if(cek_auth("auth_peraturan") or $bagian!="peraturan")  
	$data['link'] = array(anchor('sop/add/'.$bagian.'/xx','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>',  array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	
	
		// Load view
		$this->load->view('template', $data);
	}




	function detail($bagian,$lihat)
		
	{
	

		// cari data dari database
		$depan = $this->Sop_model->get_depan_by_id($lihat);
		
		// Data untuk mengisi field2 form
		$data['h2_title'] = $depan->judul;


		$data['content']		=  $depan->isi;
        									
$data['link'] =array();
  if(cek_auth("auth_peraturan") or $bagian!="peraturan") 
  {
 	array_push($data['link'],anchor('sop/update/'.$depan->id_sop."/".$bagian,'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		array_push($data['link'], anchor('sop/delete/'.$depan->id_sop.'/'.$bagian,'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array('class' => 'btn btn-danger btn-lg','role'=> 'button','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")));
}
	

	// Load view
		$this->load->view('template2', $data);







	}
		












		
	/**
	 * Hapus data depan
	 */
	function delete($id_sop,$bagian)
	{
		if($bagian=="peraturan")
	cek_auth("auth_peraturan",1);
	
	
		$this->Sop_model->delete($id_sop);
		$this->session->set_flashdata('message', '1 data '.$bagian.' berhasil dihapus');
		
		redirect('sop/index/'.$bagian);
	}
	
	/**
	 * Pindah ke halaman tambah depan
	 */
	function add($bagian)
	{		
	
	if($bagian=="peraturan")
	cek_auth("auth_peraturan",1);
	
	
		$data 			= $this->data;
				$data['subtitle'] = $bagian;
		$data['custom_view'] 		= 'sop/sop_form';
		$data['form_action']	= site_url('sop/add_process/'.$bagian);
		$data['link'] 			= array('link_back' => "<a href='javascript: history.go(-1)' class=back>kembali</a>");
		
		
		$data['my_editor']="";
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data depan
	 */
	function add_process($bagian)
	{
		$data 			= $this->data;
			$data['subtitle'] = $bagian;
		$data['custom_view'] 		= 'sop/depan_form';
		$data['form_action']	= site_url('sop/add_process/'.$bagian);
		$data['link'] 			= array('link_back' => "<a href='javascript: history.go(-1)' class=back>kembali</a>");
	
	
			// Persiapan data
			$depan = array(
			'judul'		=> $this->input->post('judul'),
							'isi'		=> $this->input->post('my_editor'),
							'bagian'		=> $bagian
						);
			// Proses penyimpanan data di table depan
			$this->Sop_model->add($depan);
			
			$this->session->set_flashdata('message', 'Satu data '.$bagian.' berhasil disimpan!');
			redirect('sop/index/'.$bagian);
			
	}
	
	/**
	 * Pindah ke halaman update depan
	 */
	function update($id_sop,$bagian)
	{
	
		if($bagian=="peraturan")
	cek_auth("auth_peraturan",1);
	
		$data			= $this->data;
			$data['h2_title'] = "SOP ".$bagian;
		$data['custom_view'] 		= 'sop/sop_form';
		$data['form_action']	= site_url('sop/update_process/'.$bagian);
	$data['link'] 			= array('link_back' => "<a href='javascript: history.go(-1)' class='btn btn-success btn-lg'><span class='glyphicon glyphicon-arrow-left' aria-hidden='true'></span></a>");
	
		// cari data dari database
		$depan = $this->Sop_model->get_depan_by_id($id_sop);
				
		// buat session untuk menyimpan data primary key (id_sop)
		$this->session->set_userdata('id_sop', $depan->id_sop);
		
		// Data untuk mengisi field2 form
		$data['default']['id_sop'] 	= $depan->id_sop;		
				$data['default']['judul'] 	= $depan->judul;		
		$data['my_editor']= $depan->isi;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data depan
	 */
	function update_process($bagian)
	{
		$data 			= $this->data;
				$data['subtitle'] = $bagian;
		$data['custom_view'] 		= 'sop/sop_form';
		$data['form_action']	= site_url('sop/update_process'.$bagian);
		$data['link'] 			= array('link_back' => "<a href='javascript: history.go(-1)' class=back>kembali</a>");
										
		// Set validation rules
		//$this->form_validation->set_rules('depan', 'Depan', 'required|max_length[32]');
		
		
			// save data
			$depan = array(
							'isi'		=> $this->input->post('my_editor'),	'judul'		=> $this->input->post('judul')
						);
			$this->Sop_model->update($this->session->userdata('id_sop'),$depan);
			
			$this->session->set_flashdata('message', 'Satu data '.$bagian.' berhasil diupdate!');
			redirect('sop/index/'.$bagian);
		
	}
	
	
	/**
	 * Cek apakah $id_sop valid, agar tidak ganda
	 */
	function valid_id($id_sop)
	{
		if ($this->Sop_model->valid_id($id_sop) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "depan dengan Kode $id_sop sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_sop valid, agar tidak ganda. Hanya untuk proses update data depan
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_sop');
		$new_id			= $this->input->post('id_sop');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->Sop_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "Depan dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END Depan Class

/* End of file depan.php */
/* Location: ./system/application/controllers/depan.php */
