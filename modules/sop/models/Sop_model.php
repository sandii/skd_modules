<?php
/**
 * Marketing_model Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Sop_model extends CI_Model
{
    /**
     * Constructor
     */
    public function Sop_model()
    {
        parent::__construct();

        // the TRUE paramater tells CI that you'd like to return the database object.
        $this->db_service = $this->load->database('db_service', true);
    }

    // Inisialisasi nama tabel yang digunakan
    public $table = 'sop';

    /**
     * Mendapatkan semua data depan, diurutkan berdasarkan id_depan
     */
    public function get_depan()
    {

        $this->db_service->order_by('id_sop');
        return $this->db_service->get('isi');
    }

    public function get_all($bagian, $limit, $offset)
    {
        $this->db_service->select('*');
        $this->db_service->from($this->table);
        $this->db_service->where('bagian', $bagian);
        $this->db_service->limit($limit, $offset);
        $this->db_service->order_by('id_sop', 'asc');
        return $this->db_service->get()->result();
    }

    /**
     * Mendapatkan data sebuah depan
     */
    public function get_depan_by_id($id_depan)
    {
        return $this->db_service->get_where($this->table, array('id_sop' => $id_depan), 1)->row();
    }

    public function get_depan_rand()
    {
        $this->db_service->where('bagian', 'depan');
        $this->db_service->order_by('rand()');
        $this->db_service->limit(1);
        return $this->db_service->get($this->table)->row();
    }

    /**
     * Menghapus sebuah data depan
     */
    public function delete($id_depan)
    {
        $this->db_service->delete($this->table, array('id_sop' => $id_depan));
    }

    /**
     * Tambah data depan
     */
    public function add($depan)
    {
        $this->db_service->insert($this->table, $depan);
    }

    /**
     * Update data depan
     */
    public function update($id_depan, $depan)
    {
        $this->db_service->where('id_sop', $id_depan);
        $this->db_service->update($this->table, $depan);
    }

    public function count_all($bagian)
    {
        $this->db_service->select('*');
        $this->db_service->from($this->table);
        $this->db_service->where('bagian', $bagian);

        return $this->db_service->count_all_results();

    }

    /**
     * Validasi agar tidak ada depand dengan id ganda
     */

}
// END Siswa_model Class

/* End of file depan_model.php */
/* Location: ./system/application/models/depan_model.php */
