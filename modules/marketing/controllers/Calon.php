<?php
/**
 * calon Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Calon extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */


	var $title = 'Project';
		  	var $alamat = 'marketing/calon';


  function Calon()
	{
		parent::__construct();
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Calon_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);
		$this->load->model('produksi/Order_model', '', TRUE);
		$this->load->model('produksi/Produk_model', '', TRUE);
		$this->load->model('produksi/Pemroses_model', '', TRUE);
		$this->load->model('sistem/Variable_sistem_model', '', TRUE);



	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;

	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());


    $this->load->helper('fungsi');

	}


		function index($pj="semua")
	{

		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = "calon";

		$data['arsip'] = true;

$query1="proses";
$query2="";

   if($this->input->post('cari'))
     {

     $pj=$this->input->post('pj');


     }


 $data['default']["pj"]=$pj;


if($pj=="semua")
$cari_pj="";
else $cari_pj=$pj;



$hasil= $this->Pegawai_model->get_aktif()->result();




$data["options_pj"]["semua"]="semua";

foreach ($hasil as $row)
{
	$data["options_pj"][$row->id_pegawai]=$row->nama;

}

$cari_kons="";

// Load data
$calon= $this->Calon_model->aktif('', '',$cari_kons,$cari_pj,$query1,$query2)->result();










			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table width=100% >',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

      $heading=array("data"=>"no", "width"=>"90%") ;

    	$this->table->set_heading(array("data"=>"no", "width"=>"2%"),
      array("data"=>"konsumen", "width"=>"10%"),array("data"=>"PJ", "width"=>"6%"),
      array("data"=>"project", "width"=>"25%"),

      array("data"=>"progress", "width"=>"24%"),
      array("data"=>"follow up"),
      array("data"=>"detail", "width"=>"8%")
    );
			$i = 0;

			foreach ($calon as $row)
			{

$terakhirx=$row->terakhir;
$hari=ceil($terakhirx/(24*60*60));
$skr=(mktime(7, 0, 0, date("n"), date("j"), date("Y"))/(24*60*60));
 $sisa=$skr-$hari;

  if($sisa==0)
  $sisa='barusan';
  else
  $sisa=$sisa.' hari  lalu';


 $timeline=hitung_sisa($row->terakhir2);


  $sisa="<span class='glyphicon glyphicon-zoom-in'>";

 $logx=explode("<font",$row->calon_log);
  $log="<font ".end($logx);
$log=substr($log, 0,150);

$perusahaanx=$this->Konsumen_model->get_konsumen_by_id($row->id_konsumen);

 $perusahaanx2 =anchor_popup('konsumen/konsumendetil/index/'.$row->id_konsumen,$perusahaanx->perusahaan,array('class' => '','width'=>1000));
$baru="";
$pegawai=$this->Pegawai_model->get_pegawai_by_id($row->id_pegawai);

        $this->table->add_row(++$i,   $perusahaanx2
        ,!empty($pegawai->nama)?$pegawai->nama:''
          ,$row->pertanyaan
       ,$log
,$timeline       ,anchor_popup('marketing/calondetil/index/'.$row->id_calon."/".$baru,$sisa,array('class' => 'btn btn-info btn-xs','width'=>1000))



										);
			}
			$data['table'] = $this->table->generate();

		$this->load->view('template', $data);
	}


	function arsip($pj="semua",$offset = 0)
	{

		$data = $this->data;
		$data['h2_title'] = "arsip ".$this->title;
		$data['custom_view'] = "calon";

		$data['arsip'] = true;


$limit=10;
$query1="beres";
$query2="order";


   if($this->input->post('cari'))
     {


     $pj=$this->input->post('pj');


     }


 $data['default']["pj"]=$pj;


if($pj=="semua")
$cari_pj="";
else $cari_pj=$pj;



$hasil= $this->Pegawai_model->get_aktif()->result();




$data["options_pj"]["semua"]="semua";

foreach ($hasil as $row)
{
	$data["options_pj"][$row->id_pegawai]=$row->nama;

}

$cari_kons="";

// Load data
$calon= $this->Calon_model->aktif($limit, $offset,$cari_kons,$cari_pj,$query1,$query2)->result();


$num_rows= $this->Calon_model->aktif("","",$cari_kons,$cari_pj,$query1,$query2)->num_rows();




		if ($num_rows > 0)
		{

$uri_segment = 5;
			// Generate pagination
			$config['base_url'] = site_url($this->alamat.'/arsip/'.$pj.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $limit;
			$config['uri_segment'] = $uri_segment;



$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';






			$this->pagination->initialize($config);





			$data['pagination'] = $this->pagination->create_links();


			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table width=100% >',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

      $heading=array("data"=>"no", "width"=>"90%") ;

    	$this->table->set_heading(array("data"=>"no", "width"=>"2%"),
      array("data"=>"konsumen", "width"=>"10%"),array("data"=>"PJ", "width"=>"6%"),
      array("data"=>"project", "width"=>"25%"),

      array("data"=>"progress", "width"=>"24%"),
      array("data"=>"detail", "width"=>"8%")
    );
			$i = 0 + $offset;

			foreach ($calon as $row)
			{


//$kar = array(" ",".", ",", "%", "(", ")", ">","<","!","@","#","$","^","&","*","+","=","{","}","[","]","|","\\",":",";","\'","\"","?","/","~","`");
//$url_perusahaan = str_replace($kar, "-",$row->nama);





  $sisa="<span class='glyphicon glyphicon-zoom-in'>";

 $logx=explode("<font",$row->calon_log);
  $log="<font ".end($logx);


$perusahaanx=$this->Konsumen_model->get_konsumen_by_id($row->id_konsumen);

$pegawai=$this->Pegawai_model->get_pegawai_by_id($row->id_pegawai);


 $perusahaanx2 =anchor_popup('konsumen/konsumendetil/index/'.$row->id_konsumen,$perusahaanx->perusahaan,array('class' => '','width'=>1000));
$baru="";


        $this->table->add_row(++$i,   $perusahaanx2
				,!empty($pegawai->nama)?$pegawai->nama:''

          ,$row->pertanyaan
       ,$log
       ,anchor_popup('marketing/calondetil/index/'.$row->id_calon."/".$baru,$sisa,array('class' => 'btn btn-info btn-xs','width'=>1000))



										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data calon!';
		}



		$this->load->view('template', $data);
	}


	/**
	 * Menghapus data calon dengan id_calon tertentu
	 */

	/**
	 * Menampilkan form tambah calon
	 */
	function add($status="lama",$id_konsumen="")
	{
		$data	= $this->data;
		$data['h2_title'] 		= 'tambah project';
		$data['status']=$status;
		$data['custom_view'] 		= 'calon_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$status."/".$id_konsumen);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);


										$pegawai = $this->Pegawai_model->get_aktif()->result();
										$data['default']['id_pegawai']=$_SESSION['id_user'];
										foreach($pegawai as $row)
										{
											$data['options_pegawai'][$row->id_pegawai]=$row->nama;
										}

		$this->load->view('template', $data);
	}
	/**
	 * Proses tambah data calon
	 */
	function add_process($status="lama",$id_konsumen="")
	{
			$data	= $this->data;
		$data['h2_title'] 		= 'tambah project';
		$data['status']=$status;
		$data['custom_view'] 		= 'calon_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$status."/".$id_konsumen);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
									);

										$pegawai = $this->Pegawai_model->get_aktif()->result();
										$data['default']['id_pegawai']=$_SESSION['id_user'];
										foreach($pegawai as $row)
										{$data['options_pegawai'][$row->id_pegawai] = $row->nama;}



$terakhir=$id_konsumen;


			// save data

 $hari=array(1=>"senin",2=>"selasa",3=>"rabu",4=>"kamis",5=>"jumat",6=>"sabtu",7=>"minggu");



$follow_up_lg=date("Y-m-d", time()+(86400*$this->input->post('follow_up_lg')))." 09:00:00";


$commentx="<font color=blue>".$_SESSION['username'].': '.$hari[date("N")]." ". date("d-m-Y")."</font> ";



		$commentx.=$this->input->post('jawaban')."<br>";



			$calon = array(
						  'id_konsumen' => $terakhir,
						'pertanyaan'		=> $this->input->post('pertanyaan'),
						'id_pegawai'		=> $this->input->post('id_pegawai'),

			'tanggal' => date("Ymd"),
			'follow_up_lg'=>$follow_up_lg,
						'pertanyaan'		=> $this->input->post('pertanyaan'),
								'calon_log'		=> $commentx,
						'status'		=> 'proses',
'follow_up' => date("Ymd") ,'jenis'=>'lama'


            );


if($status=="baru"){


							$calon['nama']=$this->input->post('perusahaan');
							$calon['kontak']		= $this->input->post('kontak');


			$calon['via']		= $this->input->post('via');
				$calon['jenis']	="baru";
          }



			$this->Calon_model->add($calon);




			$this->session->set_flashdata('message', 'Satu data calon berhasil disimpan!');
			redirect($this->alamat.'/index/semua/aktif');

	}

	/**
	 * Menampilkan form update data calon
	 */


	/**
	 * Validasi untuk id_calon, agar tidak ada calon dengan id_calon sama
	 */
	function valid_id_calon($id_calon)
	{
		if ($this->calon_model->valid_id_calon($id_calon) == TRUE)
		{
			$this->form_validation->set_message('valid_id_calon', "calon dengan id_calon $id_calon sudah terdaftar");
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	// cek apakah valid untuk update?
	function valid_id_calon2()
	{
		// cek agar tidak ada id_calon ganda, khusus untuk proses update
		$current_id_calon 	= $this->session->userdata('id_calon');
		$new_id_calon		= $this->input->post('id_calon');

		if ($new_id_calon === $current_id_calon)
		{
			return TRUE;
		}
		else
		{
			if($this->calon_model->valid_id_calon($new_id_calon) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id_calon2', "calon dengan id_calon $new_id_calon sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}

}
// END calon Class

/* End of file calon.php */
/* Location: ./system/application/controllers/calon.php */
