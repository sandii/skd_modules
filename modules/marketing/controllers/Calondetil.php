<?php
/**
 * calon Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Calondetil extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */

	var $limit = 10;
	var $title = 'project';
		  	var $alamat = 'marketing/calondetil';


  function Calondetil()
	{
		parent::__construct();
		$this->load->model('calon_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);


	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;

	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());

	  	$this->load->helper('fungsi');


	}

	/**
	 * Iid_calonialisasi variabel untuk $title(untuk id element <body>), dan
	 * $limit untuk membatasi penampilan data di tabel
	 */

	/**
	 * Memeriksa user state, jika dalam keadaan login akan menjalankan fungsi get_all()
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_calon,$baru=null)
	{
			$this->get_all($id_calon,$baru);

	}

	/**
	 * Mendapatkan semua data calon di database dan menampilkannya di tabel
	 */
	function get_all($id_calon,$baru=null)
	{
		$data = $this->data;
		$data['h2_title'] = $this->title.' detail';
		$data['custom_view'] = 'calondetil';
		$data['link_frame']=site_url('marketing/comment_calon/index/'.$id_calon);
		$data['form_action']	= site_url($this->alamat.'/add_comment/'.$id_calon."/".$baru);

		// Offset

		// Load data

  	$calon = $this->calon_model->get_calon_by_id($id_calon);



			// Generate pagination


			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");


	$konsumen = $this->Konsumen_model->get_konsumen_by_id($calon->id_konsumen);

	$pegawai = $this->Pegawai_model->get_pegawai_by_id($calon->id_pegawai);



 if($baru)
 $perusahaan=$calon->nama;
    else
  $perusahaan=
    anchor_popup('konsumendetil/index/'.$konsumen->id_konsumen,$konsumen->perusahaan,array('class' => 'detail','width'=>1000));


  $this->table->add_row('<b>nama',
 $perusahaan
  );



 $this->table->add_row('<b>start project',
date('d-m-Y', strtotime($calon->tanggal)
));



	 $this->table->add_row('<b>project',$calon->pertanyaan);
//	 $this->table->add_row('<b>jawaban',$calon->jawaban);
// $this->table->add_row('<b>pj',!empty($pegawai->nama)?$pegawai->nama:'');

$datastatusw ="";

$pegawai = $this->Pegawai_model->get_aktif()->result();
// $data['default']['id_pegawai']=$_SESSION['id_user'];
foreach($pegawai as $row)
{
	$datastatusw .= "<option value=".site_url()."/marketing/calondetil/update_status6/".$row->id_pegawai.'/'.$id_calon;
	if($row->id_pegawai==$calon->id_pegawai)
	$datastatusw .= " selected ";
	$datastatusw .=">".$row->nama."</option>";
}


$statusw="
<form name='jump5'>
<select name='myjumpbox'
OnChange='location.href=jump5.myjumpbox.options[selectedIndex].value' class=form_field>
$datastatusw
</select>
</form>";



$this->table->add_row("<b>pj",$statusw);


  $terakhirx=$calon->terakhir;
$hari=ceil($terakhirx/(24*60*60));
$skr=(mktime(7, 0, 0, date("n"), date("j"), date("Y"))/(24*60*60));




 $sisa=$skr-$hari;

  if($sisa==0)
  $sisa='barusan ';
  else
  $sisa=$sisa.' hari  lalu ';




 $sisa.=anchor($this->alamat.'/update_status3/'.$calon->id_calon."/".$baru,'barusan udah difollow up',array('class'=> 'update'));



   $this->table->add_row('<b>terakhir follow up',$sisa);



$data_status=array("proses","beres","batal");
$data_status2=array("tidak"=>"tidak menjawab dengan pasti","mahal"=>"harga kemahalan","rumit"=>"terlalu rumit, produksi blm bisa","waktu"=>"waktu terlalu mepet","kontak"=>"gak bisa dikontak","gakjadi"=>"gak jadi dibikin");



  $status=$calon->status;
   $alasan=$calon->alasan;
		$datastatusx ="";
		$j=0;
		while(!empty($data_status[$j]))
		   {
			$datastatusx .= "<option value=".site_url()."/marketing/calondetil/update_status/".$data_status[$j].'/'.$id_calon."/".$baru;

			if($data_status[$j]==$status)
     $datastatusx .= " selected ";
      $datastatusx .=">".$data_status[$j]."</option>";

			$j++;
		   }

		$datastatusy ="";
foreach($data_status2 as $m=>$q)
		   {
			$datastatusy .= "<option value=".site_url()."/marketing/calondetil/update_status2/".$m.'/'.$id_calon."/".$baru;

			if($m==$alasan)
     $datastatusy .= " selected ";
      $datastatusy .=">".$q."</option>";


		   }

$follow=hitung_tanggal($calon->follow_up_lg);

$datastatusk="";
    $options_jam=array("00.00","01.00","02.00","03.00","04.00","05.00","06.00","07.00","08.00","09.00","10.00","11.00","12.00","13.00","14.00","15.00","16.00","17.00","18.00","19.00","20.00","21.00","22.00","23.00");
	   $j=0;
		while(!empty($options_jam[$j]))
		   {
			$datastatusk .= "<option value=".site_url()  ."/marketing/calondetil/update_status5/".$j.'/'.$id_calon."/".$baru;

			if($options_jam[$j]==$follow[1])
     $datastatusk .= " selected ";

      $datastatusk .=">".$options_jam[$j]."</option>";

			$j++;
		   }








$statusx="
<form name='jump'>
<select name='myjumpbox'
OnChange='location.href=jump.myjumpbox.options[selectedIndex].value' class=form_field>
$datastatusx
</select>
</form>";

 $statusy="
<form name='jump2'>
<select name='myjumpbox'
OnChange='location.href=jump2.myjumpbox.options[selectedIndex].value' class=form_field >
$datastatusy
</select><form>";


 if($status=="order" and $baru==null)
$statusx.=	anchor_popup('produksi/orderdetil/add/'.$calon->id_konsumen.'/1','proses ke input order',array('class' => 'add'));


		$this->table->add_row("<b>status",$statusx);



    if($status=="batal" )
   $this->table->add_row('<b>penyebab',$statusy);






   $statusz="
<form name='jump3' action=".site_url()."/marketing/calondetil/update_status4/".$id_calon."/".$baru ." method=post>
		 <input type=text id='datepicker' name=follow_up_lg  value='".$follow[0]."'



     >
   </p>
</form>";





// jam:

  //       echo form_dropdown('jam_setting', $options_jam, isset($calon->follow_up_lg) ? $calon->follow_up_lg : 17,"class=form_field");




 $statusk="
<form name='jump4'> jam:
<select name='myjumpbox'
OnChange='location.href=jump4.myjumpbox.options[selectedIndex].value' class=form_field>
$datastatusk
</select>
</form>";


$table="<table ><td valign=top>$statusz<td valign=top>$statusk</table>";


   $this->table->add_row("<b>difollow up lagi",$table);



			$data['tablex'] = $this->table->generate();


		// Load view
		$this->load->view('template2', $data);
	}

	/**
	 * Menghapus data calon dengan id_calon tertentu
	 */
	function delete($id_calon)
	{
		$this->calon_model->delete($id_calon);
	//	$this->session->set_flashdata('message', '1 data absen berhasil dihapus');

		echo"data berhasil dihapus";
	}

	/**
	 * Menampilkan form tambah calon
	 */


	/**
	 * Menampilkan form update data calon
	 */
	function update($id_calon)
	{
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.'> Update';
		$data['main_view'] 		= 'calon/calondetil_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_calon);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_calon,'kembali', array('class' => 'back'))
										);




		// cari data dari database
		$calon = $this->calon_model->get_calon_by_id($id_calon);




$data['default']['status']	= $calon->status;
$data['default']['alasan']	= $calon->alasan;


		$this->load->view('template2', $data);
	}

	/**
	 * Proses update data calon
	 */
	function update_process($id_calon)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';
		$data['main_view'] 		= 'calondetil_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_calon);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/'.$this->session->userdata('id_calon'),'kembali', array('class' => 'back'))
		);







		// jika proses validasi sukses, maka lanjut mengupdate data

			// save data
			$absen = array(
							'status'		=> $this->input->post('status'),
	'alasan'		=> $this->input->post('alasan')


          	);
			$this->calon_model->update($this->session->userdata('id_calon'), $absen);
			// $this->Absen_model->update($id_absen, $absen);

			// set pesan
			$this->session->set_flashdata('message', 'Satu data calon berhasil diupdate!');

			redirect($this->alamat.'/index/'.$this->session->userdata('id_calon'));


	}

	/**
	 * Validasi untuk id_calon, agar tidak ada calon dengan id_calon sama
	 */

function add_comment($id_calon,$baru=null)
	{       $tulisan=$this->input->post('insert_comment');

		    $hari=array(1=>"senin",2=>"selasa",3=>"rabu",4=>"kamis",5=>"jumat",6=>"sabtu",7=>"minggu");


		if( !empty($tulisan))
		{$commentx="<font color=blue>".$_SESSION['username'].": ".$hari[date("N")]." ". date("d-m-Y")."</font> ";



		$commentx.=$this->input->post('insert_comment')."<br><br>";
		$this->calon_model->update_comment($id_calon,$commentx);}
		//$this->session->set_flashdata('message', 'status order berhasil diupdate');
		redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}


 	function update_status($status_baru,$id_calon,$baru=null)
	{
		$statusx=array('status'=>$status_baru);

		$this->calon_model->update($id_calon,$statusx);
		$this->session->set_flashdata('message', 'status order berhasil diupdate');
		redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}


 	function update_status2($status_baru,$id_calon,$baru=null)
	{
		$statusx=array('alasan'=>$status_baru);

		$this->calon_model->update($id_calon,$statusx);
		$this->session->set_flashdata('message', 'status order berhasil diupdate');
		redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}
	function update_status3($id_calon,$baru=null)
	{
		$statusx=array('follow_up' => date("Ymd"));

		$this->calon_model->update($id_calon,$statusx);


  	$this->session->set_flashdata('message', 'status order berhasil diupdate');

  	redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}

		function update_status4($id_calon,$baru=null)
	{
		$calon = $this->calon_model->get_calon_by_id($id_calon);



$follow=hitung_tanggal($calon->follow_up_lg);


  $revisi=$this->input->post('follow_up_lg')." ".$follow[1].":00:00";

		$statusx=array('follow_up_lg' => $revisi);
		$this->calon_model->update($id_calon,$statusx);


  	$this->session->set_flashdata('message', 'status order berhasil diupdate');

  	redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}


	function update_status5($jam,$id_calon,$baru=null)
	{


  	$calon = $this->calon_model->get_calon_by_id($id_calon);



$follow=hitung_tanggal($calon->follow_up_lg);


  $revisi=$follow[0]." ".$jam.":00:00";

		$statusx=array('follow_up_lg' => $revisi);
		$this->calon_model->update($id_calon,$statusx);


  	$this->session->set_flashdata('message', 'status order berhasil diupdate');

  	redirect($this->alamat.'/index/'.$id_calon."/".$baru);
	}


	function update_status6($status_baru,$id_calon)
	{
		$statusx=array('id_pegawai'=>$status_baru);

		$this->calon_model->update($id_calon,$statusx);
		$this->session->set_flashdata('message', 'status pj berhasil diupdate');
		redirect($this->alamat.'/index/'.$id_calon);
	}

}
// END calon Class

/* End of file calon.php */
/* Location: ./system/application/controllers/calon.php */
