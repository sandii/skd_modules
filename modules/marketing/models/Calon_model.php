<?php
class Calon_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Calon_model()
	{
		parent::__construct();
	}

	// Iid_calonialisasi perusahaan tabel calon
	var $table = 'calon';


	/**
	 * Mendapatkan data semua calon
	 */
	function get_all($kons="",$limit, $offset)
	{
		//$this->db->select('*');
		//$this->db->from($this->table);

		//$this->db->limit($limit, $offset);
		//$this->db->order_by('perusahaan', 'asc');
		//return $this->db->get()->result();
		//empty($offset)?$offset="0":"";
		//empty($limit)?$limit="0":"";


		$sql= "select *,datediff(now(),pertama)as tgl_pertama from calon ";

    if(!empty($kons))
    {$sql.=" where perusahaan like '%".$kons."%' or contact_person like '%".$kons."%' "; }

    $sql.=" order by id_calon desc ";

    $sql.=" limit " .$offset.",".$limit;
    return $this->db->query($sql)->result();
	}


	function get_pj()
	{
				$this->db->distinct('pj');

		$this->db->from($this->table);

		return $this->db->get()->result();


	}





		function aktif($limit,$offset,$kons="",$pj="",$query1,$query2=null)
	{


$this->db->select ('*,UNIX_TIMESTAMP(follow_up) as terakhir,UNIX_TIMESTAMP(follow_up_lg) as terakhir2');

		$this->db->from('calon');
		$this->db->where( '(status ="'.$query1.'" or status="'.$query2.'") ');

     if(!empty($kons))
		$this->db->where('perusahaan like '. "'%".$kons."%'");
     if(!empty($pj))
		$this->db->where('id_pegawai',$pj);



if(!empty($limit))
		$this->db->limit($limit, $offset);

$this->db->order_by('updated_at', 'desc');



	return $this->db->get();

	}


	function ambil($pj="",$jenis)
	{


$this->db->select ('*,UNIX_TIMESTAMP(follow_up) as terakhir,UNIX_TIMESTAMP(follow_up_lg) as terakhir2');

		$this->db->from('calon');

	$this->db->where('jenis',$jenis);
		$this->db->where('status',"proses");
     if(!empty($pj))
		$this->db->where('pj',$pj);


$this->db->order_by('id_calon', 'desc');
	return $this->db->get();
	}




	function count_all($kons="",$query1,$query2)
	{

		$this->db->from('konsumen,calon');
		$this->db->where('calon.id_konsumen = konsumen.id_konsumen and (calon.status="'.$query1.'" or calon.status="'.$query2.'") ');

     if(!empty($kons))
		$this->db->where('perusahaan like '. "'%".$kons."%'");
	return $this->db->get()->num_rows();


	}





	function get_calon()
	{
		$this->db->order_by('perusahaan');
		return $this->db->get('calon');
	}

	/**
	 * Mendapatkan data seorang calon dengan id_calon tertentu
	 */
	function get_calon_by_id($id_calon)
	{

	$this->db->select ('*,UNIX_TIMESTAMP(follow_up) as terakhir');

		return $this->db->get_where($this->table, array('id_calon' => $id_calon))->row();
	}

	/**
	 * Menghitung jumlah baris tabel calon
	 */


	/**
	 * Menghapus data calon tertentu
	 */
	function delete($id_calon)
	{
		$this->db->delete($this->table, array('id_calon' => $id_calon));
	}

	/**
	 * Menambah data calon
	 */
	function add($calon)
	{
		$this->db->insert($this->table, $calon);
	}

	/**
	 * Update data calon
	 */
	function update($id_calon, $calon)
	{
		$this->db->where('id_calon', $id_calon);
		$this->db->update($this->table, $calon);
	}

	/**
	 * Cek id_calon agar tidak ada data calon yang sama
	 */
	function valid_id_calon($id_calon)
	{
		$query = $this->db->get_where($this->table, array('id_calon' => $id_calon));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

		function update_comment($id_calon, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_calon', $id_calon);
		$hasil=$this->db->get($this->table);

		$komen=$hasil->row()->calon_log;

		$komen.=$comment;
		//$this->db->where('id_order', $id_order);
		//$this->db->update($this->table, $comment);

	$sql= " update `calon` set calon_log='".$komen."' where id_calon =".$id_calon;
	$this->db->query($sql);


	}


function baru($tahunan=0,$tgl1="",$tgl2="")
	{


$this->db->select (array('month(pertama)as tanggalm','year(pertama)as tahun','count(*) as totalm'));

		$this->db->from('calon');



		$this->db->where('omzet > 0');



    if(!empty($tgl1))
		$this->db->where("pertama >'".$tgl1."'");
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("pertama <'".$tgl2."'");
}

		$this->db->order_by('pertama', 'asc');
if($tahunan!=0)
		$this->db->group_by('year(pertama)');
else
		$this->db->group_by('EXTRACT(YEAR_MONTH FROM pertama)');

		return $this->db->get();
	}


function jadi_order($tahunan=0,$tgl1="",$tgl2="",$status)
	{


$this->db->select (array('month(tanggal)as tanggalm','year(tanggal)as tahun','count(*) as totalm'));

		$this->db->from('calon');



		$this->db->where("status ='".$status."'");



    if(!empty($tgl1))
		$this->db->where("tanggal >'".$tgl1."'");
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("tanggal <'".$tgl2."'");
}

		$this->db->order_by('tanggal', 'asc');
if($tahunan!=0)
		$this->db->group_by('year(tanggal)');
else
		$this->db->group_by('EXTRACT(YEAR_MONTH FROM tanggal)');

		return $this->db->get();
	}





function batal($tahunan=0,$tgl1="",$tgl2="",$status)
	{


$this->db->select (array('month(tanggal)as tanggalm','year(tanggal)as tahun','count(*) as totalm'));

		$this->db->from('calon');


			$this->db->where("status ='batal'");
		$this->db->where("alasan ='".$status."'");



    if(!empty($tgl1))
		$this->db->where("tanggal >'".$tgl1."'");
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("tanggal <'".$tgl2."'");
}

		$this->db->order_by('tanggal', 'asc');
if($tahunan!=0)
		$this->db->group_by('year(tanggal)');
else
		$this->db->group_by('EXTRACT(YEAR_MONTH FROM tanggal)');

		return $this->db->get();
	}





}
// END calon_model Class

/* End of file calon_model.php */
/* Location: ./system/application/models/calon_model.php */
