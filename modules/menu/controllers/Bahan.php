<?php
/**
 * Bahan Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bahan extends CI_Controller  {
	/**
	 * Constructor
	 */
	
  var $title = 'bahan harian';
	
  
  function Bahan()
	{
		parent::__construct();
		$this->load->model('Bahan_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
  
        $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());

  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahan,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data bahan
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
		$data['sub_view'] = 'menu/menu';
		$data['sub_title'] = 'bahan';
		// Load data
		$query = $this->Bahan_model->get_all();
		$bahan = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Bahan','satuan', 'Actions');
			$i = 0;
			
			foreach ($bahan as $row)
			{
				$this->table->add_row(++$i,  $row->bahan,$row->satuan,
										anchor('menu/bahan/update/'.$row->id_bahan,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('menu/bahan/delete/'.$row->id_bahan,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahan!';
		}		
		
		$data['link'] = array('link_add' => anchor('menu/bahan/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data bahan
	 */
	function delete($id_bahan)
	{
		$this->Bahan_model->delete($id_bahan);
		$this->session->set_flashdata('message', '1 data bahan berhasil dihapus');
		
		redirect('menu/bahan');
	}
	
	/**
	 * Pindah ke halaman tambah bahan
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/bahan_form';
		$data['form_action']	= site_url('menu/bahan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/bahan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data bahan
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/bahan/bahan_form';
		$data['form_action']	= site_url('menu/bahan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/bahan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	

		
	
			// Persiapan data
			$bahan = array(
							'bahan'		=> $this->input->post('bahan'),
							'satuan'		=> $this->input->post('satuan')
						);
			// Proses penyimpanan data di table bahan
			$this->Bahan_model->add($bahan);
			
			$this->session->set_flashdata('message', 'Satu data bahan berhasil disimpan!');
			redirect('menu/bahan/');
		
		// Jika validasi gagal
	
	}
	
	/**
	 * Pindah ke halaman update bahan
	 */
	function update($id_bahan)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'menu/bahan_form';
		$data['form_action']	= site_url('menu/bahan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/bahan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$bahan = $this->Bahan_model->get_bahan_by_id($id_bahan);
				
		// buat session untuk menyimpan data primary key (id_bahan)
		$this->session->set_userdata('id_bahan', $bahan->id_bahan);
		
		// Data untuk mengisi field2 form
		$data['default']['id_bahan'] 	= $bahan->id_bahan;		
		$data['default']['bahan']		= $bahan->bahan;
		$data['default']['satuan']		= $bahan->satuan;		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bahan
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'menu/bahan_form';
		$data['form_action']	= site_url('menu/bahan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/bahan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
	
	
			// save data
			$bahan = array(
							'bahan'		=> $this->input->post('bahan'),
							'satuan'		=> $this->input->post('satuan')
						);
			$this->Bahan_model->update($this->session->userdata('id_bahan'),$bahan);
			
			$this->session->set_flashdata('message', 'Satu data bahan berhasil diupdate!');
			redirect('menu/bahan');
		}
	
	
		
}
// END Bahan Class

/* End of file bahan.php */
/* Location: ./system/application/controllers/bahan.php */
