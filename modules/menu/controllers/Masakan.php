<?php
/**
 * Masakan Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Masakan extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $limit = 20;
  var $title="setup masakan";
var $alamat='menu/masakan/get_last_ten_masakan';
  
  function Masakan()
	{
		parent::__construct();
		$this->load->model('Masakan_model', '', TRUE);
		$this->load->model('Masakandetil_model', '', TRUE);
		$this->load->model('Bahan_model', '', TRUE);

		




// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
   
    $this->data['title']=$this->title;
 
  
	      $this->load->helper('fungsi');
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());

      
  }
	
	
	
	/**
	 * Iid_konsumenialisasi variabel untuk $limit dan $title(untuk id element <body>)
	 */
	


	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman masakan,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
		// Hapus data session yang digunakan pada proses update data masakan
		$this->session->unset_userdata('id_masakan');
		$this->session->unset_userdata('tanggal');
			

			$this->get_last_ten_masakan();
	
	}
	
	/**
	 * Menampilkan 10 data masakan terkini
	 */
	function get_last_ten_masakan($status='',$offset = 0)
	{
		$data=$this->data;
    $data['h2_title']=$this->title;
$data['sub_title']="masakan";
		

			
		$data['sub_view'] = 'menu/menu';
	
	
	
		$data_status=array("sayur","lauk","tambahan");
		
		
		
				$statusx="";

		if(empty($status))
		$status="sayur";
		
		
		
		
		
		
	/*	
		
		$i=0;
		while(!empty($data_status[$i]))
		   {
			
			
			if($data_status[$i]==$status)
			$statusx .= "<div id=update_status_active>".$data_status[$i]."</div>";
			
			else
			$statusx .= anchor('menu/masakan/get_last_ten_masakan/'.$data_status[$i].'/',$data_status[$i],array('id'=> 'update_status'));
			
			$i++;
		   }
		   $data['navs']=$statusx;
	*/	   

$data_nav=array(
		 array("nama"=>'sayur',"judul"=>"sayur","link"=>$this->alamat),
		 array("nama"=>"lauk","judul"=>"lauk","link"=>$this->alamat.'/lauk'),
		 array("nama"=>"tambahan","judul"=>"tambahan","link"=>$this->alamat.'/tambahan'),

);

$data['navs']=bikin_nav($data_nav,$status);		
		
		
		
	
		$statusy="='$status'";
		  // echo $offset;
		// Offset
		$uri_segment = 5;
		$offset = $this->uri->segment($uri_segment);
		
		// Load data dari tabel masakan
		$masakans = $this->Masakan_model->get_last_ten_masakan($this->limit, $offset,$statusy)->result();
		$num_rows = $this->Masakan_model->count_all_num_rows($statusy);
		
		
		if ($num_rows > 0) // Jika query menghasilkan data
		{
			// Membuat pagination			
			$config['base_url'] = site_url('menu/masakan/get_last_ten_masakan/'.$status.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			// Set template tabel, untuk efek selang-seling tiap baris
			$tmpl = array( 'table_open'    => '<table bmasakan="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			// Set heading untuk tabel
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'masakan', 'Actions');
			
			// Penomoran baris data
			$i = 0 + $offset;
			
			foreach ($masakans as $masakan)
			{
			
				
				
		
				
				// Penyusunan data baris per baris, perhatikan pembuatan link untuk updat dan delete
				$this->table->add_row(++$i, anchor_popup('menu/masakandetil/index/'.$masakan->id_masakan,$masakan->masakan,array('class' => 'update','width'=>1000)),
										
                    anchor('menu/masakan/update/'.$masakan->id_masakan,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('menu/masakan/delete/'.$masakan->id_masakan,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
									);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data masakan!';
		}		
		
		$data['link'] = array('link_add' => anchor('menu/masakan/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
	
	
	
	
	
		// Load default view
		$this->load->view('template', $data);
	}
	
	/**
	 * Menghapus data masakan
	 */
	function delete($id_masakan)
	{
		$this->Masakan_model->delete($id_masakan);
		$this->session->set_flashdata('message', '1 data masakan berhasil dihapus');
		
		redirect('menu/masakan');
	}
	
	/**
	 * Berpindah ke form untuk entry data masakansi baru
	 */
	function add()
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/masakan_form';
		$data['form_action']	= site_url('menu/masakan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/masakan/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	
  
  	// data marketing untuk dropdown menu

		
	$bahan = $this->Bahan_model->get_all()->result();
	
			$data['options_bahan'][0] = "pilih bahan";
		foreach($bahan as $row)
		{
			$data['options_bahan'][$row->id_bahan] = $row->bahan." (". $row->satuan.")";
		}
	 
  
  	$this->load->view('template', $data);
	}
	
	/**
	 * Proses untuk entry data masakansi baru
	 */
	function add_process()
	{
		// Iid_konsumenialisasi data umum
		$data			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/masakan_form';
		$data['form_action']	= site_url('menu/masakan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/masakan/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
	 //Set validation rules
 	//$this->form_validation->set_rules('id_konsumen', 'id_konsumen', 'required');


		
	//	if ($this->form_validation->run() == TRUE)
	//	{

$id_terakhirxx = $this->Masakan_model->get_last_id_masakan()->row();
$id_terakhir = $id_terakhirxx->id_masakan;

$id_terakhir++;
//echo $id_terakhir;				
// Prepare data untuk disimpan di tabel
			// Proses simpan data masakansi
			
		
$id_bahan=$this->input->post('id_bahan');
$jumlah=$this->input->post('jumlah');
 $i=0;
 

  while ($id_bahan[$i])
			{
$masakandetilx = array( 'id_masakan' => $id_terakhir,
'id_bahan' => $id_bahan[$i],
'jumlah' => $jumlah[$i],
						);			
			
		
	$this->Masakandetil_model->add($masakandetilx);		
			$i++;
			} 
  
$masakan = array( 'id_masakan' => $id_terakhir,
'masakan' => $this->input->post('masakan'),
'kategori' => $this->input->post('kategori')
						);  
$this->Masakan_model->add($masakan);
			
			$this->session->set_flashdata('message', 'Satu data masakan berhasil disimpan!');
			redirect('menu/masakan');
		
    
    //}
//	else
	//	{		
//			$this->load->view('template', $data);
//		}		
	}
	
	/**
	 * Berpindah ke form untuk update data masakansi
	 */
	function update($id_masakan)
	{
		// Iid_konsumenialisasi data umum
		$data 			= $this->data;
		
		$data['h2_title'] 		= $this->title.' > Update Data';
		$data['custom_view'] 		= 'menu/masakan_form';
		$data['form_action']	= site_url('menu/masakan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/masakan/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
  	$data['update']=1;
  
  	
  									
		
		// cari data dari database
		$masakan = $this->Masakan_model->get_masakan_by_id($id_masakan)->row();
		
		// buat session untuk menyimpan data primary key (id_masakan)
		$this->session->set_userdata('id_masakan', $masakan->id_masakan);
		
		// Data untuk mengisi field2 form
	
			$data['default']['masakan'] 	= $masakan->masakan;
		$data['default']['kategori'] 	= $masakan->kategori;
		
	
		
		
			
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses untuk update data masakansi
	 */
	function update_process()
	{
		// Iid_konsumenialisasi data umum
		$data		= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Data';
		$data['custom_view'] 		= 'menu/masakan_form';
		$data['form_action']	= site_url('menu/masakan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/masakan/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
			
	
		// jika proses validasi sukses, maka lanjut update masakan
	
			// Cek semester yang sedang aktif
	
						
			// Simpan data
			$masakan = array(
'kategori' => $this->input->post('kategori'),
'masakan' => $this->input->post('masakan')
						);
			$this->Masakan_model->update($this->session->userdata('id_masakan'), $masakan);
						
			// set pesan
			$this->session->set_flashdata('message', 'Satu data masakan berhasil diupdate!');
			
			redirect('menu/masakan');
	
	}	
	
}
	
