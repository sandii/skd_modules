<?php
/**
 * Menu Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Menu extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'jadwal masakan';
	
  
  function Menu()
	{
		parent::__construct();
		$this->load->model('Menu_model', '', TRUE);
		$this->load->model('Masakan_model', '', TRUE);
		$this->load->model('Bahan_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman menu,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{



	$terakhir=$this->Menu_model->get_last_date()->row();




	
$terakhir=strtotime($terakhir->tanggal);


$skr=mktime(0,0,0,date('m'),date('d'),date('Y'));
	 
$selisih=floor(($skr-$terakhir)/(60*60*24))+12;	 
	 



	  if($selisih>0)
{



    $menux=$this->Menu_model->get_menu_by_tanggal($selisih);
		$menu = $menux->result();
	
   	$num_rows = $menux->num_rows();
  
  	if ($num_rows > 0)
		{
			foreach ($menu as $row)
			{

$terakhir=$terakhir+(60*60*24);
if(date("w",$terakhir)==0)
$terakhir=$terakhir+(60*60*24);



$data=array("tanggal"=>date("Ymd",$terakhir), "libur"=>"");			
$this->Menu_model->update($row->id_menu,$data);	      	


}


}


}



			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data menu
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
	

	
		
		
		// Load data
		$query = $this->Menu_model->get_all();
		$menu = $query->result();
		$num_rows = $query->num_rows();
		
		
		
		 $data['pagination']="<li class=active><a>print</li>";
		 
		 $data['pagination'].="<li>".anchor_popup('menu/printmenu/','semua',array('width'=>1000))."</li>";		 
		 
		 $data['pagination'].="<li>".anchor_popup('menu/printmenu/kemarin',' | minggu kemarin',array('width'=>1000))."</li>";		 

		 $data['pagination'].="<li>".anchor_popup('menu/printmenu/sekarang',' | minggu ini',array('width'=>1000))."</li>";		 
		
		 $data['pagination'].="<li>".anchor_popup('menu/printmenu/besok',' | minggu depan',array('width'=>1000))."</li>";		 
		
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'Sayur', 'Lauk', 'tambahan', 'Actions');
			$i = 0;
			
			foreach ($menu as $row)
			{
			
			if(!empty($row->sayur))
		$sayur = $this->Masakan_model->get_masakan_by_id($row->sayur)->row()->masakan;	
	else
	$sayur="";
		if(!empty($row->lauk))
   $lauk = $this->Masakan_model->get_masakan_by_id($row->lauk)->row()->masakan;	
	  else
	$lauk="";
    	if(!empty($row->tambahan))
    $tambahan = $this->Masakan_model->get_masakan_by_id($row->tambahan)->row()->masakan;	
	else
	$tambahan="";
	
			
   				$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
				$hr = date('w', strtotime($row->tanggal));
				$hari = $hari_array[$hr];
				$tgl = date('d-m-Y', strtotime($row->tanggal));
				$tanggalx = "$hari, $tgl";   
      
      	
	if($row->tanggal==date('Y-m-d'))
	$tanggalx="<font color=blue><b>HARI INI</font>";			
  
  
  
  
  
  
  
  
  
  
  
  
  if($row->libur==1)
  {$tanggalx="<font color=red><b>LIBUR</b></font>";		
 	$xxx=anchor('menu/cancel/'.$row->id_menu,'cancel',array('class'=> 'btn btn-xs btn-success','onclick'=>"return confirm('Anda yakin akan membatalkan tanggal ini menjadi libur?')")); 
	$sayur="";$lauk="";$tambahan="";
  }
  else
	$xxx=anchor('menu/libur/'.$row->id_menu,'libur',array('class'=> 'btn btn-xs btn-info','onclick'=>"return confirm('Anda yakin akan mengubah tanggal ini menjadi libur?')"));
  
  		
				$this->table->add_row($tanggalx,  $sayur,$lauk,$tambahan,
										anchor('menu/update/'.$row->id_menu,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('menu/delete/'.$row->id_menu,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")).' '.
										$xxx		
    								);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data menu!';
		}		
		
		$data['link'] = array('link_add' => anchor('menu/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data menu
	 */
	function delete($id_menu)
	{
		$this->Menu_model->delete($id_menu);
		$this->session->set_flashdata('message', '1 data menu berhasil dihapus');
		
		redirect('menu');
	}
	
	
		function libur($id_menu)
	{
	
		$menu = array(
							'libur'		=> '1'
						
						);
			$this->Menu_model->update($id_menu,$menu);
	
	
	
		$this->session->set_flashdata('message', '1 data libur berhasil diset');
		
		
		
		
		redirect('menu');
	}
	
		function cancel($id_menu)
	{
	
		$menu = array(
							'libur'		=> ''
						
						);
			$this->Menu_model->update($id_menu,$menu);
	
		$this->session->set_flashdata('message', '1 data libur berhasil dibatalkan');
		
		
		
		
		redirect('menu');
	}
	
	
	/**
	 * Pindah ke halaman tambah menu
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/menu_form';
		$data['form_action']	= site_url('menu/add_process');
		$data['link'] 			= array('link_back' => anchor('menu','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
			$bahan = $this->Masakan_model->get_masakan()->result();
			$data['option_lauk'][0] =$data['option_tambahan'][0] =$data['option_sayur'][0] = "tidak ada";	 
      foreach($bahan as $row)
			{
    if($row->kategori=="sayur")
      $data['option_sayur'][$row->id_masakan] = $row->masakan;
    else if($row->kategori=="lauk")
     $data['option_lauk'][$row->id_masakan] = $row->masakan;
    else
     $data['option_tambahan'][$row->id_masakan] = $row->masakan;
  
 		}
		
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data menu
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/menu_form';
		$data['form_action']	= site_url('menu/add_process');
		$data['link'] 			= array('link_back' => anchor('menu','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		
	
			// Persiapan data
			$menu = array('sayur'	=> $this->input->post('sayur'),
							'lauk'		=> $this->input->post('lauk'),
								'tambahan'		=> $this->input->post('tambahan')
						);
			// Proses penyimpanan data di table menu
			$this->Menu_model->add($menu);
			
			$this->session->set_flashdata('message', 'Satu data menu berhasil disimpan!');
			redirect('menu/');
	
	}
	
	/**
	 * Pindah ke halaman update menu
	 */
	function update($id_menu)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'menu/menu_form';
		$data['form_action']	= site_url('menu/update_process');
		$data['link'] 			= array('link_back' => anchor('menu','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$menu = $this->Menu_model->get_menu_by_id($id_menu)->row();
			
		// buat session untuk menyimpan data primary key (id_menu)
		$this->session->set_userdata('id_menu', $menu->id_menu);
		
	//	echo $menu->sayur;
		// Data untuk mengisi field2 form
		$data['default']['sayur'] 	= $menu->sayur;		
		$data['default']['lauk'] 	= $menu->lauk;	
	 $data['default']['tambahan'] 	= $menu->tambahan;	
  
 		$bahan = $this->Masakan_model->get_masakan()->result();
			$data['option_lauk'][0] =$data['option_tambahan'][0] =$data['option_sayur'][0] = "tidak ada";	 
      foreach($bahan as $row)
			{
    if($row->kategori=="sayur")
      $data['option_sayur'][$row->id_masakan] = $row->masakan;
    else if($row->kategori=="lauk")
     $data['option_lauk'][$row->id_masakan] = $row->masakan;
    else
     $data['option_tambahan'][$row->id_masakan] = $row->masakan;
  
 		} 
  
  			
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data menu
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'menu/menu_form';
		$data['form_action']	= site_url('menu/update_process');
		$data['link'] 			= array('link_back' => anchor('menu','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
	
			// save data
			$menu = array(
							'sayur'		=> $this->input->post('sayur'),
							'lauk'		=> $this->input->post('lauk'),
							'tambahan'		=> $this->input->post('tambahan'),
						);
			$this->Menu_model->update($this->session->userdata('id_menu'),$menu);
			
			$this->session->set_flashdata('message', 'Satu data menu berhasil diupdate!');
			redirect('menu');
	
	}
	
	/**
	 * Cek apakah $id_menu valid, agar tidak ganda
	 */
	function valid_id($id_menu)
	{
		if ($this->Menu_model->valid_id($id_menu) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "menu dengan Kode $id_menu sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	


}
