<?php
/**
 * Masakandetil Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Masakandetil extends  CI_Controller {
	/**
	 * Constructor
	 */
	function Masakandetil()
	{
		parent::__construct();

		$this->load->model('Masakan_model', '', TRUE);
			$this->load->model('Bahan_model', '', TRUE);
		$this->load->model('Masakandetil_model', '', TRUE);




	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
	}
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	var $title = 'Masakandetil';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman 
	 *    ,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_masakan)
	{
		
			$this->get_all($id_masakan);
	
	}
	
	/**
	 * Tampilkan semua data masakandetil
	 */
	function get_all($id_masakan)
	{
		$data['title'] = $this->title;
		$data['h2_title'] = 'Masakan Detil';

		$data['link_frame']=site_url('comment/index/'.$id_masakan);
		$data['form_action']	= site_url('menu/masakandetil/add_comment/'.$id_masakan);
  
  
  	// Load data
  	$where="id_masakan =".$id_masakan;
		$query = $this->Masakandetil_model->get_all($where);
		$masakandetil = $query->result();
		$num_rows = $query->num_rows();
		
	  	if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table bmasakan="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'bahan', 'Jumlah', 'Operasi' );
			
				
		
			
			
			
			$i = 0;			
			foreach ($masakandetil as $row)
			{
				
				
	
		   	$bahanx = $this->Bahan_model->get_bahan_by_id($row->id_bahan);
	
		 
				
				
				$this->table->add_row(++$i, $bahanx->bahan,$row->jumlah.' '.$bahanx->satuan,
										anchor('menu/masakandetil/update/'.$row->id_masakandetil.'/'.$id_masakan,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('menu/masakandetil/delete/'.$row->id_masakandetil.'/'.$id_masakan,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
			
			 $this->table->clear();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data masakan!';
		}		
		
		$data['link'] = array('link_add' => anchor('menu/masakandetil/add/'.$id_masakan,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		
		$tmpl2 = array( 'table_open'    => '<table bmasakan="0" cellpadding="0" cellspacing="0" class=table>',
						  
              'row_alt_start'  => '<tr>',
							'row_alt_end'    => '</tr>',

						  );

			$this->table->set_template($tmpl2);


  
  		/*Set table heading */
			$this->table->set_empty("&nbsp;");
	   $masakan = $this->Masakan_model->get_masakan_by_id($id_masakan)->row();	
	
	
	
	
	
  
			$this->table->add_row('<b>nama masakan',$masakan->masakan);
			$this->table->add_row('<b>kategori',$masakan->kategori);
			
		
   
      $data['table2'] = $this->table->generate();
			
			
		
			
		   
		
		
		// Load view
		$this->load->view('template2', $data);
		
	}
		
	/**
	 * Hapus data masakandetil
	 */
	function delete($id_masakandetil,$id_masakan)
	{
		$this->Masakandetil_model->delete($id_masakandetil);
			
		$this->session->set_flashdata('message', '1 data masakan berhasil dihapus');
		
		redirect('menu/masakandetil/index/'.$id_masakan);
	}
	

	

	
	/**
	 * Pindah ke halaman tambah masakandetil
	 */
	function add($id_masakan)
	{		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Masakandetil > Tambah Data';
		$data['custom_view'] 		= 'menu/masakandetil_form';
		$data['form_action']	= site_url('menu/masakandetil/add_process/'.$id_masakan);
		$data['link'] 			= array('link_back' => anchor('menu/masakandetil/index/'.$id_masakan,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		$data['id_masakan'] 	= $id_masakan;			
		
	  $bahan = $this->Bahan_model->get_all()->result();
	
			$data['options_bahan'][0] = "pilih bahan";
		foreach($bahan as $row)
		{
			$data['options_bahan'][$row->id_bahan] = $row->bahan." (". $row->satuan.")";
		}	
		
		
    $this->load->view('template2', $data);
	}
	
	/**
	 * Proses tambah data masakandetil
	 */
	function add_process($id_masakan)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Masakandetil > Tambah Data';
		$data['custom_view'] 		= 'menu/masakandetil_form';
		$data['form_action']	= site_url('menu/masakandetil/add_process/'.$id_masakan);
		$data['id_masakan'] 	= $id_masakan;			
		$data['link'] 			= array('link_back' => anchor('menu/masakandetil','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// Set validation rules

		
	
			// Persiapan data
			$masakandetil = array('id_masakan'	=> $id_masakan,
			'id_bahan'		=> $this->input->post('id_bahan'),
			'jumlah'		=> $this->input->post('jumlah'),
		
				
						);
			// Proses penyimpanan data di table masakandetil
			$this->Masakandetil_model->add($masakandetil);
	
			
			
			$this->session->set_flashdata('message', 'Satu data masakan berhasil disimpan!');
			redirect('menu/masakandetil/index/'.$id_masakan);
	
	}
	
	/**
	 * Pindah ke halaman update masakandetil
	 */
	function update($id_masakandetil,$id_masakan)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Masakandetil > Update';
		$data['custom_view'] 		= 'menu/masakandetil_form';
		$data['form_action']	= site_url('menu/masakandetil/update_process/'.$id_masakandetil.'/'.$id_masakan);
		$data['link'] 			= array('link_back' => anchor('menu/masakandetil/index/'.$id_masakan,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$masakandetil = $this->Masakandetil_model->get_masakandetil_by_id($id_masakandetil);
				
	
		// Data untuk mengisi field2 form
		$data['default']['jumlah'] 	= $masakandetil->jumlah;		
		$data['default']['id_bahan']		= $masakandetil->id_bahan;
	
	$bahan = $this->Bahan_model->get_all()->result();
	
		
		foreach($bahan as $row)
		{
				$data['options_bahan'][$row->id_bahan] = $row->bahan." (". $row->satuan.")";
		}	
		  		
      
      	
		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data masakandetil
	 */
	function update_process($id_masakandetil,$id_masakan)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Masakandetil > Update';
		$data['custom_view'] 		= 'menu/masakandetil_form';
		$data['form_action']	= site_url('menu/masakandetil/update_process/'.$id_masakandetil.'/'.$id_masakan);
										
		// Set validation rules
	
	
			// save data
			$masakandetil = array('id_bahan'	=> $this->input->post('id_bahan'),
			'jumlah'	=> $this->input->post('jumlah')
		
						);
			$this->Masakandetil_model->update($id_masakandetil, $masakandetil);
			
			

			
			
			$this->session->set_flashdata('message', 'Satu data masakandetil berhasil diupdate!');
			redirect('menu/masakandetil/index/'.$id_masakan);
	
	}
	
	

	
	/**
	 * Cek apakah $id_masakandetil valid, agar tidak ganda
	 */

	
	/**
	 * Cek apakah $id_masakandetil valid, agar tidak ganda. Hanya untuk proses update data masakandetil
	 */

}
// END Masakandetil Class

/* End of file masakandetil.php */
/* Location: ./system/application/controllers/masakandetil.php */
