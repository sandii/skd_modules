<?php
/**
 * Menu Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Printmenu extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'menu';
	
  
  function Printmenu()
	{
		parent::__construct();
		$this->load->model('Menu_model', '', TRUE);
		$this->load->model('Masakan_model', '', TRUE);
				$this->load->model('Masakandetil_model', '', TRUE);
		$this->load->model('Bahan_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	

	function index()
	{


			$this->get_all();
		

	}

	function sekarang()
	{



$tanggal1=date("Ymd",strtotime("last sunday"));
$tanggal2=date("Ymd",strtotime("next sunday"));




$this->where="tanggal >".$tanggal1." and tanggal < ".$tanggal2;



			$this->get_all();
		

}
	function besok()
	{



$tanggal1=date("Ymd",strtotime("next sunday"));
$tanggal2=date("Ymd",strtotime("+1 week next sunday"));




$this->where="tanggal >".$tanggal1." and tanggal < ".$tanggal2;



			$this->get_all();
		

}
function kemarin()
	{



$tanggal1=date("Ymd",strtotime("-1 week last sunday"));
$tanggal2=date("Ymd",strtotime("last sunday"));




$this->where="tanggal >".$tanggal1." and tanggal < ".$tanggal2;



			$this->get_all();
		

}







	
	/**
	 * Tampilkan semua data menu
	 */
	function get_all()
	{
	
	
		$data = $this->data;
		$data['h2_title'] = $this->title;
	

		$data['sub_title'] = 'menu';
			
		// Load data
		
		$query = $this->Menu_model->get_all(!empty($this->where)?$this->where:"");
		$menu = $query->result();
		$num_rows = $query->num_rows();
		
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'masakan', 'bahan');
			$i = 0;
			
			foreach ($menu as $row)
			{
			
			if(!empty($row->sayur))
		$sayur = $this->Masakan_model->get_masakan_by_id($row->sayur)->row()->masakan;	
	else
	$sayur="";
		if(!empty($row->lauk))
   $lauk = $this->Masakan_model->get_masakan_by_id($row->lauk)->row()->masakan;	
	  else
	$lauk="";
    	if(!empty($row->tambahan))
    $tambahan = $this->Masakan_model->get_masakan_by_id($row->tambahan)->row()->masakan;	
	else
	$tambahan="";
	
			
   				$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
				$hr = date('w', strtotime($row->tanggal));
				$hari = $hari_array[$hr];
				$tgl = date('d F', strtotime($row->tanggal));
				$tanggalx = "$hari<br> $tgl";   
      
       
  		$menux=$lauk;
  		if(!empty($lauk) and (!empty($sayur)or !empty($tambahan)))
      $menux.=", ";
      $menux.=$sayur;
  		if(!empty($sayur) and !empty($tambahan) )
  		 $menux.=", ";
      $menux.=$tambahan; 		


$bahan="";

$where="id_masakan = ".$row->lauk." or id_masakan = ".$row->sayur." or id_masakan = ".$row->tambahan;

	$query2 = $this->Masakandetil_model->get_all($where);
		$masakandetil = $query2->result();
		$num_rows2 = $query->num_rows();
		
	  	if ($num_rows2 > 0)
		{

				
			foreach ($masakandetil as $row2)
			{
				
		   	$bahanx= $this->Bahan_model->get_bahan_by_id($row2->id_bahan);
		   	
		   	$bahan.=$bahanx->bahan.": ".$row2->jumlah." ".$bahanx->satuan.", ";

}
}




				$this->table->add_row($tanggalx,  $menux,$bahan
								  								);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data menu!';
		}		
		
		
		
		// Load view
		$this->load->view('template2', $data);
	}
		
	/**
	 * Hapus data menu
	 */



}
