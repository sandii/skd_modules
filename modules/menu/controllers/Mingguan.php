<?php
/**
 * Mingguan Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Mingguan extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'bahan mingguan';
	
  
  function Mingguan()
	{
		parent::__construct();
		$this->load->model('Mingguan_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman mingguan,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data mingguan
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;

		$data['sub_view'] = 'menu/menu';
		$data['sub_title'] = 'mingguan';
		// Load data
		$query = $this->Mingguan_model->get_all();
		$mingguan = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Mingguan','jumlah','satuan', 'Actions');
			$i = 0;
			
			foreach ($mingguan as $row)
			{
				$this->table->add_row(++$i,  $row->bahan,$row->jumlah,$row->satuan,
										anchor('menu/mingguan/update/'.$row->id_mingguan,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('menu/mingguan/delete/'.$row->id_mingguan,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data mingguan!';
		}		
		
		$data['link'] = array('link_add' => anchor('menu/mingguan/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data mingguan
	 */
	function delete($id_mingguan)
	{
		$this->Mingguan_model->delete($id_mingguan);
		$this->session->set_flashdata('message', '1 data mingguan berhasil dihapus');
		
		redirect('menu/mingguan');
	}
	
	/**
	 * Pindah ke halaman tambah mingguan
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/mingguan_form';
		$data['form_action']	= site_url('menu/mingguan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/mingguan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data mingguan
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'menu/mingguan/mingguan_form';
		$data['form_action']	= site_url('menu/mingguan/add_process');
		$data['link'] 			= array('link_back' => anchor('menu/mingguan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	

		
	
			// Persiapan data
			$mingguan = array(
							'bahan'		=> $this->input->post('bahan'),
							'satuan'		=> $this->input->post('satuan'),
								'jumlah'		=> $this->input->post('jumlah')
						);
			// Proses penyimpanan data di table mingguan
			$this->Mingguan_model->add($mingguan);
			
			$this->session->set_flashdata('message', 'Satu data mingguan berhasil disimpan!');
			redirect('menu/mingguan/');
		
		// Jika validasi gagal
	
	}
	
	/**
	 * Pindah ke halaman update mingguan
	 */
	function update($id_mingguan)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'menu/mingguan_form';
		$data['form_action']	= site_url('menu/mingguan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/mingguan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$mingguan = $this->Mingguan_model->get_mingguan_by_id($id_mingguan);
				
		// buat session untuk menyimpan data primary key (id_mingguan)
		$this->session->set_userdata('id_mingguan', $mingguan->id_mingguan);
		
		// Data untuk mengisi field2 form
		$data['default']['id_mingguan'] 	= $mingguan->id_mingguan;		
		$data['default']['bahan']		= $mingguan->bahan;
		$data['default']['satuan']		= $mingguan->satuan;	
    $data['default']['jumlah']		= $mingguan->jumlah;			
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data mingguan
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'menu/mingguan_form';
		$data['form_action']	= site_url('menu/mingguan/update_process');
		$data['link'] 			= array('link_back' => anchor('menu/mingguan','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
	
	
			// save data
			$mingguan = array(
							'bahan'		=> $this->input->post('bahan'),
							'jumlah'		=> $this->input->post('jumlah'),
							'satuan'		=> $this->input->post('satuan')
						);
			$this->Mingguan_model->update($this->session->userdata('id_mingguan'),$mingguan);
			
			$this->session->set_flashdata('message', 'Satu data mingguan berhasil diupdate!');
			redirect('menu/mingguan');
		}
	
	
	
}
// END Mingguan Class

/* End of file mingguan.php */
/* Location: ./system/application/controllers/mingguan.php */
