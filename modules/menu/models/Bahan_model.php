<?php
/**
 * Bahan_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bahan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Bahan_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'bahan';
	
	/**
	 * Mendapatkan semua data bahan, diurutkan berdasarkan id_bahan
	 */

	
	/**
	 * Mendapatkan data sebuah bahan
	 */
	function get_bahan_by_id($id_bahan)
	{
		return $this->db->get_where($this->table, array('id_bahan' => $id_bahan), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('bahan');
		
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data bahan
	 */
	function delete($id_bahan)
	{
		$this->db->delete($this->table, array('id_bahan' => $id_bahan));
	}
	
	/**
	 * Tambah data bahan
	 */
	function add($bahan)
	{
		$this->db->insert($this->table, $bahan);
	}
	
	/**
	 * Update data bahan
	 */
	function update($id_bahan, $bahan)
	{
		$this->db->where('id_bahan', $id_bahan);
		$this->db->update($this->table, $bahan);
	}
	
	/**
	 * Validasi agar tidak ada bahand dengan id ganda
	 */
	function valid_id($id_bahan)
	{
		$query = $this->db->get_where($this->table, array('id_bahan' => $id_bahan));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file bahan_model.php */
/* Location: ./system/application/models/bahan_model.php */
