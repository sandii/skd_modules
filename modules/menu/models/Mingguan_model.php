<?php
/**
 * Mingguan_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Mingguan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Mingguan_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'mingguan';
	
	/**
	 * Mendapatkan semua data mingguan, diurutkan berdasarkan id_mingguan
	 */
	function get_mingguan()
	{
		$this->db->order_by('id_mingguan');
		return $this->db->get('mingguan');
	}
	
	/**
	 * Mendapatkan data sebuah mingguan
	 */
	function get_mingguan_by_id($id_mingguan)
	{
		return $this->db->get_where($this->table, array('id_mingguan' => $id_mingguan), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_mingguan');
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data mingguan
	 */
	function delete($id_mingguan)
	{
		$this->db->delete($this->table, array('id_mingguan' => $id_mingguan));
	}
	
	/**
	 * Tambah data mingguan
	 */
	function add($mingguan)
	{
		$this->db->insert($this->table, $mingguan);
	}
	
	/**
	 * Update data mingguan
	 */
	function update($id_mingguan, $mingguan)
	{
		$this->db->where('id_mingguan', $id_mingguan);
		$this->db->update($this->table, $mingguan);
	}
	
	/**
	 * Validasi agar tidak ada mingguand dengan id ganda
	 */
	function valid_id($id_mingguan)
	{
		$query = $this->db->get_where($this->table, array('id_mingguan' => $id_mingguan));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file mingguan_model.php */
/* Location: ./system/application/models/mingguan_model.php */
