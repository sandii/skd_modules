<?php
/**
 * Menu_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Menu_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Menu_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'menu';
	
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */
	
  
  	function get_all($where="")
	{
		$this->db->order_by('tanggal');
		
		if(!empty($where))
			$this->db->where($where);
		
		return $this->db->get($this->table);
	}
	
  
  	function get_last_date()
	{

		
		$this->db->select ('*');
		$this->db->from('menu');
	
		$this->db->limit(1, 0);
		$this->db->order_by('tanggal desc');
		
		return $this->db->get();
	}
	
  
  
  
  function count_all_num_rows($status)
	{
		
		//$this->db->select('*');
		//$this->db->from('menu');
		//$this->db->where('menu.status '.$status);
		
		
		
		
		//return $this->db->count_all($this->table);
		
		$sql= "select * from `menu` ";
		return $this->db->query($sql)->num_rows;
		
		
	}

	
	
	/**
	 * Tampilkan 10 baris menu terkini, diurutkan berdasarkan tanggal (Descending)
	 */
	function get_last_ten_menu($limit, $offset,$status)
	{

		
		$this->db->select ('*');
		$this->db->from('menu');
		

	
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}
	
	
	
	function get_menu_by_tanggal($selisih)
	{

		
		$this->db->select ('*');
		$this->db->from('menu');
		$this->db->limit($selisih);
		$this->db->order_by('tanggal,id_menu', 'asc');	
	
  	return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data menu
	 */
	function delete($id_menu)
	{
		$this->db->where('id_menu', $id_menu);
		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel menu
	 */
	function add($menu)
	{
		$this->db->insert($this->table, $menu);
	}
	
	/**
	 * Dapatkan data menu dengan id_menu tertentu, untuk proses update
	 */
	function get_menu_by_id($id_menu)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		return $this->db->get($this->table);
			
		
	}
	function get_menu()
	{
		$this->db->select('*');
		return $this->db->get($this->table);
			
		
	}
	
	
	
	function get_last_id_menu()
	{
		$this->db->select('id_menu');
		$this->db->order_by('id_menu', 'desc');
		$this->db->limit(1,0);
		return $this->db->get($this->table);
	}
	
	/**
	 * Update data menusi
	 */
	function update($id_menu, $menu)
	{
		$this->db->where('id_menu', $id_menu);
		$this->db->update($this->table, $menu);
	}
	function update_comment($id_menu, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->komen;
		
		$komen.=$comment;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set komen='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	function update_pembayaran_log($id_menu, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set pembayaran_log='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
}
// END Menu_model Class

/* End of file menu_model.php */
/* Location: ./system/application/models/menu_model.php */
