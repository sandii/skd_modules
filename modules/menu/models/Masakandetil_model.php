<?php
/**
 * Order_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Masakandetil_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Masakandetil_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'masakandetil';
	

	function add($masakandetil)
	{
		$this->db->insert($this->table, $masakandetil);
	}

function get_all($where)
	{
		$this->db->order_by('id_masakandetil');
		$this->db->where($where);
		return $this->db->get($this->table);
	}


function delete($id_masakandetil)
	{
		$this->db->delete($this->table, array('id_masakandetil' => $id_masakandetil));
	}
	
		function get_masakandetil_by_id($id_masakandetil)
	{
		return $this->db->get_where($this->table, array('id_masakandetil' => $id_masakandetil), 1)->row();
	}
	
function update($id_masakandetil, $masakandetil)
	{
		$this->db->where('id_masakandetil', $id_masakandetil);
		$this->db->update($this->table, $masakandetil);
	}	
	
	
	
}
