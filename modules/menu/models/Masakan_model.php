<?php
/**
 * Masakan_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Masakan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Masakan_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'masakan';
	
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */
	function count_all_num_rows($status)
	{
		
		//$this->db->select('*');
		//$this->db->from('masakan');
		//$this->db->where('masakan.status '.$status);
		
		
		
		
		//return $this->db->count_all($this->table);
		
		$sql= "select * from `masakan` where masakan.kategori $status ";
		return $this->db->query($sql)->num_rows();
		
		
	}
	function count_all_num_rows_pembayaran($where)
	{
		
		$this->db->select('*');
		$this->db->from('masakan,konsumen');
		$this->db->where($where);
		$this->db->where('masakan.id_konsumen = konsumen.id_konsumen');
		
		
		return $this->db->get()->num_rows();
		
				
	}
	
	
	
	/**
	 * Tampilkan 10 baris masakan terkini, diurutkan berdasarkan tanggal (Descending)
	 */
	function get_last_ten_masakan($limit, $offset,$status)
	{
		$this->db->distinct();
		
		$this->db->select ('*');
		$this->db->from('masakan');
		$this->db->where('masakan.kategori '.$status);
	 $this->db->order_by('masakan');
	
		$this->db->limit($limit, $offset);
		return $this->db->get();
	}
	
	function get_last_ten_pembayaran($limit, $offset,$where)
	{
		$this->db->select('*');
		$this->db->from('masakan,konsumen');
		$this->db->where($where);
		$this->db->where('masakan.id_konsumen = konsumen.id_konsumen');
		
		$this->db->limit($limit, $offset);
		$this->db->masakan_by('id_masakan', 'asc');
		return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data masakan
	 */
	function delete($id_masakan)
	{
		$this->db->where('id_masakan', $id_masakan);
		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel masakan
	 */
	function add($masakan)
	{
		$this->db->insert($this->table, $masakan);
	}
	
	/**
	 * Dapatkan data masakan dengan id_masakan tertentu, untuk proses update
	 */
	function get_masakan_by_id($id_masakan)
	{
		$this->db->select('*');

		
		$this->db->where('id_masakan', $id_masakan);
		return $this->db->get($this->table);
		
	
		
		
	}
	
	function get_masakan()
	{
		$this->db->select('*');
				$this->db->order_by('masakan');
		return $this->db->get($this->table);
		
		
		
	}
	
	
	
	function get_last_id_masakan()
	{
		$this->db->select('id_masakan');
		$this->db->order_by('id_masakan', 'desc');
		$this->db->limit(1,0);
		return $this->db->get($this->table);
	}
	
	/**
	 * Update data masakansi
	 */
	function update($id_masakan, $masakan)
	{
		$this->db->where('id_masakan', $id_masakan);
		$this->db->update($this->table, $masakan);
	}
	function update_comment($id_masakan, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_masakan', $id_masakan);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->komen;
		
		$komen.=$comment;
		//$this->db->where('id_masakan', $id_masakan);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `masakan` set komen='".$komen."' where id_masakan =".$id_masakan;
	$this->db->query($sql);
			
		
	}
	function update_pembayaran_log($id_masakan, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_masakan', $id_masakan);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_masakan', $id_masakan);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `masakan` set pembayaran_log='".$komen."' where id_masakan =".$id_masakan;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
}
// END Masakan_model Class

/* End of file masakan_model.php */
/* Location: ./system/application/models/masakan_model.php */
