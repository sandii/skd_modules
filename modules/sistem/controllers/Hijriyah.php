<?php

class Hijriyah extends CI_Controller
{

    public $limit = 30;
    public $title = 'hijriyah';

    public function Hijriyah()
    {
        parent::__construct();
        $this->load->model('hijriyah_model', '', true);

        // content yang fix, ada terus di web
        $this->data['nama'] = $this->session->userdata('nama');
        $this->data['title'] = $this->title;
        $this->load->helper('fungsi');

        $this->load->library('cekker');
        $this->cekker->cek($this->router->fetch_class());
    }

    public function index()
    {
        $this->load->model('Crud_model', '', true);
        $data = $this->data;
        $data['h2_title'] = "Kalender Hijriyah";

        $data['link'] = array('link_add' => anchor('sistem/hijriyah/add/', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')));

        // Load data
        $query = $this->hijriyah_model->get_all();
        $hijriyah = $query->result();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            // Table
            /*Set table template for alternating row 'zebra'*/
            $tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
                'row_alt_start' => '<tr class="zebra">',
                'row_alt_end' => '</tr>',
            );
            $this->table->set_template($tmpl);

            /*Set table heading */
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('no', 'bulan', 'tahun hijriyah', 'tanggal masehi', 'Actions');
            $i = 0;

            foreach ($hijriyah as $row) {

                $action = anchor('sistem/hijriyah/update/' . $row->id, '<span class="glyphicon glyphicon-pencil"></span>', array('class' => 'btn btn-warning btn-xs'));
                $action .= " " . anchor('sistem/hijriyah/delete/' . $row->id, '<span class="glyphicon glyphicon-remove"></span>', array('class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Anda yakin akan menghapus data ini?')"));

                $this->table->add_row(
                    ++$i,
                    $row->bulan,
                    $row->tahun_hijriyah,
                    $row->tgl_masehi,
                    $action
                );
            }
            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data hijriyah!';
        }

        // Load default view
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data = $this->data;
        $data['h2_title'] = 'Tambah Data ' . $this->title;
        $data['custom_view'] = 'hijriyah_form';
        $data['form_action'] = site_url('sistem/hijriyah/add_process');
        $data['link'] = array('link_back' => anchor('sistem/hijriyah', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        $this->load->view('template', $data);
    }

    public function add_process()
    {
        $data = $this->data;
        $data['h2_title'] = 'Tambah Data ' . $this->title;
        $data['custom_view'] = '/hijriyah_form';
        $data['form_action'] = site_url('sistem/hijriyah/add_process');
        $data['link'] = array('link_back' => anchor('sistem/hijriyah', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // Set validation rules
        $this->form_validation->set_rules('bulan', 'bulan', 'required');
        $this->form_validation->set_rules('tahun_hijriyah', 'tahun_hijriyah', 'required');
        $this->form_validation->set_rules('tgl_masehi', 'tgl_masehi', 'required');

        // Jika validasi sukses
        if ($this->form_validation->run() == true) {
            // Persiapan data
            $hijriyah = array(
                'bulan' => $this->input->post('bulan'),
                'tahun_hijriyah' => $this->input->post('tahun_hijriyah'),
                'tgl_masehi' => $this->input->post('tgl_masehi'),
            );

            // Proses penyimpanan data di table hijriyah
            $msg = $this->hijriyah_model->add($hijriyah);

            if ($msg == "sukses") {
                $this->session->set_flashdata('message', 'Satu data hijriyah berhasil disimpan!');
            }
            if ($msg == 'duplicate') {
                $this->session->set_flashdata('message', 'data hijriyah duplicate');
            }
            redirect('sistem/hijriyah');
        }
        // Jika validasi gagal
        else {
            $this->load->view('template', $data);
        }
    }

    /**
     * Pindah ke halaman update hijriyah
     */
    public function update($id)
    {
        $data = $this->data;
        $data['h2_title'] = 'update ' . $this->title;
        $data['custom_view'] = 'hijriyah_form';
        $data['form_action'] = site_url('sistem/hijriyah/update_process/' . $id);
        $data['link'] = array('link_back' => anchor('sistem/hijriyah', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // cari data dari database
        $hijriyah = $this->hijriyah_model->get_hijriyah_by_id($id);

        // Data untuk mengisi field2 form
        $data['default']['id'] = $hijriyah->id;
        $data['default']['bulan'] = $hijriyah->bulan;
        $data['default']['tahun_hijriyah'] = $hijriyah->tahun_hijriyah;
        $data['default']['tgl_masehi'] = $hijriyah->tgl_masehi;

        $this->load->view('template', $data);
    }

    public function update_process($id)
    {
        $data = $this->data;
        $data['h2_title'] = 'update ' . $this->title;
        $data['custom_view'] = 'user_form';
        $data['form_action'] = site_url('sistem/hijriyah/update_process');
        $data['link'] = array('link_back' => anchor('sistem/hijriyah', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // Set validation rules
        $this->form_validation->set_rules('bulan', 'bulan', 'required');
        $this->form_validation->set_rules('tahun_hijriyah', 'tahun_hijriyah', 'required');
        $this->form_validation->set_rules('tgl_masehi', 'tgl_masehi', 'required');

        if ($this->form_validation->run() == true) {
            // save data

            $hijriyah = array(
                'bulan' => $this->input->post('bulan'),
                'tahun_hijriyah' => $this->input->post('tahun_hijriyah'),
                'tgl_masehi' => $this->input->post('tgl_masehi'),
            );

            $msg = $this->hijriyah_model->update($id, $hijriyah);

            if ($msg == "sukses") {
                $this->session->set_flashdata('message', 'Satu data hijriyah berhasil diupdate!');
            }
            if ($msg == 'duplicate') {
                $this->session->set_flashdata('message', 'data hijriyah sudah ada');
            }
            redirect('sistem/hijriyah');

        } else {
            $this->load->view('template', $data);
        }
    }

    public function delete($id)
    {
        $this->hijriyah_model->delete($id);
        $this->session->set_flashdata('message', 'data hijriyah berhasil dihapus');

        redirect('sistem/hijriyah');
    }

}
