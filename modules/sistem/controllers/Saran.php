<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Saran extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'perbaikan system';

  
  function saran()
	{
		parent::__construct();
		$this->load->model('saran_model', '', TRUE);

	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
   

	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun = "")
	{
	
		$data=$this->data;
    $data['h2_title']="perbaikan system";

		
if (empty($tahun))
	$tahun=date("Y");


    $mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li> ".anchor('sistem/saran/index/'.$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		

		

		
		// Load data
		$query = $this->saran_model->get_all($tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		




		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'kontributor', 'laporan error / beri saran', 'tanggapan admin');
	


	$adminx="";	
    
    	foreach ($siswa as $row)
			{
		
			if ($_SESSION['jabatan']=='admin')
$adminx= " ".anchor('sistem/saran/update/'.$row->id_saran,'reply',array('class' => 'update'));

			
			
			
				$this->table->add_row( $row->tanggal, $row->nama,  $row->isi,$row->admin.$adminx
    								
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data saran!';
		}		
		
		$data['link'] = array('link_add' => anchor('sistem/saran/add/','lapor error / beri saran', array('class' => 'btn btn-success btn-lg'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}




	function add()
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'saran';
		$data['form_action']	= site_url('sistem/saran/add_process');
		$data['link'] 			= array('link_back' => anchor('sistem/saran','<span class="glyphicon glyphicon-arrow-left"></span>', array('class' => 'btn btn-success btn-lg'))
										);
										

		
		$this->load->view('template', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process()
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'saran > Tambah Data';
		$data['custom_view'] 		= 'saran';
		$data['form_action']	= site_url('sistem/saran/add_process');
		$data['link'] 			= array('link_back' => anchor('sistem/saran/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// data kelas untuk dropdown menu
	
		// Set validation rules
		$this->form_validation->set_rules('keterangan', 'required');
    
    	
		
		
		if ($this->form_validation->run() == TRUE)
		{
		
		
		$saldo=$saldo+$this->input->post('pemasukan')-$this->input->post('pengeluaran');
		
		$xxx=date("Y-m-d");

			// save data
			$uang = array('isi' 		=> $this->input->post('saran'),
			'nama' 		=> $_SESSION['nama'],
			'tanggal' => $xxx
						);
			$this->saran_model->add($uang);
			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('sistem/saran');
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	


	function update($id_saran)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'saran';
		$data['form_action']	= site_url('sistem/saran/update_process/'.$id_saran);
		$data['link'] 			= array('link_back' => anchor('sistem/saran','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
				
		

				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data marketing
	 */
	function update_process($id_saran)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'saran';
		$data['form_action']	= site_url('sistem/saran/update_process/'.$id_saran);
		$data['link'] 			= array('link_back' => anchor('sistem/saran','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										

		// cari data dari database
		$saran = $this->saran_model->get_saran_by_id($id_saran);


$isi=$saran->admin;
if(!empty($isi))
$isi.="<br>";



$isi.="<font color=red>".date("d-m-Y")."</font> ".$this->input->post('saran');

			// save data
			$marketing = array(
							'admin'		=> $isi
						);
			$this->saran_model->update($marketing,$id_saran);
			
			$this->session->set_flashdata('message', 'Satu data saran berhasil diupdate!');
			redirect('sistem/saran');



	}




}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
