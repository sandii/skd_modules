<?php
/**
 * Menu_model Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Hijriyah_model extends CI_Model
{

    public function Hijriyah_model()
    {
        parent::__construct();
        // the TRUE paramater tells CI that you'd like to return the database object.
        $this->db_service = $this->load->database('db_service', true);
    }

    // Iid_konsumenialisasi perusahaan tabel yang digunakan
    public $table = 'hijriyah';

    /**
     * Tambah data hijriyah
     */
    public function add($hijriyah)
    {
        $data = $this->get_all();
        $data_hijriyah = $data->result();
        $num_rows = $data->num_rows();
        if ($num_rows == 0) {
            $this->db_service->insert($this->table, $hijriyah);
            return $msg = "sukses";
        }
        if ($num_rows > 0) {
            foreach ($data_hijriyah as $row) {
                if ($row->bulan == $hijriyah['bulan'] && $row->tahun_hijriyah == $hijriyah['tahun_hijriyah']) {
                    return $msg = "duplicate";
                }
            }
            $this->db_service->insert($this->table, $hijriyah);
            return $msg = "sukses";
        }

    }

    /**
     * get data hijriyah
     */
    public function get_all()
    {
        $this->db_service->order_by('id', 'desc');
        return $this->db_service->get($this->table);
    }

    /**
     * Mendapatkan data sebuah user
     */
    public function get_hijriyah_by_id($id)
    {
        return $this->db_service->get_where($this->table, array('id' => $id), 1)->row();
    }

    public function update($id, $hijriyah)
    {
        $this->db_service->where('id', $id);
        $this->db_service->update($this->table, $hijriyah);
        return $msg = "sukses";
    }

    /**
     * Menghapus sebuah data user
     */
    public function delete($id)
    {
        $this->db_service->delete($this->table, array('id' => $id));
    }

    public function get_before_date()
    {
        $this->db_service->select("*")->from('hijriyah');
        $this->db_service->where('tgl_masehi <= CURRENT_DATE()');
        $this->db_service->order_by('tgl_masehi', 'desc');
        return $this->db_service->limit(1)->get()->row();  
    }

}
