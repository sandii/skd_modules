<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Tglbackup_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Tglbackup_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'tanggal';
	
	/**
	 * Cek tabel user, apakah ada user dengan username dan password tertentu
	 */
	function check_backup($tgl)
	{
	   
     
   
		 $this->db->where('nama','backup'); 
    $this->db->where(array('tanggal <' => $tgl));
		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	
	}
	
	
function ambil_gaji()
	{
	   
     
   
		 $this->db->where('nama','gaji'); 
  		$query = $this->db->get($this->table);		
			return $query->row()->isi;
	
	
	}	
	
function update()
	{
	$sql= " update ".$this->table." set tanggal=now() where nama='backup' ";
	$this->db->query($sql);
	}
	
	
	
function update_gaji( $tgl_skr)
	{
		
		
	$sql= " update ".$this->table." set isi='$tgl_skr' where nama='gaji' ";
	$this->db->query($sql);
	}
	
	
}
// END Login_model Class

/* End of file login_model.php */ 
/* Location: ./system/application/model/login_model.php */
