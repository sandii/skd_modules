<style type="text/css">@import url("<?php echo base_url() . 'css/jquery-ui-1.8.14.custom.css'; ?>");</style>
<script type="text/javascript" src="<?php echo base_url() . 'js/jquery-1.5.1.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'js/jquery-ui-1.8.14.custom.min.js'; ?>"></script>

<script type="text/javascript">
    $(function(){				
        $('#datepicker').datepicker({dateFormat:"yy-mm-dd"});
        $('#datepicker2').datepicker({dateFormat:"yy-mm-dd"});
        $('#datepicker3').datepicker({dateFormat:"yy-mm-dd"});
    });
</script>

<?php 
	echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';
	$flashmessage = $this->session->flashdata('message');
	echo ! empty($flashmessage) ? '<p class="message">' . $flashmessage . '</p>': '';
?>

<form name="marketing_form" method="post" action="<?php echo $form_action; ?>"
onsubmit="document.getElementById('submit').disabled=true;
document.getElementById('submit').value='proses';">	
	
	<p>
    	<label for="bulan">Bulan:</label>
		<?php      
         $options['1']='1';
         $options['2']='2';
         $options['3']='3';
         $options['4']='4';
         $options['5']='5';
         $options['6']='6';
         $options['7']='7';
         $options['8']='8';
         $options['9']='9';
         $options['10']='10';
         $options['11']='11';
         $options['12']='12';
         echo form_dropdown('bulan', $options, isset($default['bulan']) ? $default['bulan'] : '',"class=form-control"); ?>	
	</p>
    <?php echo form_error('bulan','<div class="alert alert-danger" role="alert">','</div>');?>
	<p>
		<label for="tahun">Tahun:</label>
		<input type="text" class="form-control" name="tahun_hijriyah" size="11" value="<?php echo set_value('tahun_hijriyah', isset($default['tahun_hijriyah']) ? $default['tahun_hijriyah'] : ''); ?>" />
    </p>
    <?php echo form_error('tahun_hijriyah','<div class="alert alert-danger" role="alert">','</div>');?>
    <p>
	    <label for="tanggal">tanggal masehi</label>	
	    <input type=text id="datepicker"  class="form-control" name=tgl_masehi  value="<?php echo set_value('tgl_masehi', isset($default['tgl_masehi']) ? $default['tgl_masehi'] : ''); ?>">	
	</p>
    <?php echo form_error('tgl_masehi','<div class="alert alert-danger" role="alert">','</div>');?>
    <p>
        <input type="submit" name="submit" id="submit" class='btn btn-primary' value=" Simpan " />
	</p>
</form>