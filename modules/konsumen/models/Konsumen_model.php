<?php
class Konsumen_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Konsumen_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel konsumen
	var $table = 'konsumen';
	
	/**
	 * Mendapatkan data semua konsumen
	 */

function get_autocomplete($q)
	{
	
	  		$this->db->where("perusahaan like '%$q%' ");

    
    $this->db->order_by('perusahaan');
		
		
		return $this->db->get($this->table);
	}




	function get_all($limit, $offset,$kons="",$orderby,$sort)
	{
		//$this->db->select('*');
		//$this->db->from($this->table);
		
		//$this->db->limit($limit, $offset);
		//$this->db->order_by('perusahaan', 'asc');
		//return $this->db->get()->result();
		//empty($offset)?$offset="0":"";
		//empty($limit)?$limit="0":"";

		
		$sql= "select *,datediff(now(),pertama)as tgl_pertama,datediff(now(),terakhir)as tgl_terakhir from konsumen ";
    
    if(!empty($kons)) 
    {$sql.=" where perusahaan like '%".$kons."%' or contact_person like '%".$kons."%' "; }
    
    
    
    $sql.=" order by ";
    
    
    
    
    
    if($orderby=="rata")
    {
    $sql.=" (omzet/datediff(now(),pertama)) ";
    }
       else
    $sql.=$orderby;
    
		
    $sql.=" ".$sort." limit " .$offset.",".$limit;
    
    return $this->db->query($sql)->result();



	}
	
	function get_konsumen()
	{
		$this->db->order_by('perusahaan');
		return $this->db->get('konsumen');
	}
	
	/**
	 * Mendapatkan data seorang konsumen dengan id_konsumen tertentu
	 */
	function get_konsumen_by_id($id_konsumen)
	{
		return $this->db->get_where($this->table, array('id_konsumen' => $id_konsumen))->row();
	}
	
	/**
	 * Menghitung jumlah baris tabel konsumen
	 */
	function count_all($kons="")
	{
	//	return $this->db->count_all($this->table);
		

	
		$sql= "select * from konsumen ";
       
    if(!empty($kons)) 
    {$sql.=" where perusahaan like '%".$kons."%' or contact_person like '%".$kons."%'"; }
    
		return $this->db->query($sql)->num_rows();

		
	}
	
	/**
	 * Menghapus data konsumen tertentu
	 */
	function delete($id_konsumen)
	{
		$this->db->delete($this->table, array('id_konsumen' => $id_konsumen));
	}
	
	/**
	 * Menambah data konsumen
	 */
	function add($konsumen)
	{
		$this->db->insert($this->table, $konsumen);
	}
	
	/**
	 * Update data konsumen
	 */
	function update($id_konsumen, $konsumen)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->update($this->table, $konsumen);
	}
	
	/**
	 * Cek id_konsumen agar tidak ada data konsumen yang sama
	 */
	function valid_id_konsumen($id_konsumen)
	{
		$query = $this->db->get_where($this->table, array('id_konsumen' => $id_konsumen));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
		function update_comment($id_konsumen, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_konsumen', $id_konsumen);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->konsumen_log;
		
		$komen.=$comment;
		//$this->db->where('id_order', $id_order);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `konsumen` set konsumen_log='".$komen."' where id_konsumen =".$id_konsumen;
	$this->db->query($sql);
			
		
	}


function baru($tahunan=0,$tgl1="",$tgl2="")
	{
	

$this->db->select (array('month(pertama)as tanggalm','year(pertama)as tahun','count(*) as totalm'));

		$this->db->from('konsumen');
		
		
		
		$this->db->where('omzet > 0');	

 
		
    if(!empty($tgl1))
		$this->db->where("pertama >'".$tgl1."'");	
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("pertama <'".$tgl2."'");	
}
		
		$this->db->order_by('pertama', 'asc');
if($tahunan!=0)
		$this->db->group_by('year(pertama)');
else
		$this->db->group_by('EXTRACT(YEAR_MONTH FROM pertama)');

		return $this->db->get();
	}
	
	function get_last_id_konsumen()
	{
		$this->db->select('id_konsumen');
		$this->db->order_by('id_konsumen', 'desc');
		$this->db->limit(1,0 );
		return $this->db->get($this->table);
	}
	
	
	
	
	
}
// END Konsumen_model Class

/* End of file konsumen_model.php */
/* Location: ./system/application/models/konsumen_model.php */
