<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Konsumen extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 10;
	var $title = 'konsumen';

  
  function Konsumen()
	{
		parent::__construct();
		$this->load->model('Konsumen_model', '', TRUE);
			$this->load->model('sdm/Pegawai_model', '', TRUE);

	

		
	  $this->load->helper('fungsi');	

	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
  $this->cekker->cek($this->router->fetch_class());
	}
	
	/**
	 * Iid_konsumenialisasi variabel untuk $title(untuk id element <body>), dan 
	 * $limit untuk membatasi penampilan data di tabel
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menjalankan fungsi get_all()
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();

	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function get_all($kons="",$orderby="",$sort="",$offset = 0)
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = 'konsumen/konsumen';
		


   if($this->input->post('cari'))
     {
     $kons=$this->input->post('kons');
     $offset=0;
     }
      
   

   
      
    if(empty($kons) or $kons=="semua") {$kons="semua";$cari_kons=""; }
    else $cari_kons=$kons;
   
    
    
    $data["kons"]=$kons;

		


if(empty($orderby))$orderby="perusahaan";
if(empty($sort))$sort="asc";



		
		// Load data
		$konsumen = $this->Konsumen_model->get_all($this->limit, $offset,$cari_kons,$orderby,$sort);
		$num_rows = $this->Konsumen_model->count_all($cari_kons);
		
		if ($num_rows > 0)
		{
		
		$uri_segment = 6;
			// Generate pagination			
			$config['base_url'] = site_url('konsumen/get_all/'.$kons.'/'.$orderby.'/'.$sort);
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';





			
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			
			


$headerx=array("perusahaan","pertama","terakhir","omzet","rata","total_order");
$headery=array("perusahaan","lama gabung <br>(bln)","order tarakhir<br>(bln)","total<br>omzet","omzet rata2<br> perbulan","jumlah <br>transaksi");

$i=0;
		while (!empty($headerx[$i]))
{

$classx="no_sort";
$sortx="asc";

if($orderby==$headerx[$i])			
{
if($sort=="asc")
{ $classx="asc";$sortx="desc";}
else
{$classx="desc";}
}



$judul_header[$i]= anchor('konsumen/get_all/semua/'.$headerx[$i].'/'.$sortx,$headery[$i],array('class' =>'btn btn-success btn-sm'));			
$i++;
}

			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No',$judul_header[0],'Contact Person','HP','Email',$judul_header[1],$judul_header[2],$judul_header[3],$judul_header[4],$judul_header[5], 'Actions');
			$i = 0 + $offset;
			
			foreach ($konsumen as $row)
			{
			
			
		$kar = array(" ",".", ",", "%", "(", ")", ">","<","!","@","#","$","^","&","*","+","=","{","}","[","]","|","\\",":",";","\'","\"","?","/","~","`");
    $url_perusahaan = str_replace($kar, "-",$row->perusahaan);
		
  	$omzetx=number_format($row->omzet, 0, ',', '.');	
    	$tgl_pertamax=floor($row->tgl_pertama/30);
    	$tgl_terakhirx=floor($row->tgl_terakhir/30);
    	
    	empty($tgl_terakhirx)?$tgl_terakhirx="barusan":"";
    	empty($tgl_pertamax)?$tgl_pertamax="barusan":"";
    	
      if(!empty($row->tgl_pertama))
    	$ratax=number_format($row->omzet/$row->tgl_pertama*30, 0, ',', '.');	
			else
			$ratax="";
      	if(!empty($row->perusahaan))
      	$pers=$row->perusahaan;
      	else
        $pers=$row->contact_person;
      

if(cek_auth("auth_marketing"))




$marketingx='<div class="btn-group" role="group" aria-label="...">'
.anchor('marketing/calon/add/lama/'.$row->id_konsumen,'tambah project',array('class' => 'btn btn-warning btn-xs  btn-block'))."</div>";

else
$marketingx="";    



        $this->table->add_row(++$i,   anchor_popup('konsumen/konsumendetil/index/'.$row->id_konsumen,$pers,array('class' => 'detail','width'=>'1000')), $row->contact_person,$row->hp,$row->email,$tgl_pertamax,$tgl_terakhirx,
        
        anchor('produksi/order/get_last_ten_order/'.$row->id_konsumen.'/dahulu/sekarang/semua/',$omzetx,array('class' => 'detail')),
		       
        
        
        "<div align=right>".$ratax,"<div align=center>".$row->total_order,
								
	$marketingx

										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data konsumen!';
		}		
		
	
if(cek_auth("auth_marketing"))
		$data['link'] = array('link_add' => anchor('konsumen/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
	
	/**
	 * Menghapus data konsumen dengan id_konsumen tertentu
	 */
	
	/**
	 * Menampilkan form tambah konsumen
	 */
	function add()
	{		
		$data	= $this->data;
		$data['h2_title'] 		= $this->title. '> Tambah Data';
		$data['custom_view'] 		= 'konsumen_form';
		$data['form_action']	= site_url('konsumen/add_process');
		$data['link'] 			= array('link_back' => anchor('konsumen','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// data marketing untuk dropdown menu
		
		$marketing = $this->Pegawai_model->get_ar()->result();	
		
		
		$data['default']['id_pegawai']=$_SESSION['kode'];

		foreach($marketing as $row)
		{
		
			$data['options_marketing'][$row->kode] = $row->nama;
		}
		
		$this->load->view('template', $data);
	}
	/**
	 * Proses tambah data konsumen
	 */
	function add_process()
	{
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.' > Tambah Data';

		$data['custom_view'] 		= 'konsumen_form';

		$data['form_action']	= site_url('konsumen/add_process');
		$data['link'] 			= array('link_back' => anchor('absen/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
									
$marketing = $this->Pegawai_model->get_ar()->result();	
		
		
		$data['default']['id_pegawai']=$_SESSION['id_user'];
		
		foreach($marketing as $row)
		{
			$data['options_marketing'][$row->id_pegawai] = $row->nama;
		}
		
		// Set validation rules
		//$this->form_validation->set_rules('id_konsumen', 'id_konsumen', 'required|exact_length[4]|numeric|callback_valid_id_konsumen');
		$this->form_validation->set_rules('perusahaan', 'Perusahaan', 'required|max_length[50]');
		$this->form_validation->set_rules('id_pegawai', 'Account representative', 'required');
	
		
		if ($this->form_validation->run() == TRUE)
		{
		
		
		
			// save data
			$konsumen = array( 
							'perusahaan'		=> $this->input->post('perusahaan'),
							'contact_person'		=> $this->input->post('contact_person'),
							'telp_kantor'		=> $this->input->post('telp_kantor'),
							'hp'		=> $this->input->post('hp'),
							'email'		=> $this->input->post('email'),
							'alamat'		=> $this->input->post('alamat'),
						
			'pertama' => date("Ymd"),
'terakhir' => date("Ymd"),
      				'id_ar'	=> $this->input->post('id_pegawai')
      				
						);
			$this->Konsumen_model->add($konsumen);
			
			$this->session->set_flashdata('message', 'Satu data konsumen berhasil disimpan!');
			redirect('konsumen/index');
		}
		else
		{	
			//$data['default']['id_marketing'] = $this->input->post('id_marketing');
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	function valid_id_konsumen($id_konsumen)
	{
		if ($this->Konsumen_model->valid_id_konsumen($id_konsumen) == TRUE)
		{
			$this->form_validation->set_message('valid_id_konsumen', "konsumen dengan id_konsumen $id_konsumen sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	


function autocomplete()
{

	$query = $this->Konsumen_model->get_autocomplete($this->input->get('q'));
	$bahanbeli = $query->result();
	$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{

echo"[";
$i=0;
    foreach($bahanbeli as $row):
    if ($i!=0)
    echo",";   
        echo '{"name":"'.$row->perusahaan.'","id":"'.$row->id_konsumen.'"}';
  
  $i++;
    endforeach;    
echo"]";
} 

}

}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
