<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Konsumendetil extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 10;
	var $title = 'konsumen';
	var $alamat = 'konsumen/konsumendetil';

  
  function Konsumendetil()
	{
		parent::__construct();
		$this->load->model('Konsumen_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);

  	$this->load->helper('fungsi');	
	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
   $this->cekker->cek($this->router->fetch_class());
	
	}
	
	/**
	 * Iid_konsumenialisasi variabel untuk $title(untuk id element <body>), dan 
	 * $limit untuk membatasi penampilan data di tabel
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menjalankan fungsi get_all()
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_konsumen)
	{
			$this->get_all($id_konsumen);

	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function get_all($id_konsumen)
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = 'konsumen/konsumendetil';
		$data['link_frame']=site_url('konsumen/comment_konsumen/index/'.$id_konsumen);
		$data['form_action']	= site_url($this->alamat.'/add_comment/'.$id_konsumen);
		
		// Offset
		
		// Load data
	
  	$konsumen = $this->Konsumen_model->get_konsumen_by_id($id_konsumen);	
		
		

    $omzetx=number_format($konsumen->omzet, 0, ',', '.');	
  	

    	
   


			// Generate pagination			
	
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");



if(!empty($konsumen->id_ar))
	{	$marketing = $this->Pegawai_model->get_pegawai_by_kode($konsumen->id_ar);
    	$nama_ar=!empty($marketing->nama)?$marketing->nama:"";
   }
   else
   	$nama_ar="";
		
				$this->table->add_row('<b>Perusahaan',$konsumen->perusahaan);
				        $this->table->add_row('<b>alamat',$konsumen->alamat);
         $this->table->add_row('<b>telp kantor',$konsumen->telp_kantor);        
        $this->table->add_row('<b>Contact Person',$konsumen->contact_person);
                $this->table->add_row('<b>HP',$konsumen->hp);
        $this->table->add_row('<b>Email',$konsumen->email);
        


       	$this->table->add_row('<b>AR',$nama_ar); 
 



        $this->table->add_row('<b>total omzet',$omzetx);
                $this->table->add_row('<b>jumlah transaksi',$konsumen->total_order);
						
if(cek_auth("auth_marketing"))
	$data['link']=array(anchor($this->alamat.'/update/'.$konsumen->id_konsumen,'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
									//	.' '.anchor('konsumendetil/delete/'.$konsumen->id_konsumen,'hapus',array('class'=> 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")));
										

			$data['tablex'] = $this->table->generate();
	
	
		// Load view`
		$this->load->view('template2', $data);
	}
	
	/**
	 * Menghapus data konsumen dengan id_konsumen tertentu


	function delete($id_konsumen)
	{
		$this->Konsumen_model->delete($id_konsumen);
	//	$this->session->set_flashdata('message', '1 data absen berhasil dihapus');
		
		echo"data berhasil dihapus";
	}
	 */	
	/**
	 * Menampilkan form tambah konsumen
	 */

	
	/**
	 * Menampilkan form update data konsumen
	 */
	function update($id_konsumen)
	{

	
	cek_auth("auth_marketing",1);

		$data			= $this->data;
		$data['h2_title'] 		= "edit ".$this->title;
		$data['custom_view'] 		= 'konsumen_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_konsumen);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_konsumen,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// data marketing untuk dropdown menu

		
		$marketing = $this->Pegawai_model->get_ar()->result();	
		
		
	
		foreach($marketing as $row)
		{
			
			$data['options_marketing'][$row->kode] = $row->nama;
		}
		
		
		// cari data dari database
		$konsumen = $this->Konsumen_model->get_konsumen_by_id($id_konsumen);
		
		
		
		// Data untuk mengisi field2 form
		$data['default']['id_konsumen'] 		= $konsumen->id_konsumen;
		$data['default']['perusahaan'] 		= $konsumen->perusahaan;		
		$data['default']['contact_person']	= $konsumen->contact_person;
		$data['default']['alamat']	= $konsumen->alamat;
		$data['default']['email']	= $konsumen->email;
		$data['default']['telp_kantor']	= $konsumen->telp_kantor;
		$data['default']['hp']	= $konsumen->hp;

		$data['default']['id_pegawai']	= $konsumen->id_ar;

	
				
		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data konsumen
	 */
	function update_process($id_konsumen)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';
		$data['custom_view'] 		= 'konsumen_form';

		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_konsumen);

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/'.$this->session->userdata('id_konsumen'),'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		

	$marketing = $this->Pegawai_model->get_aktif()->result();	
		
		
	
		foreach($marketing as $row)
		{
				if(!empty($row->kode))
			$data['options_marketing'][$row->kode] = $row->nama;
		}


$konsumen = $this->Konsumen_model->get_konsumen_by_id($id_konsumen);
		
		
		
		// Data untuk mengisi field2 form
		$data['default']['id_konsumen'] 		= $konsumen->id_konsumen;
		$data['default']['perusahaan'] 		= $konsumen->perusahaan;		
		$data['default']['contact_person']	= $konsumen->contact_person;
		$data['default']['alamat']	= $konsumen->alamat;
		$data['default']['email']	= $konsumen->email;
		$data['default']['telp_kantor']	= $konsumen->telp_kantor;
		$data['default']['hp']	= $konsumen->hp;

		$data['default']['id_pegawai']	= $konsumen->id_ar;





		// Set validation rules
	
		$this->form_validation->set_rules('perusahaan', 'Perusahaan', 'required|max_length[50]');
		$this->form_validation->set_rules('id_pegawai', 'AR', 'required');
		
		// jika proses validasi sukses, maka lanjut mengupdate data
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$absen = array(
							'perusahaan'		=> $this->input->post('perusahaan'),
	           	'contact_person'		=> $this->input->post('contact_person'),
							'telp_kantor'		=> $this->input->post('telp_kantor'),
							'hp'		=> $this->input->post('hp'),
							'email'		=> $this->input->post('email'),
							'alamat'		=> $this->input->post('alamat'),              
              'id_ar'	=> $this->input->post('id_pegawai')
					
          
          
          	);
			$this->Konsumen_model->update($id_konsumen, $absen);
			// $this->Absen_model->update($id_absen, $absen);
			
			// set pesan
			$this->session->set_flashdata('message', 'Satu data konsumen berhasil diupdate!');
			
			redirect($this->alamat.'/index/'.$id_konsumen);
		}
		else
		{



			
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	
function add_comment($id_konsumen)
	{       $tulisan=$this->input->post('insert_comment');
		if( !empty($tulisan)) 
		{$commentx="<font color=red>".$_SESSION['nama'].": </font>";
		$commentx.=$this->input->post('insert_comment')."<br>";		
		$this->Konsumen_model->update_comment($id_konsumen,$commentx);}
		//$this->session->set_flashdata('message', 'status order berhasil diupdate');		
		redirect($this->alamat.'/index/'.$id_konsumen);
	}
		
	
	
	


}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
