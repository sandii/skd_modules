

<form name="absen_form" method="post" class=form-horizontal action="<?php echo $form_action; ?>"
onsubmit="document.getElementById('submit').disabled=true;
document.getElementById('submit').value='proses';">

	
	<p>
		<label >Perusahaan</label>
		<input  type="text" class="form-control" name="perusahaan" size="30" value="<?php echo set_value('perusahaan', isset($default['perusahaan']) ? $default['perusahaan'] : ''); ?>" />
		
	</p>
	<?php echo form_error('perusahaan','<div class="alert alert-danger" role="alert">','</div>');?>	

	<p>
		<label for="contact_person">Contact Person</label>
		<input type="text" class="form-control" name="contact_person" size="30" value="<?php echo set_value('perusahaan', isset($default['contact_person']) ? $default['contact_person'] : ''); ?>" />
		
	</p>
	<p>
		<label for="telp_kantor">Telepon kantor</label>
		<input type="text" class="form-control" name="telp_kantor" size="30" value="<?php echo set_value('telp_kantor', isset($default['telp_kantor']) ? $default['telp_kantor'] : ''); ?>" />
		
	</p>
	<p>
		<label for="hp">HP</label>
		<input type="text" class="form-control" name="hp" size="30" value="<?php echo set_value('hp', isset($default['hp']) ? $default['hp'] : ''); ?>" />
		
	</p>
	<p>
		<label for="email">Email</label>
		<input type="text" class="form-control" name="email" size="30" value="<?php echo set_value('email', isset($default['email']) ? $default['email'] : ''); ?>" />
		
	</p>
	<p>
		<label for="alamat">Alamat </label>
		<textarea class="form-control" class="form-control" name="alamat" rows="3" cols=50><?php echo set_value('alamat', isset($default['alamat']) ? $default['alamat'] : ''); ?></textarea>
	</p>
	

	<p>
		<label for="id_marketing">AR:</label>
        <?php echo form_dropdown('id_pegawai', $options_marketing, isset($default['id_pegawai']) ? $default['id_pegawai'] : '',"class=form-control"); ?>
	</p>
	<?php echo form_error('id_pegawai','<div class="alert alert-danger" role="alert">','</div>');?>

	<p>
		<input type="submit" class="btn btn-info" name="submit" id="submit" value=" Simpan " />
	</p>
</form>
