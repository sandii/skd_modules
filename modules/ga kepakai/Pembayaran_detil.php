<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pembayaran_detil extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'detil pembayaran';
	
  
  function Pembayaran_detil()
	{
		parent::__construct();
		$this->load->model('Pembayaran_model', '', TRUE);
		$this->load->model('keuangan/Akun_model', '', TRUE);	
				$this->load->model('konsumen/Konsumen_model', '', TRUE);	
						$this->load->model('produksi/Order_model', '', TRUE);	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman supplier,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_order)
	{
		

		
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
	
  
  
  
	   $order = $this->Order_model->get_order_by_id($id_order)->row();
		
		
		// Data untuk mengisi field2 form
		
	       $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);
		
		$data['perusahaan']= $konsumen->perusahaan;
		$data['total']= $order->total;
		$data['pembayaran']= $order->pembayaran;
    $data['kekurangan']=$order->total-$order->pembayaran;		 
  
  $data['pagination']=
  "
  	<p>
		<label for=id_konsumen>konsumen:</label>".
      $konsumen->perusahaan."
     	</p>
		<p>
		<label for=id_konsumen>total tagihan: </label>
    Rp.". number_format($order->total, 0, ',', '.')." 
     	</p>




		<p>
		<label for=id_konsumen>kekurangan:</label>
    Rp.".number_format($order->total-$order->pembayaran, 0, ',', '.')."
    
     	</p>";
  
  	
		// Load data
		$query = $this->Pembayaran_model->get_pembayaran_by_order2($id_order);
		$supplier = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('no','tanggal', 'jumlah','ke rekening','keterangan');
			$i = 0;
			
			foreach ($supplier as $row)
			{
			
$tujuan=$this->Akun_model->get_akun_by_id($row->tujuan)->nama;			
			
				$this->table->add_row(++$i,  $row->tanggal,
	$row->jumlah,$tujuan,$row->ket	);							
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data pembayaran!';
		}		
		
	
		
		// Load view
		$this->load->view('template2', $data);
	}
		
	/**
	 * Hapus data supplier
	 */
	
}
// END Supplier Class

/* End of file supplier.php */
/* Location: ./system/application/controllers/supplier.php */
