<?php
/**
 * Whattodo_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Whattodo_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Whattodo_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'whattodo';
	
	/**
	 * Mendapatkan semua data whattodo, diurutkan berdasarkan id_whattodo
	 */
	function get_whattodo()
	{
		$this->db->order_by('id_whattodo');
		return $this->db->get('whattodo');
	}
	
	/**
	 * Mendapatkan data sebuah whattodo
	 */
	function get_whattodo_by_id($id_whattodo,$username)
	{
		return $this->db->get_where($this->table, array('id_whattodo' => $id_whattodo,'username'=>$username), 1)->row();
	}
	
	function get_all($username)
	{
		$this->db->order_by('id_whattodo','desc');
				$this->db->where('username', $username);
				

				
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data whattodo
	 */
	function delete($id_whattodo)
	{
		$this->db->delete($this->table, array('id_whattodo' => $id_whattodo));
	}
	
	/**
	 * Tambah data whattodo
	 */
	function add($whattodo)
	{
		$this->db->insert($this->table, $whattodo);
	}
	
	/**
	 * Update data whattodo
	 */
	function update($id_whattodo, $whattodo)
	{
		$this->db->where('id_whattodo', $id_whattodo);
		$this->db->update($this->table, $whattodo);
	}
	
	/**
	 * Validasi agar tidak ada whattodod dengan id ganda
	 */
	function valid_id($id_whattodo)
	{
		$query = $this->db->get_where($this->table, array('id_whattodo' => $id_whattodo));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file whattodo_model.php */
/* Location: ./system/application/models/whattodo_model.php */
