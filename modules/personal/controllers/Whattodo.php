<?php
/**
 * Whattodo Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Whattodo extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'what to do';
	
  
  function Whattodo()
	{
		parent::__construct();
		$this->load->model('Whattodo_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
  $this->cekker->cek();


  }
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman whattodo,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
	}
	
	/**
	 * Tampilkan semua data whattodo
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
		$username=$_SESSION['username'];
		
		// Load data
		$query = $this->Whattodo_model->get_all($username);
		$whattodo = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table align=center width="80%">',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('', 'What to do', '');
			
  
    	$this->table->set_heading(array("data"=>"", "width"=>"2%"),
      array("data"=>"What to do", "width"=>"68%"),array("data"=>"", "width"=>"10%") );      
      
      
      $i = 0;
			
			foreach ($whattodo as $row)
			{
				$this->table->add_row(++$i,  $row->isi,
										anchor('personal/whattodo/update/'.$row->id_whattodo,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('personal/whattodo/delete/'.$row->id_whattodo,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin tugas ini sudah beres?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tugas belum ada!';
		}		
		
		$data['link'] = array('link_add' => anchor('personal/whattodo/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data whattodo
	 */
	function delete($id_whattodo)
	{
		$this->Whattodo_model->delete($id_whattodo);
		$this->session->set_flashdata('message', '1 tugas berhasil dihapus');
		
		redirect('personal/whattodo');
	}
	
	/**
	 * Pindah ke halaman tambah whattodo
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'whattodo_form';
		$data['form_action']	= site_url('personal/whattodo/add_process');
		$data['link'] 			= array('link_back' => anchor('personal/whattodo','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data whattodo
	 */
	function add_process()
	{
	$username=$_SESSION['username'];
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'whattodo_form';
		$data['form_action']	= site_url('personal/whattodo/add_process');
		$data['link'] 			= array('link_back' => anchor('personal/whattodo','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// Set validation rules

		$this->form_validation->set_rules('isi', 'isi', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$whattodo = array('id_whattodo'	=> $this->input->post('id_whattodo'),
							'isi'		=> $this->input->post('isi'),
								'username'		=> $username
						);
			// Proses penyimpanan data di table whattodo
			$this->Whattodo_model->add($whattodo);
			
			$this->session->set_flashdata('message', 'Satu tugas berhasil disimpan!');
			redirect('personal/whattodo/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update whattodo
	 */
	function update($id_whattodo)
	{
	
	$username=$_SESSION['username'];
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'whattodo_form';
		$data['form_action']	= site_url('personal/whattodo/update_process');
		$data['link'] 			= array('link_back' => anchor('personal/whattodo','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$whattodo = $this->Whattodo_model->get_whattodo_by_id($id_whattodo,$username);
				
		// buat session untuk menyimpan data primary key (id_whattodo)
		$this->session->set_userdata('id_whattodo', $whattodo->id_whattodo);
		
		// Data untuk mengisi field2 form
		$data['default']['id_whattodo'] 	= $whattodo->id_whattodo;		
		$data['default']['isi']		= $whattodo->isi;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data whattodo
	 */
	function update_process()
	{
	

	
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'whattodo_form';
		$data['form_action']	= site_url('personal/whattodo/update_process');
		$data['link'] 			= array('link_back' => anchor('personal/whattodo','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('isi', 'isi', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$whattodo = array(
							'isi'		=> $this->input->post('isi')
						);
			$this->Whattodo_model->update($this->session->userdata('id_whattodo'),$whattodo);
			
			$this->session->set_flashdata('message', 'Satu tugas berhasil diupdate!');
			redirect('personal/whattodo');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_whattodo valid, agar tidak ganda
	 */
	function valid_id($id_whattodo)
	{
		if ($this->Whattodo_model->valid_id($id_whattodo) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "whattodo dengan Kode $id_whattodo sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_whattodo valid, agar tidak ganda. Hanya untuk proses update data whattodo
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_whattodo');
		$new_id			= $this->input->post('id_whattodo');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->Whattodo_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "Whattodo dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END Whattodo Class

/* End of file whattodo.php */
/* Location: ./system/application/controllers/whattodo.php */
