<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Gajiku extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'penggajian';

var $nama_bulan=array(1=>"januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember");
  
  function Gajiku()
	{
		parent::__construct();
		$this->load->model('sdm/Gaji_model', '', TRUE);
		$this->load->model('sdm/Lembur_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);
		$this->load->model('sdm/Penggajian_model', '', TRUE);
		$this->load->model('sdm/Kasbon_model', '', TRUE);
	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;


	      $this->load->library('cekker');
  $this->cekker->cek();

	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun = "")
	{
		
		
$id_pegawai=$_SESSION["id_user"];		
	
	
	
		$data = $this->data;
	
		
		
			$data['h2_title'] = "Gajiku ";
		
if (empty($tahun))
	$tahun=date("Y");

    $mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor('personal/gajiku/index/'.$i,$i,array('class' => 'tahun'))."<li> ";
    else
     $pagination.="<li class=active><a>$tahun</li>";
    }

		
		$data['pagination'] = $pagination;		

		
		
		// Load data
		$query = $this->Penggajian_model->get_all($id_pegawai,$tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		

		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('bulan','pokok', 'jam lembur','fee lembur','potong kasbon','total','tanggal dibayar','slip gaji' );
	
				
			foreach ($siswa as $row)
			{
				$this->table->add_row( $this->nama_bulan[$row->bulan], $row->pokok, $row->jam_lembur,$row->lembur,$row->kasbon, $row->total,$row->tanggal,
						     
						     anchor_popup('slipgaji/index/'.$row->id_penggajian,'print', array('class' => 'add','width'=>800))
				
						     		);

}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data gaji!';
		
		
		
		}		

		
		
		
		

		
		// Load view
		$this->load->view('template', $data);
	}




	function add($id_pegawai,$tahun,$bulan)
	{		
	  $data=$this->data;
	  $data['bulan']=$bulan;
		  
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'penggajian/add';
		$data['form_action']	= site_url('penggajian/add_process/'.$id_pegawai.'/'.$tahun.'/'.$bulan);
		$data['link'] 			= array('link_back' => anchor('penggajian/index/'.$id_pegawai.'/'.$tahun,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);


$gaji = $this->Gaji_model->get_terakhir($id_pegawai);
isset($gaji->gaji)?$saldo_gaji=$gaji->gaji:$saldo_gaji=0;


$lembur=$this->Lembur_model->get_lembur($tahun,$bulan,$id_pegawai);
isset($lembur->jam)?$saldo_lembur=$lembur->jam:$saldo_lembur=0;

$kasbon = $this->Kasbon_model->get_terakhir($id_pegawai);
isset($kasbon->saldo)?$saldo_kasbon=$kasbon->saldo:$saldo_kasbon=0;

$data['default']['kasbon']=$saldo_kasbon;
$data['gaji']=$saldo_gaji;
$data['lembur']=$saldo_lembur;
										

		
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai,$tahun,$bulan)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'penggajian > Tambah Data';
		$data['custom_view'] 		= 'penggajian/add';
		$data['form_action']	= site_url('penggajian/add_process/'.$id_pegawai.'/'.$tahun.'/'.$bulan);
		$data['link'] 			= array('link_back' => anchor('penggajian/index/'.$id_pegawai.'/'.$tahun,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))										);
		$tanggal=date("Y-m-d");	
			// save data


$harga_lembur=$this->input->post('gaji')/25/8*1.5*$this->input->post('lembur');

$total=$this->input->post('gaji')+$harga_lembur-$this->input->post('kasbon');
			$uang = array(
	
			 'id_pegawai' 		=> $id_pegawai,
			 'bulan' 		=> $bulan,
			 			 'tahun' 		=> $tahun,
			 			 'tanggal' 		=> $tanggal,						 
			 			 'lembur' 		=> $harga_lembur,						 
					
					'jam_lembur'=> $this->input->post('lembur'),
					'kasbon'=> $this->input->post('kasbon'),
'pokok'=> $this->input->post('gaji'),
'total'=>$total
					
	
						);
			$this->Penggajian_model->add($uang);

// ngurangi kasbon


   $saldo_terakhir = $this->Kasbon_model->get_terakhir($id_pegawai);
    
    $saldo=$saldo_terakhir->saldo;
		
		
		
		$saldo=$saldo-$this->input->post('kasbon');


			
			
$uang = array('keterangan' 		=> "potong gaji bulan ".$this->nama_bulan[$bulan],
	
			'id_pegawai' 		=> $id_pegawai,
							'pengeluaran'		=> $this->input->post('kasbon'),
		
							'tanggal'	=> $tanggal,
											'saldo'	=> $saldo
						);
			$this->Kasbon_model->add($uang);
	


			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('penggajian/index/'.$id_pegawai.'/'.$tahun);
	
	}


	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	
	}


// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
