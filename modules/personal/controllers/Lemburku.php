<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Lemburku extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'lembur';
var $nama_bulan=array(1=>"januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember");

  
  function Lemburku()
	{
		parent::__construct();
		$this->load->model('sdm/Lembur_model', '', TRUE);
	$this->load->model('sdm/Pegawai_model', '', TRUE);
	
	// content yang fix, ada terus di web
    $this->data['title']=$this->title;



     $this->load->library('cekker');
  $this->cekker->cek();


	
	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun="")
	{
	
	if (empty($tahun))
	$tahun=date("Y");
	
	$id_pegawai=$_SESSION['id_user'];
		
		$data = $this->data;
		$data['h2_title'] = "Lemburku ";
    
		
		
		$siswa = $this->Lembur_model->get_all($id_pegawai,$tahun);
		$num_rows = $this->Lembur_model->count_all();
		
		
		
		
		
		$mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{
    //$pagination.=" $i ";
    
    if($tahun!=$i)
    $pagination.="<li>".anchor('personal/lemburku/index/'.$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</li>";
    }
		
		
		
		
		$data['pagination'] = $pagination;		
		if ($num_rows > 0)
		{
			// Generate pagination			
			$config['base_url'] = site_url('keuangan/get_all');
			
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('bulan', 'jumlah jam', 'keterangan');
	
			
			foreach ($siswa as $row)
			{
			
   
      
      
      	$this->table->add_row( $this->nama_bulan[$row->bulan],$row->jam, $row->keterangan
        
        );
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data lembur!';
		}		
		
		
		// Load view
		
		
		
		$this->load->view('template', $data);
	}






	
	
	
		}
	
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama


}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
