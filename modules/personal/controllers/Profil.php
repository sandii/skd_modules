<?php
/**
 * Pegawai Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Profil extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Profil';
	
  
  function Profil()
	{
		parent::__construct();
		$this->load->model('sdm/Pegawai_model', '', TRUE);
		$this->load->model('sdm/Cuti_model', '', TRUE);
		$this->load->model('sdm/Tunjangan_model', '', TRUE);

  // content yang fix, ada terus di web
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
   $this->cekker->cek();
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman pegawai,
	 * jika tidak akan meredirect ke halaman login
	 */

	
	/**
	 * Tampilkan semua data pegawai
	 */
	function index()
	{
		
$tahun_skr=date("Y");  
		
		
		
		$data = $this->data;
		$data['h2_title'] = $this->title;
	
		
		
		$id_pegawai=$_SESSION["id_user"];
	
		
		
		
		
		
		
		// Load data
		$row = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);

		
	
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
		
			$i = 0;


	$this->table->add_row("<b>nama",$row->nama);
		$this->table->add_row("<b>NIK",$row->nik);
			$this->table->add_row("<b>jenis kelamin",$row->kelamin);
	$this->table->add_row("<b>alamat",$row->alamat);
	$this->table->add_row("<b>tempat tgl lahir", $row->tmp_lahir."/ ".$row->tgl_lahir);
	$this->table->add_row("<b>telp",$row->telp);
	$this->table->add_row("<b>email",$row->email);
	$this->table->add_row("<b>goldar",$row->goldar);
	$this->table->add_row("<b>merokok",$row->merokok);

	$this->table->add_row("<b>anak",$row->anak);
	$this->table->add_row("<b>tgl masuk",$row->tgl_masuk);
	$this->table->add_row("<b>status",$row->status);
		$this->table->add_row("<b>tgl gajian",$row->gajian);
			$this->table->add_row("<b>no. rekening",$row->norek);
		
	if(!empty($row->tgl_keluar))	
	$this->table->add_row("<b>tgl keluar",$row->tgl_keluar);
else
{
$lama_bekerja="";
$tahun_bekerja=floor($row->lama_bekerja/12);
if($tahun_bekerja>0)
$lama_bekerja=$tahun_bekerja." tahun ";
$lama_bekerja.=$row->lama_bekerja%12 ." bulan";

$this->table->add_row("<b>lama bekerja",$lama_bekerja);  
  }
  
 

$jumlah_cuti=	 $this->Cuti_model->jumlah_cuti($tahun_skr,$id_pegawai,"cuti");
$jumlah_ijin=	 $this->Cuti_model->jumlah_cuti($tahun_skr,$id_pegawai,"ijin");

$tunjangan = $this->Tunjangan_model->get_terakhir($id_pegawai,$tahun_skr);
isset($tunjangan->saldo)?$saldo_tunjangan=$tunjangan->saldo:$saldo_tunjangan=0;

 
  
  
$this->table->add_row("<b>cuti ",$jumlah_cuti->total);
$this->table->add_row("<b>ijin ",$jumlah_ijin->total);
$this->table->add_row("<b>tunjangan kesehatan yg sudah diambil",$saldo_tunjangan);

  
	
  
  		$data['table'] = $this->table->generate();
	
			
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data pegawai
	 */
	
	
	/**
	 * Pindah ke halaman tambah pegawai
	 */

	
	/**
	 * Pindah ke halaman update pegawai
	 */
	function update()
	{
		
		$id_pegawai=$_SESSION["id_user"];
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('profil/update_process');
		$data['link'] 			= array('link_back' => anchor('profil','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$pegawai = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
				
		// buat session untuk menyimpan data primary key (id_pegawai)
		
		// Data untuk mengisi field2 form
		
		$data['default']['nama']		= $pegawai->nama;
		$data['default']['alamat']		= $pegawai->alamat;
		$data['default']['telp']		= $pegawai->telp;
		$data['default']['email']		= $pegawai->email;
		$data['default']['tmp_lahir']		= $pegawai->tmp_lahir;
		$data['default']['tgl_lahir']		= $pegawai->tgl_lahir;
		$data['default']['tgl_masuk']		= $pegawai->tgl_masuk;
		$data['default']['tgl_keluar']		= $pegawai->tgl_keluar;
		$data['default']['status']		= $pegawai->status;
		$data['default']['anak']		= $pegawai->anak;
		$data['default']['goldar']		= $pegawai->goldar;
		$data['default']['gajian']		= $pegawai->gajian;
		$data['default']['norek']		= $pegawai->norek;
		$data['default']['merokok']		= $pegawai->merokok; 
      		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data pegawai
	 */
	function update_process()
	{
		$id_pegawai=$_SESSION["id_user"];
		
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('pegawai/update_process');
		$data['link'] 			= array('link_back' => anchor('pegawai','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('nama', 'nama', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$pegawai = array(

							'nama'		=> $this->input->post('nama'),
							'alamat'		=> $this->input->post('alamat'),
							'tmp_lahir'		=> $this->input->post('tmp_lahir'),
							'tgl_lahir'		=> $this->input->post('tgl_lahir'),
							'tgl_masuk'		=> $this->input->post('tgl_masuk'),
							'telp'		=> $this->input->post('telp'),
							'email'		=> $this->input->post('email'),
							'goldar'		=> $this->input->post('goldar'),
							'status'		=> $this->input->post('status'),
							'anak'		=> $this->input->post('anak'),
								'norek'		=> $this->input->post('norek'),
									'merokok'		=> $this->input->post('merokok'),
							'gajian'		=> $this->input->post('gajian')
						);

$xx=$this->input->post('tgl_keluar');					
if(!empty($xx))
$pegawai['tgl_keluar']=$this->input->post('tgl_keluar');



			$this->Pegawai_model->update($id_pegawai,$pegawai);
			
			$this->session->set_flashdata('message', 'Satu data pegawai berhasil diupdate!');
			redirect('profil/index');
		}
		else
		{		
			$this->load->view('template2', $data);
		}
	}
	
	/**
	 * Cek apakah $id_pegawai valid, agar tidak ganda
	 */
	function valid_id($id_pegawai)
	{
		if ($this->Pegawai_model->valid_id($id_pegawai) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "pegawai dengan Kode $id_pegawai sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_pegawai valid, agar tidak ganda. Hanya untuk proses update data pegawai
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_pegawai');
		$new_id			= $this->input->post('id_pegawai');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->Pegawai_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "Pegawai dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END Pegawai Class

/* End of file pegawai.php */
/* Location: ./system/application/controllers/pegawai.php */
