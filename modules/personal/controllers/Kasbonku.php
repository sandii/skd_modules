<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Kasbonku extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'kasbon';

  
  function kasbonku()
	{
		parent::__construct();
		$this->load->model('sdm/kasbon_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);

	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
	  $this->cekker->cek();


	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun = "")
	{
		
		
		$id_pegawai=$_SESSION["id_user"];
		
		
	
		$data = $this->data;
		$data['h2_title'] = "kasbon";
    
		
if (empty($tahun))
	$tahun=date("Y");


    $mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor('personal/kasbonku/index/'.$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</li>";
    }

		
		$data['pagination'] = $pagination;		

		

		
		// Load data
		$query = $this->kasbon_model->get_all($id_pegawai,$tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		




		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','keterangan', 'kasbon', 'pembayaran', 'saldo');
	
			
			foreach ($siswa as $row)
			{
				$this->table->add_row( $row->tanggal,  $row->keterangan, $row->pemasukan,$row->pengeluaran,$row->saldo
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data kasbon!';
		}		
		
		
		// Load view
		$this->load->view('template', $data);
	}




	
	/**
	 * Proses tambah data siswa
	 */
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	
	
	// cek apakah valid untuk update?
	
}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
