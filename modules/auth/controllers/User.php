<?php
/**
 * User Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class User extends CI_Controller  {
	/**
	 * Constructor
	 */
	var $title = 'user';


	function User()
	{
		parent::__construct();
		$this->load->model('User_model', '', TRUE);
			$this->load->model('sdm/Jabatan_model', '', TRUE);


			
			


	
	
	
  $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
    $this->data['status_admin']=$this->session->userdata('status_admin');

    $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
    
    


  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman user,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		
		
	}
	
	/**
	 * Tampilkan semua data user
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;

		
		// Load data
		$query = $this->User_model->get_all();
		$user = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Username','Nama','kode','Jabatan', 'Actions');
			$i = 0;
			
			foreach ($user as $row)
			{
			
			
			
			$xx=anchor('auth/user/update/'.$row->id_pegawai,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'));
			if($row->tgl_masuk==null)
			$xx.=" ".anchor('auth/user/delete/'.$row->id_pegawai,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"));
				
			
				$this->table->add_row(++$i,  $row->username,  $row->nama,$row->kode, $row->jabatan, $xx
										
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data user!';
		}		
		
		$data['link'] = array('link_add' => anchor('auth/user/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
function delete($id_user)
	{
		$this->User_model->delete($id_user);
		$this->session->set_flashdata('message', '1 data user berhasil dihapus');
		
		redirect('auth/user');
	}	
	
	
	
	/**
	 * Pindah ke halaman tambah user
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'user_form';
		$data['form_action']	= site_url('auth/user/add_process');
		$data['link'] 			= array('link_back' => anchor('auth/user','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
    $jabatan = $this->Jabatan_model->get_jabatan()->result();
  	foreach($jabatan as $row)
			{
  
      $data['option_jabatan'][$row->nama] = $row->nama;
 		}
  	
  	
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data user
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'user_form';
		$data['form_action']	= site_url('auth/user/add_process');
		$data['link'] 			= array('link_back' => anchor('auth/user','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// Set validation rules

		$this->form_validation->set_rules('username', 'Username', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
		
		

		$passwordx=md5($this->input->post('password'));  	
		
			// Persiapan data
			$user = array('id_pegawai'	=> $this->input->post('id_user'),
							'username'		=> $this->input->post('username'),
						'nama'		=> $this->input->post('nama'),
						'password'		=> $passwordx,
						'jabatan'		=> $this->input->post('jabatan')
						);
			// Proses penyimpanan data di table user
			$this->User_model->add($user);
			
			$this->session->set_flashdata('message', 'Satu data user berhasil disimpan!');
			redirect('auth/user/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update user
	 */
	function update($id_user)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'user_form';
		$data['form_action']	= site_url('auth/user/update_process/'.$id_user);
		$data['link'] 			= array('link_back' => anchor('auth/user','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$user = $this->User_model->get_user_by_id($id_user);
				
	
		// Data untuk mengisi field2 form
		$data['default']['id_user'] 	= $user->id_pegawai;		
		$data['default']['username']		= $user->username;
		$data['default']['nama']		= $user->nama;

		$data['default']['jabatan']		= $user->jabatan;
	
	$jabatan = $this->Jabatan_model->get_jabatan()->result();
  	foreach($jabatan as $row)
			{
  
      $data['option_jabatan'][$row->nama] = $row->nama;
 		}
  
  
  
      		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data user
	 */
	function update_process($id_user)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'user_form';
		$data['form_action']	= site_url('auth/user/update_process');
		$data['link'] 			= array('link_back' => anchor('auth/user','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
			$this->form_validation->set_rules('username', 'Username', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			
			
			$user = array(
			'jabatan'		=> $this->input->post('jabatan'),
			'nama'=> $this->input->post('nama'),
				
							'username'		=> $this->input->post('username')				);
						
			
    if($this->input->post('password'))
		$user['password']=md5($this->input->post('password'));  

						
						
			$this->User_model->update($id_user, $user);
			
			$this->session->set_flashdata('message', 'Satu data user berhasil diupdate!');
			redirect('auth/user');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}


	function gantipass()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Ganti Password';
		$data['custom_view'] 		= 'gantipass_form';
		$data['form_action']	= site_url('auth/user/gantipass_process');

	    		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data user
	 */
	

	
	/**
	 * Cek apakah $id_user valid, agar tidak ganda
	 */
	function valid_id($id_user)
	{
		if ($this->User_model->valid_id($id_user) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "user dengan Kode $id_user sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_user valid, agar tidak ganda. Hanya untuk proses update data user
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_user');
		$new_id			= $this->input->post('id_user');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->User_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "User dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END User Class

/* End of file user.php */
/* Location: ./system/application/controllers/user.php */
