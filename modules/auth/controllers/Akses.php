<?php
/**
 * User Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Akses extends CI_Controller  {
	/**
	 * Constructor
	 */
	var $title = 'akses';

var $alamat = 'auth/akses';

	function Akses()
	{
		parent::__construct();
		$this->load->model('auth/User_model', '', TRUE);
			$this->load->model('sdm/Jabatan_model', '', TRUE);
			$this->load->model('Akses_model', '', TRUE);
	
  $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
 

 $this->load->helper('fungsi');
       $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
 
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman user,
	 * jika tidak akan meredirect ke halaman login
	 */
	
	
	/**
	 * Tampilkan semua data user
	 */
	function index($ada_menu=false)
	{



		$data = $this->data;
		$data['h2_title'] = $this->title;




		$data["navs"]='
        <li '.($ada_menu?"":"class=active").'>'.
	anchor($this->alamat,'tanpa menu').'
        </li>
        <li  '.($ada_menu?"class=active":"").'>'.anchor($this->alamat.'/index/true','dengan menu').'</li>
     ';




$data_nav=array(
		 array("nama"=>'',"judul"=>"tanpa menu","link"=>$this->alamat),
		 array("nama"=>"ada_menu","judul"=>"dengan menu","link"=>$this->alamat.'/index/true'),
);

$data['navbar']=bikin_nav($data_nav,$ada_menu);




		$form_action	= site_url('auth/akses/update_akses_process');	
		// Load data
	

			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			
      
	$query2 = $this->Jabatan_model->get_jabatan();
		$jabatan = $query2->result();
	   
      $jabatanx['nama']=array();
       $jabatanx['id']=array();



			foreach ($jabatan as $row2)
			{ 
	if($row2->nama!='admin')			
      array_push($jabatanx['nama'],$row2->nama);     
      }

      
      $xx=array('halaman');
      
      $menu=array_merge($xx,$jabatanx['nama']);
      array_push($menu,"action");
      $this->table->set_heading($menu);
	

if($ada_menu)
	$where="id_menu_utama !=0";
else
	$where="id_menu_utama = 0";




	$query = $this->Akses_model->get_halaman($where);
	$user = $query->result();
		
$kategori="xxx";
		
			foreach ($user as $row)
			{
					


if($kategori!=$row->id_menu_utama)
{


if($row->id_menu_utama)
{	$nama_menu=$this->Akses_model->get_menu_utama_by_id($row->id_menu_utama)->nama;
	$tampilan='<h3>'.$nama_menu.'</h3>';


}
else
	$tampilan='';



$this->table->add_row($tampilan);

$kategori=$row->id_menu_utama;
}

			$hasil=array();
		



			foreach($jabatanx['nama'] as $yy)
			{

if($row->semua==null or $row->semua==0)
{

	$akses=$this->Akses_model->get_akses($yy,$row->id);
  $status=" ";
   if($akses>0)
 $status="checked";
  				
			array_push($hasil,"<input type=checkbox name=cek_akses[$yy] value=1 $status>");

}
else
			array_push($hasil,"ok");

			}


if($row->id_menu_utama)
$judulx=$row->judul;
else
$judulx=$row->nama;


$zz=array('<form name="akses_form" method="post" action="'.$form_action.'"><input type=hidden name=controller value='.$row->id.'>'
	.  $judulx);
$tampilan=array_merge($zz,$hasil);

array_push($tampilan,'<input type="submit" name="submit" id="submit" value=" update " class="btn btn-info btn-xs" /></form> '.

										anchor('auth/akses/update/'.$row->id,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor('auth/akses/delete/'.$row->id,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")));


				$this->table->add_row($tampilan);
				
				
			}
		
			$data['table'] = $this->table->generate();
		
		
		$data['link'] = array('link_add' => anchor('auth/akses/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	
	
	/**
	 * Proses update data user
	 */
	function update_akses_process()
	{

										
		// Set validation rules
		
		
	
			// save data
			
			$this->Akses_model->delete_all($this->input->post('controller'));
			
			$cek_akses=$this->input->post('cek_akses');
		
			
		
		
    foreach($cek_akses as $id_jabatan=>$jabatan)
			{
		
    	
			$user = array('jabatan'=>$id_jabatan,'halaman'=>$this->input->post('controller'));
			
			$this->Akses_model->add($user);
      }
      
      //'id_jabatan'		=> $this->input->post('jabatan'),
				//			'username'		=> $this->input->post('username')				);
						
			
   
						
						
			//$this->User_model->update($this->session->userdata('id_user'), $user);
			
			$this->session->set_flashdata('message', 'data berhasil diupdate!');
			redirect('auth/akses');
	
	}

	
function delete($id_user)
	{


			$this->Akses_model->delete_all($id_user);
		$this->Akses_model->delete_halaman($id_user);
		$this->session->set_flashdata('message', '1 data akses berhasil dihapus');
		
		redirect('auth/akses');
	}
	
	
	
	/**
	 * Pindah ke halaman tambah user
	 */
	function add()
	{	


		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'akses_form';
		$data['form_action']	= site_url('auth/akses/add_process');
		$data['link'] 			= array('link_back' => anchor('auth/akses','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
    $hasil= $this->Akses_model->get_menu_utama();
$data["options_kategori"][0]="tanpa menu";
foreach ($hasil as $row)
$data["options_kategori"][$row->id_menu_utama]=$row->nama;
  	
  	
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data user
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'akses_form';
		$data['form_action']	= site_url('auth/akses/add_process');
		$data['link'] 			= array('link_back' => anchor('auth/akses','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
	
    $hasil= $this->Akses_model->get_menu_utama();
$data["options_kategori"][0]="tanpa menu";
foreach ($hasil as $row)
$data["options_kategori"][$row->id_menu_utama]=$row->nama;

		// Set validation rules

		$this->form_validation->set_rules('nama', 'nama', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$user = array(
							
						'nama'		=> $this->input->post('nama'),
						'id_menu_utama'		=> $this->input->post('kategori'),
						'judul'		=> $this->input->post('judul'),
						'alamat'		=> $this->input->post('alamat'),
						'semua'		=> $this->input->post('semua'),


											);
			// Proses penyimpanan data di table user
			$this->Akses_model->add_halaman($user);
			
			$this->session->set_flashdata('message', 'Satu halaman akses berhasil disimpan!');
			redirect('auth/akses/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update user
	 */
	function update($id_halaman)
	{


		$data 			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'akses_form';
		$data['form_action']	= site_url('auth/akses/update_process');
		$data['link'] 			= array('link_back' => anchor('auth/akses','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$halaman = $this->Akses_model->get_halaman_by_id($id_halaman);
				
		// buat session untuk menyimpan data primary key (id_user)
		$this->session->set_userdata('id_halaman', $halaman->id);
		
		// Data untuk mengisi field2 form
		$data['default']['id_halaman'] 	= $halaman->id;		
		
		$data['default']['nama']		= $halaman->nama;
		$data['default']['alamat']		= $halaman->alamat;
		$data['default']['judul']		= $halaman->judul;
		$data['default']['semua']		= $halaman->semua;
		$data['default']['kategori']		= $halaman->id_menu_utama;
		
	
  
  
   $hasil= $this->Akses_model->get_menu_utama();
$data["options_kategori"][0]="tanpa menu";
foreach ($hasil as $row)
$data["options_kategori"][$row->id_menu_utama]=$row->nama;


      		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data user
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'akses_form';
		$data['form_action']	= site_url('auth/akses/update_process');
		$data['link'] 			= array('link_back' => anchor('auth/akses','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	



		// Set validation rules
			$this->form_validation->set_rules('nama', 'nama', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			
			
			$user = array('nama'=> $this->input->post('nama'),

'id_menu_utama'=> $this->input->post('kategori'),
'judul'=> $this->input->post('judul'),
'alamat'=> $this->input->post('alamat'),
'semua'=> $this->input->post('semua')


				);
						
			

						
						
			$this->Akses_model->update_halaman($this->session->userdata('id_halaman'), $user);
			
			$this->session->set_flashdata('message', 'Satu data user berhasil diupdate!');
			redirect('auth/akses');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}

	
	
	
	
	

}
