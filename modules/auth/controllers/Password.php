<?php
/**
 * Semester Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Password extends  CI_Controller {
	/**
	 * Constructor, load Semester_model
	 */

	var $title = 'ganti password';

	function Password()
	{
	
  	parent::__construct();
		$this->load->model('Login_model', '', TRUE);
		$this->load->model('auth/User_model', '', TRUE);
		
		
				$this->data['user_id']=$this->session->userdata('user_id');
		$this->data['nama']=$this->session->userdata('nama');
				$this->data['username']=$this->session->userdata('username');
   
    $this->data['title']=$this->title;
		
		
	
	      $this->load->library('cekker');
 //$this->cekker->cek($this->router->fetch_class());	
		
	}
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */

	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menjalankan fungsi get_semester()
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
	
			$this->ganti_password();
	
	}
		
	/**
	 * Mengambil data dari database, menampilkan data dalam tabel
	 */
	function ganti_password($offset = 0) {
	
	$data=$this->data;
	
		$data['custom_view'] = 'gantipass_form';		
		$data['form_action']	= site_url('auth/password/update_process');
		
		
		
	
		$this->load->view('template', $data);
	}
	
	/**
	 * Mengaktifkan sebuah semester
	 */	
function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update password';
		$data['custom_view'] 		= 'user/gantipass_form';
		$data['form_action']	= site_url('auth/password/update_process');
	

$xx = md5($this->input->post('password_lama'));
if ($this->Login_model->check_user($_SESSION['username'],$xx) == false)
{	
	$this->session->set_flashdata('message', 'password lama anda salah');
$this->load->view('template', $data);
}
else
{


$this->form_validation->set_rules('password_baru', 'password baru', 'required');
$this->form_validation->set_rules('password_baru2', 'confirm password baru', 'required|matches[password_baru]');
			
			
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$xx=md5($this->input->post('password_baru'));
			$user = array(
			'password'=> $xx);
			$this->User_model->update(($_SESSION['id_user']), $user);
			
			$this->session->set_flashdata('message', 'password berhasil diganti');
			redirect('auth/password');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	}


}
// END Semester Class

/* End of file semester.php */
/* Location: ./system/application/controllers/semester.php */
