<?php
/**
 * Login Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Login extends CI_Controller {
	/**
	 * Constructor
	 */
	function login()
	{
		parent::__construct();
		$this->load->model('auth/Login_model', '', TRUE);
		$this->load->model('sistem/Variable_sistem_model', '', TRUE);
	
		$this->load->dbutil();

	  $this->load->helper('autorun');
	  

	}
	
	
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
  
		
		if (isset($_SESSION['jabatan']) and $_SESSION['perusahaan']=="pb" )
		{
		redirect('personal/whattodo');
		}
		else
		{
	




$data['perusahaan']=$this->Variable_sistem_model->get_variable_sistem_by_id("perusahaan")->isi;

			$this->load->view('template_login',$data);
		
		}
	}
	
	/**
	 * Memproses login
	 */ 
	function process_login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			
			
			if ($this->Login_model->check_user($username, $password) == TRUE)
			{
				
        $nama_login = $this->Login_model->get_nama($username);
    

        
     			
$perusahaan=$this->Variable_sistem_model->get_variable_sistem_by_id("perusahaan")->isi;

				$_SESSION['perusahaan']=$perusahaan;			
				$_SESSION['username']=$nama_login->username;
				//$_SESSION['bagian']="pb";
				$_SESSION['nama']=$nama_login->nama;
				$_SESSION['jabatan']=$nama_login->jabatan;
				$_SESSION['id_user']=$nama_login->id_pegawai;
      			$_SESSION['kode']=$nama_login->kode;
      
   
  
  
  
   jalankan();	
    
      
      //------------------------------------------
       
        
        
        
        
        
        
        
        
				redirect('personal/whattodo');
			}
			else
		
				$this->session->set_flashdata('message', 'Maaf, username dan atau password Anda salah');
			redirect('auth/login/index');
			}
		
		else
		{
			$this->load->view('auth/login_view');
		}
	}
	
	/**
	 * Memproses logout
	 */
	function process_logout()
	{  
	
	       //$this->session->unset_userdata('username');
        //$this->session->unset_userdata('user_id');
        //$this->session->unset_userdata('nama');
        //$this->session->unset_userdata('jabatan');
	

	session_destroy();
	//	$this->session->sess_destroy();
		redirect('auth/login', 'refresh');
	}


/*
function backup_db()
{

$prefs = array(
              

                'format'      => 'gzip',             // gzip, zip, txt
                                                    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );
$this->load->dbutil();
$backup =& $this->dbutil->backup($prefs); 
$this->load->helper('file');
$tgl=date("Y-m-d");
write_file('./backup/project '.$tgl.'.gzip', $backup);
$tglxx= $this->Tglbackup_model->update();
}

*/


}
// END Login Class

/* End of file login.php */
/* Location: ./system/application/controllers/login.php */
