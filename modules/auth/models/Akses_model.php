<?php
/**
 * User_model Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Akses_model extends CI_Model
{
    /**
     * Constructor
     */
    public function Akses_model()
    {
        parent::__construct();
        $this->db_service = $this->load->database('db_service', true);
    }

    // Inisialisasi nama tabel yang digunakan
    public $table = 'halaman_holding';
    public $table2 = 'akses';
    public $table3 = 'variable_sistem';

    /**
     * Mendapatkan semua data user, diurutkan berdasarkan id_user
     */

    public function get_variable_sistem_by_id($id_saran)
    {
        return $this->db->get_where($this->table3, array('nama' => $id_saran), 1)->row();
    }

    public function update_variable_sistem($id_user, $user)
    {
        $this->db->where('nama', $id_user);
        $this->db->update($this->table3, $user);
    }

    public function ambil_menu_semua($jabatan)
    {

        $this->db_service->distinct();
        $this->db_service->select(array('menu_utama_holding.id_menu_utama', 'menu_utama_holding.nama', 'halaman_holding.id',
            'halaman_holding.judul', 'halaman_holding.alamat', 'semua', 'urutan'));
        $this->db_service->from('halaman_holding,menu_utama_holding');

        $this->db_service->where(
            "halaman_holding.id_menu_utama!=0 and halaman_holding.id_menu_utama=menu_utama_holding.id_menu_utama"
        );
        $this->db_service->order_by('urutan, halaman_holding.id', 'asc');
        $querydbservice = $this->db_service->get()->result();

        $data = array();
        $data['menu'] = $querydbservice;
        $data['akses'] = null;

        if ($jabatan != 'admin') {
            $this->db->distinct();
            $this->db->select('halaman')->from('akses');
            $this->db->where("jabatan = '" . $jabatan . "'");
            $querydb = $this->db->get()->result();
            $data['akses'] = $querydb;
        }

        return $data;

    }

    public function get_halaman($wherex = null)
    {

        if ($wherex) {
            $this->db_service->where($wherex);
        }

        $this->db_service->order_by('id_menu_utama', 'asc');

        return $this->db_service->get($this->table);
    }

    public function get_menu_utama()
    {
        $this->db_service->order_by('id_menu_utama');
        return $this->db_service->get("menu_utama_holding")->result();
    }

    public function get_menu_utama_by_id($id_user)
    {
        return $this->db_service->get_where('menu_utama_holding', array('id_menu_utama' => $id_user), 1)->row();
    }

    /**
     * Mendapatkan data sebuah user
     */
    public function get_halaman_by_id($id_user)
    {
        return $this->db_service->get_where($this->table, array('id' => $id_user), 1)->row();
    }

    public function get_id_by_halaman($id_user)
    {
        return $this->db_service->get_where($this->table, array('nama' => $id_user), 1)->row();
    }

    public function get_akses($id_jabatan, $id_halaman)
    {
        return $this->db->get_where($this->table2, array('jabatan' => $id_jabatan, 'halaman' => $id_halaman), 1)->num_rows();

    }

    public function get_akses_who($id_halaman)
    {

        $this->db->from('jabatan, pegawai, akses');

        $this->db->where('akses.jabatan = jabatan.nama');
        $this->db->where('pegawai.jabatan = jabatan.nama');
        $this->db->where('halaman = "' . $id_halaman . '"');

        $this->db->where('tgl_keluar is  null');

        return $this->db->get();

    }

    public function get_akses_jabatan($jabatan)
    {

        $this->db->where('jabatan', $jabatan);
        return $this->db->get($this->table2);
    }

    /**
     * Menghapus sebuah data user
     */
    public function delete_all($controller)
    {
        $query = $this->db_service->get_where('halaman_holding', array('id' => $controller));
        foreach ($query->result() as $row) {
            $id = $row->id;
        }
        $this->db->delete($this->table2, array('halaman' => $id));
    }

    public function delete_halaman($id_user)
    {
        $this->db_service->delete($this->table, array('id' => $id_user));
    }

    /**
     * Tambah data user
     */
    public function add($akses)
    {
        $this->db->insert($this->table2, $akses);
    }

    public function add_halaman($akses)
    {
        $this->db_service->insert($this->table, $akses);
    }

    /**
     * Update data user
     */
    public function update_halaman($id_user, $user)
    {
        $this->db->where('id', $id_user);
        $this->db->update($this->table, $user);
    }

    /**
     * Validasi agar tidak ada userd dengan id ganda
     */

}
