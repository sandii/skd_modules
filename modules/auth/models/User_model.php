<?php
/**
 * User_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class User_model extends CI_Model {
	/**
	 * Constructor
	 */
	function User_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'pegawai';

	
	/**
	 * Mendapatkan semua data user, diurutkan berdasarkan id_user
	 */
	function get_user()
	{
		$this->db->order_by('id_pegawai');
		return $this->db->get('user');
	}
	
	
	
	/**
	 * Mendapatkan data sebuah user
	 */
	function get_user_by_id($id_user)
	{
		return $this->db->get_where($this->table, array('id_pegawai' => $id_user), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_pegawai');
		$this->db->where("tgl_keluar is NULL");

		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data user
	 */
	function delete($id_user)
	{
		$this->db->delete($this->table, array('id_pegawai' => $id_user));
	}
	
	/**
	 * Tambah data user
	 */
	function add($user)
	{
		$this->db->insert($this->table, $user);
	}
	
	/**
	 * Update data user
	 */
	function update($id_user, $user)
	{
		$this->db->where('id_pegawai', $id_user);
		$this->db->update($this->table, $user);
	}
	
	/**
	 * Validasi agar tidak ada userd dengan id ganda
	 */
	function valid_id($id_user)
	{
		$query = $this->db->get_where($this->table, array('id_pegawai' => $id_user));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
