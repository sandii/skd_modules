<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Login_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Login_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'user';
	var $table2 = 'pegawai';
	
	/**
	 * Cek tabel user, apakah ada user dengan username dan password tertentu
	 */
	function check_user($username, $password)
	{
	
	
		$this->db->where("tgl_keluar is NULL ");
		$query = $this->db->get_where($this->table2, array('username' => $username, 'password' => $password), 1, 0);
		
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
function get_nama($username)
	{
		return $this->db->get_where($this->table2, array('username' => $username))->row();
	}	
	
	
}
// END Login_model Class

/* End of file login_model.php */ 
/* Location: ./system/application/model/login_model.php */
