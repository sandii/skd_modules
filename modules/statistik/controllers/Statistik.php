<?php

class Statistik extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $limit = 10;
  var $title="statistik";

  
  function Statistik()
	{
		parent::__construct();
		$this->load->model('produksi/Order_model', '', TRUE);
		$this->load->model('produksi/Orderdetil_model', '', TRUE);
			$this->load->model('marketing/Calon_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		
// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
  
    $this->data['title']=$this->title;
 
  
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());    
  }

	function index()
	{
		// Hapus data session yang digunakan pada proses update data order
		$this->session->unset_userdata('id_order');
		$this->session->unset_userdata('tanggal');
			
			$this->omzet();	
	}
	
	/**
	 * Menampilkan 10 data order terkini
	 */
	function omzet($tahunan=0,$kons='',$tgl1="",$tgl2="")
	{
		$data=$this->data;
    $data['h2_title']=$this->title." omzet";

		$data['custom_view'] = 'statistik/main';
	

     if($this->input->post('cari'))
     {
     $kons=$this->input->post('kons');
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

     }
  
    if(empty($kons) or $kons=="semua") {$kons="semua";$cari_kons=""; }
    else $cari_kons=$kons;
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
    
    


if(!empty($kons) and $kons!="semua")
$data['nama_konsumen'] = $this->Konsumen_model->get_konsumen_by_id($kons)->perusahaan;

    
    $data["kons"]=$kons;
		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    	
		// Load data dari tabel order
		
		
		$cari_kons=  str_replace("-", "%",$cari_kons);
		$orders = $this->Order_model->statistik($tahunan,$cari_kons,$cari_tgl1,$cari_tgl2)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		if($tahunan==1)
		 $bulan=$order->tahun;
		
	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm/1000000;
			
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			
			$data["grafik"]=$grafik;
			$data["tahunan"]=$tahunan;
			
		// Load default view
		if($tahunan!=1)
$data['link'] =  array(anchor('statistik/omzet/1/','tahunan', array('class' => 'btn btn-lg btn-success')));
else		      
$data['link'] =array( anchor('statistik/','bulanan', array('class' => 'btn btn-lg btn-success')));
		      
		      
									
		$this->load->view('template', $data);
	}
	
	
	function baru($tahunan=0,$tgl1="",$tgl2="")
	{
		$data=$this->data;
    $data['h2_title']=$this->title. " konsumen baru";

		$data['custom_view'] = 'statistik/baru';
	

     if($this->input->post('cari'))
     {
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

     }
  
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
    
    
    
		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    	
		// Load data dari tabel order
		$orders = $this->Konsumen_model->baru($tahunan,$cari_tgl1,$cari_tgl2)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		if($tahunan==1)
		 $bulan=$order->tahun;
		
	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm;
			
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			
			$data["grafik"]=$grafik;
			$data["tahunan"]=$tahunan;
			
		// Load default view
		if($tahunan!=1)
$data['link'] =  array(anchor('statistik/baru/1/','tahunan', array('class' => 'btn btn-success btn-lg')));
else		      
$data['link'] = array(anchor('statistik/baru','bulanan', array('class' => 'btn btn-success btn-lg')));
		      
		      
									
		$this->load->view('template', $data);
	}	


function order($tahunan=0,$tgl1="",$tgl2="")
	{
		$data=$this->data;
    $data['h2_title']=$this->title." order vs produk";

		$data['custom_view'] = 'statistik/baru';
	

     if($this->input->post('cari'))
     {
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

     }
  
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
    
    
    
		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    	
		// Load data dari tabel order
		$orders = $this->Konsumen_model->baru($tahunan,$cari_tgl1,$cari_tgl2)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		if($tahunan==1)
		 $bulan=$order->tahun;
		
	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm;
			
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			
			$data["grafik"]=$grafik;
			$data["tahunan"]=$tahunan;
			
		// Load default view
		if($tahunan!=1)
$data['link'] =  array(anchor('statistik/baru/1/','tahunan', array('class' => 'btn btn-lg btn-success')));
else		      
$data['link'] = array(anchor('statistik/baru','bulanan', array('class' => 'btn btn-lg btn-success')));
		      
		      
									
		$this->load->view('template', $data);
	}	




	function marketing($tahunan=0,$tgl1="",$tgl2="")
	{
		$data=$this->data;
    $data['h2_title']=$this->title." order vs batal";

		$data['custom_view'] = 'statistik/baru';
	

     if($this->input->post('cari'))
     {
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

     }
  
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
    
    
    
		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    	
		// Load data dari tabel order
		
		
$status="order";
		$orders = $this->Calon_model->jadi_order($tahunan,$cari_tgl1,$cari_tgl2,$status)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		  if($tahunan==1)
		 $bulan=$order->tahun;
		
    	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm;
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			$data["grafik"]=$grafik;
			
			
    $status="batal";
		$orders = $this->Calon_model->jadi_order($tahunan,$cari_tgl1,$cari_tgl2,$status)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		  if($tahunan==1)
		 $bulan=$order->tahun;
		
    	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm;
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			$data["grafik2"]=$grafik;
			$data["label"]="order";
			$data["label2"]="batal";
			
			$data["tahunan"]=$tahunan;
			
		// Load default view
		if($tahunan!=1)
$data['link'] =  array(anchor('statistik/marketing/1/','tahunan', array('class' => 'btn btn-lg btn-success')));
else		      
$data['link'] = array(anchor('statistik/marketing','bulanan', array('class' => 'btn btn-lg btn-success')));
		      
		      
									
		$this->load->view('template', $data);
	}	
	
	
	
function batal($tahunan=0,$tgl1="",$tgl2="")
	{
		$data=$this->data;
    $data['h2_title']=$this->title;

		$data['custom_view'] = 'statistik/banyak';
	

     if($this->input->post('cari'))
     {
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

     }
  
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
    
    
    
		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    	
		// Load data dari tabel order
		
		
$data_status=array("tidak","mahal","rumit","waktu","kontak","gakjadi");
	
			$data["label"]=array("tidak menjawab dengan pasti","harga kemahalan","terlalu rumit, produksi blm bisa","waktu terlalu mepet","gak bisa dikontak","gak jadi dibikin");


	//$status="order";

$j=0;

foreach($data_status as $batal)
{
		$orders = $this->Calon_model->batal($tahunan,$cari_tgl1,$cari_tgl2,$batal)->result();
		$grafik=array();	$i=0;
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
			
	
		  if($tahunan==1)
		 $bulan=$order->tahun;
		
    	else
			 $bulan=mktime(0, 0, 0,  $order->tanggalm,1, $order->tahun)*1000;
			
			if($tahunan==1 and $i==0){
			$grafik[$i]["x"]=$bulan-1;	
			$grafik[$i]["y"]=0; $i++;}
			
				$grafik[$i]["x"]=$bulan;
				$grafik[$i]["y"]=$order->totalm;
				$i++;
			}
				if($tahunan==1){
			while($i<=10)
			{
			$grafik[$i]["x"]=$bulan++;	
			$grafik[$i]["y"]=0;
			
			$i++;
			}
			
			}
			
			$data["grafik"][$j]=$grafik;
			$j++;
	}		
			




			
			$data["tahunan"]=$tahunan;
			
		// Load default view
		if($tahunan!=1)
$data['link'] =  array(anchor('statistik/batal/1/','tahunan', array('class' => 'btn btn-success btn-lg')));
else		      
$data['link'] =  array(anchor('statistik/batal','bulanan', array('class' => 'btn btn-success btn-lg')));
		      
		      
									
		$this->load->view('template', $data);
	}		


}

