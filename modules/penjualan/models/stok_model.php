<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */                       
class Stok_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Stok_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'produk';
	var $table2 = 'produkstok';
	
	/**
	 * Mendapatkan semua data marketing, diurutkan berdasarkan id_marketing
	 */
	
	/**
	 * Mendapatkan data sebuah marketing
	 */
	function get_stok_by_id($id_marketing)
	{
	
			$this->db->select('*');
		$this->db->where('id_produk', $id_marketing);
	return $this->db->get($this->table)->row();
	}
	
	function get_all($id_stok,$limit=null, $offset=null)
	{
		$this->db->order_by('id_stokdetil','desc');
		$this->db->where('id_stok', $id_stok);	




if(!empty($limit))
		$this->db->limit($limit, $offset);
	

		return $this->db->get($this->table2);
	}

	function get_gaji($query)
	{
		$this->db->where('tgl_keluar is null');
		$this->db->where($query);
		$this->db->where("tgl_masuk !='0000-00-00'");
		return $this->db->get($this->table);
	}

	function get_terakhir($id_pegawai,$tanggal=null)
	{
			$this->db->select ('*');
		$this->db->from($this->table2);
				$this->db->where('id_stok', $id_pegawai);

				if($tanggal)
		
		$this->db->where("tanggal <='".$tanggal."'");


		$this->db->limit(1,0);



		$this->db->order_by('id_stokdetil', 'desc');	


$hasil=$this->db->get()->row();

 $saldo=0;
    if(!empty($hasil->saldo))
    $saldo=$hasil->saldo;
  	return $saldo;
	}


		function get_hpp_terakhir($id_pegawai,$tanggal=null)
	{
			$this->db->select ('*');
		$this->db->from($this->table2);
				$this->db->where('id_stok', $id_pegawai);

								if($tanggal)
				$this->db->where("tanggal <='".$tanggal."'");

		$this->db->limit(1,0);
		$this->db->order_by('id_stokdetil', 'desc');	


$hasil=$this->db->get()->row();

 $saldo=0;
    if(!empty($hasil->saldo))
    $saldo=$hasil->hpp;
  	return $saldo;
	}


function hitung_stok($tahun)
	{
		


		$this->db->select ('*');

		$hasil=$this->db->get($this->table)->result();


$itungan=0;

 $tanggal=$tahun."-12-31 23:59:59";
	
foreach ($hasil as $row)
			{
	$stok=$this->get_terakhir($row->id_produk,$tanggal);
if($stok>0)
{		

	$hpp=$this->get_hpp_terakhir($row->id_produk,$tanggal);
$itungan+=$hpp*$stok;

//echo $row->nama." stok: ".$stok."hpp:".$hpp."<br>";

}

	
}


		return $itungan;


	}



function hitung_opname($tahun)
	{
		
 $tanggal=$tahun."-12-31 23:59:59";


		$this->db->select('*');
		$this->db->from('produkstok');
	$itungan=0;	


		$this->db->where('kode','opn');
		$this->db->where("tanggal <='".$tanggal."'");

$hasil=$this->db->get()->result();

  foreach ($hasil as $row)
			{

$itungan+=($row->kurang-$row->tambah)*$row->hpp;
			}
		





		return $itungan;


	}





	
		function get_aktif()
	{
	
	
	
		$this->db->select('*');
	
		$this->db->order_by('id_stok');
		
		return $this->db->get($this->table);
	}
	
		function get_nonaktif()
	{
		$this->db->order_by('id_pegawai');
		$this->db->where('tgl_keluar is not NULL');
		
		return $this->db->get($this->table);
	}
	
	
	/**
	 * Menghapus sebuah data marketing
	 */
	function delete($id_marketing)
	{
		$this->db->delete($this->table, array('id_pegawai' => $id_marketing));
	}
	
	/**
	 * Tambah data marketing
	 */
	function add($marketing)
	{
		$this->db->insert($this->table, $marketing);
	}
	
	function add_detil($marketing)
	{
		$this->db->insert($this->table2, $marketing);
	}
	
	/**
	 * Update data marketing
	 */
	function update($id_marketing, $marketing)
	{
		$this->db->where('id_stok', $id_marketing);
		$this->db->update($this->table, $marketing);
	}
	
	/**
	 * Validasi agar tidak ada marketingd dengan id ganda
	 */

function history_opname($limit, $offset,$id_produk)
	{
		$this->db->select('*');
		$this->db->from('produkstok');
		


		$this->db->where('kode','opn');
		
     if(!empty($id_produk))
		$this->db->where('id_stok',$id_produk);
		//$this->db->where('orderx.id_order = orderdetil.id_order');
		//$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');
		
		if($limit!=0)
		$this->db->limit($limit, $offset);


   //  if(!empty($kons))
	//	$this->db->where('konsumen.id_konsumen',$kons);	

		$this->db->order_by('tanggal', 'desc');
		return $this->db->get();
	}


}
// END Siswa_model Class

/* End of file marketing_model.php */
/* Location: ./system/application/models/marketing_model.php */
