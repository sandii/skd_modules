<?php
/**
 * Order_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Orderdetil_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Orderdetil_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'orderdetil';
	

	function add($orderdetil)
	{
		$this->db->insert($this->table, $orderdetil);
	}

function get_all($id_order)
	{
		$this->db->order_by('id_orderdetil');
		$this->db->where('id_order', $id_order);
		return $this->db->get($this->table);
	}


function delete($id_orderdetil)
	{
		$this->db->delete($this->table, array('id_orderdetil' => $id_orderdetil));
	}
	
		function get_orderdetil_by_id($id_orderdetil)
	{
		return $this->db->get_where($this->table, array('id_orderdetil' => $id_orderdetil), 1)->row();
	}
	
function update($id_orderdetil, $orderdetil)
	{
		$this->db->where('id_orderdetil', $id_orderdetil);
		$this->db->update($this->table, $orderdetil);
	}	
	
	

	
	
	
	
	
	
	
	
	
}
