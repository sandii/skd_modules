<?php
/**
 * Menu_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Belanjastok_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Belanjastok_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'k_belanjastok';
		var $table2 = 'k_belanjastokdetil';
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */
	
  
function get_omzet_akun($id_kategori,$tgl1,$tgl2)
	{

$this->db->select ('sum(jumlah*k_belanjastokdetil.harga) as total');
$this->db->from('k_belanjastok,k_belanjastokdetil,k_bahanbeli');


		$this->db->where('k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok');
		$this->db->where('k_bahanbeli.akun',$id_kategori);
		$this->db->where('k_bahanbeli.id_bahanbeli = k_belanjastokdetil.id_barang');




	$this->db->where("tanggal >'".$tgl1."'");	
		

		if(!empty($tgl2))
		
 
	   $this->db->where("tanggal <'".$tgl2."'");	

	return $this->db->get()->row();
}

function get_omzet_barang($id_produk,$tgl1,$tgl2)
	{

$this->db->select ('sum(jumlah*harga) as total');
$this->db->from('k_belanjastok,k_belanjastokdetil');


		$this->db->where('k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok');
	
	$this->db->where('id_barang',$id_produk);	




	$this->db->where("tanggal >'".$tgl1."'");	
		

		if(!empty($tgl2))
	
 	
	   $this->db->where("tanggal <'".$tgl2."'");	



		return $this->db->get()->row();
	



	}




  	function get_all($limit, $offset,$tgl1,$tgl2=null)
	{

	  
 	$this->db->select('*');
		$this->db->from($this->table);	
			$this->db->limit($limit, $offset);	
			$this->db->where("tanggal >=",$tgl1);
			
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	
		
	
				$this->db->order_by('id_belanjastok', 'desc');	
		return $this->db->get(); 
  
 
  
  
  }

function hitung_omzet($tgl1,$tgl2)
	{
	

		$this->db->select ('sum(total) as total_belanja');

		$this->db->from('k_belanjastok');

		$this->db->where("tanggal >='".$tgl1."'");	
		
 	  
	   $this->db->where("tanggal <='".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}





  	function get_history($limit, $offset,$id_barang)
	{


$this->db->distinct();	
$this->db->select (array('tanggal','id_barang','id_supplier','harga','jumlah','keterangan'));
	$this->db->from("k_belanjastok,k_belanjastokdetil");	
	

			$this->db->limit($limit, $offset);	


		$this->db->where('id_barang', $id_barang);
		$this->db->where("k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok");
	


			$this->db->order_by('id_belanjastokdetil', 'desc');	
		return $this->db->get();


	}
	function count_history($id_barang)
	{
$this->db->distinct();	
$this->db->select (array('tanggal','id_barang','id_supplier','harga','jumlah'));


	$this->db->from("k_belanjastok,k_belanjastokdetil");	

	


		$this->db->where('id_barang', $id_barang);

		$this->db->where("k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok");


	
			$this->db->order_by('id_belanjastokdetil', 'desc');	
		return $this->db->get()->num_rows();

		
	}
	

	


		function arsip($limit, $offset,$tgl1="",$tgl2="",$sup="",$bar="")
	{
	


$this->db->distinct();	
$this->db->select (array('k_belanjastok.id_belanjastok','tanggal','tanggal_input','id_supplier','total','nota'));

		$this->db->from('k_belanjastok,k_belanjastokdetil');
		$this->db->limit($limit, $offset);
		$this->db->where('k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok');	

     if(!empty($sup))
		$this->db->where('id_supplier',$sup);	

  if(!empty($bar))
		$this->db->where('id_barang',$bar);	


    if(!empty($tgl1))
		$this->db->where("tanggal >='".$tgl1."'");	
    if(!empty($tgl2))
    {
 		$this->db->where("tanggal <='".$tgl2."'");	
}
		
		$this->db->order_by('k_belanjastok.tanggal', 'desc');

		return $this->db->get();
	}

	function count_arsip($tgl1="",$tgl2="",$sup="",$bar="")
	{
	


$this->db->distinct();	
$this->db->select (array('k_belanjastok.id_belanjastok','tanggal','tanggal_input','id_supplier','total'));

		$this->db->from('k_belanjastok,k_belanjastokdetil');

		$this->db->where('k_belanjastok.id_belanjastok = k_belanjastokdetil.id_belanjastok');	

     if(!empty($sup))
			$this->db->where('id_supplier',$sup);	

    if(!empty($tgl1))
		$this->db->where("tanggal >='".$tgl1."'");	
    if(!empty($tgl2))
    {
 		$this->db->where("tanggal <='".$tgl2."'");	
}

if(!empty($bar))
		$this->db->where('id_barang',$bar);	
		

		return $this->db->get()->num_rows();
	}

	



	
	 	function get_belanja_by_id($tahun)
	{
		$this->db->select('*');
		$this->db->from($this->table);	
				$this->db->where('id_belanjastok', $tahun);
	
	
		return $this->db->get()->row();
	}
	
	
	function count_all_num_rows($tgl1,$tgl2=null)
	{
		$this->db->select('*');
		$this->db->from($this->table);		
			$this->db->where("tanggal >=",$tgl1);
			
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	
	
		return $this->db->get()->num_rows();
	}	
	
	
	
	
  	function get_detil($tahun)
	{
		$this->db->select('*');
		$this->db->from($this->table2);	
		
		$this->db->where('id_belanjastok', $tahun);
	
		
		return $this->db->get();
	}




	
  
  
  
  	function count_all()
	{
		return $this->db->count_all($this->table);
	}

	
	
	/**
	 * Tampilkan 10 baris menu terkini, diurutkan berdasarkan tanggal (Descending)
	 */

	
	
	function get_menu_by_tanggal($selisih)
	{

		
		$this->db->select ('*');
		$this->db->from('menu');
		$this->db->limit($selisih);
		$this->db->order_by('tanggal,id_menu', 'asc');	
	
  	return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data menu
	 */
	function delete($id_menu)
	{

		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel menu
	 */
	function add_belanja($menu)
	{
		$this->db->insert($this->table, $menu);
	}

	function add($menu)
	{
		$this->db->insert($this->table2, $menu);
	}
	
	/**
	 * Dapatkan data menu dengan id_menu tertentu, untuk proses update
	 */
	
	function get_terakhir()
	{
			$this->db->select ('*');
		$this->db->from($this->table);
		$this->db->limit(1,0);
		$this->db->order_by('id_belanjastok', 'desc');	

  	return $this->db->get()->row();
		
		
	}
	
	
	
	
	
	
	function get_menu()
	{
		$this->db->select('*');
		return $this->db->get($this->table);
			
		
	}
	
	
	
	function get_last_id_menu()
	{
		$this->db->select('id_menu');
		$this->db->order_by('id_menu', 'desc');
		$this->db->limit(1,0);
		return $this->db->get($this->table);
	}
	
	/**
	 * Update data menusi
	 */
	function update($id_menu, $menu)
	{
		$this->db->where('id_menu', $id_menu);
		$this->db->update($this->table, $menu);
	}
	function update_comment($id_menu, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->komen;
		
		$komen.=$comment;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set komen='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	function update_pembayaran_log($id_menu, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set pembayaran_log='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
}
// END Menu_model Class

/* End of file menu_model.php */
/* Location: ./system/application/models/menu_model.php */
