<?php
/**
 * Order_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Order_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Order_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'orderx';
	
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */



	
		function arsip($limit, $offset,$kons="",$tgl1="",$tgl2="",$bar="")
	{
	


$this->db->distinct();	
$this->db->select (array('orderx.id_order','tanggal','pembayaran','perusahaan','total','diskon'));

		$this->db->from('orderx,konsumen,orderdetil');


if($limit!=0)
$this->db->limit($limit, $offset);


		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');	
		$this->db->where('orderx.id_order = orderdetil.id_order');	

     if(!empty($kons))
		$this->db->where('konsumen.id_konsumen',$kons);	
		   

		    if(!empty($bar))
		$this->db->where('barang = "'.$bar.'"');		
		
    if(!empty($tgl1))
		$this->db->where("tanggal >'".$tgl1."'");	
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("tanggal <'".$tgl2."'");	
}
		
		$this->db->order_by('orderx.tanggal', 'desc');

		return $this->db->get();
	}
	
function statistik($tahunan=0,$kons="",$tgl1="",$tgl2="")
	{
	

$this->db->select (array('orderx.id_order','month(tanggal)as tanggalm','year(tanggal)as tahun','sum(total) as totalm'));

		$this->db->from('orderx,konsumen');
		
		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');	

     if(!empty($kons))
		$this->db->where('perusahaan like '. "'%".$kons."%'");	
		    
		
    if(!empty($tgl1))
		$this->db->where("tanggal >'".$tgl1."'");	
    if(!empty($tgl2))
    {
    $tgl2.=" 23:59:59";
		$this->db->where("tanggal <'".$tgl2."'");	
}
		
		$this->db->order_by('orderx.tanggal', 'asc');
if($tahunan!=0)
		$this->db->group_by('year(tanggal)');
else
		$this->db->group_by('EXTRACT(YEAR_MONTH FROM tanggal)');

		return $this->db->get();
	}
	
	function hitung_omzet($tgl1,$tgl2)
	{
	

		$this->db->select ('sum(total-diskon) as total_omzet');

		$this->db->from('orderx');

		$this->db->where("tanggal >'".$tgl1."'");	
		
 	   $tgl2.=" 23:59:59";
	   $this->db->where("tanggal <'".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}
	
	function hitung_hpp($tgl1,$tgl2)
	{
	

		$this->db->select ('sum(total_hpp) as totalx');

		$this->db->from('orderx');

		$this->db->where("tanggal >'".$tgl1."'");	
		
 	   $tgl2.=" 23:59:59";
	   $this->db->where("tanggal <'".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}


	function hitung_piutang($tahun)
	{
	

		$this->db->select ('sum(total-pembayaran-diskon) as total_piutang');

		$this->db->from('orderx');

//$tgl1=$tahun."-1-1";


	//	$this->db->where("tanggal >'".$tgl1."'");	
		$this->db->where("total - pembayaran - diskon >0");	
		
 	   $tgl2=$tahun."-12-31 23:59:59";
	   $this->db->where("tanggal <'".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}
	







	
	
	function count_all_num_rows_pembayaran($where)
	{
		
		$this->db->select('*');
		$this->db->from('orderx,konsumen');
		$this->db->where($where);
		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');
		$this->db->order_by('tanggal desc');
		
		
		return $this->db->get();
		
				
	}
	
	
	
	





	
	
	/**
	 * Tampilkan 10 baris order terkini, diurutkan berdasarkan tanggal (Descending)
	 */
	function get_last_ten_order($status)
	{
		//$this->db->distinct();


		$this->db->select (array('pembayaran','id_ar','orderx.id_order','tanggal','keterangan','orderx.pengiriman','perusahaan','total','barang'));
		$this->db->from('orderx, konsumen,orderdetil');
		$this->db->where('orderdetil.status '.$status);
		$this->db->where('orderx.id_order = orderdetil.id_order');		
		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');	
		$this->db->order_by('orderx.tanggal', 'desc');

		return $this->db->get();
	}
	
	
	
	
	function history_produk($limit, $offset,$id_produk,$kons)
	{
		$this->db->select('*');
		$this->db->from('orderx,orderdetil,konsumen');
		
     if(!empty($id_produk))
		$this->db->where('barang',$id_produk);
		$this->db->where('orderx.id_order = orderdetil.id_order');
		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');
		
		if($limit!=0)
		$this->db->limit($limit, $offset);


     if(!empty($kons))
		$this->db->where('konsumen.id_konsumen',$kons);	

		$this->db->order_by('id_orderdetil', 'desc');
		return $this->db->get();
	}
	
	
	
	
	
	
	function get_last_ten_pembayaran($limit, $offset,$where)
	{
		$this->db->select('*');
		$this->db->from('orderx,konsumen');
		$this->db->where($where);
		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');
		
		$this->db->limit($limit, $offset);
		$this->db->order_by('id_order', 'desc');
		return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data order
	 */
	function delete($id_order)
	{
		$this->db->where('id_order', $id_order);
		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel order
	 */
	function add($order)
	{
		$this->db->insert($this->table, $order);
	}
	
	/**
	 * Dapatkan data order dengan id_order tertentu, untuk proses update
	 */
	function get_order_by_id($id_order)
	{
		$this->db->select('*');
		$this->db->where('id_order', $id_order);
		return $this->db->get($this->table);
		
	
		
		
	}

function get_komen_by_id($id_order)
	{
	
		$this->db->where('id_order', $id_order);
		return $this->db->get("komen");
		
	
		
		
	}



	
	function get_last_id_order()
	{
		$this->db->select('id_order');
		$this->db->order_by('id_order', 'desc');
		$this->db->limit(1,0 );
		return $this->db->get($this->table);
	}
	
	
	function get_orderdetil($id_orderdetil)
	{
		$this->db->select('*');
		$this->db->from('orderx,konsumen,orderdetil');

		$this->db->where('orderx.id_konsumen = konsumen.id_konsumen');
		$this->db->where('orderx.id_order = orderdetil.id_order');		
		$this->db->where('orderdetil.id_orderdetil',$id_orderdetil);

		return $this->db->get()->row();
	}
	
	
	
	/**
	 * Update data ordersi
	 */
	function update($id_order, $order)
	{
		$this->db->where('id_order', $id_order);
		$this->db->update($this->table, $order);
	}
	function update_comment($id_order, $comment)
	{



	$this->db->where('id_order', $id_order);
	$hasil=$this->db->get("komen");


if($hasil->num_rows()>0)
{
	$komen=$hasil->row()->komen;
$komen.=$comment;

$sql= " update `komen` set komen='".$komen."' where id_order =".$id_order;
	$this->db->query($sql);
	



}
else
{

$order=array("id_order"=>$id_order,"komen"=>$comment);
$this->db->insert("komen", $order);
}


	



		//$this->db->where('id_order', $id_order);
		//$
			
		
	}
	function update_pembayaran_log($id_order, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_order', $id_order);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_order', $id_order);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `orderx` set pembayaran_log='".$komen."' where id_order =".$id_order;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
	
	
	
	function konsumen_pertama($id_konsumen)
	{
  	$this->db->select('*');
		$this->db->from('orderx');
		$this->db->where('orderx.id_konsumen ='.$id_konsumen);
		$this->db->order_by('tanggal', 'asc');
		return $this->db->get();
  }


	function konsumen_terakhir($id_konsumen)
	{
  	$this->db->select_max('tanggal');
		$this->db->from('orderx');
		$this->db->where('orderx.id_konsumen ='.$id_konsumen);
		return $this->db->get();
  }	
  
  
		function omzet($id_konsumen)
	{
  	$this->db->select_sum('total');
		$this->db->from('orderx');
		$this->db->where('orderx.id_konsumen ='.$id_konsumen);
		return $this->db->get();
  }	
  
	
	
	
	
	
	
	
	
	
	
}
// END Order_model Class

/* End of file order_model.php */
/* Location: ./system/application/models/order_model.php */
