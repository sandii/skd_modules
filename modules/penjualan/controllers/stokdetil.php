<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Stokdetil extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 20;
	var $title = 'stok';

  
  function stokdetil()
	{
		parent::__construct();
		$this->load->model('Stok_model', '', TRUE);

	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_pegawai,$offset=0)
	{
		$konsumen = $this->Stok_model->get_stok_by_id($id_pegawai);
	
		$data = $this->data;
		$data['title'] = "stokdetil: ".$konsumen->nama;
    $data['main_view'] = 'main';
		

	

		// Load data
		$query = $this->Stok_model->get_all($id_pegawai);

		$siswa =$this->Stok_model->get_all($id_pegawai,$this->limit,$offset)->result();
		$num_rows = $query->num_rows();
		




		
		if ($num_rows > 0)
		{



				$uri_segment = 4;
      		$config['base_url'] = site_url('penjualan/stokdetil/index/'.$id_pegawai."/");
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();









			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','keterangan', 'penambahan', 'pengurangan', 'stok akhir');
	
			
			foreach ($siswa as $row)
			{

if($row->kode=='djl')
$keterangan        = anchor_popup('orderdetil/index/'.$row->id_detail,$row->keterangan, array('class' => 'link_tambah','width'=>800));
else
$keterangan=$row->keterangan;


				$this->table->add_row( $row->tanggal,  $keterangan, $row->tambah,$row->kurang,$row->saldo
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data stokdetil!';
		}		
		
		$data['link'] = array('link_add' => anchor('penjualan/stokdetil/add/'.$id_pegawai,'stok opname', array('class' => 'btn btn-info'))
								);
		
		// Load view
		$this->load->view('template2', $data);
	}




	function add($id_pegawai)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.' > Opname';
		$data['custom_view'] 		= 'stok_add';
		$data['form_action']	= site_url('penjualan/stokdetil/add_process/'.$id_pegawai);

										
	$data['link'] 			= array('link_back' => anchor('penjualan/stokdetil/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);

		
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'stokdetil > Tambah Data';
		$data['main_view'] 		= 'stok_add';
		$data['form_action']	= site_url('penjualan/stokdetil/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor('penjualan/stokdetil/index/'.$id_pegawai,'kembali', array('class' => 'back'))
										);
										
		// data kelas untuk dropdown menu
	
		// Set validation rules
		$this->form_validation->set_rules('keterangan', 'required');
    
    
    $saldo_terakhir = $this->Stok_model->get_terakhir($id_pegawai);
    $hpp_terakhir = $this->Stok_model->get_hpp_terakhir($id_pegawai);
   
   $saldo=0;
    if(!empty($saldo_terakhir))
    $saldo=$saldo_terakhir;
		
		
		
		if ($this->form_validation->run() == TRUE)
		{
		
		
		$saldo=$saldo+$this->input->post('pemasukan')-$this->input->post('pengeluaran');
		
		


			// save data
			$uang = array('keterangan' 		=> $this->input->post('keterangan'),
	
			'id_stok' 		=> $id_pegawai,
							'tambah'		=> $this->input->post('pemasukan'),
							'kurang'	=> $this->input->post('pengeluaran'),
							'tanggal'	=> $this->input->post('tanggal'),
											'saldo'	=> $saldo,
											'kode' =>'opn',
											'hpp'=>$hpp_terakhir
						);
			$this->Stok_model->add_detil($uang);
			
			$this->session->set_flashdata('message', 'data stok berhasil disimpan!');
			redirect('penjualan/stokdetil/index/'.$id_pegawai);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */

}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
