<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Produk extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Produk';
  var $limit = '20';
	
  
  function Produk()
	{
		parent::__construct();
		$this->load->model('penjualan/Produk_model', '', TRUE);
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Stok_model', '', TRUE);
		$this->load->model('penjualan/Belanjastok_model', '', TRUE);
		$this->load->model('keuangan/Supplier_model', '', TRUE);
		$this->load->model('keuangan/Satuan_model', '', TRUE);

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
  	$this->load->helper('fungsi');		

	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data bahanbeli
	 */
	function get_all($tampilan="full",$tgl1="",$tgl2="")
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['main_view'] = 'main_tanggal';
		
	




 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


/**


*/




		// Load data
		
		//$query = $this->Produk_model->get_omzet_kategori($tgl1,$cari_tgl2);
		
		$query = $this->Produk_model->get_kategori();

		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();

		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

if(cek_auth("auth_order"))
$omzet="omzet";else $omzet="";


			$this->table->set_heading('No', 'nama', $omzet);
			$i = 0;
		$totaly=0;

			foreach ($bahanbeli as $row)
			{


if(cek_auth("auth_order"))
{$totalx = $this->Produk_model->get_omzet_kategori($row->id_kategori,$tgl1,$cari_tgl2)->total;
$totalz=  "<div align=right>". number_format($totalx, 0, ',', '.');
$totaly+=$totalx;
}
else
$totalz="";



				$this->table->add_row(++$i,  
        
        anchor('penjualan/produk/detail/'.$tampilan."/".$row->id_kategori."/".$tgl1."/".$tgl2,$row->nama, array('class' => 'detail')),
    
      $totalz
        	);


			}

if(cek_auth("auth_order"))
			$this->table->add_row("",  
        
       "<div align=right><h5>total",
    
        "<div align=right>". number_format($totaly, 0, ',', '.')
        	);

			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
	
		
	if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
	
	


	
		function detail($tampilan="full",$id_akundetil,$tgl1="",$tgl2="")
	{
		$data = $this->data;
	
  
  /**



  */

		$bahanbeli = $this->Produk_model->get_kategori_by_id($id_akundetil);  
  	$data['h2_title'] = anchor('penjualan/produk/get_all/'.$tampilan."/".$tgl1."/".$tgl2,'produk').	
    
    " > ". $bahanbeli->nama;  ;
		$data['custom_view'] = 'main_tanggal';


if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


		
		// Load data
		$query = $this->Produk_model->get_all($id_akundetil);
		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			

if(cek_auth("auth_order"))
{$xx="omzet";
$yy="action";

}
else
{
$xx="";
$yy="";


}

			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

			$this->table->set_heading('No', 'nama', 'satuan','stok','stok minimal','hpp','harga jual',$xx,$yy);
			$i = 0;
			
	$totaly=0;
			foreach ($bahanbeli as $row)
			{


	//	$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
		

if(cek_auth("auth_order"))
{
$edit=   anchor('penjualan/produk/update/'.$id_akundetil."/".$row->id_produk,'edit',array('class' => 'update'));
$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
$totaly+=$totalx;
$totalz= "<div align=right>". number_format($totalx, 0, ',', '.');
}
else
{
$edit="";$totalz="";
}


  $saldo_terakhir = $this->Stok_model->get_terakhir($row->id_produk);

				$this->table->add_row(++$i,  
        
      
        anchor_popup('penjualan/produk/history/'.$row->id_produk,$row->nama, array('class' => 'detail'))	, $row->satuan,
        anchor_popup('penjualan/stokdetil/index/'.$row->id_produk,$saldo_terakhir, array('class' => 'detail'))	,
$row->minimal,$row->hpp,$row->jual,
           $totalz, $edit

     );	
        
 
			}

if(cek_auth("auth_order"))

$this->table->add_row("","<h5 align=right>total", "<div align=right>". number_format($totaly, 0, ',', '.'));



			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
if(cek_auth("auth_order"))

	
		
		$data['link'] = array('link_add' => anchor('penjualan/produk/add/'.$id_akundetil,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
);

		// Load view
		if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
		

		function kurang()
	{
		$data = $this->data;
	
  


  	$data['h2_title'] = "stok yang kurang";
		$data['main_view'] = 'main';

 
  
    

		
		// Load data
		$query = $this->Produk_model->get_all_barang();
		


		// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

			$this->table->set_heading('No', 'nama','stok','stok minimal','hpp','harga jual');



$i=0;
foreach ($query as $row)
			{
	

$saldo_terakhir = $this->Stok_model->get_terakhir($row->id_produk);

if($saldo_terakhir<$row->minimal)

$this->table->add_row(++$i,  
        
      
        anchor_popup('penjualan/produk/history/'.$row->id_produk,$row->nama, array('class' => 'detail'))	,
        anchor_popup('penjualan/stokdetil/index/'.$row->id_produk,$saldo_terakhir, array('class' => 'detail'))	,
$row->minimal,$row->hpp,$row->jual

     );	


	}



			$data['table'] = $this->table->generate();

		$this->load->view('template', $data);
	}
		

	function delete($id_bahanbeli)
	{
		$this->Bahanbeli_model->delete($id_bahanbeli);
		$this->session->set_flashdata('message', '1 data bahanbeli berhasil dihapus');
		
		redirect('bahanbeli');
	}
	
	/**
	 * Pindah ke halaman tambah bahanbeli
	 */

/**


*/

	function add($id_akundetil)
	{		
		cek_auth("auth_marketing",1);
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Tambah Data';
		$data['custom_view'] 		= 'penjualan/produk_add';
		$data['form_action']	= site_url('penjualan/produk/add_process/'.$id_akundetil);
	
	$data['link'] 			= array('link_back' => anchor('penjualan/produk/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);


		 	$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}

		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data bahanbeli
	 */
	function add_process($id_akundetil)
	{
		cek_auth("auth_marketing",1);
	$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Tambah Data';

	$data['custom_view'] 		= 'penjualan/produk_add';
		$data['form_action']	= site_url('penjualan/produk/add_process/'.$id_akundetil);
	
	$data['link'] 			= array('link_back' => anchor('penjualan/produk/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	

 	$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}




		$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing'),
							'hpp'		=> $this->input->post('hpp'),
							'satuan'		=> $this->input->post('satuan'),
							'jual'		=> $this->input->post('harga'),
							'minimal'		=> $this->input->post('minimal'),

							
              	'id_kategori'		=>$id_akundetil								
						);
			// Proses penyimpanan data di table bahanbeli
			$this->Produk_model->add($bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil disimpan!');
			redirect('penjualan/produk/detail/full/'.$id_akundetil);
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update bahanbeli
	 */
	function update($id_akundetil,$id_bahanbeli)
	{
		cek_auth("auth_marketing",1);
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';

		$data['form_action']	= site_url('penjualan/produk/update_process/'.$id_akundetil."/".$id_bahanbeli);
	   	
		
	$data['custom_view'] 		= 'penjualan/produk_add';

	
	$data['link'] 			= array('link_back' => anchor('penjualan/produk/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	


		// cari data dari database
		$bahanbeli = $this->Produk_model->get_produk_by_id($id_bahanbeli);
				
		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;
		$data['default']['satuan']		= $bahanbeli->satuan;
		$data['default']['hpp']="asfd";
		$data['default']['harga']		= $bahanbeli->jual;
		$data['default']['minimal']		= $bahanbeli->minimal;


	$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}

				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bahanbeli
	 */
	function update_process($id_akundetil,$id_bahanbeli)
	{

cek_auth("auth_marketing",1);
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';
		$data['form_action']	= site_url('penjualan/produk/update_process/'.$id_akundetil."/".$id_bahanbeli);
	   	
		
	$data['custom_view'] 		= 'penjualan/produk_add';

	
	$data['link'] 			= array('link_back' => anchor('penjualan/produk/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
    	
		// cari data dari database
		$bahanbeli = $this->Produk_model->get_produk_by_id($id_bahanbeli);
				
	
$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}

		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;
$data['default']['hpp']="asfd";

		// Data untuk mengisi field2 form
		$data['default']['nama']		= $bahanbeli->nama;
				
		$data['default']['harga']		= $bahanbeli->jual;


	$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[50]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing'),
							'jual'		=> $this->input->post('harga'),

							'satuan'		=> $this->input->post('satuan'),
							'minimal'		=> $this->input->post('minimal')
				
							
						);
			$this->Produk_model->update($id_bahanbeli,$bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil diupdate!');
			redirect('penjualan/produk/detail/full/'.$id_akundetil);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
/**


*/

function history($id_barang,$offset = 0)
	{
		$data = $this->data;

		

		$bahanbeli2 = $this->Stok_model->get_stok_by_id($id_barang);


  
  	$data['h2_title'] = $bahanbeli2->nama ;
		$data['main_view'] = 'main';
		
		// Load data
		$query = $this->Belanjastok_model->get_history($this->limit,$offset,$id_barang);
		$bahanbeli = $query->result();
		
		$num_rows = $this->Belanjastok_model->count_history($id_barang);



		


		
		if ($num_rows > 0)
		{

		$uri_segment = 4;
      $config['base_url'] = site_url('bahanbeli/history/'.$id_barang.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
	
	

			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" id=tablex>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'keterangan','harga','jumlah beli','total','tanggal beli','supplier');
			$i = 0;
			
			foreach ($bahanbeli as $row)
			{
$nama_supplier=$this->Supplier_model->get_supplier_by_id($row->id_supplier)->nama;

				$this->table->add_row(++$i,   $row->keterangan,
       
 "<div align=right>". number_format($row->harga, 0, ',', '.'),
  "<div align=right>". number_format($row->jumlah, 0, ',', '.'),
    "<div align=right>". number_format($row->harga*$row->jumlah, 0, ',', '.')
       ,$row->tanggal,$nama_supplier);
        
 
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
	
		
		// Load view
		$this->load->view('template2', $data);
	}
		


	/**
	 * Cek apakah $id_bahanbeli valid, agar tidak ganda
	 */

}
// END Bahanbeli Class

/* End of file bahanbeli.php */
/* Location: ./system/application/controllers/bahanbeli.php */
