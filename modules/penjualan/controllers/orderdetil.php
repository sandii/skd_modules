<?php
/**
 * Orderdetil Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Orderdetil extends  CI_Controller {
	/**
	 * Constructor
	 */
	function Orderdetil()
	{
		parent::__construct();
		$this->load->model('penjualan/Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('penjualan/Orderdetil_model', '', TRUE);
		
		$this->load->model('Produk_model', '', TRUE);
		$this->load->model('penjualan/Stok_model', '', TRUE);

	$this->load->model('produksi/Pemroses_model', '', TRUE);
	
    $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
  
  	$this->load->helper('fungsi');		
		
  
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	var $title = 'Orderdetil';
		var $alamat = 'penjualan/orderdetil';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman orderdetil,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_order)
	{
		
			$this->get_all($id_order);
		
	}
	
	/**
	 * Tampilkan semua data orderdetil
	 */
	function get_all($id_order)
	{
		$data['title'] = $this->title;
		$data['h2_title'] = 'Order Detil';
		$data['custom_view'] = 'penjualan/orderdetil';
		$data['link_frame']=site_url('produksi/comment/index/'.$id_order);

		$data['form_action']	= site_url('penjualan/orderdetil/add_comment/'.$id_order);
  
  
  	// Load data
		$query = $this->Orderdetil_model->get_all($id_order);
		$orderdetil = $query->result();
		$num_rows = $query->num_rows();
		
	  	if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" width=100% class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( 'Nama Barang', 'Jumlah', 'Harga Satuan','Sub Total','status','barang sudah disiapkan?');
			
		

			
			
			
			$i = 0;			
			foreach ($orderdetil as $row)
			{
				
				
		$datastatusx="";
		$j=0;
		
				
	





	
		   
		  			
				$hargax=number_format($row->harga, 0, ',', '.');
				$jumlahx=number_format($row->jumlah, 0, ',', '.');
				
				
		
	
	
/**



*/



		if($row->diambil=="sudah")
{
	$data_status=array("proses","siap","finish");

if($row->status=="proses")
	$diambil=anchor('penjualan/orderdetil/diambil/belum/'.$row->id_orderdetil."/".$id_order,'<font color=#36ff00>sudah</font>', array('class' => 'link','onclick'=>"return confirm('Anda yakin akan mengembalikan barang ini?')"));	
else
	$diambil="<font color=#36ff00>sudah</font>";
}
		else
{	
	$data_status=array("po","proses","batal");
if($row->status=="proses")
	$diambil=anchor('penjualan/orderdetil/diambil/sudah/'.$row->id_orderdetil."/".$id_order,'<font color=#ff8181>belum</font>', array('class' => 'link','onclick'=>"return confirm('Anda yakin akan sudah menyediakan barang ini?')"));	
else
	$diambil='<font color=#ff8181>belum</font>';
}		
	


	while(!empty($data_status[$j]))
		   {
			
			
			$datastatusx .= "<option value=".site_url()."/penjualan/orderdetil/update_status/".$data_status[$j].'/'.$id_order.'/'.$row->id_orderdetil;
      
			if($data_status[$j]==$row->status)
		$datastatusx .= " selected ";
      
      $datastatusx .=">".$data_status[$j]."</option>";
			
				
			$j++;
		   }



 if(cek_auth("auth_order")) 
 {       
$statusx="
<form name='jump$i'>
<select name='myjumpbox'
OnChange='location.href=jump$i.myjumpbox.options[selectedIndex].value' class=form_field>
$datastatusx
</select>
</form>";



}
else
{
$statusx=$row->status."<br>";
}








	$nama_barang = $this->Produk_model->get_produk_by_id($row->barang)->nama;
	

if($row->status=="pending" or $row->status=="po")
		$edit=anchor('penjualan/orderdetil/update/'.$row->id_orderdetil.'/'.$id_order,$nama_barang,array('class' => 'update'));
else
$edit=$nama_barang;


$subtotal=number_format($row->jumlah*$row->harga, 0, ',', '.');

$i++;

				$this->table->add_row($edit ,$jumlahx,$hargax,$subtotal
								
             
 								
										, $statusx, $diambil
										
										);
			}
			$data['tablex'] = $this->table->generate();
			
			 $this->table->clear();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data order!';
		}		
		
	if(cek_auth("auth_marketing"))
		$data['link'] =  array(anchor($this->alamat.'/add/'.$id_order,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));




			 $data['link']['link_print']         = anchor_popup('penjualan/invoice/index/'.$id_order,'<span class="glyphicon glyphicon-print  " aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'));



		 $data['link_pengiriman']         = anchor('penjualan/orderdetil/pengiriman/'.$id_order,'instruksi pengiriman');
		$data['link_pengiriman2']         = anchor('penjualan/orderdetil/pengiriman_hasil/'.$id_order,'hasil pengiriman');
						
		
		
		
		
		$tmpl2 = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  
              'row_alt_start'  => '<tr >',
							'row_alt_end'    => '</tr>',

						  );

			$this->table->set_template($tmpl2); 
  	
			$this->table->set_empty("&nbsp;");
	  $order = $this->Order_model->get_order_by_id($id_order)->row();
		$konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);	

			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

$xx=array("diambil"=>"diambil","diantar"=>"diantar ke alamat","jasa"=>"pakai jasa pengiriman");

$yy=array("disertakan"=>"disertakan dengan barang","terpisah"=>"dikirim terpisah","email"=>"diemail","tidak"=>"tidak pakai");



if($order->pengiriman)  	
$this->table->add_row('<b>pengiriman',$xx[$order->pengiriman]);

if($order->jasa)  	
$this->table->add_row('<b>jasa pengirim',$order->jasa);

if($order->ongkir)
$this->table->add_row('<b>ongkir',$order->ongkir);
if($order->invoice)
$this->table->add_row('<b>invoice',$yy[$order->invoice]);

if($order->jenis_pembayaran)
$this->table->add_row('<b>pembayaran',$order->jenis_pembayaran);


if($order->ket_kirim)  	
$this->table->add_row('<b>keterangan lainnya',$order->ket_kirim);



$this->table->add_row('',"");


$data['table3'] = $this->table->generate();


	
	
	$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
	
	
	
  $this->table->set_empty("&nbsp;");
$this->table->set_template($tmpl);



if($order->pengiriman=="diambil")
{
$judul1="yg menyerahkan";
$judul2="yg ngambil";
$judul3="surat jalan";
}
else if($order->pengiriman=="diantar")
{
$judul1="yg mengirim";
$judul2="yg menerima";
$judul3="surat jalan";
}
else if($order->pengiriman=="jasa")
{
$judul1="yg mengirim";
$judul2="perkiraan sampai (hari)";
$judul3="resi";
}
else
{
$judul1="";
$judul2="";
$judul3="";

}




if($order->penerima)
$this->table->add_row('<b>'.$judul1,$order->penerima);
if($order->waktu)
$this->table->add_row('<b>'.$judul2,$order->waktu);
if($order->resi)
$this->table->add_row('<b>no '.$judul3,$order->resi);

if($order->koli)
$this->table->add_row('<b>jumlah koli',$order->koli);
if($order->tanggal_kirim)
$this->table->add_row('<b>tanggal kirim',$order->tanggal_kirim);

$this->table->add_row('',"");


$data['table4'] = $this->table->generate();

	
			$totalx=number_format($order->total, 0, ',', '.');
			$this->table->add_row('<b>konsumen',anchor_popup("konsumen/konsumendetil/index/".$order->id_konsumen,$konsumen->perusahaan,array('class' => 'detail','width'=>1000)));
			$this->table->add_row('<b>Harga Total',$totalx);
		if(!empty($order->diskon))
			$this->table->add_row('<b>diskon',number_format($order->diskon, 0, ',', '.'));	
			$this->table->add_row('<b>pembayaran',number_format($order->pembayaran, 0, ',', '.'));	     
			
			//$this->table->add_row('hapus order ini');			    
			
      $data['table2'] = $this->table->generate();
			
 

 
 
     
 		// Load view
		$this->load->view('template_orderdetil', $data);
	}
		

	
	
	function add_comment($id_order)
	{       $tulisan=$this->input->post('insert_comment');
		if( !empty($tulisan)) 
		{ 

    $commentx="<font color=blue>".date("d M").": ";
    $commentx.=$_SESSION['username'].": </font>";
		
		$commentx.=$this->input->post('insert_comment')."<br>";		
		$this->Order_model->update_comment($id_order,$commentx);}
		//$this->session->set_flashdata('message', 'status order berhasil diupdate');		
		redirect('penjualan/orderdetil/index/'.$id_order);
	}
	
	




	function update_status($status_baru,$id_order,$id_orderdetil)
	{
	
	   cek_auth("auth_order",1);
		$statusx=array('status'=>$status_baru);
		
		$this->Orderdetil_model->update($id_orderdetil,$statusx);
		$this->session->set_flashdata('message', 'status order berhasil diupdate');
		
	 	$this->update_harga($id_order);
		
		
		
		
		
		redirect('penjualan/orderdetil/index/'.$id_order);
	}


/**



*/

function diambil($diambil,$id_orderdetil,$id_order)
	{
	
	cek_auth("auth_order",1);

	



		
	

	$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
		
	    
    $saldo_terakhir = $this->Stok_model->get_terakhir($orderdetil->barang);
   

    $hpp_terakhir = $this->Stok_model->get_hpp_terakhir($orderdetil->barang);


	

	$order = $this->Order_model->get_order_by_id($id_order)->row();

$id_konsumen=$order->id_konsumen;
$konsumen=$this->Konsumen_model->get_konsumen_by_id($id_konsumen)->perusahaan;


if($diambil=="sudah")
{ $keterangan="dibeli ". $konsumen;
	$kurang=$orderdetil->jumlah;
	$tambah=0;



$produk=$this->Produk_model->get_produk_by_id($orderdetil->barang);


	$orderdetilx = array(
			'hpp'	=> $produk->hpp
          	);
			$this->Orderdetil_model->update($id_orderdetil, $orderdetilx);










if($saldo_terakhir<$orderdetil->jumlah)
{


	$this->session->set_flashdata('message', 'stok barang kurang');
redirect('penjualan/orderdetil/index/'.$id_order);

}



}
else
{ $keterangan=$konsumen." ga jadi beli";

	$jumlah_ambil=$orderdetil->jumlah;


	$tambah=$orderdetil->jumlah;
	$kurang=0;

}




$statusx=array('diambil'=>$diambil);
		
		$this->Orderdetil_model->update($id_orderdetil,$statusx);


		$saldo_terakhir=$saldo_terakhir+$tambah-$kurang;
		
		
			// save data
			$uang = array('keterangan' 		=> $keterangan,
	'id_detail'=>$id_order,
	'kode'=>'djl',
			'id_stok' 		=> $orderdetil->barang,
							'kurang'		=> $kurang,
							'tambah'	=> $tambah,
							'tanggal'	=> date("Y-m-d"),
											'saldo'	=> $saldo_terakhir,

											'hpp'=>  $hpp_terakhir
						);
			$this->Stok_model->add_detil($uang);
	


			$this->session->set_flashdata('message', 'status order berhasil diupdate');
		

		
		redirect('penjualan/orderdetil/index/'.$id_order);
	}



	
	/**



	 * Pindah ke halaman tambah orderdetil
	 */
	function add($id_order,$baru=null)
	{		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > Tambah Order';
		$data['custom_view'] 		= 'penjualan/orderdetil_form';
		$data['form_action']	= site_url('penjualan/orderdetil/add_process/'.$id_order.'/'.$baru);
	
$data['link_produk'] 	=anchor_popup('penjualan/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
  

    if($baru)
   {
	$data['link'] = array('link_back' =>'<a href=javascript:window.close(); class="btn btn-success btn-lg"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></a>');


  
		$orderdetil = $this->Konsumen_model->get_konsumen_by_id($id_order);
				
	
		
		$data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['jasa']		= $orderdetil->jasa;

		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']		= $orderdetil->jenis_pembayaran;
 
  


$data['konsx']=$id_order;   
   
   } else
   {		

  
  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));

	$data['default']['pengiriman'] 	= "";	


$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;


}


     $data['boleh_edit']=true;
      
      





		$data['id_order'] 	= $id_order;			
		$data['baru'] 	= $baru;
    $this->load->view('template2', $data);
	}
	
	/**
	 * Proses tambah data orderdetil
	 */
	function add_process($id_order,$baru=null)
	{
	   
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > Tambah Order';
		$data['custom_view'] 		= 'penjualan/orderdetil_form';
		$data['form_action']	= site_url('penjualan/orderdetil/add_process/'.$id_order.'/'.$baru);
		$data['id_order'] 	= $id_order;	
		$data['baru'] 	= $baru;	
$data['link_produk'] 	=anchor_popup('penjualan/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
	
    
  	
         $data['boleh_edit']=true;
         
         
    if($baru)
    {

  	$data['link'] 			= array('link_back' => '<a href=javascript:window.close(); class=back><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></a>', array('class' => 'btn btn-success btn-lg','role'=> 'button'));
    
    	$orderdetil = $this->Konsumen_model->get_konsumen_by_id($id_order);
				
	
		
		$data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['jasa']		= $orderdetil->jasa;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']		= $orderdetil->jenis_pembayaran;
    
    $data['konsx']=$id_order;   
    }
    else		
		
  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));



		$data['id_order'] 	= $id_order;			
		$data['baru'] 	= $baru;

	$data['default']['pengiriman'] 	= "";	



$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;
	
		// Set validation rules

		$this->form_validation->set_rules('barang', 'barang', 'required');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
		

    if($baru)
    {

$id_terakhirxx = $this->Order_model->get_last_id_order()->row();
$id_terakhir = $id_terakhirxx->id_order;

$id_terakhir++; 

$order = array( 'id_order' => $id_terakhir,
'id_konsumen' =>$id_order,
'ket_kirim' =>$this->input->post('ket_kirim'),

  	'pengiriman' 	=> $this->input->post('pengiriman'),		
  	'ongkir' 	=> $this->input->post('ongkir'),			
		'jasa'		=> $this->input->post('jasa'),			
		'invoice'		=> $this->input->post('invoice'),		
		'jenis_pembayaran'	=> $this->input->post('jenis_pembayaran')		



			);  






$this->Order_model->add($order);


		$orderdetil2 = array('jasa'=> $this->input->post('jasa'),
			
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran') );

			$this->Konsumen_model->update($id_order, $orderdetil2);



   $id_order=$id_terakhir; 
    }
    
  
    
    	// Persiapan data
			$orderdetil = array('id_order'=> $id_order,
			'barang'		=> $this->input->post('barang'),
			'jumlah'		=> $this->input->post('jumlah'),
			'harga'		=> $this->input->post('harga')
		
			
				
						);
			// Proses penyimpanan data di table orderdetil
			$this->Orderdetil_model->add($orderdetil);
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'Satu data order berhasil disimpan!');
			redirect('penjualan/orderdetil/index/'.$id_order);
		}
		// Jika validasi gagal
		else
		{    	
			$this->load->view('template2', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update orderdetil
	 */
	function update($id_orderdetil,$id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > Update';
		$data['custom_view'] 		= 'penjualan/orderdetil_form';
		$data['form_action']	= site_url('penjualan/orderdetil/update_process/'.$id_orderdetil.'/'.$id_order);


  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


$data['link_produk'] 	=anchor_popup('penjualan/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
	
		// cari data dari database
		$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
				
	
		// Data untuk mengisi field2 form
		$data['default']['barang'] 	= $orderdetil->barang;		
		$data['default']['jumlah']		= $orderdetil->jumlah;
		
		$data['default']['harga']		= $orderdetil->harga;

			$data['default']['pengiriman'] 	= "";	


$data['default']['nama_barang'] = $this->Produk_model->get_produk_by_id($orderdetil->barang)->nama;
$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;


	
    $data['boleh_edit']		= cek_auth("auth_order");		
	      

 		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function update_process($id_orderdetil,$id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > Update';
		
		$data['custom_view'] 		= 'penjualan/orderdetil_form';
		$data['form_action']	= site_url('penjualan/orderdetil/update_process/'.$id_orderdetil.'/'.$id_order);


  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


$data['link_produk'] 	=anchor_popup('penjualan/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
										
		// Set validation rules


    $data['boleh_edit']		= cek_auth("auth_order");		


	$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
				
	
		// Data untuk mengisi field2 form
		$data['default']['barang'] 	= $orderdetil->barang;		
		$data['default']['jumlah']		= $orderdetil->jumlah;
	
		$data['default']['harga']		= $orderdetil->harga;
	
		$data['default']['pengiriman'] 	= "";	


$data['default']['nama_barang'] = $this->Produk_model->get_produk_by_id($orderdetil->barang)->nama;





$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;








		$this->form_validation->set_rules('barang', 'barang', 'required');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$orderdetil = array('barang'	=> $this->input->post('barang'),
			'jumlah'	=> $this->input->post('jumlah'),
			'harga'	=> $this->input->post('harga')
						
          
          
          	);
			$this->Orderdetil_model->update($id_orderdetil, $orderdetil);
			
			
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'Satu data orderdetil berhasil diupdate!');
			redirect('penjualan/orderdetil/index/'.$id_order);
		}
		else
		{		
			$this->load->view('template2', $data);
		}
	}
	
	function pengiriman($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'instruksi pengiriman';
		$data['custom_view'] 		= 'produksi/pengiriman';
		$data['form_action']	= site_url('penjualan/orderdetil/pengiriman_process/'.$id_order);
	
  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));

	
		// cari data dari database
		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();
				
	
		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;		
		$data['default']['ongkir']		= $orderdetil->ongkir;
		$data['default']['ket_kirim']		= $orderdetil->ket_kirim;
  	$data['default']['pengiriman']		= $orderdetil->pengiriman;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']	= $orderdetil->jenis_pembayaran;	      
 $data['boleh_edit']		= cek_auth("auth_order");		



 		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function pengiriman_process($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > pengiriman';
			$data['custom_view'] 		= 'produksi/pengiriman';
		$data['form_action']	= site_url('penjualan/orderdetil/pengiriman_process/'.$id_order);
	
  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
							
			
      
      
      
  		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();

		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;		
		$data['default']['ongkir']		= $orderdetil->ongkir;
		$data['default']['ket_kirim']		= $orderdetil->ket_kirim;
  	$data['default']['pengiriman']		= $orderdetil->pengiriman;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']	= $orderdetil->jenis_pembayaran;	      
    $data['boleh_edit']		= cek_auth("auth_order");		

    
      
      
      
      
      
      							
		// Set validation rules
		$this->form_validation->set_rules('jasa', 'jasa', 'required');




$jasa= $this->input->post('jasa');
$ongkir= $this->input->post('ongkir');
$jenis_pembayaran= $this->input->post('jenis_pembayaran');



if($this->input->post('pengiriman')=="diambil")
$jasa=$ongkir="";
else if($this->input->post('pengiriman')=="diantar")
$jasa=$ongkir="";
else if($this->input->post('pengiriman')=="jasa")
$jenis_pembayaran="";




			// save data
			$orderdetil = array('jasa'=> $jasa,		
			'ongkir'	=> $ongkir,		
			'ket_kirim'	=> $this->input->post('ket_kirim'),
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $jenis_pembayaran, 
          	);

       


			$this->Order_model->update($id_order, $orderdetil);
			
	
	$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();	
		// Data untuk mengisi field2 form
	$id_konsumen 	= $orderdetil->id_konsumen;		
  
  
  
  		$orderdetil = array('jasa'=> $this->input->post('jasa'),
			
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran'), 
          	);


			$this->Konsumen_model->update($id_konsumen, $orderdetil);
			

  		
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'data pengiriman berhasil diupdate!');
			redirect('penjualan/orderdetil/index/'.$id_order);
	
	}
	
	
	function pengiriman_hasil($id_order)
	{
	$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'hasil pengiriman';
		$data['custom_view'] 		= 'produksi/pengiriman_hasil';
		$data['form_action']	= site_url('penjualan/orderdetil/pengiriman_hasil_process/'.$id_order);
		
  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	





		// cari data dari database
		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();
				
				
$data['judul1']="";
$data['judul2']="";
$data['judul3']="";			
	if($orderdetil->pengiriman=="diambil")
{
$data['judul1']="yg menyerahkan";
$data['judul2']="yg ngambil";
$data['judul3']="surat jalan";
}
else if($orderdetil->pengiriman=="diantar")
{
$data['judul1']="yg mengirim";
$data['judul2']="yg menerima";
$data['judul3']="surat jalan";
}
else if($orderdetil->pengiriman=="jasa")
{
$data['judul1']="yg mengirim";
$data['judul2']="perkiraan sampai (hari)";
$data['judul3']="resi";
}


	
				
	
		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;	
    $data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['resi']		= $orderdetil->resi;
		$data['default']['waktu']		= $orderdetil->waktu;
		$data['default']['koli']		= $orderdetil->koli;
		$data['default']['penerima']		= $orderdetil->penerima;
		$data['default']['tanggal_kirim']	= $orderdetil->tanggal_kirim;
 		$this->load->view('template2', $data);
 		
 		
 	
   	
 		
 		
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function pengiriman_hasil_process($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'hasil pengiriman';
		$data['custom_view'] 		= 'produksi/pengiriman_hasil';
		$data['form_action']	= site_url('penjualan/pengiriman_hasil_process/'.$id_order);
		
  	$data['link'] 	= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	
								
										
		// Set validation rules
		$this->form_validation->set_rules('jasa', 'jasa', 'required');

		

			// save data
			$orderdetil = array(
      
      
 
			'resi'	=> $this->input->post('resi'),
			'penerima'	=> $this->input->post('penerima'),
			'waktu'	=> $this->input->post('waktu'),
		//	'ket_kirim'	=> $this->input->post('ket_kirim'),

			'koli'		=> $this->input->post('koli'),
         
      //'pengiriman'	=> $this->input->post('pengiriman'),
			//'invoice'	=> $this->input->post('invoice'),
			//'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran'), 
          	);


if( $this->input->post('tanggal_kirim'))
			$orderdetil['tanggal_kirim']= $this->input->post('tanggal_kirim');	          
else
$orderdetil['tanggal_kirim']=null;


			$this->Order_model->update($id_order, $orderdetil);
			
  		
	
			
			
			$this->session->set_flashdata('message', 'data pengiriman berhasil diupdate!');
			redirect('penjualan/orderdetil/index/'.$id_order);
	
	}	
	



	
function update_harga($id_order)
{
	
	$query = $this->Orderdetil_model->get_all($id_order);
		$orderdetil = $query->result();
		$num_rows = $query->num_rows();
		
		$harga=0;
		$harga_hpp=0;
			foreach ($orderdetil as $row)
			{
			
      if($row->status!='pending' and $row->status!='batal')	
			$harga+=$row->jumlah*$row->harga;	
			$harga_hpp+=$row->jumlah*$row->hpp;	
				
			}
			
$order = $this->Order_model->get_order_by_id($id_order)->row();
			
$harga+=$order->ongkir;			
				
$order=array('total'=>$harga,'total_hpp'=>$harga_hpp);
	$this->Order_model->update($id_order, $order);
}
	


function autocomplete()
{

	$query = $this->Produk_model->get_autocomplete($this->input->get('q'));
	$bahanbeli = $query->result();
	$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{

echo"[";
$i=0;
    foreach($bahanbeli as $row):
    if ($i!=0)
    echo",";   
        echo '{"name":"'.$row->nama.'","id":"'.$row->id_produk.'","harga":"'.$row->hpp.'"}';
  





  $i++;
    endforeach;    
echo"]";
} 

}


	

}