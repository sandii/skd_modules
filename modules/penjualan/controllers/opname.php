<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Opname extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Opname';
	
  
  function Opname()
	{
		parent::__construct();
		$this->load->model('penjualan/Produk_model', '', TRUE);
		$this->load->model('penjualan/Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('penjualan/Stok_model', '', TRUE);

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */

		

function index($bar="",$offset=0)
	{
		$data = $this->data;
	
  
  /**



  */


  	$data['h2_title'] = "Arsip opname";



		$data['custom_view'] = 'penjualan/opname';


$limit=20;


if($this->input->post('cari'))
     {

     $kons=$this->input->post('kons');
     $bar=$this->input->post('bar');

    }




    if(empty($bar) or $bar=="semua") 
{$cari_bar="";$bar="semua";}
else
$cari_bar=$bar;





if(!empty($bar) and $bar!="semua")
$nama_barang=$data['nama_barang'] = $this->Produk_model->get_produk_by_id($bar)->nama;





	
	$data["bar"]=$bar;



		
		// Load data
		$query = $this->Stok_model->history_opname($limit, $offset,$cari_bar);
		$bahanbeli = $query->result();
		$num_rows =  $this->Stok_model->history_opname(0,0,$cari_bar)->num_rows();
		
		if ($num_rows > 0)
		{


			$uri_segment = 5;
      		$config['base_url'] = site_url('penjualan/opname/index/'.$bar.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $limit;
			$config['uri_segment'] = $uri_segment;
	

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';








			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();



			
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'barang',  'bertambah','berkurang','keterangan');
			$i = 0;
			






			foreach ($bahanbeli as $row)
			{


	//	$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
	

if($bar=="semua")
$nama_barang=$data['nama_barang'] = $this->Produk_model->get_produk_by_id($row->id_stok)->nama;



$tanggal=date("Y-m-d",strtotime($row->tanggal));

				$this->table->add_row($tanggal, $nama_barang,  $row->tambah, 
        $row->kurang
      
       

,$row->keterangan

        );	
        
 
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
		

		$this->load->view('template', $data);
	}



}
