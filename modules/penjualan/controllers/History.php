<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class History extends  CI_Controller {
	/**
	 * Constructor
	 */
	
var $title = 'Produk';
				var $alamat = 'produksi/history';

  
  function History()
	{
		parent::__construct();
		$this->load->model('penjualan/Produk_model', '', TRUE);
		$this->load->model('penjualan/Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
  $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */

		

function index($tampilan="full",$bar="",$kons="",$offset=0)
	{
		$data = $this->data;
	
  
  /**



  */


  	$data['h2_title'] = "Arsip per produk";

		$data['custom_view'] = 'produksi/history';

$limit=20;


if($this->input->post('cari'))
     {

     $kons=$this->input->post('kons');
     $bar=$this->input->post('bar');

    }

//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
  
    if(empty($kons) or $kons=="semua") 
{$cari_kons="";$kons="semua";}
else
$cari_kons=$kons;


    if(empty($bar) or $bar=="semua") 
{$cari_bar="";$bar="semua";}
else
$cari_bar=$bar;




if(!empty($kons) and $kons!="semua")
$data['nama_konsumen'] = $this->Konsumen_model->get_konsumen_by_id($kons)->perusahaan;


if(!empty($bar) and $bar!="semua")
$nama_barang=$data['nama_barang'] = $this->Produk_model->get_produk_by_id($bar)->nama;




	$data["kons"]=$kons;
	
	$data["bar"]=$bar;



		
		// Load data
		$query = $this->Order_model->history_produk($limit, $offset,$cari_bar,$cari_kons);
		$bahanbeli = $query->result();
		$num_rows =  $this->Order_model->history_produk(0,0,$cari_bar,$cari_kons)->num_rows();
		
		if ($num_rows > 0)
		{


			$uri_segment = 7;
      		$config['base_url'] = site_url($this->alamat.'/index/'.$tampilan."/".$bar.'/'.$kons.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $limit;
					$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';


			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();



			
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'konsumen','barang', 'jumlah','harga','total','status');
			$i = 0;
			






			foreach ($bahanbeli as $row)
			{


	/*	$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
		
	$spek_lengkap="";
        if(!empty($row->warna))$spek_lengkap.="<b>warna:</b>$row->warna &nbsp";
        if(!empty($row->kertas))$spek_lengkap.="<b>bahan:</b>$row->kertas  &nbsp;";
        if(!empty($row->foil))$spek_lengkap.="<b>foil:</b>$row->foil  &nbsp;";
        if($row->emboss==1)$spek_lengkap.="<b>emboss</b> &nbsp; ";
        if(!empty($row->pond))$spek_lengkap.="<b>pond:</b>$row->pond  &nbsp;";
        if(!empty($row->laminasi))$spek_lengkap.="<b>laminasi:</b>$row->laminasi  &nbsp;";
        if(!empty($row->lipat))$spek_lengkap.="<b>lipat:</b>$row->lipat  &nbsp;";
        if(!empty($row->ukuran))$spek_lengkap.="<b>ukuran:</b>$row->ukuran  &nbsp;";
        if(!empty($row->jilid))$spek_lengkap.="<b>jilid:</b>$row->jilid &nbsp; ";
        if(!empty($row->cover))$spek_lengkap.="<b>cover:</b>$row->cover &nbsp; ";
        if(!empty($row->mesin))$spek_lengkap.="<b>mesin:</b>$row->mesin  &nbsp;";
        if(!empty($row->spesifikasi))$spek_lengkap.="lainnya:$row->spesifikasi &nbsp; ";
       

*/



if($bar=="semua")
$nama_barang=$data['nama_barang'] = $this->Produk_model->get_produk_by_id($row->barang)->nama;




$totalx=$row->jumlah*$row->harga;


$tanggal=date("Y-m-d",strtotime($row->tanggal));

				$this->table->add_row($tanggal,  $row->perusahaan, $nama_barang,
        
      
       

 "<div align=right>". number_format($row->jumlah, 0, ',', '.'),

        "<div align=right>". number_format($row->harga, 0, ',', '.')
     ,   "<div align=right>". number_format($totalx, 0, ',', '.')
,$row->status

        );	
        
 
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
		
		// Load view
	if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}






	
	/**
	 * Pindah ke halaman update bahanbeli
	 */
	
	
	/**
	 * Proses update data bahanbeli
	 */
	
	



	/**
	 * Cek apakah $id_bahanbeli valid, agar tidak ganda
	 */

}
// END Bahanbeli Class

/* End of file bahanbeli.php */
/* Location: ./system/application/controllers/bahanbeli.php */
