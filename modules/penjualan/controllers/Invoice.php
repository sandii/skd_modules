<?php
/**
 * Orderdetil Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Invoice extends  CI_Controller {
	/**
	 * Constructor
	 */
	function Invoice()
	{
		parent::__construct();
	//	$this->load->model('marketing/Marketing_model', '', TRUE);
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Orderdetil_model', '', TRUE);
		$this->load->model('Produk_model', '', TRUE);
	


        $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
			var $title = 'invoice';
			var $alamat = 'penjualan/invoice';

	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman orderdetil,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_order,$tafio=1,$ttdx=1)
	{
		
			$this->get_all($id_order,$tafio,$ttdx);
		
	}
	
	/**
	 * Tampilkan semua data orderdetil
	 */
	function get_all($id_order,$tafio,$ttdx)
	{
		$data['title'] = $this->title;
		//$data['h2_title'] = 'Order Detil';
		$data['custom_view'] = 'invoice';
		$data['link_frame']=site_url('comment/index/'.$id_order);
		$data['form_action']	= site_url('orderdetil/add_comment/'.$id_order);
  
  
  $nama=$_SESSION['nama'];
 $namax=$_SESSION['username'];

 



		



  if($ttdx==1) 
$data['ttd'] =  anchor($this->alamat.'/index/'.$id_order.'/'.$tafio.'/2',"Hormat kami <br><br><br><br><br>(&nbsp;&nbsp;".$nama."&nbsp;&nbsp;&nbsp;)", array('class' => 'link_ttd'));
else
$data['ttd'] =  anchor($this->alamat.'/index/'.$id_order.'/'.$tafio.'/1',



		       
"<div id=stempel>Hormat kami<br><br><br><br><br>(&nbsp;&nbsp;".$nama."&nbsp;&nbsp;&nbsp;)</div>

<div id=gambar_ttd>
<img src=".base_url()."gambar/ttd/ttd-".$namax.".png width=90px>
</div>
"

, array('class' => 'link_ttd'));
 
 $data['stempel']="stempel_".$_SESSION['perusahaan'];


 			$data['linkx'] =  "<img src=".base_url()."gambar/corporate/".$_SESSION['perusahaan'].".png>";
  
  
  	// Load data
		$query = $this->Orderdetil_model->get_all($id_order);
		$orderdetil = $query->result();
		$num_rows = $query->num_rows();
		
	  	if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table   cellpadding="3" cellspacing="0" width=100% class=tablex>',

							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
				
		
		
	  	$this->table->set_heading(array("data"=>"no", "width"=>"5%"),
      array("data"=>"Nama Barang", "width"=>"34%"),

    
      array("data"=>"Jumlah", "width"=>"7%"),
      array("data"=>"Harga", "width"=>"10%"),
      array("data"=>"sub total", "width"=>"10%")
     
    );	
		
		
		

		$subtotaly=0;	
			
			$i = 0;			
			foreach ($orderdetil as $row)
			{
				
	
	if($row->status=="batal")
	{
continue;  
  };
	
  			
		$datastatusx="";
		$j=0;
		
				
		
	    $subtotal=$row->harga*$row->jumlah;
		   
		  	$subtotalx=number_format($subtotal, 0, ',', '.');
				$hargax=number_format($row->harga, 0, ',', '.');
				$jumlahx=number_format($row->jumlah, 0, ',', '.');
								
		$subtotaly+=$subtotal;		


		$nama_barang = $this->Produk_model->get_produk_by_id($row->barang)->nama;

				$this->table->add_row(++$i,  $nama_barang,"<div align=right>".$jumlahx,"<div align=right>".$hargax,"<div align=right>".$subtotalx);
			}
		
		
		
		



    
    	$data['table'] = $this->table->generate();
			
			 $this->table->clear();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data order!';
		}		
		
		
		
		$tmpl2 = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" >',
						  
              'row_alt_start'  => '<tr >',
							'row_alt_end'    => '</tr>',

						  );

			$this->table->set_template($tmpl2);


 
  	
			$this->table->set_empty("&nbsp;");
	  $order = $this->Order_model->get_order_by_id($id_order)->row();
		$konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);
		
	
		$tanggalx = date('d/m/Y', strtotime($order->tanggal));
	

	$data['konsumen']=$konsumen->perusahaan;
			
 	$data['tanggal']=$tanggalx;
			
 	$data['nomor']=$id_order;
		$data['pembayaran']=number_format($order->pembayaran, 0, ',', '.');
		
		if($order->ongkir)
		{$data['ongkir']=number_format($order->ongkir, 0, ',', '.'); 
			$subtotaly+=$order->ongkir;}else
			$data['ongkir']="";
			
			
    $data['total']=number_format($subtotaly, 0, ',', '.');

    if(!empty($order->diskon))
    $data['diskon']=number_format($order->diskon, 0, ',', '.');
		
		$data['kekurangan']=number_format($subtotaly-$order->pembayaran-$order->diskon, 0, ',', '.');
      	   
		
		
		// Load view

		$data['alamat']="invoice_".$_SESSION['perusahaan'];
		$this->load->view("template_slip", $data);
		
	}
		
	/**
	 * Hapus data orderdetil
	 */


	

}
// END Orderdetil Class

/* End of file orderdetil.php */
/* Location: ./system/application/controllers/orderdetil.php */
