<?php
/**
 * Order Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Rangkuman extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $limit = 10;
  var $title="order";

  
  function Rangkuman()
	{
		parent::__construct();
		$this->load->model('penjualan/Order_model', '', TRUE);
		$this->load->model('penjualan/Orderdetil_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('penjualan/Produk_model', '', TRUE);		
			$this->load->model('produksi/Pemroses_model', '', TRUE);
				$this->load->model('sdm/Pegawai_model', '', TRUE);
		$this->load->helper('autorun');
		
$this->load->helper('fungsi');



// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');

    $this->data['title']=$this->title;
 
  
      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
      
  }
	
	
	
	/**
	 * Iid_konsumenialisasi variabel untuk $limit dan $title(untuk id element <body>)
	 */

	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman order,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
		// Hapus data session yang digunakan pada proses update data order
		$this->session->unset_userdata('id_order');
		$this->session->unset_userdata('tanggal');
			

			$this->get_last_ten_order();
	
	}
	
	/**
	 * Menampilkan 10 data order terkini
	 */
	function get_last_ten_order($status='')
	{
		$data=$this->data;
    $data['h2_title']=$this->title;

		

		$data['custom_view'] = 'penjualan/rangkuman';
	
jalankan();
	

		$data_status=array("PO","proses","siap");
		
	//	if(empty($status))
	//	$status="semua";
		
	$data['status']="semua";

$xx=0;

	foreach	($data_status as $status)  
	{	
		$statusy="='$status'";
		  // echo $offset;
		// Offset
	

		// Load data dari tabel order
		$orders = $this->Order_model->get_last_ten_order($statusy)->result();		
		$nama_order="";
		$id_orderx=0;
	



$tampilan='  <div class="list-group">
         <a href="#" class="list-group-item active"><span class="glyphicon plus" aria-hidden="true"></span>
              '.$status.'
            
 </a>';


    $yy=1;
  $terakhir=false;
    if ($orders) // Jika query menghasilkan data
		{
		
$orders2=$orders;
			$order=current($orders);
			$order2=current($orders2);
			$order2=next($orders2);




			while (true)
			{



				
				if(!$order)
				break;
				




$order=current($orders);


if($order2)
$id_orderx=$order2->id_order;
else
$id_orderx="aaa";

if($order->id_order!=$id_orderx)


$terakhir=true;

else
$terakhir=false;

    
    

				$nama_perusahaan=$order->perusahaan;
	
  
  


if($terakhir)
$koma="";
else
$koma=","; 
 
/**



*/



	$nama_barang = $this->Produk_model->get_produk_by_id($order->barang)->nama;
				
      //  $nama_order.= " <div id=orderdetil>".$nama_barang.$koma."</div>";
  
   $nama_order.= "&nbsp;<span class='text-info'> ".$nama_barang.$koma."</span>";
	
 
 if($terakhir)
{	


if($order->id_ar)
{
$warna2=$this->Pegawai_model->get_pegawai_by_kode($order->id_ar);
//$ar="<div id=ar><font color=#".$warna2->warna.">".$order->id_ar."</font></div>";

$warnax=!empty($warna2->warna)?$warna2->warna:"";
 $ar="<span class='label label-info'><font color=#".$warnax.">".$order->id_ar."</font></span>";

}
else
$ar="";



$dp_min=floor($order->total/10*3);

if($order->pembayaran>=$order->total)

$bayar="<div id=lns><font color=green></font></div>";
else if ($order->pembayaran>=$dp_min)
$bayar="<div id=dp></div>";
else
$bayar="<div id=bayar><font color=red></font></div>";

$pengiriman="";
if($status=="siap")
{
if($order->pengiriman=="diambil")
$pengiriman="<div id=diambil></div>";
else if($order->pengiriman=="diantar")
$pengiriman="<div id=diantar></div>";
else if($order->pengiriman=="jasa")
$pengiriman="<div id=jasax></div>";
}

if($order->total<=5000000)
$warna='<p  class="pull-left">';
else if($order->total<=15000000)
$warna='<p class="text-success pull-left">';
else
$warna='<p class="text-danger  pull-left">';

$isi_tampilan=$warna. $yy.". ".$nama_perusahaan." </p>  "

.$nama_order."<div class=pull-right>".
        
$ar.$bayar."</div>";


$tampilan.=
				anchor_popup('penjualan/orderdetil/index/'.$order->id_order,$isi_tampilan,array('class' => 'list-group-item','width'=>2000,'height'=>900))

      .
     $pengiriman;
$nama_order="";

$yy++;
	}  
  
  	$order=next($orders);
    	$order2=next($orders2);
     	
      }

		}
				
		
		$data['tablez'][$xx] = $tampilan;
		
		
		
	$xx++;	
		}
			
	
		// Load default view
		$this->load->view('template', $data);
	}
	

}
