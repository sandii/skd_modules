<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pemroses extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'pemroses';
		var $alamat = 'produksi/pemroses';
	
  
  function Pemroses()
	{
		parent::__construct();
		$this->load->model('Pemroses_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman pemroses,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data pemroses
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;

		
		// Load data
		$query = $this->Pemroses_model->get_all();
		$pemroses = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Pemroses','warna',  'Actions');
			$i = 0;
			
			foreach ($pemroses as $row)
			{
				$this->table->add_row(++$i,  $row->nama,"<table bgcolor=#".$row->warna." width=40px><td></table>",
										anchor($this->alamat.'/update/'.$row->id,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor($this->alamat.'/delete/'.$row->id,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data pemroses!';
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data pemroses
	 */
	function delete($id_pemroses)
	{
		$this->Pemroses_model->delete($id_pemroses);
		$this->session->set_flashdata('message', '1 data pemroses berhasil dihapus');
		
		redirect($this->alamat.'');
	}
	
	/**
	 * Pindah ke halaman tambah pemroses
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'pemroses_form';
		$data['form_action']	= site_url($this->alamat.'/add_process');
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data pemroses
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'pemroses_form';
		$data['form_action']	= site_url($this->alamat.'/add_process');
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	

		$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$pemroses = array(
							'nama'		=> $this->input->post('marketing'),	'warna'		=> $this->input->post('warna')
						);
			// Proses penyimpanan data di table pemroses
			$this->Pemroses_model->add($pemroses);
			
			$this->session->set_flashdata('message', 'Satu data pemroses berhasil disimpan!');
			redirect($this->alamat.'/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update pemroses
	 */
	function update($id_pemroses)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'pemroses_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_pemroses);
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$pemroses = $this->Pemroses_model->get_pemroses_by_id($id_pemroses);
				
	
		
		$data['default']['warna']		= $pemroses->warna;
		$data['default']['marketing']		= $pemroses->nama;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data pemroses
	 */
	function update_process($id_pemroses)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'pemroses_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_pemroses);
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('marketing', 'Marketing', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$pemroses = array(
							'nama'		=> $this->input->post('marketing')
							,	'warna'		=> $this->input->post('warna')
						);
			$this->Pemroses_model->update($id_pemroses,$pemroses);
			
			$this->session->set_flashdata('message', 'Satu data pemroses berhasil diupdate!');
			redirect($this->alamat.'');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_pemroses valid, agar tidak ganda
	 */

}
// END Pemroses Class

/* End of file pemroses.php */
/* Location: ./system/application/controllers/pemroses.php */
