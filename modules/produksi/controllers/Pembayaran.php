<?php
/**
 * Order Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pembayaran extends CI_Controller
{
    /**
     * Constructor
     */

    public $limit = 20;
    public $title = "belum lunas";
    public $alamat = 'produksi/pembayaran';

    public function Pembayaran()
    {
        parent::__construct();
        $this->load->model('Order_model', '', true);
        $this->load->model('Orderdetil_model', '', true);
        $this->load->model('konsumen/Konsumen_model', '', true);
        $this->load->model('keuangan/Akun_model', '', true);
        $this->load->model('produksi/Pembayaran_model', '', true);
        $this->load->model('sdm/Pegawai_model', '', true);
        $this->load->model('keuangan/Kas_model', '', true);

        $this->load->helper('fungsi');

// content yang fix, ada terus di web
        $this->data['nama'] = $this->session->userdata('nama');

        $this->data['title'] = $this->title;

        $this->load->library('cekker');
        $this->cekker->cek($this->router->fetch_class());

    }

    public function index($year = "")
    {

        if (empty($year)) {
            $year = date("Y");
        }

        $this->get_last_ten_order($year);

    }

    /**
     * Menampilkan 10 data order terkini
     */
    public function get_last_ten_order($tahun, $id_order = 0)
    {
        $data = $this->data;
        $data['h2_title'] = $this->title;
        $data['status'] = "belum";
        $data['refresh'] = true;

        $kar = array(" ", ".", ",", "%", "(", ")", ">", "<", "!", "@", "#", "$", "^", "&", "*", "+", "=", "{", "}", "[", "]", "|", "\\", ":", ";", "\'", "\"", "?", "/", "~", "`");

        // $mulai = 2011;

		$firstOrder = $this->Order_model->firstOrder()->tanggal;
        $datetime = new DateTime($firstOrder);

        $mulai = $datetime->format('Y');

        $akhir = date("Y");
        $pagination = "";

        for ($i = $mulai; $i <= $akhir; $i++) {
            if ($tahun != $i) {
                $pagination .= "<li>" . anchor($this->alamat . '/index/' . $i, $i, array('class' => 'tahun')) . "</li>";
            } else {
                $pagination .= "<li class=active><a href=#>$tahun</a></li>";
            }

        }

        $data['pagination'] = $pagination;

        // Load data dari tabel order

        $tahunx = $tahun + 1;
        $where = "total > (pembayaran + diskon) and tanggal >='$tahun-01-01' and tanggal<'$tahunx-1-1'";
        $ordersx = $this->Order_model->count_all_num_rows_pembayaran($where);
        $orders = $ordersx->result();
        $num_rows = $ordersx->num_rows();

        if ($num_rows > 0) // Jika query menghasilkan data
        {
            // Membuat pagination
            $config['base_url'] = site_url($this->alamat . '/get_last_ten_order/');

            // Set template tabel, untuk efek selang-seling tiap baris
            $tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
                'row_alt_start' => '<tr class="zebra">',
                'row_alt_end' => '</tr>',
            );
            $this->table->set_template($tmpl);

            // Set heading untuk tabel
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('No', 'Hari, Tanggal', 'Perusahaan', '<div align=right>total</div>', '<div align=right>diskon</div>', '<div align=right>dp</div>', '<div align=right>kekurangan</div>', 'Actions');

            // Penomoran baris data
            $i = 0;
            $kekuranganx = 0;
            $datax = array();

            foreach ($orders as $order) {
                // Konversi hari dan tanggal ke dalam format Indonesia
                $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
                $hr = date('w', strtotime($order->tanggal));
                $hari = $hari_array[$hr];
                $tgl = date('d-m-Y', strtotime($order->tanggal));
                $hr_tgl = "$hari, $tgl";

                $kekurangan = $order->pembayaran;
                $kekurangany = $order->total - $kekurangan - $order->diskon;

                $kekuranganz = number_format($kekurangany, 0, ',', '.');

                $kekuranganx += $kekurangany;

                if ($kekurangan > 0) {

                    $pembayaranx = anchor_popup('produksi/pembayaran/detil/' . $order->id_order, number_format($kekurangan, 0, ',', '.'), array('class' => 'detail', 'width' => 1000));
                } else {
                    $pembayaranx = "";
                }

                $totalx = number_format($order->total, 0, ',', '.');

                // Penyusunan data baris per baris, perhatikan pembuatan link untuk updat dan delete

                $ada_pembayaran = $this->Pembayaran_model->get_pembayaran_by_order($order->id_order);

                if (cek_auth("auth_bayar")) {
                    if (!empty($ada_pembayaran)) {
                        $bayar = " " . anchor($this->alamat . '/confirm/' . $order->id_order, 'ada konfirmasi', array('class' => 'link'));
                    } else {
                        $bayar = " " . anchor($this->alamat . '/update/' . $order->id_order, 'terima pembayaran', array('class' => 'btn btn-info btn-sm'));
                    }

                    $diskon = anchor($this->alamat . '/diskon/' . $order->id_order, !empty($order->diskon) ? number_format($order->diskon, 0, ',', '.') : "beri diskon"
                        , array('class' => 'btn btn-warning btn-sm'));
                } else {
                    if (!empty($ada_pembayaran)) {
                        $bayar = " " . anchor('produksi/pembayaran/confirm/' . $order->id_order, 'sudah konfirmasi', array('class' => 'link'));
                    } else {
                        $bayar = " " . anchor('produksi/pembayaran/update/' . $order->id_order, 'konfirmasi pembayaran', array('class' => 'update'));
                    }

                    $diskon = !empty($order->diskon) ? number_format($order->diskon, 0, ',', '.') : "";

                }

                if ($order->id_ar) {
                    $warna2 = $this->Pegawai_model->get_pegawai_by_kode($order->id_ar);

                    $warnax = !empty($warna2->warna) ? $warna2->warna : "";

                    $ar = "<span class='label label-default'><font color=#" . $warnax . ">" . $order->id_ar . "</font></div>";
                } else {
                    $ar = "";
                }

                if (!empty($ada_pembayaran)) {
                    $this->table->add_row(++$i, $hr_tgl,

                        anchor('produksi/order/get_last_ten_order/' . $order->id_konsumen . '/dahulu/sekarang/', $order->perusahaan . $ar, array('class' => 'detail')),

                        "<div align=right>" .

                        anchor_popup('produksi/orderdetil/index/' . $order->id_order, $totalx, array('class' => 'detail', 'width' => 1000)) . "</div"

                        , "<div align=right>" . $diskon . "</div"

                        , "<div align=right>" . $pembayaranx . "</div", "<div align=right>" . $kekuranganz . "</div",

                        $bayar

                        //                        anchor_popup('pembayarandetil/index/'.$order->id_order,'detail',array('class'=> 'update'))
                    );
                } else {
                    $xx = array(++$i, $hr_tgl,

                        anchor('produksi/order/get_last_ten_order/' . $order->id_konsumen . '/dahulu/sekarang/', $order->perusahaan . $ar, array('class' => 'detail')),

                        "<div align=right>" .

                        anchor_popup('produksi/orderdetil/index/' . $order->id_order, $totalx, array('class' => 'detail', 'width' => 1000))

                        , "<div align=right>" . $diskon
                        ,

                        "<div align=right>" . $pembayaranx, "<div align=right>" . $kekuranganz,

                        $bayar);

                    array_push($datax, $xx);
                }

            }

            foreach ($datax as $yy) {
                $this->table->add_row($yy[0], $yy[1], $yy[2], $yy[3], $yy[4], $yy[5], $yy[6], $yy[7]);
            }

            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data order!';
        }

        if (!empty($id_order)) {
            $order = $this->Order_model->get_order_by_id($id_order)->row();
            $data['pembayaran_log'] = $order->pembayaran_log;

        } else {
            $data['pembayaran_log'] = "";
        }

        // Load default view

        if (cek_auth("auth_keuangan")) {
            if (!empty($kekuranganx)) {
                $data['link'] = array('link_add' => "total tagihan: " . number_format($kekuranganx, 0, ',', '.'));
            } else {
                $data['link'] = array('link_add' => "");
            }
        } else {
            $data['link'] = array('link_add' => "");
        }

        $this->load->view('template', $data);
    }

    public function update($id_order)
    {

        //cek_auth("auth_bayar",1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;

        $data['h2_title'] = $this->title . ' > Update Data';
        $data['custom_view'] = 'pembayaran_form';
        $data['form_action'] = site_url($this->alamat . '/update_process/' . $id_order);

        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')));

        $query2 = $this->Akun_model->get_all2(1)->result();
        foreach ($query2 as $row2) {

            $data['options_ke'][$row2->id_akundetil] = $row2->nama;

        }

        // cari data dari database
        $order = $this->Order_model->get_order_by_id($id_order)->row();

        // Data untuk mengisi field2 form

        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;
        $data['kekurangan'] = $order->total - $order->pembayaran - $order->diskon;
        $data['default']['bayar'] = $order->total - $order->pembayaran - $order->diskon;
        $data['default']['diskon'] = $order->diskon;

        // buat session untuk menyimpan data tanggal yang sedang diupdate
        $this->session->set_userdata('tanggal', $order->tanggal);

        $this->load->view('template', $data);
    }

    /**
     * Proses untuk update data ordersi
     */
    public function update_process($id_order)
    {

        //cek_auth("auth_bayar",1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;
        $data['h2_title'] = $this->title . ' > Update Data';
        $data['custom_view'] = 'pembayaran_form';
        $data['form_action'] = site_url($this->alamat . '/update_process/' . $id_order);
        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')));

        $query2 = $this->Akun_model->get_all2(1)->result();
        foreach ($query2 as $row2) {

            $data['options_ke'][$row2->id_akundetil] = $row2->nama;

        }

        $order = $this->Order_model->get_order_by_id($id_order)->row();
        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;

        $kekurangan = $order->total - $order->pembayaran - $order->diskon;
        $data['kekurangan'] = $kekurangan;

        $data['default']['bayar'] = $order->total - $order->pembayaran;
        $data['default']['diskon'] = $order->diskon;

        $bayar = $this->input->post('bayar');

        $kelebihan = 0;
        $bayar_order = $bayar;

        if ($bayar > $kekurangan) {

            $kelebihan = $bayar - $kekurangan;
            $bayar_order = $kekurangan;
        }

        $total_bayar = $this->input->post('pembayaran') + $bayar_order;

        $this->form_validation->set_rules('bayar', 'bayar ', 'required|integer');

        // Jika validasi sukses
        if ($this->form_validation->run() == true) {

            // Simpan data

            if (cek_auth("auth_bayar")) {
                $order = array(
                    'pembayaran' => $total_bayar,
                );
                $this->Order_model->update($id_order, $order);

                $this->update_history($id_order);
                $status = "approve";

                $uang = array('ket' => 'pembayaran dari ' . $konsumen->perusahaan,
                    'id_akun' => $this->input->post('ke'),
                    'kode' => 'byr',
                    'kredit' => 0,
                    'kode' => 'byr',
                    'id_detail' => $id_order,

                    'tanggal' => $this->input->post('tanggal'),
                    'id_detail' => $id_order,
                    'debet' => $bayar_order,
                );
                $this->Kas_model->transaksi($uang);

                if ($kelebihan > 0) {
                    $uang = array('ket' => 'kelebihan pembayaran dari ' . $konsumen->perusahaan,
                        'id_akun' => $this->input->post('ke'),
                        'kode' => 'lbh',
                        'kredit' => 0,
                        'tanggal' => $this->input->post('tanggal'),
                        'id_detail' => $id_order,
                        'debet' => $kelebihan,
                        'kode' => 'lbh',
                        'id_detail' => $id_order,
                    );
                    $this->Kas_model->transaksi($uang);

                    $uang = array('ket' => 'kelebihan pembayaran dari ' . $konsumen->perusahaan,
                        'id_akun' => 502,
                        'kode' => 'lbh',
                        'id_detail' => $id_order,
                        'debet' => 0,
                        'tanggal' => $this->input->post('tanggal'),
                        'id_detail' => $id_order,
                        'kredit' => $kelebihan,
                    );
                    $this->Kas_model->transaksi($uang);

                }

            } else {
                $status = "confirm";
            }

            $order = array(
                'jumlah' => $bayar,
                'id_order' => $id_order,
                'tanggal' => $this->input->post('tanggal'),
                'tujuan' => $this->input->post('ke'),
                'ket' => $this->input->post('keterangan'),
                'id_order' => $id_order,
                'status' => $status,
            );
            $this->Pembayaran_model->add($order);

            $this->session->set_flashdata('message', 'Pembayaran berhasil diupdate!');

            redirect($this->alamat . '');
        } else {

            $this->load->view('template', $data);

        }

    }

    public function diskon($id_order)
    {

        cek_auth("auth_bayar", 1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;

        $data['h2_title'] = $this->title . ' > Update diskon';
        $data['custom_view'] = 'diskon';
        $data['form_action'] = site_url($this->alamat . '/diskon_process/' . $id_order);
        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // cari data dari database
        $order = $this->Order_model->get_order_by_id($id_order)->row();

        // Data untuk mengisi field2 form

        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;
        $data['default']['diskon'] = $order->diskon;
        $data['default']['keterangan'] = $order->ket_diskon;

        $this->load->view('template', $data);
    }

    /**
     * Proses untuk update data ordersi
     */
    public function diskon_process($id_order)
    {

        cek_auth("auth_bayar", 1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;

        $data['h2_title'] = $this->title . ' > Update diskon';
        $data['custom_view'] = 'diskon';
        $data['form_action'] = site_url($this->alamat . '/diskon_process/' . $id_order);
        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // cari data dari database
        $order = $this->Order_model->get_order_by_id($id_order)->row();

        // Data untuk mengisi field2 form

        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;
        $data['default']['diskon'] = $order->diskon;

        $kekuranganx = $order->total + 1;

        $this->form_validation->set_rules('diskon', 'diskon ', 'required|less_than[' . $kekuranganx . ']');

        // Jika validasi sukses
        if ($this->form_validation->run() == true and cek_auth("auth_bayar")) {

            // Simpan data

            $order = array(
                'diskon' => $this->input->post('diskon'),
                'ket_diskon' => $this->input->post('keterangan'),
            );
            $this->Order_model->update($id_order, $order);

            $this->update_history($id_order);

            $this->session->set_flashdata('message', 'diskon berhasil diupdate!');

            redirect($this->alamat . '');

        } else {

            $this->load->view('template', $data);

        }

    }

    public function confirm($id_order)
    {

//cek_auth("auth_bayar",1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;

        $data['h2_title'] = $this->title . ' > ada konfirmasi ';
        $data['custom_view'] = 'pembayaran_form';
        $data['form_action'] = site_url($this->alamat . '/confirm_process/' . $id_order);
        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')));

        $query2 = $this->Akun_model->get_all2(1)->result();
        foreach ($query2 as $row2) {

            $data['options_ke'][$row2->id_akundetil] = $row2->nama;

        }

        // cari data dari database
        $order = $this->Order_model->get_order_by_id($id_order)->row();

        // Data untuk mengisi field2 form

        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;
        $data['kekurangan'] = $order->total - $order->pembayaran - $order->diskon;
        $data['default']['diskon'] = $order->diskon;

        $ada_pembayaran = $this->Pembayaran_model->get_pembayaran_by_order($order->id_order);
        $data['default']['bayar'] = $ada_pembayaran->jumlah;
        $data['default']['tanggal'] = date('Y-m-d', strtotime($ada_pembayaran->tanggal));
        $data['default']['ke'] = $ada_pembayaran->tujuan;
        $data['default']['keterangan'] = $ada_pembayaran->ket;

        $data['link_batal'] = anchor($this->alamat . '/delete/' . $ada_pembayaran->id_pembayaran, 'batalkan konfirmasi ini', array('class' => 'delete', 'onclick' => "return confirm('Anda yakin akan menghapus konfirmasi ini?')"));

        $this->load->view('template', $data);
    }

    /**
     * Proses untuk update data ordersi
     */
    public function confirm_process($id_order)
    {

//cek_auth("auth_bayar",1);

        // Iid_konsumenialisasi data umum
        $data = $this->data;
        $data['h2_title'] = $this->title . ' > Update Data';
        $data['custom_view'] = 'pembayaran_form';
        $data['form_action'] = site_url($this->alamat . '/confirm_process/' . $id_order);
        $data['link'] = array('link_back' => anchor($this->alamat . '/', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')));

        $query2 = $this->Akun_model->get_all2(1)->result();
        foreach ($query2 as $row2) {

            $data['options_ke'][$row2->id_akundetil] = $row2->nama;

        }

        $order = $this->Order_model->get_order_by_id($id_order)->row();
        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);
        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;

        $data['pembayaran'] = $order->pembayaran;

        $kekurangan = $order->total - $order->pembayaran - $order->diskon;
        $data['kekurangan'] = $kekurangan;

        $ada_pembayaran = $this->Pembayaran_model->get_pembayaran_by_order($order->id_order);

        $bayar = $this->input->post('bayar');

        $kelebihan = 0;
        $bayar_order = $bayar;
        if ($bayar > $kekurangan) {
            $bayar_order = $kekurangan;
            $kelebihan = $bayar - $kekurangan;
        }

        $total_bayar = $this->input->post('pembayaran') + $bayar;

        $this->form_validation->set_rules('bayar', 'bayar ', 'required|integer');

        // Jika validasi sukses
        if ($this->form_validation->run() == true) {

            // Simpan data

            if (cek_auth("auth_bayar")) {
                $order = array(
                    'pembayaran' => $total_bayar,

                );
                $this->Order_model->update($id_order, $order);

                $this->update_history($id_order);
                $status = "approve";

                $uang = array('ket' => 'pembayaran dari ' . $konsumen->perusahaan,
                    'id_akun' => $this->input->post('ke'),
                    'id_detail' => $id_order,
                    'kode' => 'byr',
                    'debet' => $bayar_order,
                    'tanggal' => $this->input->post('tanggal'),

                );
                $this->Kas_model->transaksi($uang);

                if ($kelebihan > 0) {

                    $uang = array('ket' => 'kelebihan pembayaran dari ' . $konsumen->perusahaan,
                        'id_akun' => $this->input->post('ke'),
                        'kode' => 'lbh',
                        'kredit' => 0,
                        'tanggal' => $this->input->post('tanggal'),
                        'id_detail' => $id_order,
                        'debet' => $kelebihan,
                    );
                    $this->Kas_model->transaksi($uang);

                    $uang = array('ket' => 'kelebihan pembayaran dari ' . $konsumen->perusahaan,
                        'id_akun' => 502,
                        'kode' => 'lbh',
                        'debet' => 0,
                        'tanggal' => $this->input->post('tanggal'),
                        'id_detail' => $id_order,
                        'kredit' => $kelebihan,
                    );
                    $this->Kas_model->transaksi($uang);

                }

            } else {
                $status = "confirm";
            }

            $order = array(
                'jumlah' => $bayar,
                'tanggal' => $this->input->post('tanggal'),
                'tujuan' => $this->input->post('ke'),
                'ket' => $this->input->post('keterangan'),
                'status' => $status,
            );
            $this->Pembayaran_model->update($ada_pembayaran->id_pembayaran, $order);

            $this->session->set_flashdata('message', 'Pembayaran berhasil diupdate!');

            redirect($this->alamat . '');
        } else {

            $data['default']['bayar'] = $ada_pembayaran->jumlah;
            $data['default']['tanggal'] = date('Y-m-d', strtotime($ada_pembayaran->tanggal));
            $data['default']['ke'] = $ada_pembayaran->tujuan;
            $data['default']['keterangan'] = $ada_pembayaran->ket;
            $data['default']['diskon'] = $order->diskon;

            $data['link_batal'] = anchor($this->alamat . '/delete/' . $ada_pembayaran->id_pembayaran, 'batalkan konfirmasi ini', array('class' => 'delete', 'onclick' => "return confirm('Anda yakin akan menghapus konfirmasi ini?')"));

            $this->load->view('template', $data);

        }

    }

    public function delete($id_marketing)
    {
        $this->Pembayaran_model->delete($id_marketing);
        $this->session->set_flashdata('message', '1 data konfirmasi berhasil dihapus');

        redirect($this->alamat . '');
    }

    public function update_history($id_order)
    {

        $order = $this->Order_model->get_order_by_id($id_order)->row();

        $query = $this->Order_model->konsumen_pertama($order->id_konsumen);

        $num_rows = $query->num_rows();

        $total_order = $num_rows;
        $tanggal_pertama = $query->row()->tanggal;

        $query2 = $this->Order_model->konsumen_terakhir($order->id_konsumen);

        $tanggal_terakhir = $query2->row()->tanggal;

        $query3 = $this->Order_model->omzet($order->id_konsumen);

        $omzet = $query3->row()->total;

        //$this->Order_model->update($id_order, $order);
        $history = array(
            'pertama' => $tanggal_pertama,
            'terakhir' => $tanggal_terakhir,
            'omzet' => $omzet,
            'total_order' => $total_order,
        );
        $this->Konsumen_model->update($order->id_konsumen, $history);

    }

    public function rangkuman($tgl1 = "", $tgl2 = "", $offset = 0)
    {

        $data = $this->data;
        $data['title'] = "pembayaran";
        $data['h2_title'] = "pembayaran";
        $data['custom_view'] = 'main_tanggal';

        if ($this->input->post('cari')) {

            $tgl1 = $this->input->post('tgl1');
            $tgl2 = $this->input->post('tgl2');
            $offset = 0;
        }

        $seminggu = date("Y-m-d", mktime(0, 0, 0, date("n"), date("j") - 7, date("Y")));

        if (empty($tgl1)) {$tgl1 = $seminggu;
            $cari_tgl1 = $seminggu;} else {
            $cari_tgl1 = $tgl1;
        }

        if (empty($tgl2) or $tgl2 == "sekarang") {$tgl2 = "sekarang";
            $cari_tgl2 = "";} else {
            $cari_tgl2 = $tgl2;
        }

        $data["tgl1"] = $tgl1;
        $data["tgl2"] = $tgl2;

        // Load data
        $query = $this->Pembayaran_model->get_rangkuman($this->limit, $offset, $cari_tgl1, $cari_tgl2);

        $siswa = $query->result();

        $num_rows = $this->Pembayaran_model->get_rangkuman("", $offset, $cari_tgl1, $cari_tgl2)->num_rows();

        if ($num_rows > 0) {
            // Generate pagination

            $uri_segment = 6;
            $config['base_url'] = site_url($this->alamat . '/rangkuman/' . $tgl1 . '/' . $tgl2 . '/');
            $config['total_rows'] = $num_rows;
            $config['per_page'] = $this->limit;
            $config['uri_segment'] = $uri_segment;

            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class=active><a href=#>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';

            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();

            // Table
            /*Set table template for alternating row 'zebra'*/
            $tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
                'row_alt_start' => '<tr class="zebra">',
                'row_alt_end' => '</tr>',
            );
            $this->table->set_template($tmpl);

            /*Set table heading */
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('tanggal', 'konsumen', 'jumlah', 'ke rekening', 'keterangan');

            foreach ($siswa as $row) {

                $order = $this->Order_model->get_order_by_id($row->id_order)->row();

                // Data untuk mengisi field2 form

                $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

                $tujuan = $this->Akun_model->get_akun_by_id($row->tujuan)->nama;

                $this->table->add_row($row->tanggal, $konsumen->perusahaan, format_uang($row->jumlah), $tujuan, $row->ket

                );
            }
            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data pembayaran!';
        }

        // Load view
        $this->load->view('template', $data);
    }

    public function detil($id_order)
    {

        $data = $this->data;
        $data['h2_title'] = "data pembayaran";

        $order = $this->Order_model->get_order_by_id($id_order)->row();

        // Data untuk mengisi field2 form

        $konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);

        $data['perusahaan'] = $konsumen->perusahaan;
        $data['total'] = $order->total;
        $data['pembayaran'] = $order->pembayaran;
        $data['kekurangan'] = $order->total - $order->pembayaran;

        $data['pagination'] =
        "
  	<p>
		<label for=id_konsumen>konsumen:</label>" .
        $konsumen->perusahaan . "
     	</p>
		<p>
		<label for=id_konsumen>total tagihan: </label>
    Rp." . number_format($order->total, 0, ',', '.') . "
     	</p>




		<p>
		<label for=id_konsumen>kekurangan:</label>
    Rp." . number_format($order->total - $order->pembayaran, 0, ',', '.') . "

     	</p>";

        // Load data
        $query = $this->Pembayaran_model->get_pembayaran_by_order2($id_order);
        $supplier = $query->result();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            // Table
            /*Set table template for alternating row 'zebra'*/
            $tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
                'row_alt_start' => '<tr class="zebra">',
                'row_alt_end' => '</tr>',
            );
            $this->table->set_template($tmpl);

            /*Set table heading */
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('no', 'tanggal', 'jumlah', 'ke rekening', 'keterangan');
            $i = 0;

            foreach ($supplier as $row) {

                $tujuan = $this->Akun_model->get_akun_by_id($row->tujuan)->nama;

                $this->table->add_row(++$i, $row->tanggal,
                    $row->jumlah, $tujuan, $row->ket);
            }
            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data pembayaran!';
        }

        // Load view
        $this->load->view('template2', $data);
    }

}

// END Order Class

/* End of file order.php */
/* Location: ./system/application/controllers/order.php */
