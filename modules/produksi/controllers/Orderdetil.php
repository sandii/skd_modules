<?php
/**
 * Orderdetil Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Orderdetil extends  CI_Controller {
	/**
	 * Constructor
	 */
	function Orderdetil()
	{
		parent::__construct();
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Orderdetil_model', '', TRUE);
		$this->load->model('Komplain_model', '', TRUE);
		$this->load->model('Produk_model', '', TRUE);
	$this->load->model('Pemroses_model', '', TRUE);
	
    $this->load->library('cekker');
   $this->cekker->cek($this->router->fetch_class());
  
  	$this->load->helper('fungsi');		
$this->alamat_gambar='/gambar/order/';
  }
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	var $title = 'Orderdetil';
		var $alamat = 'produksi/orderdetil';

	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman orderdetil,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_order)
	{
		
			$this->get_all($id_order);
		
	}
	
	/**
	 * Tampilkan semua data orderdetil
	 */
	function get_all($id_order)
	{
		$data['title'] = $this->title;
		$data['h2_title'] = 'Order Detil';
		$data['custom_view'] = 'orderdetil';
		$data['link_frame']=site_url('produksi/comment/index/'.$id_order);
		$data['form_action']	= site_url($this->alamat.'/add_comment/'.$id_order);
  
  
  	// Load data
		$query = $this->Orderdetil_model->get_all($id_order);
		$orderdetil = $query->result();
		$num_rows = $query->num_rows();
		
	  	if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" width=100% class=table >',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( 'Nama Barang', 'Tema','Jumlah order', 'Harga Satuan','Sub Total','spesifikasi', 'status','gambar');
			
		

	$data_status = $this->Order_model->get_produksi();



			
			$i = 0;			
			foreach ($orderdetil as $row)
			{
				
				
		$datastatusx="";

		
				
			foreach	($data_status as $status)  



	
		   {
			
			
			$datastatusx .= "<option value=".site_url()."/produksi/orderdetil/update_status/".$status->nama.'/'.$id_order.'/'.$row->id_orderdetil;
      
			if($status->nama==$row->status)
		$datastatusx .= " selected ";
      
      $datastatusx .=">".$status->judul."</option>";
			
		//	else
		//	$statusx .= anchor('orderdetil/update_status/'.$data_status[$j].'/'.$id_order.'/'.$row->id_orderdetil,$data_status[$j],array('id'=> 'update_status','onclick'=>"return confirm('Anda yakin akan mengubah data order ini?')"));
			
		   }

$tambahan=array('marketing'=>'follow up','desain'=>'desain','siap'=>'siap kirim','finish'=>'finish','pending'=>'pending','batal'=>'batal');

foreach	($tambahan as $key=>$status)  

{
			$datastatusx .= "<option value=".site_url()."/produksi/orderdetil/update_status/".$key.'/'.$id_order.'/'.$row->id_orderdetil;

			if($row->status==$key)
		$datastatusx .= " selected ";
      
      $datastatusx .=">$status</option>";


}


		  			
				$hargax=number_format($row->harga, 0, ',', '.');
				$jumlahx=number_format($row->jumlah, 0, ',', '.');
				
				
				
				$spek_lengkap="";

		
	$spek=$this->Order_model->get_spesifikasi();
foreach ($spek as $spesifikasi) 
{	$nama_field=$spesifikasi->nama;

 if(!empty($row->$nama_field))$spek_lengkap.="<span class='text-warning'>".$spesifikasi->judul.":</span>".$row->$nama_field." &nbsp";



//	$orderdetil[$spesifikasi->nama]=$this->input->post($spesifikasi->nama); 


}	
  if(!empty($row->spesifikasi))$spek_lengkap.="<span class='text-warning'>lainnya:</span>$row->spesifikasi &nbsp; ";
      




$datastatusy = "<option value=".site_url()."/produksi/orderdetil/update_status_proses/0/".$id_order.'/'.$row->id_orderdetil.">blm</option>";
		$j=0;
		
		
		$query2 = $this->Pemroses_model->get_all();
		$orderdetil2 = $query2->result();	
				
		
		foreach ($orderdetil2 as $row2)
			{
				
		$datastatusy .= "<option value=".site_url()."/produksi/orderdetil/update_status_proses/".$row2->nama.'/'.$id_order.'/'.$row->id_orderdetil;
      
			if($row2->nama==$row->proses)
		$datastatusy .= " selected ";
      
      $datastatusy .=">".$row2->nama."</option>";

$j++;
}


 if(cek_auth("auth_order")) 
 {       
$statusx="
<form name='jump$i'>
<select name='myjumpbox'
OnChange='location.href=jump$i.myjumpbox.options[selectedIndex].value' >
$datastatusx
</select>
</form>";

$statusy="
<form name='jumpx$i'>
<select name='myjumpboxx'
OnChange='location.href=jumpx$i.myjumpboxx.options[selectedIndex].value'  >
$datastatusy
</select>
</form>";

}
else
{
$statusx="status: ".$row->status."<br>";
$statusy=$row->proses."<br>";
}
	
//penampakan
if(empty($row->gambar))
$penampakan=anchor_popup($this->alamat."/gambar/".$row->id_orderdetil.'/'.$id_order,'<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>',array("class"=>"btn btn-warning","height"=>1000,"width"=>1000));
else
$penampakan=anchor_popup($this->alamat."/gambar/".$row->id_orderdetil.'/'.$id_order,"<img src=".base_url().$this->alamat_gambar.$row->gambar."_thumb.jpg >",array("height"=>700,"width"=>1000));
	



				
// cek ada komplain gak
$xxx=$this->Komplain_model->check_komplain($row->id_orderdetil);
		if($xxx)
	//$komplain="<div style='font-size:15px; color:red; font-weight:bold'>komplain</div>";		

	$komplain=anchor('produksi/komplain/update/'.$xxx->id_komplain."/".$id_order,'<font color=red>komplain</font>', array('class' => 'link'));	

		else
	$komplain=anchor('produksi/komplain/add/'.$id_order."/".$row->id_orderdetil,'sukses', array('class' => 'link'));	
		
		
		


	$nama_barang = $this->Produk_model->get_produk_by_id($row->barang)->nama;
	



$cekx=$this->Order_model->get_produksi_by_nama($row->status);
$boleh_edit=!empty($cekx->boleh_edit)?$cekx->boleh_edit:'';

if($row->status=='marketing' or $row->status=='pending'or $row->status=='desain')
		$edit=anchor($this->alamat.'/update/'.$row->id_orderdetil.'/'.$id_order,$nama_barang,array('class' => 'update'));
else
$edit=$nama_barang;


$subtotal=number_format($row->jumlah*$row->harga, 0, ',', '.');

$i++;

				$this->table->add_row($edit ,$row->tema,$jumlahx,$hargax,$subtotal,$spek_lengkap
								
             
 								
										, $statusx.$statusy.$komplain,$penampakan
										
										);
  
				


			}
			$data['table'] = $this->table->generate();
			
			 $this->table->clear();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data order!';
		}		
		
	if(cek_auth("auth_marketing"))
			$data['link'] =  array(anchor($this->alamat.'/add/'.$id_order,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));




			 $data['link']['link_print']         = anchor_popup('produksi/invoice/index/'.$id_order,'<span class="glyphicon glyphicon-print  " aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'));
		 $data['link_pengiriman']         = anchor($this->alamat.'/pengiriman/'.$id_order,'instruksi pengiriman');
		$data['link_pengiriman2']         = anchor($this->alamat.'/pengiriman_hasil/'.$id_order,'hasil pengiriman');
						
		
		
		
		
		$tmpl2 = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" >',
						  
              'row_alt_start'  => '<tr >',
							'row_alt_end'    => '</tr>',

						  );

			$this->table->set_template($tmpl2);


 
  	
			$this->table->set_empty("&nbsp;");
	  $order = $this->Order_model->get_order_by_id($id_order)->row();
		$konsumen = $this->Konsumen_model->get_konsumen_by_id($order->id_konsumen);
	

	
	
	
	

			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

$xx=array("diambil"=>"diambil","diantar"=>"diantar ke alamat","jasa"=>"pakai jasa pengiriman");

$yy=array("disertakan"=>"disertakan dengan barang","terpisah"=>"dikirim terpisah","email"=>"diemail","tidak"=>"tidak pakai");



$tt=false;

if($order->pengiriman) 
{$this->table->add_row('<b>pengiriman',$xx[$order->pengiriman]);
$tt=true;}
if($order->jasa)  	
{$this->table->add_row('<b>jasa pengirim',$order->jasa);
$tt=true;}
if($order->ongkir)
{$this->table->add_row('<b>ongkir',$order->ongkir);
$tt=true;}if($order->invoice)
{$this->table->add_row('<b>invoice',$yy[$order->invoice]);
$tt=true;}
if($order->jenis_pembayaran)
{$this->table->add_row('<b>pembayaran',$order->jenis_pembayaran);
$tt=true;}

if($order->ket_kirim)  	
{$this->table->add_row('<b>keterangan lainnya',$order->ket_kirim);
$tt=true;}


if(!$tt)
$this->table->add_row('','');


$data['table3'] = $this->table->generate();

	
	$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
	
	
	
  $this->table->set_empty("&nbsp;");
$this->table->set_template($tmpl);



if($order->pengiriman=="diambil")
{
$judul1="yg menyerahkan";
$judul2="yg ngambil";
$judul3="surat jalan";
}
else if($order->pengiriman=="diantar")
{
$judul1="yg mengirim";
$judul2="yg menerima";
$judul3="surat jalan";
}
else if($order->pengiriman=="jasa")
{
$judul1="yg mengirim";
$judul2="perkiraan sampai (hari)";
$judul3="resi";
}
else
{
$judul1="";
$judul2="";
$judul3="";

}


if($order->penerima)
$this->table->add_row('<b>'.$judul1,$order->penerima);
if($order->waktu)
$this->table->add_row('<b>'.$judul2,$order->waktu);
if($order->resi)
$this->table->add_row('<b>no '.$judul3,$order->resi);

if($order->koli)
$this->table->add_row('<b>jumlah koli',$order->koli);
if($order->tanggal_kirim)
$this->table->add_row('<b>tanggal kirim',$order->tanggal_kirim);

$this->table->add_row('',"");


$data['table4'] = $this->table->generate();

	
			$totalx=number_format($order->total, 0, ',', '.');
			$this->table->add_row('<b>konsumen',anchor_popup("konsumen/konsumendetil/index/".$order->id_konsumen,$konsumen->perusahaan,array('class' => 'detail','width'=>1000)));
			$this->table->add_row('<b>Harga Total',$totalx);
		if(!empty($order->diskon))
			$this->table->add_row('<b>diskon',number_format($order->diskon, 0, ',', '.'));	
			$this->table->add_row('<b>pembayaran',number_format($order->pembayaran, 0, ',', '.'));	     
			
			//$this->table->add_row('hapus order ini');			    
			
      $data['table2'] = $this->table->generate();
			
 






	$jadwalx = $this->Order_model->get_jadwal_by_id($id_order,$data_status)->row();




$xx=$tanggal_unix=$jadwalx->tanggal_unix;
$deathline_unix=$jadwalx->deathline_unix;
    

  $tgl_deathline=date("Ymd",$deathline_unix);
 









$hari=array(1=>"sn","sl","rb","km","jm","sb","mng");     
      
 $jadwal="<table width=100% id=table_jadwal bordercolor=white border=1  style='color:white; font-size:12px;'><tr><td width=40 align=center  >".

 anchor($this->alamat.'/jadwal/'.$row->id_orderdetil.'/'.$id_order,'<span class="glyphicon glyphicon-time" aria-hidden="true"></span>',array('class' => 'btn btn-danger','id'=>'bottom_link'))

."</td>";


$warna="black";
$tanggal=date("j M",$tanggal_unix);


while($deathline_unix)
{


$yy=date("N",$xx);	
$zz=date("Ymd",$xx);

if($zz==date("Ymd"))
$background= "class=hari_ini";
else
$background="";



$qq=0;

foreach	($data_status as $status)  
	{

if($status->warna)
{
	$nama_tabel=$status->nama."_unix";
	$tanggalx=date("Ymd",$jadwalx->$nama_tabel);

	
	if($zz==$tanggalx)
	$tanggal='<b>'.$status->nama."</b><br>".date("j M H\:00",$jadwalx->$nama_tabel);	


	if($zz<=$tanggalx and $qq==0)
	{$warna=$status->warna;	$qq=1;}
else if ($qq==0)
{
$warna='grey';

}

}

	}
if($yy!=7)
$jadwal.="<td bgcolor=$warna $background >";

if($zz==$tgl_deathline)
{
$tanggal=date("j M H\:00",$jadwalx->deathline_unix);		
$jadwal.="<span class=pull-right>$hari[$yy] <br>$tanggal</span>";
	
break;
}

if($yy!=7)
$jadwal.="$hari[$yy] $tanggal";

$xx+=24*60*60;
$tanggal="";
} 
 $jadwal.="</table><br>";
      
      
      
 $data['jadwal']=$jadwal;     
 		// Load view
		$this->load->view('template_orderdetil', $data);
	}
		

	
	function add_comment($id_order)
	{       $tulisan=$this->input->post('insert_comment');
		if( !empty($tulisan)) 
		{ 

    $commentx="<font color=blue>".date("d M").": ";
    $commentx.=$_SESSION['username'].": </font>";
		
		$commentx.=$this->input->post('insert_comment')."<br>";		
		$this->Order_model->update_comment($id_order,$commentx);}
		//$this->session->set_flashdata('message', 'status order berhasil diupdate');		

		redirect($this->alamat.'/index/'.$id_order);
	}
	


	function update_status($status_baru,$id_order,$id_orderdetil)
	{
	
	cek_auth("auth_order",1);
		$statusx=array('status'=>$status_baru);
		
		$this->Orderdetil_model->update($id_orderdetil,$statusx);
		$this->session->set_flashdata('message', 'status order berhasil diupdate');
		
	 	$this->update_harga($id_order);
		
		
		
		
		
		redirect($this->alamat.'/index/'.$id_order);
	}



	function update_status_proses($status_baru,$id_order,$id_orderdetil)
	{
		cek_auth("auth_order",1);
		$statusx=array('proses'=>$status_baru);
		
		$this->Orderdetil_model->update($id_orderdetil,$statusx);
		$this->session->set_flashdata('message', 'proses order berhasil diupdate');
		
	 	$this->update_harga($id_order);
		
		
		
		
		
		redirect($this->alamat.'/index/'.$id_order);
	}


	
	/**
	 * Pindah ke halaman tambah orderdetil
	 */
	function add($id_order,$baru=null)
	{		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'tambah item order';
		$data['custom_view'] 		= 'orderdetil_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_order.'/'.$baru);
	
$data['link_produk'] 	=anchor_popup('produksi/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
  

    if($baru)
   { $data['link'] = array('link_back' => "<a href=javascript:window.close();  class='btn btn-success btn-lg'> batal</a>")	;							
   
  
		$orderdetil = $this->Konsumen_model->get_konsumen_by_id($id_order);
				
	
		
		$data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['jasa']		= $orderdetil->jasa;

		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']		= $orderdetil->jenis_pembayaran;
  

		

 

$data['konsx']=$id_order;   
   
   } else
   {		
  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	$data['default']['pengiriman'] 	= "";	


$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;


}


$spek=$this->Order_model->get_spesifikasi();
foreach ($spek as $spesifikasi)
$data['datafields'][$spesifikasi->nama]=array('judul'=>$spesifikasi->judul); 	



     $data['boleh_edit']=true;
      
      							
		$data['id_order'] 	= $id_order;			
		$data['baru'] 	= $baru;
    $this->load->view('template2', $data);
	}
	
	/**
	 * Proses tambah data orderdetil
	 */
	function add_process($id_order,$baru=null)
	{
	   
		$data['title'] 			= $this->title;


		$data['h2_title'] 		= 'tambah item order';

		$data['custom_view'] 		= 'orderdetil_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_order.'/'.$baru);
		$data['id_order'] 	= $id_order;	
		$data['baru'] 	= $baru;	
	
    
$data['link_produk'] 	=anchor_popup('produksi/produk/get_all/preview','<span class="glyphicon glyphicon-zoom-in"></span>', array('class' => 'link_tambah3','width'=>'1000'));
  	
         $data['boleh_edit']=true;
         
         
    if($baru)
    {
    	$data['link'] 			= array('link_back' => "<a href=javascript:window.close(); class=back> batal</a>")	;
    
    	$orderdetil = $this->Konsumen_model->get_konsumen_by_id($id_order);
				
	
		
		$data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['jasa']		= $orderdetil->jasa;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']		= $orderdetil->jenis_pembayaran;
    
   










    $data['konsx']=$id_order;   
    }
    else		
  {	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
	$data['default']['pengiriman'] 	= "";

$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;


		}
		
								

		$data['id_order'] 	= $id_order;			
		$data['baru'] 	= $baru;


	$spek=$this->Order_model->get_spesifikasi();
foreach ($spek as $spesifikasi)
$data['datafields'][$spesifikasi->nama]=array('judul'=>$spesifikasi->judul); 	
			







		// Set validation rules

		$this->form_validation->set_rules('barang', 'barang', 'required');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
		
    if($baru)
    {

$id_terakhirxx = $this->Order_model->get_last_id_order()->row();
$id_terakhir = $id_terakhirxx->id_order;

$id_terakhir++; 

$order = array( 'id_order' => $id_terakhir,
'id_konsumen' =>$id_order,
'ket_kirim' =>$this->input->post('ket_kirim'),

  	'pengiriman' 	=> $this->input->post('pengiriman'),		
  	'ongkir' 	=> $this->input->post('ongkir'),			
		'jasa'		=> $this->input->post('jasa'),			
		'invoice'		=> $this->input->post('invoice'),		
		'jenis_pembayaran'	=> $this->input->post('jenis_pembayaran')		

			);  


if($this->input->post('deathline'))
{$deathline=$this->input->post('deathline')." ".$this->input->post('jam').":00:00";

$order['deathline']=$deathline;



}




$this->Order_model->add($order);


		$orderdetil2 = array('jasa'=> $this->input->post('jasa'),
			
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran') );

			$this->Konsumen_model->update($id_order, $orderdetil2);



   $id_order=$id_terakhir; 
    }
    
  
    
    	// Persiapan data
			$orderdetil = array('id_order'=> $id_order,
			'barang'		=> $this->input->post('barang'),
			'jumlah'		=> $this->input->post('jumlah'),
			'harga'		=> $this->input->post('harga'),
			'tema'		=> $this->input->post('tema'),
			'spesifikasi'		=> $this->input->post('spesifikasi')
				
						);



foreach ($spek as $spesifikasi)
$orderdetil[$spesifikasi->nama]=$this->input->post($spesifikasi->nama); 	
			







			// Proses penyimpanan data di table orderdetil
			$this->Orderdetil_model->add($orderdetil);
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'Satu data order berhasil disimpan!');
			redirect($this->alamat.'/index/'.$id_order);
		}
		// Jika validasi gagal
		else
		{    	
			$this->load->view('template2', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update orderdetil
	 */
	function update($id_orderdetil,$id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'edit item order';
		$data['custom_view'] 		= 'orderdetil_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_orderdetil.'/'.$id_order);
		
  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


		$data['link_produk'] 	=anchor_popup('produksi/produk/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));
	
		// cari data dari database
		$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
				
	
		// Data untuk mengisi field2 form
		$data['default']['barang'] 	= $orderdetil->barang;		
		$data['default']['jumlah']		= $orderdetil->jumlah;
		$data['default']['tema']		= $orderdetil->tema;
		$data['default']['harga']		= $orderdetil->harga;
		$data['default']['spesifikasi']		= $orderdetil->spesifikasi;







		$data['default']['pengiriman'] 	= "";	




	$spek=$this->Order_model->get_spesifikasi();
foreach ($spek as $spesifikasi)
{ $nama_field=$spesifikasi->nama;
	$data['datafields'][$nama_field]=array('judul'=>$spesifikasi->judul,'data'=>$orderdetil->$nama_field); 	
			}			



$data['default']['nama_barang'] = $this->Produk_model->get_produk_by_id($orderdetil->barang)->nama;
$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;


	
    $data['boleh_edit']		= cek_auth("auth_order");		
	      

 		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function update_process($id_orderdetil,$id_order)
	{
		$data['title'] 			= $this->title;
		
		$data['h2_title'] 		= 'edit item order';

		$data['custom_view'] 		= 'orderdetil_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_orderdetil.'/'.$id_order);
		$data['link_produk'] 	=anchor_popup('produksi/produk/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));
										
		// Set validation rules


    $data['boleh_edit']		= cek_auth("auth_order");		


	$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
				
	
		// Data untuk mengisi field2 form
		$data['default']['barang'] 	= $orderdetil->barang;		
		$data['default']['jumlah']		= $orderdetil->jumlah;
		$data['default']['tema']		= $orderdetil->tema;
		$data['default']['harga']		= $orderdetil->harga;
		$data['default']['spesifikasi']		= $orderdetil->spesifikasi;
		
		$data['default']['pengiriman'] 	= "";	


$data['default']['nama_barang'] = $this->Produk_model->get_produk_by_id($orderdetil->barang)->nama;

	$spek=$this->Order_model->get_spesifikasi();
foreach ($spek as $spesifikasi)
{ $nama_field=$spesifikasi->nama;
	$data['datafields'][$nama_field]=array('judul'=>$spesifikasi->judul,'data'=>$orderdetil->$nama_field); 	
			}			




$data['konsx'] = $this->Order_model->get_order_by_id($id_order)->row()->id_konsumen;








		$this->form_validation->set_rules('barang', 'barang', 'required');
		$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$orderdetil = array('barang'	=> $this->input->post('barang'),
			'jumlah'	=> $this->input->post('jumlah'),
			'harga'	=> $this->input->post('harga'),
						'tema'	=> $this->input->post('tema'),
							'spesifikasi'		=> $this->input->post('spesifikasi')
          	);


foreach ($spek as $spesifikasi)
{ $nama_field=$spesifikasi->nama;
	$orderdetil[$nama_field]=$this->input->post($nama_field); 	
			}			








			$this->Orderdetil_model->update($id_orderdetil, $orderdetil);
			
			
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'Satu data orderdetil berhasil diupdate!');
			redirect($this->alamat.'/index/'.$id_order);
		}
		else
		{		

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


			$this->load->view('template2', $data);
		}
	}
	
	function pengiriman($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'instruksi pengiriman';
		$data['custom_view'] 		= 'pengiriman';
		$data['form_action']	= site_url($this->alamat.'/pengiriman_process/'.$id_order);
  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


										
	
		// cari data dari database
		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();
				
	
		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;		
		$data['default']['ongkir']		= $orderdetil->ongkir;
		$data['default']['ket_kirim']		= $orderdetil->ket_kirim;
  	$data['default']['pengiriman']		= $orderdetil->pengiriman;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']	= $orderdetil->jenis_pembayaran;	      
 $data['boleh_edit']		= cek_auth("auth_order");		



 		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function pengiriman_process($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > pengiriman';
		$data['custom_view'] 		= 'pengiriman';
		$data['form_action']	= site_url($this->alamat.'/pengiriman_process/'.$id_order);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
			
      
      
      
  		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();

		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;		
		$data['default']['ongkir']		= $orderdetil->ongkir;
		$data['default']['ket_kirim']		= $orderdetil->ket_kirim;
  	$data['default']['pengiriman']		= $orderdetil->pengiriman;
		$data['default']['invoice']		= $orderdetil->invoice;
		$data['default']['jenis_pembayaran']	= $orderdetil->jenis_pembayaran;	      
    $data['boleh_edit']		= cek_auth("auth_order");		

    
      
      
      
      
      
      							
		// Set validation rules
		$this->form_validation->set_rules('jasa', 'jasa', 'required');




$jasa= $this->input->post('jasa');
$ongkir= $this->input->post('ongkir');
$jenis_pembayaran= $this->input->post('jenis_pembayaran');



if($this->input->post('pengiriman')=="diambil")
$jasa=$ongkir="";
else if($this->input->post('pengiriman')=="diantar")
$jasa=$ongkir="";
else if($this->input->post('pengiriman')=="jasa")
$jenis_pembayaran="";




			// save data
			$orderdetil = array('jasa'=> $jasa,		
			'ongkir'	=> $ongkir,		
			'ket_kirim'	=> $this->input->post('ket_kirim'),
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $jenis_pembayaran, 
          	);

       


			$this->Order_model->update($id_order, $orderdetil);
			
	
	$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();	
		// Data untuk mengisi field2 form
	$id_konsumen 	= $orderdetil->id_konsumen;		
  
  
  
  		$orderdetil = array('jasa'=> $this->input->post('jasa'),
			
         
      'pengiriman'	=> $this->input->post('pengiriman'),
			'invoice'	=> $this->input->post('invoice'),
			'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran'), 
          	);


			$this->Konsumen_model->update($id_konsumen, $orderdetil);
			

  		
			$this->update_harga($id_order);
			
			
			$this->session->set_flashdata('message', 'data pengiriman berhasil diupdate!');
			redirect($this->alamat.'/index/'.$id_order);
	
	}
	
	
	function pengiriman_hasil($id_order)
	{
	$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'hasil pengiriman';
		$data['custom_view'] 		= 'pengiriman_hasil';
		$data['form_action']	= site_url($this->alamat.'/pengiriman_hasil_process/'.$id_order);
	
  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));


								
	









		// cari data dari database
		$orderdetil = $this->Order_model->get_order_by_id($id_order)->row();
				
				
$data['judul1']="";
$data['judul2']="";
$data['judul3']="";			
	if($orderdetil->pengiriman=="diambil")
{
$data['judul1']="yg menyerahkan";
$data['judul2']="yg ngambil";
$data['judul3']="surat jalan";
}
else if($orderdetil->pengiriman=="diantar")
{
$data['judul1']="yg mengirim";
$data['judul2']="yg menerima";
$data['judul3']="surat jalan";
}
else if($orderdetil->pengiriman=="jasa")
{
$data['judul1']="yg mengirim";
$data['judul2']="perkiraan sampai (hari)";
$data['judul3']="resi";
}

					
	
		// Data untuk mengisi field2 form
		$data['default']['jasa'] 	= $orderdetil->jasa;	
    $data['default']['pengiriman'] 	= $orderdetil->pengiriman;		
		$data['default']['resi']		= $orderdetil->resi;
		$data['default']['waktu']		= $orderdetil->waktu;
		$data['default']['koli']		= $orderdetil->koli;
		$data['default']['penerima']		= $orderdetil->penerima;
		$data['default']['tanggal_kirim']	= $orderdetil->tanggal_kirim;
 		$this->load->view('template2', $data);
 		
 		
 	
   	
 		
 		
	}
	
	/**
	 * Proses update data orderdetil
	 */
	function pengiriman_hasil_process($id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'hasil pengiriman';
		$data['custom_view'] 		= 'pengiriman_hasil';
		$data['form_action']	= site_url($this->alamat.'/pengiriman_hasil_process/'.$id_order);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('jasa', 'jasa', 'required');

		

			// save data
			$orderdetil = array(
      
      
 
			'resi'	=> $this->input->post('resi'),
			'penerima'	=> $this->input->post('penerima'),
			'waktu'	=> $this->input->post('waktu'),
		//	'ket_kirim'	=> $this->input->post('ket_kirim'),

			'koli'		=> $this->input->post('koli'),
         
      //'pengiriman'	=> $this->input->post('pengiriman'),
			//'invoice'	=> $this->input->post('invoice'),
			//'jenis_pembayaran'		=> $this->input->post('jenis_pembayaran'), 
          	);


if( $this->input->post('tanggal_kirim'))
			$orderdetil['tanggal_kirim']= $this->input->post('tanggal_kirim');	          
else
$orderdetil['tanggal_kirim']=null;


			$this->Order_model->update($id_order, $orderdetil);
			
			
			
			$this->session->set_flashdata('message', 'data pengiriman berhasil diupdate!');
			redirect($this->alamat.'/index/'.$id_order);
	
	}	
	


function jadwal($id_orderdetil,$id_order)
	{
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Orderdetil > jadwal';
		$data['custom_view'] 		= 'jadwal_form';
		$data['form_action']	= site_url($this->alamat.'/jadwal_process/'.$id_orderdetil.'/'.$id_order);

  	$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		

							
	
	$order = $this->Order_model->get_order_by_id($id_order)->row();
		



	$data_status = $this->Order_model->get_produksi();



		
		
				
			foreach	($data_status as $status)  

		   {

if($status->warna)
{
$nama_tabel=$status->nama;
	$ambil_tanggal=hitung_tanggal($order->$nama_tabel);
		

  	$data['datafield'][$status->nama]=array('deathline'=>$ambil_tanggal[0],'jam'=>$ambil_tanggal[1]);
		

}
}

	$deathline=hitung_tanggal($order->deathline);

if($deathline[0]){
	$data['default']['deathline'] 	= $deathline[0];				
	$data['default']['jam'] 	= $deathline[1];
	}



		$this->load->view('template2', $data);
	
	}
	
	

	
	/**
	 * Proses update data orderdetil
	 */
	function jadwal_process($id_orderdetil,$id_order)
	{
			


$order=array();

if($this->input->post('deathline'))
{$deathline=$this->input->post('deathline')." ".$this->input->post('jam').":00:00";
$order['deathline']=$deathline;}





$data_status = $this->Order_model->get_produksi();



	
			foreach	($data_status as $status)  

		   {

if($status->warna )
{


$data_jam=$this->input->post($status->nama);

if(empty($data_jam))
$cetak=null;
else
$cetak=$data_jam." ".$this->input->post('jam_'.$status->nama).":00:00";
$order[$status->nama]=$cetak;
}

}

if($order)
$this->Order_model->update($id_order, $order);
			
			$this->session->set_flashdata('message', 'Satu data orderdetil berhasil diupdate!');
			redirect($this->alamat.'/index/'.$id_order);








	}
	


	
function update_harga($id_order)
{
	
	$query = $this->Orderdetil_model->get_all($id_order);
		$orderdetil = $query->result();
		$num_rows = $query->num_rows();
		
		$harga=0;
			foreach ($orderdetil as $row)
			{
			
      if($row->status!='pending' and $row->status!='batal')	
			$harga+=$row->jumlah*$row->harga;	
				
			}
			
$order = $this->Order_model->get_order_by_id($id_order)->row();
			
$harga+=$order->ongkir;			
				
$order=array('total'=>$harga);
	$this->Order_model->update($id_order, $order);
}
	


function autocomplete()
{

	$query = $this->Produk_model->get_autocomplete($this->input->get('q'));
	$bahanbeli = $query->result();
	$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{

echo"[";
$i=0;
    foreach($bahanbeli as $row):
    if ($i!=0)
    echo",";   
        echo '{"name":"'.$row->nama.'","id":"'.$row->id_produk.'"}';
  
  $i++;
    endforeach;    
echo"]";
} 

}

function gambar($id_orderdetil,$id_order)

{

$data['title'] 			= $this->title;
		$data['h2_title'] 		= '';
		$data['custom_view'] 		= 'gambar';
		$data['form_action']	= site_url($this->alamat.'/upload/'.$id_orderdetil."/".$id_order);
		
	
		// cari data dari database
	

	$orderdetil = $this->Orderdetil_model->get_orderdetil_by_id($id_orderdetil);
				
	
		// Data untuk mengisi field2 form
		$data['gambar'] 	= $orderdetil->gambar;		
	




 		$this->load->view('template2', $data);




}


	function upload($id_orderdetil,$id_order)
	{




$nama_folder=floor($id_orderdetil/1000);

if(!is_dir(".".$this->alamat_gambar.$nama_folder))
mkdir(".".$this->alamat_gambar.$nama_folder);


		$config['upload_path'] = ".".$this->alamat_gambar.$nama_folder;
		$config['allowed_types'] = 'jpg';
		$config['max_size']	= '10000';
		$config['max_width']  = '10240';
		$config['max_height']  = '7680';
		$config['file_name']  = $id_orderdetil.'.jpg';
$config['overwrite']=true;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{






			$this->session->set_flashdata('message', 'gambar tidak berhasil diupload');
			redirect($this->alamat.'/gambar/'.$id_orderdetil."/".$id_order);
	


		}
		else
		{





$this->load->library('image_lib'); 





$config2['image_library'] = 'gd2';
$config2['source_image']	=".".$this->alamat_gambar.$nama_folder."/".$id_orderdetil.".jpg";
$config2['create_thumb'] = true;
$config2['maintain_ratio'] = TRUE;
$config2['width']	= 110;
$config2['height']	= 80;


$this->image_lib->initialize($config2); 

$this->image_lib->resize();

 $this->image_lib->clear();

$xx=$this->upload->data();
if($xx['image_width']>980 or $xx['image_height']>650)
{
$config3['image_library'] = 'gd2';
$config3['source_image']	=".".$this->alamat_gambar.$nama_folder."/".$id_orderdetil.".jpg";
$config3['maintain_ratio'] = TRUE;
$config3['width']	= 980;
$config3['height']	= 650;
$config3['create_thumb'] = false;


$this->image_lib->initialize($config3); 

$this->image_lib->resize();
}








	$orderdetil = array(
			'gambar'	=>$nama_folder."/".$id_orderdetil 

          	);
			$this->Orderdetil_model->update($id_orderdetil, $orderdetil);


	
			$this->session->set_flashdata('message', 'gambar berhasil diupload!');
			redirect($this->alamat.'/gambar/'.$id_orderdetil."/".$id_order);
	

		
		}
	}
	

}