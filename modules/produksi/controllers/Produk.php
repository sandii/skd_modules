<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Produk extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Produk';
			var $alamat = 'produksi/produk';

  
  function Produk()
	{
		parent::__construct();
		$this->load->model('Produk_model', '', TRUE);
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
  	$this->load->helper('fungsi');		

	  $this->load->library('cekker');
  $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data bahanbeli
	 */
	function get_all($tampilan="full",$tgl1="",$tgl2="")
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = 'main_tanggal';
		
	




 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


/**


*/




		// Load data
		
		//$query = $this->Produk_model->get_omzet_kategori($tgl1,$cari_tgl2);
		
		$query = $this->Produk_model->get_kategori();

		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();

		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

if(cek_auth("auth_order"))
$omzet="<div align=right>omzet</div>";else $omzet="";


			$this->table->set_heading('No', 'nama', $omzet);
			$i = 0;
		$totaly=0;

			foreach ($bahanbeli as $row)
			{


if(cek_auth("auth_order"))
{$totalx = $this->Produk_model->get_omzet_kategori($row->id_kategori,$tgl1,$cari_tgl2)->total;
$totalz=  "<div align=right>". number_format($totalx, 0, ',', '.');
$totaly+=$totalx;
}
else
$totalz="";



				$this->table->add_row(++$i,  
        
        anchor($this->alamat.'/detail/'.$tampilan."/".$row->id_kategori."/".$tgl1."/".$tgl2,$row->nama, array('class' => 'detail')),
    
      $totalz
        	);


			}

if(cek_auth("auth_order"))
			$this->table->add_row("",  
        
       "<div align=right><h5>total",
    
        "<div align=right>". number_format($totaly, 0, ',', '.')
        	);

			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
	
		
	if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
	
	


	
		function detail($tampilan="full",$id_akundetil,$tgl1="",$tgl2="")
	{
		$data = $this->data;
	
  
  /**



  */

		$bahanbeli = $this->Produk_model->get_kategori_by_id($id_akundetil);  
  	$data['h2_title'] = anchor($this->alamat.'/get_all/'.$tampilan."/".$tgl1."/".$tgl2,'produk').	
    
    " > ". $bahanbeli->nama;  ;
		$data['custom_view'] = 'main_tanggal';


if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


		
		// Load data
		$query = $this->Produk_model->get_all($id_akundetil);
		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			

if(cek_auth("auth_order"))
{$xx="<div align=right>omzet</div>";
$yy="action";

}
else
{
$xx="";
$yy="";


}

			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

			$this->table->set_heading('No', 'nama',$xx,$yy);
			$i = 0;
			
	$totaly=0;
			foreach ($bahanbeli as $row)
			{


	//	$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
		

if(cek_auth("auth_order"))
{
$edit=   anchor($this->alamat.'/update/'.$id_akundetil."/".$row->id_produk,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'));
$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
$totaly+=$totalx;
$totalz= "<div align=right>". number_format($totalx, 0, ',', '.');
}
else
{
$edit="";$totalz="";
}

				$this->table->add_row(++$i,  
        
      
        anchor('produksi/history/index/'.$tampilan."/".$row->id_produk,$row->nama, array('class' => 'detail'))	




        , $totalz, $edit

     );	
        
 
			}

if(cek_auth("auth_order"))

$this->table->add_row("","<h5 align=right>total", "<div align=right>". number_format($totaly, 0, ',', '.'));



			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
if(cek_auth("auth_order"))

		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$id_akundetil,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
		


	function delete($id_bahanbeli)
	{
		$this->Bahanbeli_model->delete($id_bahanbeli);
		$this->session->set_flashdata('message', '1 data bahanbeli berhasil dihapus');
		
		redirect('bahanbeli');
	}
	
	/**
	 * Pindah ke halaman tambah bahanbeli
	 */
	function add($id_akundetil)
	{		
		cek_auth("auth_marketing",1);
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_akundetil);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
		

		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data bahanbeli
	 */
	function add_process($id_akundetil)
	{
		cek_auth("auth_marketing",1);
	$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_akundetil);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		






		$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing'),
							
              	'id_kategori'		=>$id_akundetil								
						);
			// Proses penyimpanan data di table bahanbeli
			$this->Produk_model->add($bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil disimpan!');
			redirect($this->alamat.'/detail/full/'.$id_akundetil);
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update bahanbeli
	 */
	function update($id_akundetil,$id_bahanbeli)
	{
		cek_auth("auth_marketing",1);
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_akundetil."/".$id_bahanbeli);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
		// cari data dari database
		$bahanbeli = $this->Produk_model->get_produk_by_id($id_bahanbeli);
				
		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;




				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bahanbeli
	 */
	function update_process($id_akundetil,$id_bahanbeli)
	{

cek_auth("auth_marketing",1);
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_akundetil."/".$id_bahanbeli);
	

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/detail/full/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));



	  	
		// cari data dari database
		$bahanbeli = $this->Produk_model->get_produk_by_id($id_bahanbeli);
				
		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;


		// Data untuk mengisi field2 form
		$data['default']['nama']		= $bahanbeli->nama;
				


	$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[50]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing')
				
							
						);
			$this->Produk_model->update($id_bahanbeli,$bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil diupdate!');
			redirect($this->alamat.'/detail/full/'.$id_akundetil);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	



	/**
	 * Cek apakah $id_bahanbeli valid, agar tidak ganda
	 */

}
// END Bahanbeli Class

/* End of file bahanbeli.php */
/* Location: ./system/application/controllers/bahanbeli.php */
