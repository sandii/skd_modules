<?php

class Order extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $limit = 20;
  var $title="arsip per order";
			var $alamat = 'produksi/order';

  
  function Order()
	{
		parent::__construct();
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('Orderdetil_model', '', TRUE);
		
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Produk_model', '', TRUE);
	


// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
 

      $this->load->library('cekker');
  $this->cekker->cek($this->router->fetch_class());
     
  }

	function index()
	{
		// Hapus data session yang digunakan pada proses update data order
		$this->session->unset_userdata('id_order');
		$this->session->unset_userdata('tanggal');
			
			$this->get_last_ten_order();	
	}
	
	/**
	 * Menampilkan 10 data order terkini
	 */
	function get_last_ten_order($kons='',$tgl1="",$tgl2="",$bar="",$offset = 0)
	{
		$data=$this->data;
    $data['h2_title']=$this->title;

	  
		$data['custom_view'] = 'order';
		$data_status=array("pending","marketing","desain","setting","konveksi","cetak","finishing","siap");
	

	//	$offset = $this->uri->segment($uri_segment);
		
    		
     
     if($this->input->post('cari'))
     {
     $kons=$this->input->post('kons');
     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $bar=$this->input->post('bar');

     $offset=0;
     }
      
   	
   
      
    if(empty($kons) or $kons=="semua") {$kons="semua";$cari_kons=""; }
    else $cari_kons=$kons;
    if(empty($tgl1)or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
   if(empty($bar)or $bar=="semua" ) {$bar="semua";$cari_bar="";}
   else $cari_bar=$bar;
    
  


      $data["kons"]=$kons;
	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
    $data["bar"]=$bar;	





if(!empty($bar) and $bar!="semua")
$data['nama_barang'] = $this->Produk_model->get_produk_by_id($bar)->nama;


if(!empty($kons) and $kons!="semua")
$data['nama_konsumen'] = $this->Konsumen_model->get_konsumen_by_id($kons)->perusahaan;

		
		// Load data dari tabel order
		
		
		
		$orders = $this->Order_model->arsip($this->limit, $offset,$cari_kons,$cari_tgl1,$cari_tgl2,$cari_bar)->result();
	
		$num_rows = $this->Order_model->arsip(0, 0,$cari_kons,$cari_tgl1,$cari_tgl2,$cari_bar)->num_rows();


	
    	
    	
		
		if ($num_rows > 0) // Jika query menghasilkan data
		{
			// Membuat pagination			
			$uri_segment = 8;
	       $config['base_url'] = site_url($this->alamat.'/get_last_ten_order/'.$kons.'/'.$tgl1.'/'.$tgl2.'/'.$bar.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
					$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';





			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			


			// Set template tabel, untuk efek selang-seling tiap baris
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" width=100% class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			// Set heading untuk tabel
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Hari,tgl','Perusahaan','order','total','kekurangan');
			
			// Penomoran baris data
			$i = 0 + $offset;
			
			foreach ($orders as $order)
			{
				// Konversi hari dan tanggal ke dalam format Indonesia
				$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
				$hr = date('w', strtotime($order->tanggal));
				$hari = $hari_array[$hr];
				$tgl = date('d-m-Y', strtotime($order->tanggal));
				$hr_tgl = "$hari, $tgl";
				
      	
				
			$totalx=number_format($order->total, 0, ',', '.');	
				
				
				$selisih=($order->total)-($order->pembayaran)-($order->diskon);
				if($selisih>0)
				{
				$selisih=number_format($selisih, 0, ',', '.');	
				$pembayaran=$selisih;
				}
        else
				$pembayaran="lunas";
				
		
  
		$query2 = $this->Orderdetil_model->get_all($order->id_order);
		$orderdetil = $query2->result();
		$num_rows2 = $query2->num_rows();
		
		
		$isi_order="";
	  	if ($num_rows2 > 0)
		{  
  
  $xx=0;
  foreach ($orderdetil as $row2)
			{
  
  if($xx!=0)
  $isi_order.=", ";
  

$nama_barang = $this->Produk_model->get_produk_by_id($row2->barang)->nama;

  $isi_order.= $nama_barang;
    
    $xx++;
    }
    	}	
				// Penyusunan data baris per baris, perhatikan pembuatan link untuk updat dan delete
		


				$this->table->add_row(++$i, $hr_tgl,$order->perusahaan,
        
        	anchor_popup('produksi/orderdetil/index/'.$order->id_order,$isi_order,array('class' => 'detail','width'=>1000)),
        "<div align=right>".$totalx,
									
                    anchor_popup('produksi/pembayaran/detil/'.$order->id_order,$pembayaran,array('class' => 'update','width'=>1000))

    //http://localhost/tafioproject/index.php/pembayaran_detil/index/5537                
                    );
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data order!';
		}		
		
	
	
		// Load default view
		$this->load->view('template', $data);
	}
	

	

	
	/**
	 * Menghapus data order
	 */

	
	/**
	 * Berpindah ke form untuk entry data ordersi baru
	 */

}

