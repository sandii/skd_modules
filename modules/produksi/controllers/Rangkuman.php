<?php
/**
 * Order Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Rangkuman extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $limit = 10;
  var $title="produksi";

  
  function Rangkuman()
	{
		parent::__construct();
		$this->load->model('Order_model', '', TRUE);
		$this->load->model('Orderdetil_model', '', TRUE);
		$this->load->model('konsumen/Konsumen_model', '', TRUE);
		$this->load->model('Produk_model', '', TRUE);		
			$this->load->model('Pemroses_model', '', TRUE);
				$this->load->model('sdm/Pegawai_model', '', TRUE);
		$this->load->helper('autorun');
		
$this->load->helper('fungsi');



    $this->data['nama']=$this->session->userdata('nama');

    $this->data['title']=$this->title;
 
  
      $this->load->library('cekker');
  $this->cekker->cek($this->router->fetch_class());
      $this->router->fetch_method();
  }
	
	
	
	
	function index()
	{
		
		$data=$this->data;
    $data['h2_title']=$this->title;

		

		$data['custom_view'] = 'rangkuman';
		$data['refresh'] = TRUE;


	
	$data['status']="semua";

$xx=0;



	$ordersx = $this->Order_model->get_produksi();


$zz=0;

	foreach	($ordersx as $row)  
	{	


$status=$row->nama;

		$statusy="'$status'";

if(!empty($row->jadwal))
	$jadwal=$status;
else
$jadwal=null;

		$orders = $this->Order_model->get_last_ten_order($statusy,$jadwal)->result();
		
		$nama_order="";
		$id_orderx=0;
   
    
$tampilan='  <div class="list-group">
         <a href="#" class="list-group-item active"><span class="glyphicon '.$row->icon.'" aria-hidden="true"></span>
              '.$row->judul.'
            
 </a>';


    $yy=1;
  $terakhir=false;
    if ($orders) // Jika query menghasilkan data
		{
		
			$orders2=$orders;
			$order=current($orders);
			$order2=current($orders2);
			$order2=next($orders2);

			while (true)
			{

		
				if(!$order)
				break;	
$order=current($orders);

if($order2)
$id_orderx=$order2->id_order;
else
$id_orderx="aaa";

if($order->id_order!=$id_orderx)

$terakhir=true;
else
$terakhir=false;
				$nama_perusahaan=$order->perusahaan;
	
  
 if($order->proses)
{
$warna=$this->Pemroses_model->get_warna($order->proses);
$prod="<span class='label label-info'>".$order->proses."</span>";
}
else
$prod="";


if($terakhir)
$koma="";
else
$koma=","; 
 

	$nama_barang = $this->Produk_model->get_produk_by_id($order->barang)->nama;
				
        $nama_order.= "&nbsp;<span class='text-info'> ".$prod.$nama_barang.$koma."</span>";

  $timeline="";

		if($row->jadwal)
		{
		

		
  $akhir=!empty($order->status_unix)?$order->status_unix:'';	


  $timeline=hitung_sisa($akhir);
    }				    
         
  
 if($terakhir)
{	


if(!empty($order->id_ar))
{
$warna2=$this->Pegawai_model->get_pegawai_by_kode($order->id_ar);

$warnax=!empty($warna2->warna)?$warna2->warna:"";

 $ar="<span class='label label-default'><font color=#".$warnax.">".$order->id_ar."</font></span>";
}
else
$ar="";

$dp_min=floor($order->total/10*3);

if($order->pembayaran>=$order->total)

//$bayar="<div id=lns><font color=green></font></div>";
$bayar=' <span class="label label-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>';
else if ($order->pembayaran>=$dp_min)
$bayar=' <span class="label label-warning"><span class="glyphicon glyphicon-hourglass" aria-hidden="true"></span></span>';

else
$bayar=' <span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>';




if($order->total<=5000000)
$warna='<p  class="pull-left">';
else if($order->total<=15000000)
$warna='<p class="text-success pull-left">';
else
$warna='<p class="text-danger  pull-left">';


$isi_tampilan=$warna. $yy.". ".$nama_perusahaan." </p>  "

.$nama_order."<div class=pull-right>".
        
$timeline.$ar.$bayar."</div>";


$tampilan.=anchor_popup('produksi/orderdetil/index/'.$order->id_order,$isi_tampilan,array('width'=>2000,'height'=>900,'class'=>'list-group-item'));
$nama_order="";


$zz++;
$yy++;

	}  
	$data['total_baris'] =$zz;
  
  	$order=next($orders);
    	$order2=next($orders2);
     	
      }

		}
		
		$data['tablex'][$xx] = $tampilan."</div>";
			
	$xx++;	
		}	


	
		// Load default view
		$this->load->view('template', $data);
	}
	

}
