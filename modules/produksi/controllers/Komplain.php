<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Komplain extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'komplain';
		var $alamat = 'produksi/komplain';

  
  function komplain()
	{
		parent::__construct();
		$this->load->model('komplain_model', '', TRUE);
		$this->load->model('order_model', '', TRUE);
		$this->load->model('Produk_model', '', TRUE);

	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun = "")
	{
	
		$data=$this->data;
    $data['h2_title']=$this->title;

		
if (empty($tahun))
	$tahun=date("Y");


    $mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor($this->alamat.'/index/'.$i,$i,array('class' => 'tahun'))."</li>";
    else
     $pagination.="<li class=active><a href=#>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		
		
		// Load data
		$query = $this->komplain_model->get_all($tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();


		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'konsumen','order','komplain', 'bagian','evaluasi','solusi'," ");
	
			
			foreach ($siswa as $row)
			{
				
				

		$orderxx= $this->order_model->get_orderdetil($row->id_orderdetil);
		

				
				
				if(!empty($orderxx->id_konsumen))
				$link_konsumen=anchor_popup('konsumen/konsumendetil/index/'.$orderxx->id_konsumen,$orderxx->perusahaan,array('class' => 'detail') );
        else
        $link_konsumen="dihapus";


				if(!empty($orderxx->id_konsumen))
				$link_konsumen=anchor_popup('konsumen/konsumendetil/index/'.$orderxx->id_konsumen,$orderxx->perusahaan,array('class' => 'detail') );
        else
        $link_konsumen="dihapus";


		if(!empty($orderxx->id_order))
		{ 	$nama_barang = $this->Produk_model->get_produk_by_id($orderxx->barang)->nama;
				$link_order=anchor_popup('produksi/orderdetil/index/'.$orderxx->id_order,$nama_barang,array('class' => 'detail') );
      }

        else
        $link_order="dihapus";


								
				$this->table->add_row( $row->tanggal,$link_konsumen
						      ,
						      	
$link_order,
	      
						$row->komplain, $row->bagian,$row->evaluasi,$row->solusi,
									anchor($this->alamat.'/update/'.$row->id_komplain,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'))									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data komplain!';
		}		
		
		
		// Load view
		$this->load->view('template', $data);
	}




	function add($id_order,$id_orderdetil)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'komplain_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_order."/".$id_orderdetil);
			
  	$data['link'] 			= array('link_back' => anchor('produksi/orderdetil/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));



		
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_order,$id_orderdetil)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'komplain > Tambah Data';
		$data['custom_view'] 		= 'komplain_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_order."/".$id_orderdetil);
  	$data['link'] 			= array('link_back' => anchor('produksi/orderdetil/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
										
		// data kelas untuk dropdown menu
	
		// Set validation rules
		$this->form_validation->set_rules('komplain', 'required');
    
    
		
		
		
		if ($this->form_validation->run() == TRUE)
		{
		


	
    $commentx="<font color=red>".$_SESSION['nama']." yg menginput</font>";
    
    if($this->input->post('evaluasi'))
    $commentx.="<br>".$this->input->post('evaluasi');

    
    

	
		
			// save data
			$uang = array('id_orderdetil' => $id_orderdetil,
			'komplain' 		=> $this->input->post('komplain'),
			'bagian' 		=> $this->input->post('bagian'),
			'solusi' 		=> $this->input->post('solusi'),

			'tanggal'=> date("Ymd"),
			'evaluasi'		=> $commentx
						);
			$this->komplain_model->add($uang);
			
			$this->session->set_flashdata('message', 'data komplain berhasil disimpan!');
			redirect('produksi/orderdetil/index/'.$id_order);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	

	
	
function update($id_komplain,$id_order=null)
	{
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.'> Update';
		$data['custom_view'] 		='komplain_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_komplain."/".$id_order);
		
		
		if($id_order)
		$data['link'] 			= array('link_back' => anchor('produksi/orderdetil/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);		
		else
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		
		
		// cari data dari database
		$konsumen = $this->komplain_model->get_komplain_by_id($id_komplain);
		

		
		// Data untuk mengisi field2 form
		$data['default']['komplain'] 		= $konsumen->komplain;
		$data['default']['evaluasi'] 		= $konsumen->evaluasi;		
		$data['default']['bagian']	= "$konsumen->bagian";
		$data['default']['solusi'] 		= $konsumen->solusi;
		
		if($id_order)		
		$this->load->view('template2', $data);
		else
		$this->load->view('template', $data);		
	}
	
	/**
	 * Proses update data konsumen
	 */
	function update_process($id_komplain,$id_order=null)
	{
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.'> Update';
		$data['custom_view'] 		= $this->alamat.'/add';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_komplain."/".$id_order);
		
		
		if($id_order)
		$data['link'] 			= array('link_back' => anchor('produksi/orderdetil/index/'.$id_order,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);		
		else
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
    
    
    								
	
		$this->form_validation->set_rules('komplain', 'required');
		
		// jika proses validasi sukses, maka lanjut mengupdate data
		if ($this->form_validation->run() == TRUE)
		{

		$konsumen = $this->komplain_model->get_komplain_by_id($id_komplain);

		
$yy=$this->input->post('evaluasi');		
$evaluasi=$konsumen->evaluasi;
$bagianx=$konsumen->bagian;


if($bagianx!=$this->input->post('bagian'))
{
	$evaluasi.="<br><font color=blue>".$_SESSION['nama']." mengubah bagian menjadi".$this->input->post('bagian')." </font>";	
}


		
if(!empty($yy))
$evaluasi.="<br><font color=red>".$_SESSION['nama'].": </font>".$yy;



			// save data
			$absen = array(
							'komplain'		=> $this->input->post('komplain'),
				'bagian'=> $this->input->post('bagian'),
				'solusi'=> $this->input->post('solusi'),

					'evaluasi'=>$evaluasi
          
          
          	);
			$this->komplain_model->update($id_komplain, $absen);
			$this->session->set_flashdata('message', 'Satu data komplain berhasil diupdate!');
			
			if($id_order)
			redirect('produksi/orderdetil/index/'.$id_order);
			else
			redirect($this->alamat.'');
			
		}
		else
		{
		if($id_order)
			$this->load->view('template2', $data);
else		
					$this->load->view('template', $data);

		
		}
	}
	
		
	

}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
