<style type="text/css">@import url("<?php echo base_url() . 'css/jquery-ui-1.8.14.custom.css'; ?>");</style>
<script type="text/javascript" src="<?php echo base_url() . 'js/jquery-1.5.1.min.js'; ?>"></script> 
<script type="text/javascript" src="<?php echo base_url() . 'js/jquery-ui-1.8.14.custom.min.js'; ?>"></script> 

    <script type="text/javascript">
			$(function(){				
				$('#datepicker').datepicker({dateFormat:"yy-mm-dd"});

		
			});
		</script>
<?php 
	
	echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';

	$flashmessage = $this->session->flashdata('message');
	echo ! empty($flashmessage) ? '<p class="message">' . $flashmessage . '</p>': '';
?>

<form name="order_form" method="post" action="<?php echo $form_action; ?>"
onsubmit="document.getElementById('submit').disabled=true;
document.getElementById('submit').value='proses';">
	
	
		<p>
		<label for="id_konsumen">konsumen:</label>
      <?=$perusahaan?>
     	</p>
		<p>
		<label for="id_konsumen">total tagihan: </label>
    Rp.  <?=number_format($total, 0, ',', '.')?> <input type=hidden name=total value='<? echo !empty($total)?$total:0; ?>'> 
     	</p>
<?php



?>

<p>

		<label for="Deathline">diskon:</label>

	 Rp.  <?=number_format($default['diskon'], 0, ',', '.')?> 
</p>


		<p>
		<label for="id_konsumen">kekurangan:</label>
    Rp.  <?=number_format($kekurangan, 0, ',', '.')?> <input type=hidden name=pembayaran value='<? echo !empty($pembayaran)?$pembayaran:0; ?>'> 
    <input type=hidden name=kekurangan value='<? echo !empty($kekurangan)?$kekurangan:0; ?>'> 
    
     	</p>
	
<p>
		<label for="Deathline">Pembayaran sekarang:</label>
		Rp. <input type="text" class="form-control" name="bayar" size="10" value="<?php echo set_value('bayar', isset($default['bayar']) ? $default['bayar'] : ''); ?>" />
		<?php echo form_error('bayar','<div class="alert alert-danger" role="alert">','</div>');?>			
</p>


<p>
		<label for="Deathline">masuk ke kas:</label>

        <?php echo form_dropdown('ke', $options_ke, isset($default['ke']) ? $default['ke'] : '',"class=form-control"); ?>
		
</p>

<p>
		<label for="Deathline">tanggal masuk:</label>
	 <input type=text id="datepicker" class=form-control name=tanggal  value=<?php echo set_value('tanggal', isset($default['tanggal']) ? $default['tanggal'] : date("Y-m-d")); ?> >
</p>


  
<p>
		<label for="Deathline">Keterangan:</label>
		<input type="text" class="form-control" name="keterangan" size="50" value="<?php echo set_value('keterangan', isset($default['keterangan']) ? $default['keterangan'] : ''); ?>"  />
		
	</p>
<p>


		<label for="Deathline">&nbsp;</label>
	<? echo ! empty($link_batal) ? $link_batal: '';	?>
	</p>
 
			
	<?php echo form_error('order','<div class="alert alert-danger" role="alert">','</div>');?>

	<p>
		<input type="submit" name="submit" class='btn btn-info' id="submit" value=" Simpan " />
	</p>
</form>

