<?php
/**
 * User_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pemroses_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Pemroses_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'pemroses';
	
	/**
	 * Mendapatkan semua data user, diurutkan berdasarkan id_user
	 */
	function get_all()
	{
		$this->db->order_by('id');
		return $this->db->get($this->table);
	}
	
		function get_pemroses_by_id($id_pemroses)
	{
		return $this->db->get_where($this->table, array('id' => $id_pemroses))->row();
	}
	
		function get_warna($id_pemroses)
	{
		return $this->db->get_where($this->table, array('nama' => $id_pemroses))->row();
	}
	
	
	
		function update($id_marketing, $marketing)
	{
		$this->db->where('id', $id_marketing);
		$this->db->update($this->table, $marketing);
	}
	
	
	
		function add($marketing)
	{
		$this->db->insert($this->table, $marketing);
	}
	
		function delete($id_marketing)
	{
		$this->db->delete($this->table, array('id' => $id_marketing));
	}
	
	
		function get_pemroses()
	{
		$this->db->select('*');
				$this->db->order_by('id');
		return $this->db->get($this->table);
		
		
		
	}

}
// END Siswa_model Class

/* End of file user_model.php */
/* Location: ./system/application/models/marketing_model.php */
