<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pembayaran_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Pembayaran_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'k_pembayaran';
	
	/**
	 * Mendapatkan semua data pembayaran, diurutkan berdasarkan id_pembayaran
	 */
	function get_pembayaran()
	{
		$this->db->order_by('id_pembayaran');
		return $this->db->get('pembayaran');
	}

  
  function get_rangkuman($limit, $offset,$tgl1,$tgl2=null)
	{
		$this->db->select('*');
		$this->db->from($this->table);	
		
	$this->db->where("status","approve");
		
	$this->db->where("tanggal >=",$tgl1);

			
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	

if($limit)
			$this->db->limit($limit, $offset);	
				$this->db->order_by('tanggal', 'desc');	

	

		return $this->db->get();
	}
	
	

		function get_pembayaran_by_order($id_order)
	{
		return $this->db->get_where($this->table, array('id_order' => $id_order,'status'=>'confirm'), 1)->row();
	}
	
		function get_pembayaran_by_order2($id_order)
	{
	
    	$this->db->where('id_order', $id_order);
    	$this->db->where('status', 'approve');
  		return $this->db->get($this->table);
  }	
	
	
	/**
	 * Mendapatkan data sebuah pembayaran
	 */
	function get_pembayaran_by_id($id_pembayaran)
	{
		return $this->db->get_where($this->table, array('id_pembayaran' => $id_pembayaran), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_pembayaran');
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data pembayaran
	 */
	function delete($id_pembayaran)
	{
		$this->db->delete($this->table, array('id_pembayaran' => $id_pembayaran));
	}
	
	/**
	 * Tambah data pembayaran
	 */
	function add($pembayaran)
	{
		$this->db->insert($this->table, $pembayaran);
	}
	
	/**
	 * Update data pembayaran
	 */
	function update($id_pembayaran, $pembayaran)
	{
		$this->db->where('id_pembayaran', $id_pembayaran);
		$this->db->update($this->table, $pembayaran);
	}
	
	/**
	 * Validasi agar tidak ada pembayarand dengan id ganda
	 */
	function valid_id($id_pembayaran)
	{
		$query = $this->db->get_where($this->table, array('id_pembayaran' => $id_pembayaran));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file pembayaran_model.php */
/* Location: ./system/application/models/pembayaran_model.php */
