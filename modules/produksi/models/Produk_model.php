<?php
/**
 * User_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Produk_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Produk_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'produk';
	var $table2 = 'kategori';
	
	/**
	 * Mendapatkan semua data user, diurutkan berdasarkan id_user
	 */
	function get_all($kategori)
	{
		
	$this->db->where('id_kategori', $kategori);
		$this->db->order_by('id_produk');

		return $this->db->get($this->table);
	}
	
function get_kategori()
	{
		$this->db->order_by('id_kategori');
		return $this->db->get($this->table2);
	}
	

function get_omzet_kategori($id_kategori,$tgl1,$tgl2)
	{

$this->db->select ('sum(jumlah*harga) as total');
$this->db->from('orderx,orderdetil,produk');


		$this->db->where('orderx.id_order = orderdetil.id_order');
		$this->db->where('produk.id_kategori',$id_kategori);
		$this->db->where('produk.id_produk = barang');
		$this->db->where('status != "batal"');
		$this->db->where('status != "pending"');



	$this->db->where("tanggal >'".$tgl1."'");	
		

		if(!empty($tgl2))
		{
 	   $tgl2.=" 23:59:59";
	   $this->db->where("tanggal <'".$tgl2."'");	
}



		return $this->db->get()->row();
	








	}

	function get_omzet_barang($id_produk,$tgl1,$tgl2)
	{

$this->db->select ('sum(jumlah*harga) as total');
$this->db->from('orderx,orderdetil');


		$this->db->where('orderx.id_order = orderdetil.id_order');
	
	$this->db->where('barang',$id_produk);	


		$this->db->where('status != "batal"');
		$this->db->where('status != "pending"');



	$this->db->where("tanggal >'".$tgl1."'");	
		

		if(!empty($tgl2))
		{
 	   $tgl2.=" 23:59:59";
	   $this->db->where("tanggal <'".$tgl2."'");	
}


		return $this->db->get()->row();
	








	}


		function get_produk_by_id($id_produk)
	{
		return $this->db->get_where($this->table, array('id_produk' => $id_produk))->row();
	}

		function get_kategori_by_id($kategori)
	{
		return $this->db->get_where($this->table2, array('id_kategori' => $kategori))->row();
	}


	
		function update($id_marketing, $marketing)
	{
		$this->db->where('id_produk', $id_marketing);
		$this->db->update($this->table, $marketing);
	}
	
	
function get_autocomplete($q)
	{
	
	  		$this->db->where("nama like '%$q%' ");

    
    $this->db->order_by('nama');
		
		
		return $this->db->get($this->table);
	}


	
		function add($marketing)
	{
		$this->db->insert($this->table, $marketing);
	}
	
		function delete($id_marketing)
	{
		$this->db->delete($this->table, array('id_produk' => $id_marketing));
	}
	
	
		function get_produk()
	{
		$this->db->select('*');
				$this->db->order_by('id_produk');
		return $this->db->get($this->table);
		
		
		
	}

}
// END Siswa_model Class

/* End of file user_model.php */
/* Location: ./system/application/models/marketing_model.php */
