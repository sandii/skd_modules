<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Satuan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Satuan_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'satuan';
	
	/**
	 * Mendapatkan semua data satuan, diurutkan berdasarkan id_satuan
	 */
	function get_satuan()
	{
		$this->db->order_by('id_satuan');
		return $this->db->get('satuan');
	}
	
	/**
	 * Mendapatkan data sebuah satuan
	 */
	function get_satuan_by_id($id_satuan)
	{
		return $this->db->get_where($this->table, array('id_satuan' => $id_satuan), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_satuan');
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data satuan
	 */
	function delete($id_satuan)
	{
		$this->db->delete($this->table, array('id_satuan' => $id_satuan));
	}
	
	/**
	 * Tambah data satuan
	 */
	function add($satuan)
	{
		$this->db->insert($this->table, $satuan);
	}
	
	/**
	 * Update data satuan
	 */
	function update($id_satuan, $satuan)
	{
		$this->db->where('id_satuan', $id_satuan);
		$this->db->update($this->table, $satuan);
	}
	
	/**
	 * Validasi agar tidak ada satuand dengan id ganda
	 */
	function valid_id($id_satuan)
	{
		$query = $this->db->get_where($this->table, array('id_satuan' => $id_satuan));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file satuan_model.php */
/* Location: ./system/application/models/satuan_model.php */
