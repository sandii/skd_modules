<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Supplier_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Supplier_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'k_supplier';
	
	/**
	 * Mendapatkan semua data supplier, diurutkan berdasarkan id_supplier
	 */
	function get_supplier()
	{
		$this->db->order_by('id_supplier');
		return $this->db->get('supplier');
	}

	function get_supplier_kategori()
	{

		return $this->db->get('k_supplier_kategori')->result();
	}


	
	/**
	 * Mendapatkan data sebuah supplier
	 */
	function get_supplier_by_id($id_supplier)
	{
		return $this->db->get_where($this->table, array('id_supplier' => $id_supplier), 1)->row();
	}
	
function get_kategori_by_id($id_supplier)
	{
		return $this->db->get_where("k_supplier_kategori", array('id_supplier_kategori' => $id_supplier), 1)->row();
	}



	function get_all($nama=null,$id_supplier_kategori=null,$limit=null, $offset=null)
	{
		$this->db->order_by('nama');
	
if(!empty($id_supplier_kategori))
		$this->db->where('id_supplier_kategori', $id_supplier_kategori);


if(!empty($nama))
		$this->db->where("nama like '%$nama%'");


if($limit)
			$this->db->limit($limit, $offset);	
				



				$this->db->order_by('nama', 'desc');	




		return $this->db->get($this->table);
	}
	
	function get_supplier_q($q)
	{
		$this->db->order_by('nama');
		$this->db->where("nama like '%$q%'");
		return $this->db->get($this->table);
	}
	
	
	
	/**
	 * Menghapus sebuah data supplier
	 */
	function delete($id_supplier)
	{
		$this->db->delete($this->table, array('id_supplier' => $id_supplier));
	}
	
	/**
	 * Tambah data supplier
	 */
	function add($supplier)
	{
		$this->db->insert($this->table, $supplier);
	}
	
	/**
	 * Update data supplier
	 */
	function update($id_supplier, $supplier)
	{
		$this->db->where('id_supplier', $id_supplier);
		$this->db->update($this->table, $supplier);
	}
	
	/**
	 * Validasi agar tidak ada supplierd dengan id ganda
	 */
	function valid_id($id_supplier)
	{
		$query = $this->db->get_where($this->table, array('id_supplier' => $id_supplier));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file supplier_model.php */
/* Location: ./system/application/models/supplier_model.php */
