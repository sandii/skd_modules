<?php
/**
 * Menu_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Hutang_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Hutang_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'k_hutang';
		var $table2 = 'k_hutangdetil';
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */
	
  
function hitung_hutang($tahun,$status)
	{
	

		$this->db->select ('sum(total-pembayaran) as total_hutang');

		$this->db->from($this->table);
$tgl1=$tahun."-1-1";
		$this->db->where("tanggal >='".$tgl1."'");


		$this->db->where("status",$status);
		
 	$tgl2=$tahun."-12-31"; 
	   $this->db->where("tanggal <='".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}
	


  	function get_all($where)
	{

	  
 
 


	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where($where);		
		$this->db->order_by('tanggal desc');
		
		
		return $this->db->get();
  }

  function get_rangkuman($limit, $offset,$tgl1,$tgl2=null)
	{
		$this->db->select('*,k_hutangdetil.keterangan as keterangan_detil');
		$this->db->from(array($this->table2,$this->table));	
		
		
	$this->db->where("k_hutangdetil.tanggal >=",$tgl1);
	$this->db->where("k_hutang.id_hutang = k_hutangdetil.id_hutang");
			
			if(!empty($tgl2))
			$this->db->where("k_hutangdetil.tanggal <=",$tgl2);	

if($limit)
			$this->db->limit($limit, $offset);	
				$this->db->order_by('k_hutangdetil.tanggal', 'desc');	

	

		return $this->db->get();
	}

function count_all_num_rows($tgl1,$tgl2=null)
	{
		$this->db->select('*');
		$this->db->from($this->table);		
			$this->db->where("tanggal >=",$tgl1);
			
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	
	
		return $this->db->get()->num_rows();
	}	
	
	


  	function get_history($limit, $offset,$id_barang)
	{


$this->db->distinct();	
$this->db->select (array('tanggal','id_barang','id_supplier','harga','jumlah'));
	$this->db->from("k_hutang,k_hutangdetil");	
	

			$this->db->limit($limit, $offset);	


		$this->db->where('id_barang', $id_barang);
		$this->db->where("k_hutang.id_hutang = k_hutangdetil.id_hutang");
	


			$this->db->order_by('id_hutangdetil', 'desc');	
		return $this->db->get();


	}
	function count_history($id_barang)
	{
$this->db->distinct();	
$this->db->select (array('tanggal','id_barang','id_supplier','harga','jumlah'));


	$this->db->from("k_hutang,k_hutangdetil");	

	


		$this->db->where('id_barang', $id_barang);

		$this->db->where("k_hutang.id_hutang = k_hutangdetil.id_hutang");


	
			$this->db->order_by('id_hutangdetil', 'desc');	
		return $this->db->get()->num_rows();

		
	}
	

	


		function arsip($limit, $offset,$tgl1="",$tgl2="",$sup="",$bar="")
	{
	


$this->db->distinct();	
$this->db->select (array('k_hutang.id_hutang','tanggal','tanggal_input','id_supplier','total','nota'));

		$this->db->from('k_hutang,k_hutangdetil');
		$this->db->limit($limit, $offset);
		$this->db->where('k_hutang.id_hutang = k_hutangdetil.id_hutang');	

     if(!empty($sup))
		$this->db->where('id_supplier',$sup);	

  if(!empty($bar))
		$this->db->where('id_barang',$bar);	


    if(!empty($tgl1))
		$this->db->where("tanggal >='".$tgl1."'");	
    if(!empty($tgl2))
    {
 		$this->db->where("tanggal <='".$tgl2."'");	
}
		
		$this->db->order_by('k_hutang.tanggal', 'desc');

		return $this->db->get();
	}

	function count_arsip($tgl1="",$tgl2="",$sup="",$bar="")
	{
	


$this->db->distinct();	
$this->db->select (array('k_hutang.id_hutang','tanggal','tanggal_input','id_supplier','total'));

		$this->db->from('k_hutang,k_hutangdetil');

		$this->db->where('k_hutang.id_hutang = k_hutangdetil.id_hutang');	

     if(!empty($sup))
			$this->db->where('id_supplier',$sup);	

    if(!empty($tgl1))
		$this->db->where("tanggal >='".$tgl1."'");	
    if(!empty($tgl2))
    {
 		$this->db->where("tanggal <='".$tgl2."'");	
}

if(!empty($bar))
		$this->db->where('id_barang',$bar);	
		

		return $this->db->get()->num_rows();
	}

	



	
	 	function get_hutang_by_id($tahun)
	{
		$this->db->select('*');
		$this->db->from($this->table);	
				$this->db->where('id_hutang', $tahun);
	
	
		return $this->db->get()->row();
	}
	
	
	
	
	
  	function get_detil($tahun)
	{
		$this->db->select('*');
		$this->db->from($this->table2);	
		
		$this->db->where('id_hutang', $tahun);
	
		
		return $this->db->get();
	}




	
  
  
  
  	function count_all()
	{
		return $this->db->count_all($this->table);
	}

	
	
	/**
	 * Tampilkan 10 baris menu terkini, diurutkan berdasarkan tanggal (Descending)
	 */

	
	
	function get_menu_by_tanggal($selisih)
	{

		
		$this->db->select ('*');
		$this->db->from('menu');
		$this->db->limit($selisih);
		$this->db->order_by('tanggal,id_menu', 'asc');	
	
  	return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data menu
	 */
	function delete($id_menu)
	{

		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel menu
	 */
	function add_hutang_detil($menu)
	{
		$this->db->insert($this->table2, $menu);
	}

	function add($menu)
	{
		$this->db->insert($this->table, $menu);
	}
	
	/**
	 * Dapatkan data menu dengan id_menu tertentu, untuk proses update
	 */
	
	function get_terakhir()
	{
			$this->db->select ('*');
		$this->db->from($this->table);
		$this->db->limit(1,0);
		$this->db->order_by('id_hutang', 'desc');	

  	return $this->db->get()->row();
		
		
	}
	
	
	
	
	
	
	function get_menu()
	{
		$this->db->select('*');
		return $this->db->get($this->table);
			
		
	}
	
	
	
	function get_last_id_menu()
	{
		$this->db->select('id_menu');
		$this->db->order_by('id_menu', 'desc');
		$this->db->limit(1,0);
		return $this->db->get($this->table);
	}
	
	/**
	 * Update data menusi
	 */
	function update($id_menu, $menu)
	{
		$this->db->where('id_hutang', $id_menu);
		$this->db->update($this->table, $menu);
	}
	function update_comment($id_menu, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->komen;
		
		$komen.=$comment;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set komen='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	function update_pembayaran_log($id_menu, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set pembayaran_log='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
}
// END Menu_model Class

/* End of file menu_model.php */
/* Location: ./system/application/models/menu_model.php */
