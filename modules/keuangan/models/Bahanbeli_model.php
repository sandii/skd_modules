<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bahanbeli_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Bahanbeli_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'k_bahanbeli';
	
	/**
	 * Mendapatkan semua data bahanbeli, diurutkan berdasarkan id_bahanbeli
	 */
	function get_bahanbeli()
	{
		$this->db->order_by('id_bahanbeli');
		return $this->db->get('bahanbeli');
	}
	
	
	
	
	
	/**
	 * Mendapatkan data sebuah bahanbeli
	 */
	function get_bahanbeli_by_id($id_bahanbeli)
	{
		return $this->db->get_where($this->table, array('id_bahanbeli' => $id_bahanbeli), 1)->row();
	}
	
	
	function get_bahan_by_akun($id_bahanbeli)
	{  $this->db->order_by('nama');
		return $this->db->get_where($this->table, array('akun' => $id_bahanbeli));

		
	}
	
	
	
	function get_all()
	{
		$this->db->order_by('id_bahanbeli');
		return $this->db->get($this->table);
	}
		
	function get_beli($q)
	{
	
	  		$this->db->where("nama like '%$q%' and(akun like '6%' or akun  like '7%')");

    
    $this->db->order_by('nama');
		
		
		return $this->db->get($this->table);
	}
	
	/**
	 * Menghapus sebuah data bahanbeli
	 */
	function delete($id_bahanbeli)
	{
		$this->db->delete($this->table, array('id_bahanbeli' => $id_bahanbeli));
	}
	
	/**
	 * Tambah data bahanbeli
	 */
	function add($bahanbeli)
	{
		$this->db->insert($this->table, $bahanbeli);
	}
	
	/**
	 * Update data bahanbeli
	 */
	function update($id_bahanbeli, $bahanbeli)
	{
		$this->db->where('id_bahanbeli', $id_bahanbeli);
		$this->db->update($this->table, $bahanbeli);
	}
	
	/**
	 * Validasi agar tidak ada bahanbelid dengan id ganda
	 */
	function valid_id($id_bahanbeli)
	{
		$query = $this->db->get_where($this->table, array('id_bahanbeli' => $id_bahanbeli));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file bahanbeli_model.php */
/* Location: ./system/application/models/bahanbeli_model.php */
