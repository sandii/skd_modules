<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Kas_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Kas_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'k_bukubesar';
	
	/**
	 * Mendapatkan semua data kas, diurutkan berdasarkan id_kas
	 */
	function get_kas($kas,$limit, $offset,$tgl1,$tgl2=null)
	{
	
  
  $this->db->select('*');
		$this->db->from($this->table);	
			$this->db->limit($limit, $offset);	
			$this->db->where("tanggal >=",$tgl1);
			
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	
			
				$this->db->where('id_akun', $kas);
		
	
				$this->db->order_by('id_kas', 'desc');	
		return $this->db->get(); 
		
	}
	
	
		function count_all_num_rows($kas,$tgl1,$tgl2=null)
	{
		$this->db->select('*');
		$this->db->from($this->table);		
			$this->db->where("tanggal >=",$tgl1);
				
				$this->db->where('id_akun', $kas);
		
			if(!empty($tgl2))
			$this->db->where("tanggal <=",$tgl2);	
	
		return $this->db->get()->num_rows();
	}
	

	function hitung_omzet($id_akun,$tgl1,$tgl2)
	{
	

		$this->db->select ('sum(debet) as total_debet, sum(kredit) as total_kredit');

		$this->db->from('k_bukubesar');
		$this->db->where('id_akun',$id_akun);	

		$this->db->where("tanggal >='".$tgl1."'");	
		
 	  
	   $this->db->where("tanggal <='".$tgl2."'");	
		
	
		return $this->db->get()->row();
	}

	






		 
	function get_terakhir($kas)
	{
		$this->db->where('id_akun', $kas);
		$this->db->order_by('id_kas','desc');
		$this->db->limit(1, 0);
		return $this->db->get($this->table)->row();

	}
	
	
	
	/**
	 * Mendapatkan data sebuah kas
	 */
	function get_kas_by_id($id_kas)
	{
		return $this->db->get_where($this->table, array('id_kas' => $id_kas), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_kas');
		return $this->db->get($this->table);
	}
	

	function total_akun($id_akun,$dari,$sampai)
	{
		$this->db->order_by('id_kas');
		return $this->db->get($this->table);
	}
	
	
	
	/**
	 * Menghapus sebuah data kas
	 */
	function delete($id_kas)
	{
		$this->db->delete($this->table, array('id_kas' => $id_kas));
	}
	
	/**
	 * Tambah data kas
	 */
	function add($kas)
	{
		$this->db->insert($this->table, $kas);
	}




function transaksi($kas,$reverse=null)
	{
	

	if(!$reverse)
	 $saldo_terakhir = $this->get_terakhir($kas["id_akun"])->saldo+$kas['debet']-$kas['kredit'];
	else
	 $saldo_terakhir = $this->get_terakhir($kas["id_akun"])->saldo-$kas['debet']+$kas['kredit'];


	
$kas["saldo"]=$saldo_terakhir;
		
		
		
	
		$this->db->insert($this->table, $kas);
    	
  $this->update_akun($kas["id_akun"],array('saldo'=> $saldo_terakhir));
  
  
  
  
  }






function update_akun($id_kas, $kas)
	{
		$this->db->where('id_akundetil', $id_kas);
		$this->db->update('k_akundetil', $kas);
	}
	



	
	/**
	 * Update data kas
	 */
	function update($id_kas, $kas)
	{
		$this->db->where('id_kas', $id_kas);
		$this->db->update($this->table, $kas);
	}
	
	/**
	 * Validasi agar tidak ada kasd dengan id ganda
	 */
	function valid_id($id_kas)
	{
		$query = $this->db->get_where($this->table, array('id_kas' => $id_kas));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file kas_model.php */
/* Location: ./system/application/models/kas_model.php */
