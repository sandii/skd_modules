<?php
/**
 * Marketing_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Akun_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Akun_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'k_akun';
		var $table2 = 'k_akundetil';
	
	/**
	 * Mendapatkan semua data akun, diurutkan berdasarkan id_akun
	 */
	function get_all2($akun)
	{
		$this->db->order_by('id_akundetil');
				$this->db->where('id_akun', $akun);
		return $this->db->get($this->table2);
	}


	function hitung_kas($id_akun)
	{
	

		$this->db->select ('sum(saldo) as total_kas');

		$this->db->from($this->table2);
		$this->db->where('id_akun',$id_akun);	
	
		return $this->db->get()->row();
	}
	
	
	function get_akundetil($q)
	{
			$this->db->select('*,k_akun.nama as nama_akun,k_akundetil.nama as nama_akundetil ');
		$this->db->from('k_akun,k_akundetil');
	
	  		$this->db->where("k_akundetil.nama like '%$q%' ");

  
  	$this->db->where('k_akundetil.id_akun = k_akun.id_akun');
		$this->db->order_by('k_akundetil.id_akundetil');

	
	
	
    
    $this->db->order_by('k_akundetil.nama');
		
		
		return $this->db->get();
	}
	
	
	
	/**
	 * Mendapatkan data sebuah akun
	 */
	function get_akun_by_id($id_akun)
	{
		return $this->db->get_where($this->table2, array('id_akundetil' => $id_akun), 1)->row();
	}
	
	function get_all()
	{
		$this->db->order_by('id_akun');
		return $this->db->get($this->table);
	}
	
		function get_pengeluaran()
	{

$this->db->distinct();	
$this->db->select (array('id_akundetil','k_akun.nama as nama_akun','k_akundetil.nama'));

			$this->db->from('k_akun,k_akundetil');

		$this->db->order_by('id_akundetil');
		
    		$this->db->where("(k_akundetil.id_akun = 6 or k_akundetil.id_akun  = 7)");
    		$this->db->where( 'k_akun.id_akun = k_akundetil.id_akun ');
    

    return $this->db->get();
		
		
	}
	
	
	
	
	/**
	 * Menghapus sebuah data akun
	 */
	function delete($id_akun)
	{
		$this->db->delete($this->table, array('id_akun' => $id_akun));
	}
	
	/**
	 * Tambah data akun
	 */
	function add($akun)
	{
		$this->db->insert($this->table2, $akun);
	}
	
	/**
	 * Update data akun
	 */
	function update($id_akun, $akun)
	{
		$this->db->where('id_akundetil', $id_akun);
		$this->db->update($this->table2, $akun);
	}
	
	/**
	 * Validasi agar tidak ada akund dengan id ganda
	 */
	function valid_id($id_akun)
	{
		$query = $this->db->get_where($this->table, array('id_akun' => $id_akun));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file akun_model.php */
/* Location: ./system/application/models/akun_model.php */
