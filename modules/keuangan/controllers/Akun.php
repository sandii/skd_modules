<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Akun extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'akun';
	
  
  function Akun()
	{
		parent::__construct();
		$this->load->model('Akun_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman akun,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data akun
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;

		
		// Load data
		$query = $this->Akun_model->get_all();
		$akun = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( 'no','Akun', 'Actions');
	
			
			foreach ($akun as $row)
			{
			
			
	
  $this->table->add_row( 	$row->id_akun,"<h4>".$row->nama," ");
  
  	
			 
			 
			$query2 = $this->Akun_model->get_all2($row->id_akun);
		$akun2 = $query2->result();
    		$num_rows2 = $query2->num_rows();
      
     
      
      	foreach ($akun2 as $row2)
			{	 
			
			
		
	$this->table->add_row( 	$row2->id_akundetil,$row2->nama,
	  anchor('keuangan/akun/update/'.$row2->id_akundetil,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')));			
			}

			
			
				
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data akun!';
		}		
		
		$data['link'] = array('link_add' => anchor('keuangan/akun/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data akun
	 */
	function delete($id_akun)
	{
		$this->Akun_model->delete($id_akun);
		$this->session->set_flashdata('message', '1 data akun berhasil dihapus');
		
		redirect('keuangan/akun');
	}
	
	/**
	 * Pindah ke halaman tambah akun
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'akun_form';
		$data['form_action']	= site_url('keuangan/akun/add_process');
		$data['link'] 			= array('link_back' => anchor('keuangan/akun','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data akun
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'akun_form';
		$data['form_action']	= site_url('keuangan/akun/add_process');
		$data['link'] 			= array('link_back' => anchor('keuangan/akun','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// Set validation rules
	
		$this->form_validation->set_rules('akun', 'akun', 'required|max_length[32]');
	
  $akun= $this->input->post('akun');

  
  
  	
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$akun = array('id_akundetil'	=> $this->input->post('akun'),
							'nama'		=> $this->input->post('nama'),
							'id_akun'		=> $akun[0]	
						);
			// Proses penyimpanan data di table akun
			$this->Akun_model->add($akun);
			
			$this->session->set_flashdata('message', 'Satu data akun berhasil disimpan!');
			redirect('keuangan/akun/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update akun
	 */
	function update($id_akun)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'akun_form';
		$data['form_action']	= site_url('keuangan/akun/update_process/'.$id_akun);
		$data['link'] 			= array('link_back' => anchor('keuangan/akun','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$akun = $this->Akun_model->get_akun_by_id($id_akun);
				
		// buat session untuk menyimpan data primary key (id_akun)
		$this->session->set_userdata('id_akun', $akun->id_akun);
		
		// Data untuk mengisi field2 form
		$data['default']['akun'] 	= $akun->id_akundetil;		
		$data['default']['nama']		= $akun->nama;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data akun
	 */
	function update_process($id_akun)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'akun_form';
		$data['form_action']	= site_url('keuangan/akun/update_process/'.$id_akun);
		$data['link'] 			= array('link_back' => anchor('keuangan/akun','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('akun', 'Akun', 'required|max_length[32]');


$akun= $this->input->post('akun');

  

		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$akun = array(
							'id_akundetil'		=> $this->input->post('akun'),
							'nama'		=> $this->input->post('nama')
							,'id_akun'=>$akun[0]	
						);
			$this->Akun_model->update($id_akun,$akun);
			
			$this->session->set_flashdata('message', 'Satu data akun berhasil diupdate!');
			redirect('keuangan/akun');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_akun valid, agar tidak ganda
	 */

}
// END Akun Class

/* End of file akun.php */
/* Location: ./system/application/controllers/akun.php */
