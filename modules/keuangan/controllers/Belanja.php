<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Belanja extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 20;
	var $title = 'belanja';

  
  function belanja()
	{
		parent::__construct();
	$this->load->model('Belanja_model', '', TRUE);
$this->load->model('Bahanbeli_model', '', TRUE);
$this->load->model('Supplier_model', '', TRUE);	
$this->load->model('Akun_model', '', TRUE);	
$this->load->model('Kas_model', '', TRUE);	
$this->load->model('Hutang_model', '', TRUE);	

	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	

	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tgl1="",$tgl2="",$sup="",$bar="",$offset = 0)
	{
	
		$data=$this->data;
    $data['h2_title']=$this->title;
   
		
	$data['custom_view']="belanja_main";	

 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $sup=$this->input->post('sup');
     $bar=$this->input->post('bar');
     
     $offset=0;
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1) or $tgl1=="dahulu") {$tgl1="dahulu";$cari_tgl1="";}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
   if(empty($sup)or $sup=="semua" ) {$sup="semua";$cari_sup="";}
   else $cari_sup=$sup;
   if(empty($bar)or $bar=="semua" ) {$bar="semua";$cari_bar="";}
   else $cari_bar=$bar;




		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
		  $data["sup"]=$sup;
	  $data["bar"]=$bar;

if(!empty($sup) and $sup!="semua")
	$data['nama_supplier']=$this->Supplier_model->get_supplier_by_id($sup)->nama;	  
	
if(!empty($bar) and $bar!="semua")
$data['nama_barang'] = $this->Bahanbeli_model->get_bahanbeli_by_id($bar)->nama;





	$query = $this->Belanja_model->arsip($this->limit,$offset,$cari_tgl1,$cari_tgl2,$cari_sup,$cari_bar);

		$num_rows = $this->Belanja_model->count_arsip($cari_tgl1,$cari_tgl2,$cari_sup,$cari_bar);

		$siswa = $query->result();
	


		
		if ($num_rows > 0)
		{
	
			// Membuat pagination			
			$uri_segment = 8;
      $config['base_url'] = site_url('keuangan/belanja/index/'.$tgl1.'/'.$tgl2.'/'.$sup.'/'.$bar.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
				$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
	
	
	
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal belanja', 'tanggal input', 'supplier','barang',  'no nota', 'total');
	
			
			foreach ($siswa as $row)
			{




	// Load data
		$query2 = $this->Belanja_model->get_detil($row->id_belanja);

		$siswa2 = $query2->result();
		$num_rows2 = $query2->num_rows();
		


		$xx=0;
		if ($num_rows2 > 0)
		{
		
$barangx="";
			
			foreach ($siswa2 as $row2)
			{
	if($xx==1)
		$barangx.=", ";

		$barang=$this->Bahanbeli_model->get_bahanbeli_by_id($row2->id_barang);


$nama_barang=$barang->nama;
	
	$barangx.=$nama_barang;

    		$xx=1;
	    			
			}

}


			
			$nama_supplier=$this->Supplier_model->get_supplier_by_id($row->id_supplier)->nama;
			$this->table->add_row( $row->tanggal, $row->tanggal_input,$nama_supplier,$barangx,"<div align=right>".$row->nota,  "<div align=right>".
			anchor_popup('keuangan/belanja/detail/'.$row->id_belanja,number_format($row->total, 0, ',', '.'), array('class' => 'detail'))

			);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data belanja!';
		}		
		

		// Load view
		$this->load->view('template', $data);
	}


	function detail($id_belanja)
	{
	
		$data=$this->data;
    $data['h2_title']=$this->title;
    
		

		

$belanja=$this->Belanja_model->get_belanja_by_id($id_belanja);



$nama_supplier=$this->Supplier_model->get_supplier_by_id($belanja->id_supplier);




$data['pagination']="
<p> <label>supplier: </label>$nama_supplier->nama</p>
<p> <label>diskon: </label>$belanja->diskon</p>

<p> <label>total: </label>$belanja->total</p>

<p> <label>no nota: </label>$belanja->nota</p>
<p> <label>tanggal: </label>$belanja->tanggal</p>

 


";



		
		// Load data
		$query = $this->Belanja_model->get_detil($id_belanja);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		


		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('barang', 'keterangan', 'jumlah','satuan',  'harga','subtotal');
	
			
			foreach ($siswa as $row)
			{
			
		$barang=$this->Bahanbeli_model->get_bahanbeli_by_id($row->id_barang);
$nama_barang=$barang->nama;
$satuan=$barang->satuan;
				$this->table->add_row( $nama_barang, $row->keterangan,$row->jumlah,$satuan,$row->harga, 
        $row->harga*$row->jumlah
			);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data belanja!';
		}		
		

		
		// Load view
		$this->load->view('template2', $data);
	}

/**


*/
	function add($id_akundetil)
	{		
		$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	  $data=$this->data;
		$data['h2_title'] 		= "kas ".$query." > ".$this->title;
		$data['custom_view'] 		= 'belanja_add';


		$data['form_action']	= site_url('keuangan/belanja/add_process/'.$id_akundetil);

$data['link_barang'] 	=anchor_popup('bahanbeli/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));
$data['link_supplier'] 	=anchor_popup('supplier/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));


		$this->load->view('template', $data);
	}
	
	
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_akundetil)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'belanja/add';
		$data['form_action']	= site_url('keuangan/belanja/add_process');

$data['link_barang'] 	=anchor_popup('bahanbeli/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));
$data['link_supplier'] 	=anchor_popup('supplier/get_all/preview',' ', array('class' => 'link_tambah3','width'=>'1000'));


	$benar=false;
	for($i=1;$i<=10;$i++)	
  {

   $xx=$this->input->post('barang'.$i);
    if(empty($xx))
    break;
    
    
  
    
     $yy=$this->input->post('harga'.$i);
     $zz=$this->input->post('jumlah'.$i);
     
 if(!empty($yy) and !empty($zz))
 $benar=true;    
else
{
 $benar=false;    
break;
}
}



$aa=$this->input->post('supplier');
if(empty($aa))
$benar=false;	
	
	
	
		
		// Jika validasi sukses
		if ($benar==true)
		{
		

$id_terakhirxx = $this->Belanja_model->get_terakhir();
$id_terakhir = $id_terakhirxx->id_belanja;

$id_terakhir++; 




$total=0;

for($i=1;$i<=10;$i++)	
  {
    
    $xx=$this->input->post('barang'.$i);
    if(empty($xx))
    break;
    
   
   
   $total+=$this->input->post('harga'.$i)*$this->input->post('jumlah'.$i);
   
    
    	// Persiapan data
			$orderdetil = array('id_belanja'=> $id_terakhir,
			'id_barang'		=> $this->input->post('barang'.$i),
			'jumlah'		=> $this->input->post('jumlah'.$i),
			'harga'		=> $this->input->post('harga'.$i),
      'keterangan'		=> $this->input->post('ket'.$i)					
						);
			// Proses penyimpanan data di table orderdetil
			$this->Belanja_model->add($orderdetil);	
  

		$yy=array("harga"=> $this->input->post('harga'.$i));

			$this->Bahanbeli_model->update($this->input->post('barang'.$i),$yy);	


  
  }

$total=ceil($total);

  $total-=$this->input->post('diskon');
  
  
 
$tanggal_input=date("Y-m-d");
$order = array( 

  	'id_belanja' 	=> $id_terakhir,		
  	'id_supplier' 	=> $this->input->post('supplier'),		
  	'nota' 	=> $this->input->post('nota'),	
  	'diskon' 	=> $this->input->post('diskon'),	

		'total'		=> $total,			
		'tanggal'		=> $this->input->post('tanggal'),		
		'tanggal_input'	=> $tanggal_input	
			);  
			
			
$nama_supplier=$this->Supplier_model->get_supplier_by_id($this->input->post('supplier'))->nama;			

$this->Belanja_model->add_belanja($order);
 



if($id_akundetil=="hutang")
{



			$uang = array('nama' 		=> $nama_supplier,
						'keterangan' 		=> "belanja",

			'total' 		=> $total,
						'tanggal' 		=> $this->input->post('tanggal'),
						'status' 		=> "hutang"
						);
			$this->Hutang_model->add($uang);
						










			$this->session->set_flashdata('message', 'Satu data order berhasil disimpan!');
			redirect('hutang/index/');
			


}
	else
	{
	$uang = array('ket' 		=> 'pembelian ke '.$nama_supplier,
						'id_akun' 		=> $id_akundetil,
		
			'debet' 		=> 0,
						'tanggal' 		=> $this->input->post('tanggal'),
						'id_detail'=>$id_terakhir,
						'kredit' 		=> $total,
		'kode'		=> "blj",
					'id_detail'		=>	$id_terakhir	

						);
			$this->Kas_model->transaksi($uang);
			
			$this->session->set_flashdata('message', 'Satu data order berhasil disimpan!');
			redirect('keuangan/kas_detil/index/'.$id_akundetil);
			
}




			}
		else
		{	
			$this->session->set_flashdata('message', 'pengisian ada yg salah!');
			redirect('keuangan/belanja/add');
		}		
	}
	


	function autocomplete()
{

	$query = $this->Bahanbeli_model->get_beli($this->input->get('q'));
	$bahanbeli = $query->result();
	$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{

echo"[";
$i=0;
    foreach($bahanbeli as $row):
    if ($i!=0)
    echo",";   
        echo '{"name":"'.$row->nama.'","id":"'.$row->id_bahanbeli.'","satuan":"'.$row->satuan.'","harga":"'.$row->harga.'"}';
  
  $i++;
    endforeach;    
echo"]";
} 


}

	function autosupplier()
{




	$query = $this->Supplier_model->get_supplier_q($this->input->get('q'));
	$bahanbeli = $query->result();
	$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{

echo"[";
$i=0;
    foreach($bahanbeli as $row):
    if ($i!=0)
    echo",";   
        echo '{"name":"'.$row->nama.'","id":"'.$row->id_supplier.'"}';
  
  $i++;
    endforeach;    
echo"]";
} 


}
}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
