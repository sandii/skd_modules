<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bahanbeli extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'jenis pengeluaran';
		var $limit = 20;
  
  function Bahanbeli()
	{
		parent::__construct();
		$this->load->model('Bahanbeli_model', '', TRUE);
		$this->load->model('Akun_model', '', TRUE);
		$this->load->model('Satuan_model', '', TRUE);
		$this->load->model('Belanja_model', '', TRUE);
		$this->load->model('Supplier_model', '', TRUE);


	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data bahanbeli
	 */
	function get_all($tampilan="full",$tgl1="",$tgl2="")
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = 'main_tanggal';
		
		// Load data
		


 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }


    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;








		$query = $this->Akun_model->get_pengeluaran();
		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No akun','nama akun', 'nama','<div class=pull-right>omzet</div>');
			$i = 0;
		$totaly=0;		
			foreach ($bahanbeli as $row)
			{


$totalx = $this->Belanja_model->get_omzet_akun($row->id_akundetil,$tgl1,$cari_tgl2)->total;

$totaly+=$totalx;

				$this->table->add_row($row->id_akundetil, $row->nama_akun,


        
        anchor('keuangan/bahanbeli/detail/'.$tampilan."/".$row->id_akundetil."/".$tgl1."/".$tgl2,$row->nama, array('class' => 'detail')) , "<div align=right>". number_format($totalx, 0, ',', '.')
        
        	);
			}

			$this->table->add_row("",  "", 
        
       "<div align=right><h5>total",
    
        "<div align=right>". number_format($totaly, 0, ',', '.')
        	);
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
	
		
		// Load view
		if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
	
	
	
		function detail($tampilan="full",$id_akundetil,$tgl1="",$tgl2="")
	{
		$data = $this->data;
	
  
  

		$bahanbeli = $this->Akun_model->get_akun_by_id($id_akundetil);
			


  
  	$data['h2_title'] = anchor('keuangan/bahanbeli/get_all/'.$tampilan."/".$tgl1."/".$tgl2,'jenis pengeluaran').	
    
    " > ". $bahanbeli->nama;  ;
		$data['custom_view'] = 'main_tanggal';
		
		
if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }

     if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   





	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


		// Load data
		$query = $this->Bahanbeli_model->get_bahan_by_akun($id_akundetil);
		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'nama','satuan','harga terakhir','omzet','action');
			$i = 0;
				$totaly=0;
			foreach ($bahanbeli as $row)
			{

$totalx = $this->Belanja_model->get_omzet_barang($row->id_bahanbeli,$tgl1,$cari_tgl2)->total;
$totaly+=$totalx;	



				$this->table->add_row(++$i,  
        anchor_popup('keuangan/bahanbeli/history/'.$row->id_bahanbeli,$row->nama,array('class' => 'detail','width'=>800))

        ,$row->satuan,"<div align=right>".$row->harga, 

"<div align=right>". number_format($totalx, 0, ',', '.'),
        anchor('keuangan/bahanbeli/update/'.$tampilan."/".$id_akundetil."/".$row->id_bahanbeli,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')));	
        
 
			}
$this->table->add_row("","","","<h5 align=right>total", "<div align=right>". number_format($totaly, 0, ',', '.'));

			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
		$data['link'] = array('link_add' => anchor('keuangan/bahanbeli/add/'.$tampilan."/".$id_akundetil,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
	if($tampilan=="preview")
		$this->load->view('template2', $data);
			else
		$this->load->view('template', $data);
	}
		

		function history($id_barang,$offset = 0)
	{
		$data = $this->data;

		

		$bahanbeli2 = $this->Bahanbeli_model->get_bahanbeli_by_id($id_barang);


  
  	$data['h2_title'] = $bahanbeli2->nama ;
	
		
		// Load data
		$query = $this->Belanja_model->get_history($this->limit,$offset,$id_barang);
		$bahanbeli = $query->result();
		
		$num_rows = $this->Belanja_model->count_history($id_barang);



		


		
		if ($num_rows > 0)
		{

		$uri_segment = 5;
      $config['base_url'] = site_url('keuangan/bahanbeli/history/'.$id_barang.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
				$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
	
	

			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'keterangan','harga','jumlah beli','total','tanggal beli','supplier');
			$i = 0+$offset;
			
			foreach ($bahanbeli as $row)
			{

$ambil_supplier=	$this->Supplier_model->get_supplier_by_id($row->id_supplier);			
$nama_supplier=!empty($ambil_supplier->nama)?$ambil_supplier->nama:"";

				$this->table->add_row(++$i,   $row->keterangan,
       
 "<div align=right>". number_format($row->harga, 0, ',', '.'),
  "<div align=right>". number_format($row->jumlah, 0, ',', '.'),
    "<div align=right>". number_format($row->harga*$row->jumlah, 0, ',', '.')
       ,$row->tanggal,$nama_supplier);
        
 
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
	
		
		// Load view
		$this->load->view('template2', $data);
	}
		
	/**
	 * Hapus data bahanbeli
	 */
	function delete($id_bahanbeli)
	{
		$this->Bahanbeli_model->delete($id_bahanbeli);
		$this->session->set_flashdata('message', '1 data bahanbeli berhasil dihapus');
		
		redirect('keuangan/bahanbeli');
	}
	
	/**
	 * Pindah ke halaman tambah bahanbeli
	 */
	function add($tampilan,$id_akundetil)
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'bahanbeli_form';
		$data['form_action']	= site_url('keuangan/bahanbeli/add_process/'.$tampilan."/".$id_akundetil);
		$data['link'] 			= array('link_back' => anchor('keuangan/bahanbeli/detail/'.$tampilan."/".$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
		
		
		
		$marketing = $this->Akun_model->get_pengeluaran()->result();	
		
		
	  	$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}


		$data['default']['akun']		= $id_akundetil;




		$query = $this->Akun_model->get_pengeluaran();
		$query2 = $query->result();
		
	

      	foreach ($query2 as $row2)
			{	 
  				$data['options_akun'][$row2->id_akundetil] = $row2->nama;

			}
	

	

		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data bahanbeli
	 */
	function add_process($tampilan,$id_akundetil)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'bahanbeli_form';
		$data['form_action']	= site_url('keuangan/bahanbeli/add_process/'.$id_akundetil);
		$data['link'] 			= array('link_back' => anchor('keuangan/bahanbeli/detail/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
			
	$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}




		$this->form_validation->set_rules('nama', 'nama', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$bahanbeli = array(
							'nama'		=> $this->input->post('nama'),
							'satuan'		=> $this->input->post('satuan')	,
							'harga'		=> $this->input->post('harga')	,
              	'akun'		=>$id_akundetil								
						);
			// Proses penyimpanan data di table bahanbeli
			$this->Bahanbeli_model->add($bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil disimpan!');
			redirect('keuangan/bahanbeli/detail/'.$tampilan."/".$id_akundetil);
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template2', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update bahanbeli
	 */
	function update($tampilan,$id_akundetil,$id_bahanbeli)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'bahanbeli_form';
		$data['form_action']	= site_url('keuangan/bahanbeli/update_process/'.$tampilan."/".$id_akundetil."/".$id_bahanbeli);
		$data['link'] 			= array('link_back' => anchor('keuangan/bahanbeli/detail/'.$tampilan."/".$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
    	
		// cari data dari database
		$bahanbeli = $this->Bahanbeli_model->get_bahanbeli_by_id($id_bahanbeli);
				



		// Data untuk mengisi field2 form
		$data['default']['satuan'] 	= $bahanbeli->satuan;		
		$data['default']['nama']		= $bahanbeli->nama;
		$data['default']['harga']		= $bahanbeli->harga;



		$data['default']['akun']		= $bahanbeli->akun;




		$query = $this->Akun_model->get_pengeluaran();
		$query2 = $query->result();
		
	

      	foreach ($query2 as $row2)
			{	 
  				$data['options_akun'][$row2->id_akundetil] = $row2->nama;

			}





$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}

				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bahanbeli
	 */
	function update_process($tampilan,$id_akundetil,$id_bahanbeli)
	{


	$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'bahanbeli_form';
		$data['form_action']	= site_url('keuangan/bahanbeli/update_process/'.$tampilan."/".$id_akundetil."/".$id_bahanbeli);
		$data['link'] 			= array('link_back' => anchor('keuangan/bahanbeli/detail/'.$tampilan."/".$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button')));
   	
	






		// cari data dari database
		$bahanbeli = $this->Bahanbeli_model->get_bahanbeli_by_id($id_bahanbeli);
				

$query2 = $this->Satuan_model->get_all()->result();
      	foreach ($query2 as $row2)
			{	 
  				$data['options_satuan'][$row2->nama] = $row2->nama;

			}






		// Data untuk mengisi field2 form
		$data['default']['satuan'] 	= $bahanbeli->satuan;		
		$data['default']['nama']		= $bahanbeli->nama;
		$data['default']['harga']		= $bahanbeli->harga;

			
	
		$data['default']['akun']		= $bahanbeli->akun;




		$query = $this->Akun_model->get_pengeluaran();
		$query2 = $query->result();
		
	

      	foreach ($query2 as $row2)
			{	 
  				$data['options_akun'][$row2->id_akundetil] = $row2->nama;

			}







	$this->form_validation->set_rules('nama', 'nama', 'required|max_length[50]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$bahanbeli = array(
				'akun'		=> $this->input->post('akun'),
							'nama'		=> $this->input->post('nama')
							,'satuan'		=> $this->input->post('satuan')
							,'harga'		=> $this->input->post('harga')
						);
			$this->Bahanbeli_model->update($id_bahanbeli,$bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil diupdate!');
			redirect('keuangan/bahanbeli/detail/'.$tampilan."/".$id_akundetil);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_bahanbeli valid, agar tidak ganda
	 */

}
// END Bahanbeli Class

/* End of file bahanbeli.php */
/* Location: ./system/application/controllers/bahanbeli.php */
