<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Supplier extends  CI_Controller {
	/**
	 * Constructor
	 */
  var $title = 'supplier';
  var $alamat = 'keuangan/supplier';
  var $limit = 20;
	
  
  function Supplier()
	{
		parent::__construct();
	
		$this->load->model('Crud_model', '', TRUE);
	
  // content yang fix, ada terus di web

    $this->data['h2_title']=$this->title;
	  $this->load->helper('fungsi');		
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
$lookup_kategori=array('nama_table'=>'k_supplier_kategori','id'=>'id_supplier_kategori','isi'=>'nama');
	
	$data_field=array('nama'=>array(),
	'id_supplier_kategori'=>array('judul'=>'ketegori','lookup_table'=>$lookup_kategori),
	'telp'=>array(),
	'alamat'=>array('jenis'=>'textarea'),
	'keterangan'=>array('jenis'=>'textarea'));

$fitur=array('');
$fitur['search']=array('nama','id_supplier_kategori');
$fitur['pagination']=array('per_page'=>$this->limit,'uri_segment'=>6, 'base_url' => site_url($this->alamat.'/index/'));
$fitur['validation']=array('nama'=>'required|max_length[32]','id_supplier_kategori'=>'is_natural_no_zero');

if(cek_auth("auth_keuangan"))
$fitur['tambah']=$fitur['edit']=true;


$primary='id_supplier';
$data_utama=array('alamat'=>$this->alamat,'nama_table'=>'k_supplier','primary'=>$primary,'order_by'=>'id_supplier_kategori,nama');
 
   $this->load->library('Crud', array(
   'data_utama'=>$data_utama
   ,'fitur'=>$fitur
   ,'data_field'=>$data_field
   ));
	
}
	
	function index()
	{

$datax=func_get_args();
$tampilan=array_merge($this->data,$this->crud->retrieve($datax));
$this->load->view('template',$tampilan);
	}


 function delete($id = 0)
{$this->crud->delete($id);  
 } 
   
	function add($save = FALSE)			
		{$this->edit($save);}
	
	
function edit($id = -1)
	{
	
$tampilan=array_merge($this->data,$this->crud->update($id));
$this->load->view('template',$tampilan);
   }    



}
