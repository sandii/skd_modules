<?php
/**
 * Neraca Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Neraca extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'neraca';
	
  
  function Neraca()
	{
		parent::__construct();

		$this->load->model('produksi/Order_model', '', TRUE);
		$this->load->model('keuangan/Kas_model', '', TRUE);
		$this->load->model('sdm/Penggajian_model', '', TRUE);
		$this->load->model('keuangan/Belanja_model', '', TRUE);
		$this->load->model('keuangan/Akun_model', '', TRUE);
		$this->load->model('sdm/Pegawai_model', '', TRUE);
	
		$this->load->model('sdm/Kasbon_model', '', TRUE);
		$this->load->model('keuangan/Hutang_model', '', TRUE);
		$this->load->model('sdm/Tunjangan_model', '', TRUE);

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman neraca,
	 * jika tidak akan meredirect ke halaman login
	 */

	

	
	/**
	 * Tampilkan semua data neraca
	 */
	function index($tahun="")
	{





		$data = $this->data;
		$data['h2_title'] = $this->title;
	



    $mulai=2014;

		
if(empty($tahun))
	$tahun=date("Y");



		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor('keuangan/neraca/index/'.$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		

$sampai=12;
if($tahun==2014)
	$dari=11;
else
	$dari=1;

   


	$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( 'nama akun','<div class=pull-right>debit</div>','<div class=pull-right>kredit</div>');
		
			$i = 0;




 $tgl1=$tahun."-".$dari."-1";




 $tgl2=$tahun."-12-31";
 

/**


*/



$hitung=$this->Order_model->hitung_omzet($tgl1,$tgl2);


$hitung_piutang=$this->Order_model->hitung_piutang($tahun);


$hitung_lain2=$this->Kas_model->hitung_omzet(502,$tgl1,$tgl2);

$hitung_kas=$this->Akun_model->hitung_kas(1);

$hitung_sdm=$this->Penggajian_model->hitung_omzet($tgl1,$tgl2);

$hitung_tunjangan=$this->Tunjangan_model->hitung_tunjangan($tgl1,$tgl2);


$hitung_belanja=$this->Belanja_model->hitung_omzet($tgl1,$tgl2);

$hitung_piutang_lain=$this->Hutang_model->hitung_hutang($tahun,"piutang");

$hitung_hutang=$this->Hutang_model->hitung_hutang($tahun,"hutang");
$hitung_modal=$this->Kas_model->get_terakhir(401);




$total_kasbon=0;
$query=$this->Pegawai_model->get_all();
$pegawai = $query->result();

foreach ($pegawai as $row) {


$kasbon=$this->Kasbon_model->get_terakhir($row->id_pegawai);
if(!empty($kasbon->saldo))
$total_kasbon+= $kasbon->saldo;

}



$this->table->add_row("<h5>Kas","<div align=right>".number_format($hitung_kas->total_kas, 0, ',', '.'),'');
$this->table->add_row("<h5>piutang",'','');
$this->table->add_row("piutang dagang","<div align=right>".number_format($hitung_piutang->total_piutang, 0, ',', '.'),'');
$this->table->add_row("kasbon pegawai","<div align=right>".number_format($total_kasbon, 0, ',', '.'),'');
$this->table->add_row("piutang lain2","<div align=right>".number_format($hitung_piutang_lain->total_hutang, 0, ',', '.'),'');
$this->table->add_row("<h5>hutang",'',"<div align=right>".number_format($hitung_hutang->total_hutang, 0, ',', '.'));
$this->table->add_row("<h5>modal",'',"<div align=right>".number_format($hitung_modal->saldo, 0, ',', '.'));
$this->table->add_row("<h5>Pemasukan",'','');
$this->table->add_row("rutin","","<div align=right>".number_format($hitung->total_omzet, 0, ',', '.'));
$this->table->add_row("lain2","","<div align=right>".number_format($hitung_lain2->total_kredit, 0, ',', '.'));
$this->table->add_row("<h5>Pengeluaran",'','');
$this->table->add_row("gaji pegawai","<div align=right>".number_format($hitung_sdm->total_gaji, 0, ',', '.'),"");
$this->table->add_row("tunjangan pegawai","<div align=right>".number_format($hitung_tunjangan->total_tunjangan, 0, ',', '.'));
$this->table->add_row("belanja","<div align=right>".number_format($hitung_belanja->total_belanja, 0, ',', '.'),"");



$total_debet=$hitung_sdm->total_gaji+$hitung_belanja->total_belanja+$hitung_kas->total_kas
+$hitung_piutang->total_piutang+$total_kasbon+$hitung_piutang_lain->total_hutang+$hitung_tunjangan->total_tunjangan;


$total_kredit=$hitung->total_omzet+$hitung_lain2->total_kredit+$hitung_hutang->total_hutang+$hitung_modal->saldo;


$this->table->add_row("<h5>saldo","<h5><div align=right>".number_format($total_debet, 0, ',', '.'),"<h5><div align=right>".number_format($total_kredit, 0, ',', '.'));




  		$data['table'] = $this->table->generate();

		
	
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data neraca
	 */
	

}
// END Neraca Class

/* End of file neraca.php */
/* Location: ./system/application/controllers/neraca.php */
