<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Hutang extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 20;
	var $title = 'hutang piutang';

  
  function hutang()
	{
		parent::__construct();
	$this->load->model('Hutang_model', '', TRUE);
$this->load->model('Bahanbeli_model', '', TRUE);
$this->load->model('Supplier_model', '', TRUE);	
$this->load->model('Akun_model', '', TRUE);	
$this->load->model('Kas_model', '', TRUE);	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	

	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($tahun="")
	{
	

	if (empty($tahun))
	$tahun=date("Y");

		$data=$this->data;
    $data['h2_title']=$this->title;

		
  $mulai=2014;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor('keuangan/hutang/index/'.$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		
	
		// Load data dari tabel order
		
		$tahunx=$tahun+1;
		$where="tanggal >='$tahun-01-01' and tanggal<'$tahunx-1-1'";
		$ordersx = $this->Hutang_model->get_all($where);
		$siswa = $ordersx->result();
    $num_rows = $ordersx->num_rows();
		



		
		if ($num_rows > 0)
		{
	
			
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal',  'status','nama','keterangan', '<div class=pull-right>jumlah</div>','pembayaran','');
			
			foreach ($siswa as $row)
			{

if($row->status=="hutang")
{

$statusx='<p class="text-danger">';
}
else
$statusx='<p class="text-success">';



$pembayaran=$row->pembayaran;
$link_bayar=anchor('keuangan/hutang/bayar/'.$row->id_hutang,'bayar', array('class' => 'add','role'=>"button"));
if(empty($pembayaran))
	$bayar="0";
else if($pembayaran==$row->total)
{	
$bayar=anchor_popup('keuangan/hutang/detail/'.$row->id_hutang,"lunas", array('class' => 'detail'));
$link_bayar="";
}
else	
$bayar=anchor_popup('keuangan/hutang/detail/'.$row->id_hutang,$row->pembayaran, array('class' => 'detail'));
			
			$this->table->add_row( $row->tanggal,$statusx."<b>".$row->status, $row->nama,$row->keterangan,"<div align=right>".$row->total,  
			$bayar,
			$link_bayar);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data hutang!';
		}		
		


		$data['link'] = array(
'link_add1' => anchor('keuangan/belanja/add/hutang','belanja',array('class' => 'btn btn-success btn-lg','role'=> 'button')),
'link_add2' => anchor('keuangan/hutang/add/hutang','hutang baru',array('class' => 'btn btn-success btn-lg','role'=> 'button')),
'link_add3' => anchor('keuangan/hutang/add/piutang','piutang baru',array('class' => 'btn btn-success btn-lg','role'=> 'button')),
   
    );

		// Load view
		$this->load->view('template', $data);
	}


	function detail($id_hutang)
	{
	
		$data=$this->data;




$xx=$this->Hutang_model->get_hutang_by_id($id_hutang);
$data['total']=$xx->total;
$data['nama']=$xx->nama;
$data['kekurangan']=$xx->total-$xx->pembayaran;

if($xx->status=="hutang")
{ $namax="pemberi hutang";
  $data['h2_title']="hutang detail";
}
else
 {

 $namax="yang berhutang ";	
  $data['h2_title']="piutang detail";
}


$data['pagination']=
'<p> <label>'.$namax.': </label>'.$xx->nama.
'<p> <label>total: </label>'.$xx->total.

"<p><label>sudah dibayar: </label>".$xx->pembayaran
;

			
		// Load data
		$query = $this->Hutang_model->get_detil($id_hutang);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		


		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);


$hutang=$this->Hutang_model->get_hutang_by_id($id_hutang);







			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','pembayaran', 'keterangan');
	
			
			foreach ($siswa as $row)
			{
			
					$this->table->add_row( $row->tanggal,$row->jumlah,$row->keterangan
       
			);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data hutang!';
		}		
		

		
		// Load view
		$this->load->view('template2', $data);
	}




	function rangkuman($tgl1="",$tgl2="",$offset = 0)
	{
	
		$data = $this->data;
		$data['title'] = "hutang piutang";
			$data['h2_title'] = "hutang piutang";
    $data['custom_view'] = 'main_tanggal';
		


			
	if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $offset=0;
     }



	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
   
    if(empty($tgl1)) {$tgl1=$seminggu;$cari_tgl1=$seminggu;}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
  

		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;	
	



		// Load data
		$query = $this->Hutang_model->get_rangkuman($this->limit,$offset,$cari_tgl1,$cari_tgl2);

		$siswa = $query->result();
		
		$num_rows = $this->Hutang_model->get_rangkuman("",$offset,$cari_tgl1,$cari_tgl2)->num_rows();

	



		
		if ($num_rows > 0)
		{
			// Generate pagination			
		$uri_segment = 5;
      $config['base_url'] = site_url('keuangan/hutang/rangkuman/'.$tgl1.'/'.$tgl2.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
				$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
					
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);







			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','status','nama','debet','kredit', 'keterangan');
	
			
			foreach ($siswa as $row)
			{
			
if($row->status=="hutang")
	
{
	$statusx="bayar hutang";
$debet=0;
$kredit=$row->jumlah;

}
else
	{$statusx="terima pembayaran";

$kredit=0;
$debet=$row->jumlah;

}

					$this->table->add_row( $row->tanggal,$statusx,$row->nama,$debet,$kredit,$row->keterangan_detil
       
			);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data hutang!';
		}		
		

		
		// Load view
		$this->load->view('template', $data);
	}
	function add($status)
	{		
	
	  $data=$this->data;
	
		$data['custom_view'] 		= 'add_hutang';
		$data['form_action']	= site_url('keuangan/hutang/add_process/'.$status);
		$data['link'] 			= array('link_back' => anchor('keuangan/hutang','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);

if($status=="hutang"){
$data['namax']="pemberi hutang";
$data['kex']="ke";
$data['h2_title'] =  anchor('keuangan/hutang','hutang')." > tambah hutang";

	}
else
	{	
$data['kex']="dari";
$data['namax']="yg berhutang";
$data['h2_title'] 		=  anchor('keuangan/hutang','hutang')." > tambah piutang";
	}




  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	 
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}


		$this->load->view('template', $data);
	}
	
	

	
	/**
	 * Proses tambah data siswa
	 */
	function add_process($status)
	{
	 	  $data=$this->data;
		$data['h2_title'] 		=  anchor('keuangan/hutang','hutang')." > add";
		$data['custom_view'] 		= 'add_hutang';
		$data['form_action']	= site_url('keuangan/hutang/add_process/');
		$data['link'] 			= array('link_back' => anchor('keuangan/hutang','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
	
											);


if($status=="hutang"){
$data['namax']="pemberi hutang";
$data['kex']="ke";
$data['h2_title'] =  anchor('keuangan/hutang','hutang')." > tambah hutang";

	}
else
	{	
$data['kex']="dari";
$data['namax']="yg berhutang";
$data['h2_title'] 		=  anchor('keuangan/hutang','hutang')." > tambah piutang";
	}




  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	 
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}
		       
 
	

		$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
			
		
		if ($this->form_validation->run() == TRUE)
		{
		

		
if($status=="hutang")
{
$debet=$this->input->post('jumlah');
$kredit=0;

$key="hutang dari ";
}

	else
{
$kredit=$this->input->post('jumlah');
$debet=0;
$key="piutang ke ";

}


		$id_terakhir=$this->Hutang_model->get_terakhir()->id_hutang;
		$id_terakhir++;
		
			// save data
			$uang = array(

'id_hutang'=>$id_terakhir,

				'nama' 		=> $this->input->post('pemberi'),
						'keterangan' 		=> $this->input->post('keterangan'),
		
			'total' 		=> $this->input->post('jumlah'),
						'tanggal' 		=> $this->input->post('tanggal'),
						'status' 		=> $status
						);
			$this->Hutang_model->add($uang);





			// save data
			$uang = array('ket' 		=> $key.$this->input->post('pemberi'),
						'id_akun' 		=> $this->input->post('ke'),
		'kode' 		=> "htg",
		'id_detail'=>$id_terakhir,
			'debet' 		=> $debet,
						'tanggal' 		=> $this->input->post('tanggal'),
						'kredit' 		=> $kredit
						);
			$this->Kas_model->transaksi($uang);
						

	
						





	$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('keuangan/hutang/');
		}
		else
		{	
			$this->session->set_flashdata('message', 'pengisian ada yg salah!');
			redirect('keuangan/hutang/add');
		}		
	}
	
function bayar($id_hutang)
	{		
	
	  $data=$this->data;
		$data['h2_title'] 		=  anchor('keuangan/hutang','hutang')." > bayar";
		$data['custom_view'] 		= 'bayar_hutang';
		$data['form_action']	= site_url('keuangan/hutang/bayar_process/'.$id_hutang);

		$data['link'] 			= array('link_back' => anchor('keuangan/hutang','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))

										);


$xx=$this->Hutang_model->get_hutang_by_id($id_hutang);
$data['total']=$xx->total;
$data['nama']=$xx->nama;
$data['kekurangan']=$xx->total-$xx->pembayaran;
if($xx->status=="hutang")
{
$data["namax"]="pemberi hutang";
$data["kex"]="dari";
}
else
{
$data["namax"]="yang berhutang";
$data["kex"]="ke";
}


  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	 
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}


		$this->load->view('template', $data);
	}
	
	

	
	/**
	 * Proses tambah data siswa
	 */
	function bayar_process($id_hutang)
	{
		
	  $data=$this->data;
		$data['h2_title'] 		=  anchor('keuangan/hutang','hutang')." > bayar";
		$data['custom_view'] 		= 'bayar_hutang';
		$data['form_action']	= site_url('keuangan/hutang/bayar_process/'.$id_hutang);
	
		$data['link'] 			= array('link_back' => anchor('keuangan/hutang','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))

										);


$xx=$this->Hutang_model->get_hutang_by_id($id_hutang);
$data['total']=$xx->total;
$data['nama']=$xx->nama;
$data['kekurangan']=$xx->total-$xx->pembayaran;
if($xx->status=="hutang")
{
$data["namax"]="pemberi hutang";
$data["kex"]="dari";
}
else
{
$data["namax"]="yang berhutang";
$data["kex"]="ke";
}


  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	 
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}
		       
 
$xx=$this->Hutang_model->get_hutang_by_id($id_hutang);

$total=$data['total']=$xx->total;
$kekurangan=$data['kekurangan']=$xx->total-$xx->pembayaran;


$maksimal=$kekurangan+1;


		$this->form_validation->set_rules('jumlah', 'jumlah', 'required|less_than['.$maksimal.']|integer');
			
		
		if ($this->form_validation->run() == TRUE)
		{



$total_bayar=$xx->pembayaran+$this->input->post('jumlah');
		
			

if($xx->status=="hutang")
{
$debet=0;
$kredit=$this->input->post('jumlah');
$key="bayar hutang ke ";

}
else
{
$key="bayar hutang dari ";
$kredit=0;
$debet=$this->input->post('jumlah');
}



			// save data
			$uang = array('ket' 		=> $key.$xx->nama,
						'id_akun' 		=> $this->input->post('ke'),
		'kode'=>"bht",
		'id_detail'=>$id_hutang,
			'kredit' 		=> $kredit,
						'tanggal' 		=> $this->input->post('tanggal'),
						'debet' 		=> $debet
						);
			$this->Kas_model->transaksi($uang);
						

			
			// save data
			$uang = array('id_hutang' 		=> $id_hutang,
						'keterangan' 		=> $this->input->post('keterangan'),
		
			'jumlah' 		=> $this->input->post('jumlah'),
						'tanggal' 		=> $this->input->post('tanggal')
						);
			$this->Hutang_model->add_hutang_detil($uang);
						

		$uang = array(
	"pembayaran"=>$total_bayar
						);
			$this->Hutang_model->update($id_hutang,$uang);
						





	$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('keuangan/hutang/');
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	

	
}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
