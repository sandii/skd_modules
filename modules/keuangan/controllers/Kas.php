<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Kas extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'kas';

  
  function kas()
	{
		parent::__construct();
		$this->load->model('belanja_model', '', TRUE);
$this->load->model('Bahanbeli_model', '', TRUE);
	$this->load->model('Akun_model', '', TRUE);
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;

	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	

	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index()
	{
	
		$data=$this->data;
    $data['h2_title']=$this->title;



			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading( 'no','Akun', '<div class=pull-right>saldo</div>');
	

			
			
	
			 
			 
			$query2 = $this->Akun_model->get_all2(1);
		$akun2 = $query2->result();
    		$num_rows2 = $query2->num_rows();
      
     
      $total=0;
      	foreach ($akun2 as $row2)
			{	 
			
			
		$total+=$row2->saldo;
	$this->table->add_row( 	$row2->id_akundetil,

  		anchor('keuangan/kas_detil/index/'.$row2->id_akundetil,$row2->nama, array('class' => 'detail'))

  ,
"<div align=right>".
  number_format($row2->saldo, 0, ',', '.')

  );			
			}

	$this->table->add_row( "",

  		"<h5>total"

  ,
"<div align=right><h5>".
  number_format($total, 0, ',', '.')

  );			

			$data['table'] = $this->table->generate();

		
		// Load view
		$this->load->view('template', $data);
	}





}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
