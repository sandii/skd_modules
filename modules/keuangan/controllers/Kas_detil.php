<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Kas_detil extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 50;
	var $title = 'kas';

  
  function kas_detil()
	{
		parent::__construct();

		$this->load->model('Akun_model', '', TRUE);
		$this->load->model('Kas_model', '', TRUE);
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_akundetil,$tgl1="",$tgl2="",$offset = 0)
	{
	
	
	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	
	
		$data=$this->data;
    $data['h2_title']= anchor('keuangan/kas',$this->title).": ".$query;

    $data['custom_view'] = 'main_tanggal';


 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $offset=0;
     }



	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
   
    if(empty($tgl1)) {$tgl1=$seminggu;$cari_tgl1=$seminggu;}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
  

		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
		
		


		
		// Load data
		$query = $this->Kas_model->get_kas($id_akundetil,$this->limit,$offset,$cari_tgl1,$cari_tgl2);

		$num_rows = $this->Kas_model->count_all_num_rows($id_akundetil,$cari_tgl1,$cari_tgl2);



		$siswa = $query->result();
	
$tanggal="";
		
		if ($num_rows > 0)
		{
			// Generate pagination			
			// Membuat pagination			
			$uri_segment = 7;
      $config['base_url'] = site_url('keuangan/kas_detil/index/'.$id_akundetil.'/'.$tgl1.'/'.$tgl2.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
				$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','kode',  'keterangan', '<div class=pull-right>debet</div>', '<div class=pull-right>kredit</div>', '<div class=pull-right>saldo</div>');
	
			
			foreach ($siswa as $row)
			{

$kode=$row->kode;
if($kode=="blj")
$ket=anchor_popup('keuangan/belanja/detail/'.$row->id_detail,$row->ket, array('class' => 'detail'));

else if($kode=="byr" or $kode=="lbh")
$ket=anchor_popup('produksi/pembayaran_detil/index/'.$row->id_detail,$row->ket, array('class' => 'detail'));

else if($kode=="gji")
$ket=anchor_popup('sdm/slipgaji/index/'.$row->id_detail,$row->ket, array('class' => 'detail'));

else if($kode=="ksb")
$ket=anchor_popup('sdm/kasbon/index/'.$row->id_detail,$row->ket, array('class' => 'detail'));
else if($kode=="tjg")
$ket=anchor_popup('sdm/tunjangan/index/'.$row->id_detail,$row->ket, array('class' => 'detail'));
else if($kode=="htg" or $kode=="bht")
$ket=anchor_popup('keuangan/hutang/detail/'.$row->id_detail,$row->ket, array('class' => 'detail'));


else
$ket=$row->ket;



if ($tanggal==$row->tanggal)
	$xx="";
else
$tanggal=$xx=$row->tanggal;


				$this->table->add_row( $xx,$kode , $ket ,"<div align=right>". number_format($row->debet, 0, ',', '.'),"<div align=right>".number_format($row->kredit, 0, ',', '.'),"<div align=right>".number_format($row->saldo, 0, ',', '.')
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data kas!';
		}		
		
		$data['link'] = array('link_add' => anchor('keuangan/kas_detil/pindah_kas/'.$id_akundetil,'transfer',array('class' => 'btn btn-success btn-lg','role'=> 'button'))								
    ,'link_pengeluaran' => anchor('keuangan/belanja/add/'.$id_akundetil,'belanja', array('class' => 'btn btn-success btn-lg','role'=> 'button'))								
    ,'link_add2' => anchor('keuangan/kas_detil/kas_masuk/'.$id_akundetil,'pemasukan lain2', array('class' => 'btn btn-success btn-lg','role'=> 'button'))								
     
    
    );
		
		// Load view
		$this->load->view('template', $data);
	}


function bukubesar($id_akundetil,$tgl1="",$tgl2="",$offset = 0)
	{
	
	
	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	
	
		$data=$this->data;
    $data['h2_title']="buku: ".$query;
    $data['custom_view'] = 'main_tanggal';


 if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $offset=0;
     }



	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
   
    if(empty($tgl1)) {$tgl1=$seminggu;$cari_tgl1=$seminggu;}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
  

		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
		
		


		
		// Load data
		$query = $this->Kas_model->get_kas($id_akundetil,$this->limit,$offset,$cari_tgl1,$cari_tgl2);

		$num_rows = $this->Kas_model->count_all_num_rows($id_akundetil,$cari_tgl1,$cari_tgl2);



		$siswa = $query->result();
	

		
		if ($num_rows > 0)
		{
			// Generate pagination			
			// Membuat pagination			
			$uri_segment = 6;
      $config['base_url'] = site_url('keuangan/kas_detil/bukubesar/'.$id_akundetil.'/'.$tgl1.'/'.$tgl2.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
				$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal', 'keterangan', 'debet', 'kredit', 'saldo');
	
			
			foreach ($siswa as $row)
			{
				$this->table->add_row( $row->tanggal, $row->ket,   $row->debet,$row->kredit,$row->saldo
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bonus!';
		}		
		
	  
    
   
		
		// Load view
		$this->load->view('template', $data);
	}
	/**
	 


	 */


	function pindah_kas($id_akundetil)
	{		
	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.': '.$query.' > transfer';
		$data['custom_view'] 		= 'pindah_kas';
		$data['form_action']	= site_url('keuangan/kas_detil/pindah_kas_process/'.$id_akundetil);
				$data['link'] 			= array('link_back' => anchor('keuangan/kas_detil/index/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);								



	
  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	         if($row2->id_akundetil != $id_akundetil)
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}



		
		$this->load->view('template', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function pindah_kas_process($id_akundetil)
	{
	 	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	 	

	 	
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.': '.$query.' > transfer';
		$data['custom_view'] 		= 'kas/pindah_kas';
		$data['form_action']	= site_url('keuangan/kas_detil/pindah_kas_process/'.$id_akundetil);
		$data['link'] 			= array('link_back' => anchor('keuangan/kas_detil/index/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);

	 
  	$query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	         if($row2->id_akundetil != $id_akundetil)
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}

 
			$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
			
		
		if ($this->form_validation->run() == TRUE)
		{
		

	 	$query3 = $this->Akun_model->get_akun_by_id($this->input->post('ke'))->nama;
		
			// save data
			$uang = array('ket' 		=> 'transfer ke '.$query3,
			'id_akun' 		=> $id_akundetil,
		
			'kredit' 		=> $this->input->post('jumlah'),
			'debet' 		=> 0,
			'tanggal' 		=> $this->input->post('tanggal'),
			'kode' 		=> "trf",

						
						);
			$this->Kas_model->transaksi($uang);
			
			
			// save data
			$uang = array('ket' 		=> 'transferan dari '.$query,
						'id_akun' 		=> $this->input->post('ke'),
		
			'debet' 		=> $this->input->post('jumlah'),
						'tanggal' 		=> $this->input->post('tanggal'),
						'kredit' 		=> 0,
			'kode' 		=> "trf"

						);
			$this->Kas_model->transaksi($uang);
						
			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('keuangan/kas_detil/index/'.$id_akundetil);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	
		function kas_masuk($id_akundetil)
	{		
	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.': '.$query.' > pemasukan lain2';
		$data['custom_view'] 		= 'kas_masuk';
		$data['form_action']	= site_url('keuangan/kas_detil/kas_masuk_process/'.$id_akundetil);
		$data['link'] 			= array('link_back' => anchor('keuangan/kas_detil/index/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										



	



		
		$this->load->view('template', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function kas_masuk_process($id_akundetil)
	{
	 	$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	 	

	 	
		$query = $this->Akun_model->get_akun_by_id($id_akundetil)->nama;
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.': '.$query.' > pemasukan lain2';
		$data['custom_view'] 		= 'kas_masuk';
		$data['form_action']	= site_url('keuangan/kas_detil/kas_masuk_process/'.$id_akundetil);
	

		$data['link'] 			= array('link_back' => anchor('keuangan/kas_detil/index/'.$id_akundetil,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
 
			$this->form_validation->set_rules('jumlah', 'jumlah', 'required');
			$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
			
		
		if ($this->form_validation->run() == TRUE)
		{
		

		
	// save data
			$uang = array('ket' 		=> $this->input->post('keterangan'),
						'id_akun' 		=> 502,
			'kode' 		=> 'ln2',
			'kredit' 		=> $this->input->post('jumlah'),
						'tanggal' 		=> $this->input->post('tanggal'),
						'debet' 		=> 0
						);
			$this->Kas_model->transaksi($uang);


			
			
			// save data
			$uang = array('ket' 		=> $this->input->post('keterangan'),
						'id_akun' 		=> $id_akundetil,
			'kode' 		=> 'ln2',
			'debet' 		=> $this->input->post('jumlah'),
			'kredit'=>0,
						'tanggal' 		=> $this->input->post('tanggal'),
			
						);
			$this->Kas_model->transaksi($uang);
						
			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect('keuangan/kas_detil/index/'.$id_akundetil);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	
}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
