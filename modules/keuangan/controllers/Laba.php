<?php
/**
 * Laba Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Laba extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'laba';
	
  
  function Laba()
	{
		parent::__construct();
	//	$this->load->model('Laba_model', '', TRUE);
		$this->load->model('produksi/Order_model', '', TRUE);
		$this->load->model('keuangan/Kas_model', '', TRUE);
		$this->load->model('sdm/Penggajian_model', '', TRUE);
		$this->load->model('keuangan/Belanja_model', '', TRUE);
		$this->load->model('sdm/Tunjangan_model', '', TRUE);
	

	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman laba,
	 * jika tidak akan meredirect ke halaman login
	 */

	

	
	/**
	 * Tampilkan semua data laba
	 */
	function index($tahun="",$dari="",$sampai="")
	{



		$data = $this->data;
		$data['h2_title'] = $this->title;
		$data['custom_view'] = 'keuangan/laba';



    $mulai=2014;

		
if(empty($tahun))
	$tahun=date("Y");



		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li>".anchor('keuangan/laba/index/'.$i,$i,array('class' => 'tahun'))." </li>";
    else
     $pagination.="<li class=active> <a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		


   
   
	if($this->input->post('cari'))
     {

     $dari=$this->input->post('dari');
     $sampai=$this->input->post('sampai');

if($tahun==date("Y") and $sampai>date("n"))
{
$sampai=date("n");

}

if($dari>$sampai)
$dari=$sampai;

     }
 

     else if($tahun==date("Y") )
     {

$dari=date("n");
$sampai=date("n");
     }
     else
{
$dari=1;
$sampai=12;
}



   
  



		$data['default']['dari'] = $dari;
		$data['default']['sampai'] = $sampai;



	$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
		
			$i = 0;




 $tgl1=$tahun."-".$dari."-1";

		
 		
 
 $tgl2=date('Y-m-t', strtotime("15-".$sampai."-".$tahun));
 

/**




*/



$hitung=$this->Order_model->hitung_omzet($tgl1,$tgl2);


$hitung_lain2=$this->Kas_model->hitung_omzet(502,$tgl1,$tgl2);
$total_lain2=$hitung_lain2->total_kredit;


$hitung_sdm=$this->Penggajian_model->hitung_omzet($tgl1,$tgl2);
$total_sdm=$hitung_sdm->total_gaji;


$hitung_belanja=$this->Belanja_model->hitung_omzet($tgl1,$tgl2);
$total_belanja=$hitung_belanja->total_belanja;



$hitung_tunjangan=$this->Tunjangan_model->hitung_tunjangan($tgl1,$tgl2);




$this->table->add_row("<h3>Pemasukan",'');
$this->table->add_row("rutin",

"<div class=pull-right>".	anchor_popup('produksi/produk/get_all/preview/'.$tgl1."/".$tgl2,number_format($hitung->total_omzet, 0, ',', '.'), array('class' => 'detail'))
."</div>");

$this->table->add_row("lain2","<div align=right>".number_format($total_lain2, 0, ',', '.')."</div>");
$this->table->add_row("<h3>Pengeluaran",'');
$this->table->add_row("gaji pegawai","<div align=right>".number_format($total_sdm, 0, ',', '.')."</div>");
$this->table->add_row("tunjangan kesehatan","<div align=right>".number_format($hitung_tunjangan->total_tunjangan, 0, ',', '.')."</div>");
$this->table->add_row("total pengeluaran","<div align=right>".number_format($total_belanja, 0, ',', '.')."</div>");



$saldo_final=$hitung->total_omzet+$total_lain2-$total_sdm-$total_belanja-$hitung_tunjangan->total_tunjangan;

$this->table->add_row("<h3>saldo","<h3><div align=right>".number_format($saldo_final, 0, ',', '.')."</div>");




  		$data['table'] = $this->table->generate();

		
	
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data laba
	 */
	

}
// END Laba Class

/* End of file laba.php */
/* Location: ./system/application/controllers/laba.php */
