<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pisau extends  CI_Controller {
	/**
	 * Constructor
	 */
  var $title = 'pisau';
  var $alamat = 'data/pisau';
  var $limit = 20;
	
  
  function Pisau()
	{
		parent::__construct();

		$this->load->model('Crud_model', '', TRUE);
	
  // content yang fix, ada terus di web

    $this->data['h2_title']=$this->title;
	  $this->load->helper('fungsi');		
   
	  $this->load->library('cekker');


    $this->cekker->cek($this->router->fetch_class());	
  





$lookup_kategori=array('nama_table'=>'kategori_pisau','id'=>'id_kategori_pisau','isi'=>'nama');

$lookup=array('ada'=>'ada','dibuang'=>'dibuang');

	
	$data_field=array('nama'=>array(),
	'id_kategori_pisau'=>array('judul'=>'ketegori','lookup_table'=>$lookup_kategori),
	'kode'=>array(),
	'keterangan'=>array('jenis'=>'textarea'),
	'status'=>array('lookup'=>$lookup),
	'gambar'=>array('jenis'=>'gambar')
	);

$fitur=array('');
$fitur['search']=array('nama','id_kategori_pisau');
$fitur['pagination']=array('per_page'=>$this->limit,'uri_segment'=>6, 'base_url' => site_url($this->alamat.'/index/'));
$fitur['validation']=array('nama'=>'required|max_length[32]','id_kategori_pisau'=>'is_natural_no_zero','status'=>'alpha');

if(cek_auth("auth_setting"))
$fitur['tambah']=$fitur['edit']=true;



$primary='id_pisau';
$data_utama=array('alamat'=>$this->alamat,'nama_table'=>'pisau','primary'=>$primary,'order_by'=>'id_kategori_pisau,kode');
 
   $this->load->library('Crud', array(
   'data_utama'=>$data_utama
   ,'fitur'=>$fitur
   ,'data_field'=>$data_field
   ));


}

	
	function index()
	{

$datax=func_get_args();

$tampilan=array_merge($this->data,$this->crud->retrieve($datax));

$this->load->view('template',$tampilan);
	}
		
	/**
	 * Hapus data supplier
	 */

 function delete($id = 0)
	{
$this->crud->delete($id);  
   }  
   

	function add($save = FALSE)
	{			

		$this->edit($save);
	}
	
function edit($id = -1)
	{
	

$tampilan=array_merge($this->data,$this->crud->update($id));

$this->load->view('template',$tampilan);

   }    

function gambar()
	{


	$datax=func_get_args();

$tampilan=array_merge($this->data,$this->crud->gambar($datax));

$this->load->view('template2',$tampilan);



	}



function upload_gambar()
	{
	

$this->crud->upload_gambar();




   }    



}
