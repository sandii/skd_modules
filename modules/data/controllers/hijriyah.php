<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Hijriyah extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'hijriyah';
		var $alamat = 'data/hijriyah';
	
  
  function Hijriyah()
	{
		parent::__construct();
		$this->load->model('auth/Akses_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	

	function index()
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/update_process');
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$pemroses = $this->Akses_model->get_variable_sistem_by_id('hijriyah');
				
	
		
		$data['default']['marketing']		= $pemroses->isi;
	
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data pemroses
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'pemroses_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/');
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('marketing', 'Marketing', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$pemroses = array(
							'isi'		=> $this->input->post('marketing')
						
						);
			$this->Akses_model->update_variable_sistem("hijriyah",$pemroses);
			
			$this->session->set_flashdata('message', 'Satu data pemroses berhasil diupdate!');
			redirect($this->alamat.'');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_pemroses valid, agar tidak ganda
	 */

}
// END Pemroses Class

/* End of file pemroses.php */
/* Location: ./system/application/controllers/pemroses.php */
