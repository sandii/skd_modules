<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Nomor extends  CI_Controller {
	/**
	 * Constructor
	 */

	  var $alamat = 'data/nomor';
	  var $limit = '20';


  function Nomor()
	{
		parent::__construct();
		$this->load->model('Crud_model', '', TRUE);

    $this->data['h2_title']='nomor penting';   
	$this->load->library('cekker');
	$this->cekker->cek($this->router->fetch_class());

$primary='id';


	
	$data_field=array('nama'=>array(),
	'nomor'=>array(),
	);

$fitur=array('');
$fitur['search']=array('nama');
$fitur['pagination']=array('per_page'=>$this->limit,'uri_segment'=>5, 'base_url' => site_url($this->alamat.'/index/'));
$fitur['validation']=array('nama'=>'required|max_length[32]','nomor'=>'required|max_length[32]');
$fitur['tambah']=$fitur['hapus']=$fitur['edit']=true;




$data_utama=array('alamat'=>$this->alamat,'nama_table'=>'telps','primary'=>$primary,'order_by'=>'nama');
 
   $this->load->library('Crud', array(
   'data_utama'=>$data_utama
   ,'fitur'=>$fitur
   ,'data_field'=>$data_field
   ));



}

	/**
	 * Tampilkan semua data jabatan
	 */
	function index()
	{
	

$datax=func_get_args();


$tampilan=array_merge($this->data,$this->crud->retrieve($datax));

$this->load->view('template',$tampilan);

	}

 function delete($id = 0)
	{
$this->crud->delete($id);  
   }  
   
function add($save = FALSE)
	{

		$this->edit($save);
	}
	
	function edit($id = -1)
	{
	
$tampilan=array_merge($this->data,$this->crud->update($id));

$this->load->view('template',$tampilan);
   }    
}
	


