<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Stokdetil extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 20;
	var $title = 'stok';

  
  function stokdetil()
	{
		parent::__construct();
		$this->load->model('Stok_sanggar_model', '', TRUE);

	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	


	function index($id_pegawai,$offset=0)
	{
		$konsumen = $this->Stok_sanggar_model->get_stok_by_id($id_pegawai);
	
		$data = $this->data;
		$data['h2_title'] = $konsumen->nama;
    $data['main_view'] = 'main';
		


		// Load data
		$query = $this->Stok_sanggar_model->get_stok_detil($id_pegawai);

		$siswa =$this->Stok_sanggar_model->get_stok_detil($id_pegawai,$this->limit,$offset)->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{



				$uri_segment = 5;
      		$config['base_url'] = site_url('data/stokdetil/index/'.$id_pegawai."/");
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
			$config['uri_segment'] = $uri_segment;


$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';






			
			$this->pagination->initialize($config);



			$data['pagination'] = $this->pagination->create_links();



			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','keterangan', 'penambahan', 'pengurangan', 'stok akhir');
	
			
			foreach ($siswa as $row)
			{



				$this->table->add_row( $row->tanggal,  $row->keterangan, $row->tambah,$row->kurang,$row->saldo
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data stokdetil!';
		}		
		
		$data['link'] = array('link_add' => anchor('data/stokdetil/add/'.$id_pegawai,'stok opname', array('class' => 'btn btn-success btn-lg'))
								);
		
		// Load view
		$this->load->view('template2', $data);
	}


	function add($id_pegawai)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.' Opname';
		$data['custom_view'] 		= 'stok_add';
		$data['form_action']	= site_url('data/stokdetil/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor('data/stokdetil/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg'))
										);
										
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai)
	{
	  $data=$this->data;
		$data['h2_title'] 		= $this->title.' Opname';
		$data['custom_view'] 		= 'stok_add';
		$data['form_action']	= site_url('data/stokdetil/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor('data/stokdetil/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg'))
										);		// data kelas untuk dropdown menu
	

    
    
    $saldo_terakhir = $this->Stok_sanggar_model->get_terakhir($id_pegawai);
 
   
   $saldo=0;
    if(!empty($saldo_terakhir))
    $saldo=$saldo_terakhir;
		
		
		
	
		$saldo=$saldo+$this->input->post('pemasukan')-$this->input->post('pengeluaran');
		
	

			// save data
			$uang = array('keterangan' 		=> $this->input->post('keterangan'),
	
			'id_stok' 		=> $id_pegawai,
							'tambah'		=> $this->input->post('pemasukan'),
							'kurang'	=> $this->input->post('pengeluaran'),
							'tanggal'	=> $this->input->post('tanggal'),
											'saldo'	=> $saldo,
										
								
						);
			$this->Stok_sanggar_model->add_detil($uang);
			
			$this->session->set_flashdata('message', 'data stok berhasil disimpan!');
			redirect('data/stokdetil/index/'.$id_pegawai);
		
			
	}
	
	

}
