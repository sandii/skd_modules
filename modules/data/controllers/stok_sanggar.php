<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Stok_sanggar extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Stok produksi';
  var $limit = '20';
	
  
  function Stok_sanggar()
	{
		parent::__construct();
		$this->load->model('Stok_sanggar_model', '', TRUE);


	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
  
  	$this->load->helper('fungsi');		

	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bahanbeli,
	 * jika tidak akan meredirect ke halaman login
	 */


	
		function index()
	{
		$data = $this->data;
	
  
  /**



  */

	  
  	$data['h2_title'] = $this->title;
		

if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');

 
    }



//	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
  
    if(empty($tgl1)) 

    	$tgl1=date("Y-m-1");
  


    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    
    else $cari_tgl2=$tgl2;
   

	$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;
	


		
		// Load data
		$query = $this->Stok_sanggar_model->get_all();
		$bahanbeli = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			


			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");

			$this->table->set_heading('No', 'nama', 'stok','');
			$i = 0;
			
	$totaly=0;
			foreach ($bahanbeli as $row)
			{


	//	$totalx = $this->Produk_model->get_omzet_barang($row->id_produk,$tgl1,$cari_tgl2)->total;
		

if(cek_auth("auth_order"))
{
$edit=   anchor('data/stok_sanggar/update/'.$row->id_stok,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'));

}
else
{
$edit="";
}

  $saldo_terakhir = $this->Stok_sanggar_model->get_terakhir($row->id_stok);

				$this->table->add_row(++$i,  
        
      
     $row->nama, 
        anchor_popup('data/stokdetil/index/'.$row->id_stok, $saldo_terakhir,array('class' => 'detail'))	,

        $edit

     );	
        
 
			}

if(cek_auth("auth_order"))

$this->table->add_row("","<h5 align=right>total", "<div align=right>". number_format($totaly, 0, ',', '.'));



			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bahanbeli!';
		}		
		
if(cek_auth("auth_produksi"))

		$data['link'] = array('link_add' => anchor('data/stok_sanggar/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg'))
								);
		
		// Load view
	

		$this->load->view('template', $data);
	}
		

/**


*/

	function add()
	{		
		
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Tambah Data';
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url('data/stok_sanggar/add_process/');
		$data['link'] 			= array('link_back' => anchor('data/stok_sanggar','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg')));
		
		 	
      
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data bahanbeli
	 */
	function add_process()
	{
		
	$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Tambah Data';
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url('data/stok_sanggar/add_process/');
		$data['link'] 			= array('link_back' => anchor('data/stok_sanggar','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg')));
	





		$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing'),
							
							
      						
						);
			// Proses penyimpanan data di table bahanbeli
			$this->Stok_sanggar_model->add($bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil disimpan!');
			redirect('data/stok_sanggar');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update bahanbeli
	 */
	function update($id_bahanbeli)
	{
		
		$data			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url('data/stok_sanggar/update_process/'.$id_bahanbeli);
		$data['link'] 			= array('link_back' => anchor('data/stok_sanggar/','kembali', array('class' => 'back')));
    	
		// cari data dari database
		$bahanbeli = $this->Stok_sanggar_model->get_produk_by_id($id_bahanbeli);
				
		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;
	


	

				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bahanbeli
	 */
	function update_process($id_bahanbeli)
	{


		$data			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update';
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url('data/stok_sanggar/update_process/'.$id_bahanbeli);
		$data['link'] 			= array('link_back' => anchor('data/stok_sanggar/','kembali', array('class' => 'back')));
    	
		// cari data dari database
		$bahanbeli = $this->Stok_sanggar_model->get_produk_by_id($id_bahanbeli);
				
		// Data untuk mengisi field2 form
			
		$data['default']['marketing']		= $bahanbeli->nama;
	

	$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[50]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$bahanbeli = array(
							'nama'		=> $this->input->post('marketing'),
							
				
							
						);
			$this->Stok_sanggar_model->update($id_bahanbeli,$bahanbeli);
			
			$this->session->set_flashdata('message', 'Satu data bahanbeli berhasil diupdate!');
			redirect('data/stok_sanggar/'.$id_akundetil);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
/**


*/


		


	/**
	 * Cek apakah $id_bahanbeli valid, agar tidak ganda
	 */

}
// END Bahanbeli Class

/* End of file bahanbeli.php */
/* Location: ./system/application/controllers/bahanbeli.php */
