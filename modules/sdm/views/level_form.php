<?php 
	
	echo ! empty($message) ? '<p class="message">' . $message . '</p>': '';

	$flashmessage = $this->session->flashdata('message');
	echo ! empty($flashmessage) ? '<p class="message">' . $flashmessage . '</p>': '';
?>

<form name="marketing_form" method="post" action="<?php echo $form_action; ?>"
onsubmit="document.getElementById('submit').disabled=true;
document.getElementById('submit').value='proses';">
	
	
	<p>
		<label for="tanggal">Nama:</label>
		<input type="text" class="form-control" name="marketing" size="30" value="<?php echo set_value('marketing', isset($default['marketing']) ? $default['marketing'] : ''); ?>" />
		
	</p>
	<?php echo form_error('marketing','<div class="alert alert-danger" role="alert">','</div>');?>	
	<p>
		<label for="tanggal">gaji pokok:</label>
		<input type="text" class="form-control" name="gaji_pokok" size="30" value="<?php echo set_value('gaji_pokok', isset($default['gaji_pokok']) ? $default['gaji_pokok'] : ''); ?>" />
		
	</p>
		<p>
		<label for="tanggal">tunjangan komunikasi:</label>
		<input type="text" class="form-control" name="komunikasi" size="30" value="<?php echo set_value('komunikasi', isset($default['komunikasi']) ? $default['komunikasi'] : ''); ?>" />
		
	</p>
		</p>
		<p>
		<label for="tanggal">tunjangan transportasi:</label>
		<input type="text" class="form-control" name="transportasi" size="30" value="<?php echo set_value('transportasi', isset($default['transportasi']) ? $default['transportasi'] : ''); ?>" />
		
	</p>
		</p>
		<p>
		<label for="tanggal">tunjangan kehadiran:</label>
		<input type="text" class="form-control" name="kehadiran" size="30" value="<?php echo set_value('kehadiran', isset($default['kehadiran']) ? $default['kehadiran'] : ''); ?>" />
		
	</p>
			<p>
		<label for="tanggal">tunjangan lama kerja:</label>
		<input type="text" class="form-control" name="lama_kerja" size="30" value="<?php echo set_value('lama_kerja', isset($default['lama_kerja']) ? $default['lama_kerja'] : ''); ?>" />
		
	</p>
	<p>
		<input type="submit" name="submit" id="submit" value=" Simpan " />
	</p>
</form>

<?php
	if ( ! empty($link))
	{
		echo '<p id="bottom_link">';
		foreach($link as $links)
		{
			echo $links . ' ';
		}
		echo '</p>';
	}
?>