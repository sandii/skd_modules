<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Gaji extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'gaji';
	var $alamat = 'sdm/gaji';

  
  function gaji()
	{
		parent::__construct();
		$this->load->model('Gaji_model', '', TRUE);
		$this->load->model('Pegawai_model', '', TRUE);
		$this->load->model('Bagian_model', '', TRUE);
		$this->load->model('Level_model', '', TRUE);
	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_pegawai)
	{
	
	$konsumen = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
	
	
	

	
		$data = $this->data;
			$data['h2_title']=$data['title'] = "format gaji : ".$konsumen->nama;
	

		

		
		// Load data
		$query = $this->Gaji_model->get_all($id_pegawai);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		




		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','bagian','level','performance','tunjangan transport','tunjangan lain2','nilai tunjangan lain2' );
	
			
			foreach ($siswa as $row)
			{


$level=$this->Level_model->get_level_by_id($row->level);
$bagian=$this->Bagian_model->get_bagian_by_id($row->bagian);


$nama_level="";
if(!empty($level))
$nama_level=$level->nama;

$nama_bagian="";
if(!empty($bagian))
$nama_bagian=$bagian->nama;

$nama_performance=array("cukup","sedang","bagus");

$performance=$row->performance;
if(empty($performance))
$performance=0;




				$this->table->add_row( $row->tanggal,  $nama_bagian, $nama_level,
			$nama_performance[$performance],	$row->transportasi,	$row->lain2,	$row->jumlah_lain2			
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data gaji!';
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$id_pegawai,'<span class="glyphicon glyphicon-pencil"></span>', array('class' => 'btn btn-lg btn-success'))
								
								
								);
		
		// Load view
		$this->load->view('template2', $data);
	}




	function add($id_pegawai)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'gaji_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
			





		
$jabatan = $this->Level_model->get_level()->result();
  	foreach($jabatan as $row)
			{
  
      $data['options_level'][$row->id_level] = $row->nama;
 		}

$jabatan = $this->Bagian_model->get_bagian()->result();
  	foreach($jabatan as $row)
			{
  
      $data['options_bagian'][$row->id_bagian] = $row->nama;
 		}



	$user = $this->Gaji_model->get_terakhir($id_pegawai);
				
	
if(!empty($user))
	{
		// Data untuk mengisi field2 form
		$data['default']['transportasi'] 	= $user->transportasi;		
		$data['default']['level']		= $user->level;
		$data['default']['bagian']		= $user->bagian;
		$data['default']['performance']		= $user->performance;

		$data['default']['lain2']		= $user->lain2;

		$data['default']['jumlah_lain2']		= $user->jumlah_lain2;
}
else
{
		$data['default']['transportasi'] 	= "";		
		$data['default']['level']		="";
		$data['default']['bagian']		= "";
		$data['default']['performance']		= "";

		$data['default']['lain2']		= "";

		$data['default']['jumlah_lain2']		="";

}










		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai)
	{
	  $data=$this->data;
		$data['h2_title'] 		= $this->alamat.' > Tambah Data';
		$data['custom_view'] 		= 'gaji_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// data kelas untuk dropdown menu
	
		// Set validation rules
		$this->form_validation->set_rules('pemasukan', 'required');
    
 
		
		
		if ($this->form_validation->run() == TRUE)
		{
		
		
		
		
			// save data
			$uang = array('keterangan' 		=> $this->input->post('keterangan'),
	
			 'id_pegawai' 		=> $id_pegawai,
							'gaji'		=> $this->input->post('pemasukan'),
					
							'tanggal'	=> $this->input->post('tanggal'),
							'bagian'	=> $this->input->post('bagian'),
							'level'	=> $this->input->post('level'),
							'performance'	=> $this->input->post('performance'),
							'transportasi'	=> $this->input->post('transportasi'),
							'lain2'	=> $this->input->post('lain2'),
							'jumlah_lain2'	=> $this->input->post('jumlah_lain2'),

						);
			$this->Gaji_model->add($uang);
			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect($this->alamat.'/index/'.$id_pegawai);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}


	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama
	 */
	
	}


// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
