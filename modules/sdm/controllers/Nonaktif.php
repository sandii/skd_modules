<?php
/**
 * Pegawai Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Nonaktif extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'Pegawai non aktif';
	
  
  function Nonaktif()
	{
		parent::__construct();
		$this->load->model('Pegawai_model', '', TRUE);
		$this->load->model('Cuti_model', '', TRUE);
		$this->load->model('Kasbon_model', '', TRUE);

  // content yang fix, ada terus di web
    $this->data['title']=$this->title;
					  $this->load->helper('fungsi');
   
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman pegawai,
	 * jika tidak akan meredirect ke halaman login
	 */


	/**
	 * Tampilkan semua data pegawai
	 */
	function index()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
		
		
		
		
		// Load data
		$query = $this->Pegawai_model->get_nonaktif();
		$pegawai = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'nama','alamat','tempat/tanggal lahir','telp','email','gol. darah','status','jumlah anak','tanggal masuk','tgl keluar','kasbon');
			$i = 0;
	
  		
			foreach ($pegawai as $row)
			{


$kasbon = $this->Kasbon_model->get_terakhir($row->id_pegawai);
isset($kasbon->saldo)?$saldo_kasbon=$kasbon->saldo:$saldo_kasbon=0;


	$this->table->add_row(++$i,anchor_popup('sdm/pegawai_detail/index/'.$row->id_pegawai,$row->nama,array('class' => 'link_aja','width'=>1000)),

  $row->alamat,
  $row->tmp_lahir."/ ".$row->tgl_lahir,
  
  
   $row->telp,
 
 

  $row->email,
    $row->goldar,
  $row->status,
   $row->anak,
     $row->tgl_masuk,
  $row->tgl_keluar,
     cek_auth("auth_bayar")?anchor_popup('sdm/kasbon/index/'.$row->id_pegawai,$saldo_kasbon,array('class' => 'link_aja','width'=>1000)):""

						      
						      			      
											);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data pegawai!';
		}		
		
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data pegawai
	 */
	function delete($id_pegawai)
	{
		$this->Pegawai_model->delete($id_pegawai);
		$this->session->set_flashdata('message', '1 data pegawai berhasil dihapus');
		
		redirect('pegawai');
	}
	
	/**
	 * Pindah ke halaman tambah pegawai
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('pegawai/add_process');
		$data['link'] 			= array('link_back' => anchor('pegawai','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data pegawai
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('pegawai/add_process');
		$data['link'] 			= array('link_back' => anchor('pegawai','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// Set validation rules
		
		$this->form_validation->set_rules('nama', 'nama', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$pegawai = array(
							'nama'		=> $this->input->post('nama'),
							'alamat'		=> $this->input->post('alamat'),
							'tmp_lahir'		=> $this->input->post('tmp_lahir'),
							'tgl_lahir'		=> $this->input->post('tgl_lahir'),
							'tgl_masuk'		=> $this->input->post('tgl_masuk'),
						
							'telp'		=> $this->input->post('telp'),
							'email'		=> $this->input->post('email'),
							'goldar'		=> $this->input->post('goldar'),
							'status'		=> $this->input->post('status'),
							'anak'		=> $this->input->post('anak'),
							'gajian'		=> $this->input->post('gajian')
												
						
						);

$xx=$this->input->post('tgl_keluar');					
if(!empty($xx))
$pegawai['tgl_keluar']=$this->input->post('tgl_keluar');

			// Proses penyimpanan data di table pegawai
			$this->Pegawai_model->add($pegawai);
			
			$this->session->set_flashdata('message', 'Satu data pegawai berhasil disimpan!');
			redirect('pegawai/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update pegawai
	 */
	function update($id_pegawai)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('pegawai/update_process');
		$data['link'] 			= array('link_back' => anchor('pegawai','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$pegawai = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
				
		// buat session untuk menyimpan data primary key (id_pegawai)
		$this->session->set_userdata('id_pegawai', $pegawai->id_pegawai);
		
		// Data untuk mengisi field2 form
		
		$data['default']['nama']		= $pegawai->nama;
		$data['default']['alamat']		= $pegawai->alamat;
		$data['default']['telp']		= $pegawai->telp;
		$data['default']['email']		= $pegawai->email;
		$data['default']['tmp_lahir']		= $pegawai->tmp_lahir;
		$data['default']['tgl_lahir']		= $pegawai->tgl_lahir;
		$data['default']['tgl_masuk']		= $pegawai->tgl_masuk;
		$data['default']['tgl_keluar']		= $pegawai->tgl_keluar;
		$data['default']['status']		= $pegawai->status;
		$data['default']['anak']		= $pegawai->anak;
		$data['default']['goldar']		= $pegawai->goldar;
		$data['default']['gajian']		= $pegawai->gajian;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data pegawai
	 */
	function update_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'pegawai/pegawai_form';
		$data['form_action']	= site_url('pegawai/update_process');
		$data['link'] 			= array('link_back' => anchor('pegawai','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('nama', 'nama', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$pegawai = array(

							'nama'		=> $this->input->post('nama'),
							'alamat'		=> $this->input->post('alamat'),
							'tmp_lahir'		=> $this->input->post('tmp_lahir'),
							'tgl_lahir'		=> $this->input->post('tgl_lahir'),
							'tgl_masuk'		=> $this->input->post('tgl_masuk'),
							'telp'		=> $this->input->post('telp'),
							'email'		=> $this->input->post('email'),
							'goldar'		=> $this->input->post('goldar'),
							'status'		=> $this->input->post('status'),
							'anak'		=> $this->input->post('anak'),
							'gajian'		=> $this->input->post('gajian')

						);

$xx=$this->input->post('tgl_keluar');					
if(!empty($xx))
$pegawai['tgl_keluar']=$this->input->post('tgl_keluar');



			$this->Pegawai_model->update($this->session->userdata('id_pegawai'),$pegawai);
			
			$this->session->set_flashdata('message', 'Satu data pegawai berhasil diupdate!');
			redirect('pegawai');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_pegawai valid, agar tidak ganda
	 */
	function valid_id($id_pegawai)
	{
		if ($this->Pegawai_model->valid_id($id_pegawai) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "pegawai dengan Kode $id_pegawai sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_pegawai valid, agar tidak ganda. Hanya untuk proses update data pegawai
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_pegawai');
		$new_id			= $this->input->post('id_pegawai');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->Pegawai_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "Pegawai dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END Pegawai Class

/* End of file pegawai.php */
/* Location: ./system/application/controllers/pegawai.php */
