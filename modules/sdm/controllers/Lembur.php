<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Lembur extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'lembur';
	var $alamat = 'sdm/lembur';
	
var $nama_bulan=array(1=>"januari","februari","maret","april","mei","juni","juli","agustus","september","oktober","november","desember");

  
  function Lembur()
	{
		parent::__construct();
		$this->load->model('Lembur_model', '', TRUE);
	$this->load->model('Pegawai_model', '', TRUE);
	

	
	
	// content yang fix, ada terus di web
    $this->data['title']=$this->title;



     $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	
	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_pegawai,$tahun="")
	{
	
	if (empty($tahun))
	$tahun=date("Y");
	
	
	
	
		$konsumen = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
		
		$data = $this->data;
		$data['h2_title'] = "Lembur: ".$konsumen->nama;
  
		
		
		$siswa = $this->Lembur_model->get_all($id_pegawai,$tahun);
		$num_rows = $this->Lembur_model->count_all();
		
		
		
		
		
		$mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{
    //$pagination.=" $i ";
    
    if($tahun!=$i)
    $pagination.="<li>".anchor($this->alamat.'/index/'.$id_pegawai."/".$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }
		
		
		
		
		$data['pagination'] = $pagination;		
		if ($num_rows > 0)
		{
			// Generate pagination			
			$config['base_url'] = site_url('keuangan/get_all');
			
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('bulan', 'jumlah jam', 'keterangan', 'sudah dibayar?','action');
	
			
			foreach ($siswa as $row)
			{
			
   if($row->dibayar=="belum")
       	$link=anchor($this->alamat.'/update/'.$row->id_lembur."/".$id_pegawai,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'));
        else
        $link="";
      
      	$this->table->add_row( $this->nama_bulan[$row->bulan],$row->jam, nl2br($row->keterangan),$row->dibayar,
        				
               $link 
               
        );
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data lembur!';
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$id_pegawai,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		
		
		
		$this->load->view('template2', $data);
	}






	function add($id_pegawai)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'lembur_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);


$data['default']['bulan']=date("n");



for($bulan=1;$bulan<=12;$bulan++)
{

$data['option_bulan'][$bulan] = $this->nama_bulan[$bulan];
 }        
              			

		
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai)
	{
	  $data=$this->data;
	  
	  
	  
	  
		$data['h2_title'] 		= 'keuangan > Tambah Data';
		$data['custom_view'] 		= 'lembur_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))

										);
										
		
		$tahun=date("Y");
$cek_minggu=$this->Lembur_model->check_bulan($id_pegawai, $tahun,$this->input->post('bulan'));

	
		if (!$cek_minggu)
		{
		
		
			// save data
			$uang = array(
				'id_pegawai'		=> $id_pegawai,
       	'bulan'		=> $this->input->post('bulan'),
       	       	'jam'		=> $this->input->post('jam'),
        	'tahun'		=> date("Y"),
       	'keterangan'		=> $this->input->post('keterangan')
       
         		);
			$this->Lembur_model->add($uang);
			
			$this->session->set_flashdata('message', 'data lembur berhasil disimpan!');
			redirect($this->alamat.'/index/'.$id_pegawai);
		}
		else
		{	
		$this->session->set_flashdata('message', 'ERROR!!!!!! data bulan ke '.$this->input->post('bulan').' sudah pernah diisi');
		redirect($this->alamat.'/index/'.$id_pegawai);
		}		
	}
	
	
	
	function update($id_lembur,$id_pegawai)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'lembur_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_pegawai);
	

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$row= $this->Lembur_model->get_lembur_by_id($id_lembur)->row();
			
		// buat session untuk menyimpan data primary key (id_menu)
$this->session->set_userdata('id_lembur', $id_lembur);
		
	//	echo $menu->sayur;
		// Data untuk mengisi field2 form

  
    $data['aksi']="update";
    
		$data['default']['jam'] 	=  $row->jam; 
    $data['default']['keterangan'] 	=  $row->keterangan;
   
    $data['default']['bulan'] 	=  $row->bulan;
  
  			
		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data menu
	 */
	function update_process($tahun)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'menu/menu_form';
		$data['form_action']	= site_url('menu/update_process');
	

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
	
	
	
	
	
			// save data
			$menu = array(
       	'keterangan'		=> $this->input->post('keterangan'),
       	'jam'		=> $this->input->post('jam')
  						);
			$this->Lembur_model->update($this->session->userdata('id_lembur'),$menu);
			
			$this->session->set_flashdata('message', 'Satu data lembur berhasil diupdate!');
			redirect($this->alamat.'/index/'.$tahun);
	
	}
	
	
	
	
	
	
	
	
	
	
		}
	
	
	/**
	 * Menampilkan form update data konsumen
	 */

	
	/**
	 * Validasi untuk id_konsumen, agar tidak ada konsumen dengan id_konsumen sama


}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
