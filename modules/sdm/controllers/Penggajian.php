<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Penggajian extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */ 
	
	var $limit = 30;
	var $title = 'penggajian';
	var $alamat = 'sdm/penggajian';


  
  function penggajian()
	{
		parent::__construct();
		$this->load->model('Gaji_model', '', TRUE);
		$this->load->model('Lembur_model', '', TRUE);
		$this->load->model('Pegawai_model', '', TRUE);
		$this->load->model('Penggajian_model', '', TRUE);
		$this->load->model('Kasbon_model', '', TRUE);
		$this->load->model('keuangan/Akun_model', '', TRUE);	
		$this->load->model('Level_model', '', TRUE);	

		$this->load->model('Bagian_model', '', TRUE);	

				$this->load->model('keuangan/Kas_model', '', TRUE);	
				$this->load->helper('fungsi');	

	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;


	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	

	}
	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_pegawai,$tahun = "")
	{
	
	$konsumen = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
	
	
		$data = $this->data;
		$data['h2_title']=$data['title'] = "gaji : ".$konsumen->nama;

		
$hijriyah_skr=tgl_hijriyah();
$tahun_skr=$hijriyah_skr[2];
$bulan_skr=$hijriyah_skr[3];



if (empty($tahun))
	$tahun=$tahun_skr;


    $mulai=1437;
		
		$akhir=$tahun_skr;
		$pagination="";


if($tahun==2015)
 $pagination.="<li class=active><a>2015</a></li>";
 else	
    $pagination.="<li> ".anchor($this->alamat.'/index/'.$id_pegawai."/".'2015',2015,array('class' => 'tahun'))." </li>";


		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li> ".anchor($this->alamat.'/index/'.$id_pegawai."/".$i,$i,array('class' => 'tahun'))." </li>";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		

		
		
		// Load data
		$query = $this->Penggajian_model->get_all($id_pegawai,$tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		

		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table >',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('bulan','pokok', 'lama kerja','bagian','performance','transport','komunikasi','kehadiran','lain2','nilai lain2','jam lembur','fee lembur','potong kasbon','bonus','total','tanggal dibayar','slip gaji' );
	
				
			foreach ($siswa as $row)
			{
				$this->table->add_row( bln_hijriyah($row->bulan), format_uang($row->pokok),format_uang($row->lama_kerja),format_uang($row->bagian),
				format_uang($row->performance), format_uang($row->transportasi),  format_uang($row->komunikasi), format_uang($row->kehadiran), $row->lain2, format_uang($row->jumlah_lain2),   $row->jam_lembur,format_uang($row->lembur),format_uang($row->kasbon),format_uang($row->bonus), format_uang($row->total),$row->tanggal,
						     
						     anchor_popup('sdm/slipgaji/index/'.$row->id_penggajian,'<span class="glyphicon glyphicon-print"></span>', array('class' => 'btn btn-xs btn-success','width'=>800))
				
						     		);
$bulan_gaji=$row->bulan+1;
}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data gaji!';
		
		}		

if(empty($bulan_gaji))
$bulan_gaji=$bulan_skr;

		
		
		if($bulan_gaji!=13 and $bulan_gaji<=$bulan_skr)
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$id_pegawai.'/'.$tahun.'/'.$bulan_gaji,'bayar gaji', array('class' => 'btn btn-success btn-lg')));


    $data['link']['link_riwayat'] = anchor_popup('sdm/gaji/index/'.$id_pegawai,'format gaji', array('class' => 'btn btn-success btn-lg','width'=>800));
		
	
		
		// Load view
		$this->load->view('template2', $data);
	}


/**



*/

	function add($id_pegawai,$tahun,$bulan)
	{		
	  $data=$this->data;
	  $data['bulan']=$bulan;
		  
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'penggajian_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai.'/'.$tahun.'/'.$bulan);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai.'/'.$tahun,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);


 $query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	  
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			}                    




$gaji = $this->Gaji_model->get_terakhir($id_pegawai);
isset($gaji->gaji)?$saldo_gaji=$gaji->gaji:$saldo_gaji=0;


$level=$this->Level_model->get_level_by_id($gaji->level);
$bagian=$this->Bagian_model->get_bagian_by_id($gaji->bagian);
$pegawai=$this->Pegawai_model->get_pegawai_by_id($id_pegawai);

$tgl_masuk=$pegawai->tgl_masuk;


if($gaji->level==2)
{

 $tanggal_spv=$this->Gaji_model->get_supervisor($id_pegawai);
$tgl_masuk=$tanggal_spv->tanggal;
}




$diff = abs(strtotime("now") - strtotime($tgl_masuk));

$tahun_kerja = floor($diff / (365*60*60*24));

$grade=$bagian->grade;

$gaji_pokok=$level->gaji_pokok;
$data['gaji']=$gaji_pokok;
$data['komunikasi']=$level->komunikasi;


$transportasi=$gaji->transportasi;

if($transportasi=="ya")
$data['transportasi']=$level->transportasi;
else
$data['transportasi']=0;

$data['kehadiran']=$level->kehadiran;


$data['nama']=$pegawai->nama;
$data['level']=$level->nama;
$data['bagianx']=$bagian->nama;



$data['bagian']=0.1*$grade*$gaji_pokok;
$data['performance']=$gaji->performance*0.1*$gaji_pokok;
$data['lain2']=$gaji->lain2;
$data['jumlah_lain2']=$gaji->jumlah_lain2;

$data['lama_kerja']=$tahun_kerja*$level->lama_kerja*$gaji_pokok/100;




$lembur=$this->Lembur_model->get_lembur($id_pegawai);
isset($lembur->total)?$saldo_lembur=$lembur->total:$saldo_lembur=0;



$kasbon = $this->Kasbon_model->get_terakhir($id_pegawai);
isset($kasbon->saldo)?$saldo_kasbon=$kasbon->saldo:$saldo_kasbon=0;

if($saldo_kasbon>500000)
	$saldo_kasbon=500000;

$data['kasbon']=$saldo_kasbon;

$data['lembur']=$saldo_lembur;



$data['bayar_lembur']=$gaji_pokok/25/8*1.5*$saldo_lembur;


		
		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai,$tahun,$bulan)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'penggajian > Tambah Data';
		$data['custom_view'] 		= 'penggajian_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai.'/'.$tahun.'/'.$bulan);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai.'/'.$tahun,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))										);
		$tanggal=date("Y-m-d");	
			// save data


$total=

$this->input->post('gaji')
+$this->input->post('lama_kerja')
+$this->input->post('bagian')
+$this->input->post('performance')
+$this->input->post('transportasi')
+$this->input->post('komunikasi')
+$this->input->post('kehadiran')
+$this->input->post('jumlah_lain2')
-$this->input->post('kasbon')
+$this->input->post('bonus')
+$this->input->post('bayar_lembur')
;


$id_terakhir=$this->Penggajian_model->get_terakhir()->id_penggajian;

$id_terakhir++;

			$uang = array(
	"id_penggajian"=>$id_terakhir,
			 'id_pegawai' 		=> $id_pegawai,
			 'bulan' 		=> $bulan,
			 			 'tahun' 		=> $tahun,
			 			 'tanggal' 		=> $tanggal,						 
			 			 'lembur' 		=> $this->input->post('bayar_lembur'),						 
					
					'jam_lembur'=> $this->input->post('lembur'),
					'bonus'=> $this->input->post('bonus'),
					'kasbon'=> $this->input->post('kasbon'),
'pokok'=> $this->input->post('gaji'),
'lama_kerja'=> $this->input->post('lama_kerja'),
'bagian'=> $this->input->post('bagian'),
'performance'=> $this->input->post('performance'),
'transportasi'=> $this->input->post('transportasi'),
'komunikasi'=> $this->input->post('komunikasi'),
'kehadiran'=> $this->input->post('kehadiran'),
'lain2'=> $this->input->post('lain2'),
'jumlah_lain2'=> $this->input->post('jumlah_lain2'),
'total'=>$total
	
						);
	$this->Penggajian_model->add($uang);
	$this->Lembur_model->update_bayar($id_pegawai);
// ngurangi kasbon

$nama = $this->Pegawai_model->get_pegawai_by_id($id_pegawai)->username;

		$uang = array('ket' 		=> 'gaji '.$nama,
						'id_akun' 		=> $this->input->post('ke'),
		'kode'=>"gji",
		'id_detail'=>$id_terakhir,
			'debet' 		=> 0,
						'tanggal' 		=> $tanggal,
						'kredit' 		=> $total
						
						);
			$this->Kas_model->transaksi($uang);






    $saldo_terakhir = $this->Kasbon_model->get_terakhir($id_pegawai);    
    $saldo=$saldo_terakhir->saldo;
		
		
		
		$saldo=$saldo-$this->input->post('kasbon');


			
if($this->input->post('kasbon'))	
{		
$uang = array('keterangan' 		=> "potong gaji bulan ".$this->nama_bulan[$bulan],
	
			'id_pegawai' 		=> $id_pegawai,
							'pengeluaran'		=> $this->input->post('kasbon'),
		
							'tanggal'	=> $tanggal,
							
											'saldo'	=> $saldo
						);
			$this->Kasbon_model->add($uang);
	
}

			
			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect($this->alamat.'/index/'.$id_pegawai.'/'.$tahun);
	
	}





function rangkuman($tgl1="",$tgl2="",$offset = 0)
	{
	

	
		$data = $this->data;

		$data['h2_title'] = "penggajian";
		$data['custom_view'] = 'main_tanggal';
		
		
	if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $offset=0;
     }



	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
   
    if(empty($tgl1)) {$tgl1=$seminggu;$cari_tgl1=$seminggu;}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
  

		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;	
	



		// Load data
		$query = $this->Penggajian_model->get_rangkuman($this->limit,$offset,$cari_tgl1,$cari_tgl2);

		$siswa = $query->result();
		
		$num_rows = $this->Penggajian_model->get_rangkuman("",$offset,$cari_tgl1,$cari_tgl2)->num_rows();

	

		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
	
$uri_segment = 6;
      $config['base_url'] = site_url($this->alamat.'/rangkuman/'.$tgl1.'/'.$tgl2.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
					$config['uri_segment'] = $uri_segment;



$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		
			




			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','nama pegawai','pokok', 'jam lembur','fee lembur','potong kasbon','bonus','total','slip gaji' );
	
				
			foreach ($siswa as $row)
			{
$nama_pegawai=$this->Pegawai_model->get_pegawai_by_id($row->id_pegawai)->nama;

				$this->table->add_row($row->tanggal, $nama_pegawai,format_uang($row->pokok), $row->jam_lembur,format_uang($row->lembur),format_uang($row->kasbon),format_uang($row->bonus), format_uang($row->total),
						     
						     anchor_popup('sdm/slipgaji/index/'.$row->id_penggajian,'<span class="glyphicon glyphicon-print"></span>', array('class' => 'btn btn-xs btn-success','width'=>800))
				
						     		);
$bulan_gaji=$row->bulan+1;
}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data gaji!';
		
		
		
		}		

		
if(empty($bulan_gaji))
$bulan_gaji=date("n");		
		
		


		


		
		// Load view
		$this->load->view('template', $data);
	}







	}


// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
