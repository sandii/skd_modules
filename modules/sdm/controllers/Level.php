<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Level extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'level';
	  	var $alamat = 'sdm/level';

  
  function Level()
	{
		parent::__construct();
		$this->load->model('Level_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman level,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data level
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
		
		// Load data
		$query = $this->Level_model->get_all();
		$level = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Level', 'gaji pokok', 'tunjangan komunikasi','tunjangan transportasi','tunjangan kehadiran','tunjangan lama kerja <br>(dalam % )','Actions');
			$i = 0;
			
			foreach ($level as $row)
			{
				$this->table->add_row(++$i,  $row->nama,$row->gaji_pokok,$row->komunikasi,$row->transportasi,$row->kehadiran,$row->lama_kerja,
										anchor($this->alamat.'/update/'.$row->id_level,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'))
										
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data level!';
		}		
		

		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data level
	 */
	function delete($id_level)
	{
		$this->Level_model->delete($id_level);
		$this->session->set_flashdata('message', '1 data level berhasil dihapus');
		
		redirect($this->alamat.'');
	}
	
	/**
	 * Pindah ke halaman tambah level
	 */
	
	
	/**
	 * Proses tambah data level
	 */
	
	
	/**
	 * Pindah ke halaman update level
	 */
	function update($id_level)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'level_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_level);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$level = $this->Level_model->get_level_by_id($id_level);
				
	
		
		
		$data['default']['marketing']		= $level->nama;
		$data['default']['gaji_pokok']		= $level->gaji_pokok;
		$data['default']['komunikasi']		= $level->komunikasi;
		$data['default']['transportasi']		= $level->transportasi;
		$data['default']['kehadiran']		= $level->kehadiran;
		$data['default']['lama_kerja']		= $level->lama_kerja;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data level
	 */
	function update_process($id_level)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'level_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_level);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		$level = $this->Level_model->get_level_by_id($id_level);
				
	
		
		
		$data['default']['marketing']		= $level->nama;
		$data['default']['gaji_pokok']		= $level->gaji_pokok;
		$data['default']['komunikasi']		= $level->komunikasi;
		$data['default']['transportasi']		= $level->transportasi;
		$data['default']['kehadiran']		= $level->kehadiran;
	
		$data['default']['lama_kerja']		= $level->lama_kerja;





		$this->form_validation->set_rules('marketing', 'Marketing', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$level = array(
							'nama'		=> $this->input->post('marketing'),
							'gaji_pokok'		=> $this->input->post('gaji_pokok'),
							'transportasi'		=> $this->input->post('transportasi'),
							'komunikasi'		=> $this->input->post('komunikasi'),
							'kehadiran'		=> $this->input->post('kehadiran'),
							'lama_kerja'		=> $this->input->post('lama_kerja'),

						);
			$this->Level_model->update($id_level,$level);
			
			$this->session->set_flashdata('message', 'Satu data level berhasil diupdate!');
			redirect($this->alamat);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_level valid, agar tidak ganda
	 */

}
// END Level Class

/* End of file level.php */
/* Location: ./system/application/controllers/level.php */
