<?php
/**
 * Pegawai Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pegawai extends CI_Controller
{
    /**
     * Constructor
     */

    public $title = 'pegawai';
    public $alamat = 'sdm/pegawai';

    public function Pegawai()
    {
        parent::__construct();
        $this->load->model('Pegawai_model', '', true);
        $this->load->model('Cuti_model', '', true);
        $this->load->model('Kasbon_model', '', true);
        $this->load->model('Tunjangan_model', '', true);
        $this->load->model('Lembur_model', '', true);
        $this->load->model('Gaji_model', '', true);

        $this->load->helper('fungsi');
        // content yang fix, ada terus di web
        $this->data['title'] = $this->title;

        $this->load->library('cekker');
        $this->cekker->cek($this->router->fetch_class());

    }

    /**
     * Inisialisasi variabel untuk $title(untuk id element <body>)
     */

    /**
     * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman pegawai,
     * jika tidak akan meredirect ke halaman login
     */

    /**
     * Tampilkan semua data pegawai
     */
    public function index($tahun = "")
    {
        $data = $this->data;
        $data['h2_title'] = $this->title;

        if (empty($tahun)) {
            $tahun = date("Y");
        }

        $firstPegawai = $this->Pegawai_model->firstPegawai();
		foreach ($firstPegawai as $pegawai) {
			$namaPegawai = $pegawai->tgl_masuk;
		}
        $datetime = new DateTime($namaPegawai);
        $mulai = $datetime->format('Y');

        $akhir = date("Y");
        $pagination = "";
        for ($i = $mulai; $i <= $akhir; $i++) {
            if ($tahun != $i) {
                $pagination .= "<li>" . anchor($this->alamat . '/index/' . $i, $i, array('class' => 'tahun')) . "</li>";
            } else {
                $pagination .= "<li class=active><a>$tahun</a></li>";
            }

        }

        $data['pagination'] = $pagination;

        // Load data
        $query = $this->Pegawai_model->get_aktif();
        $pegawai = $query->result();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            // Table
            /*Set table template for alternating row 'zebra'*/
            $tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
                'row_alt_start' => '<tr class="zebra">',
                'row_alt_end' => '</tr>',
            );
            $this->table->set_template($tmpl);

            /*Set table heading */
            $this->table->set_empty("&nbsp;");
            $this->table->set_heading('No', 'nama', 'cuti', 'ijin', cek_auth("auth_bayar") ? 'kasbon' : "", 'tunjangan', 'lembur', cek_auth("auth_bayar") ? 'tgl gaji' : '', 'lama bekerja', 'warna');
            $i = 0;

            foreach ($pegawai as $row) {

                $jumlah_cuti = $this->Cuti_model->jumlah_cuti($tahun, $row->id_pegawai, "cuti");

                $jumlah_ijin = $this->Cuti_model->jumlah_cuti($tahun, $row->id_pegawai, "ijin");

                $tahunx = date("Y");
                $tunjangan = $this->Tunjangan_model->get_terakhir($row->id_pegawai, $tahunx);
                isset($tunjangan->saldo) ? $saldo_tunjangan = $tunjangan->saldo : $saldo_tunjangan = 0;

                $lembur = $this->Lembur_model->total_lembur($tahun, $row->id_pegawai);
                isset($lembur->total) ? $saldo_lembur = $lembur->total : $saldo_lembur = 0;

                $kasbon = $this->Kasbon_model->get_terakhir($row->id_pegawai);
                isset($kasbon->saldo) ? $saldo_kasbon = $kasbon->saldo : $saldo_kasbon = 0;

                $lama_bekerja = "";
                $tahun_bekerja = floor($row->lama_bekerja / 12);

                if ($tahun_bekerja > 0) {
                    $lama_bekerja = $tahun_bekerja . " tahun ";
                }

                $lama_bekerja .= $row->lama_bekerja % 12 . " bulan";

                if (cek_auth("auth_bayar")) {

                    $tunjanganx = anchor_popup('sdm/tunjangan/index/' . $row->id_pegawai, $saldo_tunjangan, array('class' => 'link_aja', 'width' => 1000));
                } else {
                    $tunjanganx = $saldo_tunjangan;
                }

                $this->table->add_row(++$i,
                    anchor_popup('sdm/pegawai_detail/index/' . $row->id_pegawai, $row->nama, array('class' => 'link_aja', 'width' => 1000)),

                    anchor_popup('sdm/cuti/index/cuti/' . $tahun . '/' . $row->id_pegawai, $jumlah_cuti->total, array('class' => 'link_aja', 'width' => 1000)),
                    anchor_popup('sdm/cuti/index/ijin/' . $tahun . '/' . $row->id_pegawai, $jumlah_ijin->total, array('class' => 'link_aja', 'width' => 1000)),

                    cek_auth("auth_bayar") ? anchor_popup('sdm/kasbon/index/' . $row->id_pegawai, $saldo_kasbon, array('class' => 'link_aja', 'width' => 1000)) : "",

                    $tunjanganx,
                    anchor_popup('sdm/lembur/index/' . $row->id_pegawai, $saldo_lembur, array('class' => 'link_aja', 'width' => 1000)),
                    cek_auth("auth_bayar") ? anchor_popup('sdm/penggajian/index/' . $row->id_pegawai, $row->gajian, array('class' => 'link_aja', 'width' => 1200)) : "",

                    $lama_bekerja, "<table  width=40px><td bgcolor=#" . $row->warna . ">&nbsp;</table>"

                );
            }
            $data['table'] = $this->table->generate();
        } else {
            $data['message'] = 'Tidak ditemukan satupun data pegawai!';
        }

        $data['link'] = array('link_add' => anchor($this->alamat . '/add/', '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // Load view
        $this->load->view('template', $data);
    }

    /**
     * Hapus data pegawai
     */
    public function delete($id_pegawai)
    {
        $this->Pegawai_model->delete($id_pegawai);
        $this->session->set_flashdata('message', '1 data pegawai berhasil dihapus');

        redirect($this->alamat);
    }

    /**
     * Pindah ke halaman tambah pegawai
     */
    public function add()
    {
        $data = $this->data;
        $data['h2_title'] = 'Tambah Data ' . $this->title;
        $data['custom_view'] = 'pegawai_form';
        $data['form_action'] = site_url($this->alamat . '/add_process');
        $data['link'] = array('link_back' => anchor($this->alamat . '', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        $this->load->view('template', $data);
    }

    /**
     * Proses tambah data pegawai
     */
    public function add_process()
    {
        $data = $this->data;
        $data['h2_title'] = 'Tambah Data ' . $this->title;
        $data['custom_view'] = $this->nama_module . '/pegawai_form';
        $data['form_action'] = site_url($this->alamat . '/add_process');
        $data['link'] = array('link_back' => anchor($this->alamat . '', '<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg', 'role' => 'button')),
        );

        // Set validation rules

        $this->form_validation->set_rules('nama', 'nama', 'required|max_length[32]');

        // Jika validasi sukses
        if ($this->form_validation->run() == true) {
            // Persiapan data
            $pegawai = array(
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'nik' => $this->input->post('nik'),
                'kelamin' => $this->input->post('kelamin'),
                'tmp_lahir' => $this->input->post('tmp_lahir'),
                'tgl_lahir' => $this->input->post('tgl_lahir'),
                'tgl_masuk' => $this->input->post('tgl_masuk'),
                'merokok' => $this->input->post('merokok'),
                'norek' => $this->input->post('norek'),
                'telp' => $this->input->post('telp'),
                'email' => $this->input->post('email'),
                'goldar' => $this->input->post('goldar'),
                'status' => $this->input->post('status'),
                'anak' => $this->input->post('anak'),
                'gajian' => $this->input->post('gajian'),

            );

            $xx = $this->input->post('tgl_keluar');
            if (!empty($xx)) {
                $pegawai['tgl_keluar'] = $this->input->post('tgl_keluar');
            }

            // Proses penyimpanan data di table pegawai
            $this->Pegawai_model->add($pegawai);

            $this->session->set_flashdata('message', 'Satu data pegawai berhasil disimpan!');
            redirect($this->alamat);
        }
        // Jika validasi gagal
        else {
            $this->load->view('template', $data);
        }
    }

    /**
     * Pindah ke halaman update pegawai
     */

    /**
     * Cek apakah $id_pegawai valid, agar tidak ganda
     */

}
// END Pegawai Class

/* End of file pegawai.php */
/* Location: ./system/application/controllers/pegawai.php */
