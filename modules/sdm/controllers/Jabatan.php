<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Jabatan extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'jabatan';
		  	var $alamat = 'sdm/jabatan';

  
  function Jabatan()
	{
		parent::__construct();
		$this->load->model('Jabatan_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman jabatan,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data jabatan
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;

		
		// Load data
		$query = $this->Jabatan_model->get_all();
		$jabatan = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class="table" >',
						  'row_alt_start'  => '<tr>',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Jabatan', 'Actions');
			$i = 0;
			
			foreach ($jabatan as $row)
			{
				$this->table->add_row(++$i,  $row->nama,
										anchor($this->alamat.'/update/'.$row->id,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor($this->alamat.'/delete/'.$row->id,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data jabatan!';
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/','<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data jabatan
	 */
	function delete($id_jabatan)
	{
		$this->Jabatan_model->delete($id_jabatan);
		$this->session->set_flashdata('message', '1 data jabatan berhasil dihapus');
		
		redirect($this->alamat.'');
	}
	
	/**
	 * Pindah ke halaman tambah jabatan
	 */
	function add()
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/add_process');
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses tambah data jabatan
	 */
	function add_process()
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/add_process');
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	

		$this->form_validation->set_rules('marketing', 'marketing', 'required|max_length[32]');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$jabatan = array(
							'nama'		=> $this->input->post('marketing')
						);
			// Proses penyimpanan data di table jabatan
			$this->Jabatan_model->add($jabatan);
			
			$this->session->set_flashdata('message', 'Satu data jabatan berhasil disimpan!');
			redirect($this->alamat.'/');
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update jabatan
	 */
	function update($id_jabatan)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_jabatan);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$jabatan = $this->Jabatan_model->get_jabatan_by_id($id_jabatan);
				
	
		
		
		$data['default']['marketing']		= $jabatan->nama;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data jabatan
	 */
	function update_process($id_jabatan)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'standar_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_jabatan);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('marketing', 'Marketing', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$jabatan = array(
							'nama'		=> $this->input->post('marketing')
						);
			$this->Jabatan_model->update($id_jabatan,$jabatan);
			
			$this->session->set_flashdata('message', 'Satu data jabatan berhasil diupdate!');
			redirect($this->alamat);
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_jabatan valid, agar tidak ganda
	 */

}
// END Jabatan Class

/* End of file jabatan.php */
/* Location: ./system/application/controllers/jabatan.php */
