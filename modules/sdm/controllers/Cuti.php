<?php
/**
 * Cuti Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Cuti extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'cuti';
  var $alamat = 'sdm/cuti';
	
  
  function Cuti()
	{
		parent::__construct();
		$this->load->model('Cuti_model', '', TRUE);
	$this->load->model('Pegawai_model', '', TRUE);
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;
   
	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman cuti,
	 * jika tidak akan meredirect ke halaman login
	 */

	
	/**
	 * Tampilkan semua data cuti
	 */
	function index($jenis,$tahun,$id_pegawai)
	{
	
	
	
	
	
		$konsumen = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
	
	
	

	
		$data = $this->data;
		$data['h2_title'] = $jenis.": ".$konsumen->nama;

		
		// Load data
		$query = $this->Cuti_model->get_all($tahun,$id_pegawai,$jenis);
		$cuti = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table width=80%>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'tanggal', 'keterangan', 'Actions');
			
  
      			
	$this->table->set_heading(array("data"=>"no", "width"=>"2%"),
      array("data"=>"tanggal", "width"=>"10%"), array("data"=>"keterangan", "width"=>"53%"),array("data"=>"operasi", "width"=>"15%") );      			
			
			
			
			$i = 0;
			
			foreach ($cuti as $row)
			{
				$this->table->add_row(++$i,  $row->tanggal,$row->keterangan,
										anchor($this->alamat.'/update/'.$jenis.'/'.$row->id_cuti.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs')).' '.
										anchor($this->alamat.'/delete/'.$jenis.'/'.$row->id_cuti.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-remove"></span>',array('class'=> 'btn btn-danger btn-xs','onclick'=>"return confirm('Anda yakin akan membatalkan ".$jenis." ini?')"))
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'tahun '.$tahun.' belum pernah '.$jenis;
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$jenis.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template2', $data);
	}
		
	/**
	 * Hapus data cuti
	 */
	function delete($jenis,$id_cuti,$tahun,$id_pegawai)
	{
		$this->Cuti_model->delete($id_cuti);
		$this->session->set_flashdata('message', '1 data cuti berhasil dihapus');
		
		redirect($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai);
	}
	
	/**
	 * Pindah ke halaman tambah cuti
	 */
	function add($jenis,$tahun,$id_pegawai)
	{		
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data cuti';
		$data['custom_view'] 		= 'cuti_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$jenis.'/'.$tahun."/".$id_pegawai);
	
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
		
		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses tambah data cuti
	 */
	function add_process($jenis,$tahun,$id_pegawai)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'cuti_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$jenis.'/'.$tahun."/".$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))

										);
	
		// Set validation rules

		$this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		
		// Jika validasi sukses
		if ($this->form_validation->run() == TRUE)
		{
			// Persiapan data
			$cuti = array(
							'keterangan'		=> $this->input->post('alasan'),
							'id_pegawai'		=> $id_pegawai,
							'jenis'		=> $jenis,
							'tanggal'		=> $this->input->post('tanggal')
						);
			// Proses penyimpanan data di table cuti
			$this->Cuti_model->add($cuti);
			
			$this->session->set_flashdata('message', 'Satu data '.$jenis.' berhasil disimpan!');
			redirect($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai);
		}
		// Jika validasi gagal
		else
		{		
			$this->load->view('template2', $data);
		}		
	}
	
	/**
	 * Pindah ke halaman update cuti
	 */
	function update($jenis,$id_cuti,$tahun,$id_pegawai)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'Update cuti';
		$data['custom_view'] 		= 'cuti_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$jenis.'/'.$tahun."/".$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$cuti = $this->Cuti_model->get_cuti_by_id($id_cuti);
				
		// buat session untuk menyimpan data primary key (id_cuti)
		$this->session->set_userdata('id_cuti', $cuti->id_cuti);
		
		// Data untuk mengisi field2 form
		$data['default']['id_cuti'] 	= $cuti->id_cuti;		
		$data['default']['tanggal']		= $cuti->tanggal;
		$data['default']['alasan']		= $cuti->keterangan;				
		$this->load->view('template2', $data);
	}
	
	/**
	 * Proses update data cuti
	 */
	function update_process($jenis,$tahun,$id_pegawai)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'cuti_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$jenis.'/');

		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$cuti = array(
							'tanggal'		=> $this->input->post('tanggal'),
							'keterangan'		=> $this->input->post('alasan')
						);
			$this->Cuti_model->update($this->session->userdata('id_cuti'),$cuti);
			
			$this->session->set_flashdata('message', 'Satu data '.$jenis.' berhasil diupdate!');
			redirect($this->alamat.'/index/'.$jenis.'/'.$tahun."/".$id_pegawai);
		}
		else
		{		
			$this->load->view('template2', $data);
		}
	}
	
	/**
	 * Cek apakah $id_cuti valid, agar tidak ganda
	 */
	function valid_id($id_cuti)
	{
		if ($this->Cuti_model->valid_id($id_cuti) == TRUE)
		{
			$this->form_validation->set_message('valid_id', "cuti dengan Kode $id_cuti sudah terdaftar");
			return FALSE;
		}
		else
		{			
			return TRUE;
		}
	}
	
	/**
	 * Cek apakah $id_cuti valid, agar tidak ganda. Hanya untuk proses update data cuti
	 */
	function valid_id2()
	{
		// cek apakah data tanggal pada session sama dengan isi field
		// tidak mungkin seorang siswa diabsen 2 kali pada tanggal yang sama
		$current_id 	= $this->session->userdata('id_cuti');
		$new_id			= $this->input->post('id_cuti');
				
		if ($new_id === $current_id)
		{
			return TRUE;
		}
		else
		{
			if($this->Cuti_model->valid_id($new_id) === TRUE) // cek database untuk entry yang sama memakai valid_entry()
			{
				$this->form_validation->set_message('valid_id2', "Cuti dengan kode $new_id sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}
// END Cuti Class

/* End of file cuti.php */
/* Location: ./system/application/controllers/cuti.php */
