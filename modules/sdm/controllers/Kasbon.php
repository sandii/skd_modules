<?php
/**
 * Konsumen Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Kasbon extends  CI_Controller {
	/**
	 * Constructor, load Semester_model, Marketing_model


	 */
	
	var $limit = 30;
	var $title = 'kasbon';
  var $alamat = 'sdm/kasbon';

  
  function kasbon()
	{
		parent::__construct();
		$this->load->model('kasbon_model', '', TRUE);
		$this->load->model('Pegawai_model', '', TRUE);
		$this->load->model('keuangan/Akun_model', '', TRUE);
$this->load->model('keuangan/Kas_model', '', TRUE);
$this->load->helper('fungsi');
	
	
	// content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;



	      $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	


	}
	


	
	/**
	 * Mendapatkan semua data konsumen di database dan menampilkannya di tabel
	 */
	function index($id_pegawai,$tahun = "")
	{
		$konsumen = $this->Pegawai_model->get_pegawai_by_id($id_pegawai);
	
		$data = $this->data;
		$data['h2_title'] = "kasbon: ".$konsumen->nama;

		
if (empty($tahun))
	$tahun=date("Y");


    $mulai=2013;
		
		$akhir=date("Y");
		$pagination="";
		for($i=$mulai;$i<=$akhir;$i++)
		{  
    if($tahun!=$i)
    $pagination.="<li> ".anchor($this->alamat.'/index/'.$id_pegawai."/".$i,$i,array('class' => 'tahun'))."</li> ";
    else
     $pagination.="<li class=active><a>$tahun</a></li>";
    }

		
		$data['pagination'] = $pagination;		

		

		
		// Load data
		$query = $this->kasbon_model->get_all($id_pegawai,$tahun);

		$siswa = $query->result();
		$num_rows = $query->num_rows();
		




		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
			
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','keterangan', '<div class=pull-right>kasbon</div>', '<div class=pull-right>pembayaran</div>', '<div class=pull-right>saldo</div>');
	
			
			foreach ($siswa as $row)
			{
				$this->table->add_row( $row->tanggal,  $row->keterangan,format_uang($row->pemasukan),format_uang($row->pengeluaran),format_uang($row->saldo)
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data kasbon!';
		}		
		
		$data['link'] = array('link_add' => anchor($this->alamat.'/add/'.$id_pegawai,'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
								);
		
		// Load view
		$this->load->view('template2', $data);
	}




	function add($id_pegawai)
	{		
	  $data=$this->data;
		$data['h2_title'] 		= 'Tambah Data '.$this->title;
		$data['custom_view'] 		= 'kasbon_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										

		 $query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	  
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			} 



		$this->load->view('template2', $data);
	}
	/**
	 * Proses tambah data siswa
	 */
	function add_process($id_pegawai)
	{
	  $data=$this->data;
		$data['h2_title'] 		= 'kasbon > Tambah Data';
		$data['custom_view'] 		= 'kasbon_form';
		$data['form_action']	= site_url($this->alamat.'/add_process/'.$id_pegawai);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'/index/'.$id_pegawai,'<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// data kelas untuk dropdown menu
	
		// Set validation rules
	
 $query2 = $this->Akun_model->get_all2(1)->result();
      	foreach ($query2 as $row2)
			{	 

	  
  				$data['options_ke'][$row2->id_akundetil] = $row2->nama;

			} 


		$this->form_validation->set_rules('keterangan', 'required');
    
    
    $saldo_terakhir = $this->kasbon_model->get_terakhir($id_pegawai);
    
    $saldo=$saldo_terakhir->saldo;
		
		
		
		if ($this->form_validation->run() == TRUE)
		{
		
		
		$saldo=$saldo+$this->input->post('pemasukan')-$this->input->post('pengeluaran');
		
		
			// save data
			$uang = array('keterangan' 		=> $this->input->post('keterangan'),
	
			'id_pegawai' 		=> $id_pegawai,
							'pemasukan'		=> $this->input->post('pemasukan'),
							'pengeluaran'	=> $this->input->post('pengeluaran'),
							'tanggal'	=> $this->input->post('tanggal'),
											'saldo'	=> $saldo
						);
			$this->kasbon_model->add($uang);
			


$nama = $this->Pegawai_model->get_pegawai_by_id($id_pegawai)->username;


if($this->input->post('pemasukan')=="")
	$ketx=" bayar kasbon";
else
	$ketx=" kasbon";




		$uang = array('ket' 		=> $nama.$ketx,
						'id_akun' 		=> $this->input->post('ke'),
			
			'debet' 		=> $this->input->post('pengeluaran'),
						'tanggal' 		=> $this->input->post('tanggal'),
						'id_detail'=>$id_pegawai,
						'kredit' 		=> $this->input->post('pemasukan'),
						'kode'=>"ksb",
'id_detail'=>$id_pegawai
						

						);
			$this->Kas_model->transaksi($uang);











			$this->session->set_flashdata('message', 'data keungan berhasil disimpan!');
			redirect($this->alamat.'/index/'.$id_pegawai);
		}
		else
		{	
			$this->load->view('template', $data);
		}		
	}
	


function rangkuman($tgl1="",$tgl2="",$offset = 0)
	{
		
	
		$data = $this->data;
		$data['title'] = "kasbon";
			$data['h2_title'] = "kasbon";
    $data['custom_view'] = 'main_tanggal';
		


			
	if($this->input->post('cari'))
     {

     $tgl1=$this->input->post('tgl1');
     $tgl2=$this->input->post('tgl2');
     $offset=0;
     }



	$seminggu=date("Y-m-d", mktime(0, 0, 0, date("n"), date("j")-7, date("Y")));
   
   
    if(empty($tgl1)) {$tgl1=$seminggu;$cari_tgl1=$seminggu;}
    else $cari_tgl1=$tgl1;
    if(empty($tgl2)or $tgl2=="sekarang") {$tgl2="sekarang";$cari_tgl2="";}
    else $cari_tgl2=$tgl2;
  

		$data["tgl1"]=$tgl1;
    $data["tgl2"]=$tgl2;	
	



		// Load data
		$query = $this->kasbon_model->get_rangkuman($this->limit,$offset,$cari_tgl1,$cari_tgl2);

		$siswa = $query->result();
		
		$num_rows = $this->kasbon_model->get_rangkuman("",$offset,$cari_tgl1,$cari_tgl2)->num_rows();

	

		
		if ($num_rows > 0)
		{
			// Generate pagination			
		
	
$uri_segment = 5;
      $config['base_url'] = site_url($this->alamat.'/rangkuman/'.$tgl1.'/'.$tgl2.'/');
			$config['total_rows'] = $num_rows;
			$config['per_page'] = $this->limit;
					$config['uri_segment'] = $uri_segment;

$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class=active><a href=#>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';


			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
		
					
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0"  class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('tanggal','nama pegawai','keterangan', '<div class=pull-right>kasbon</div>', '<div class=pull-right>pembayaran</div>');
	
			
			foreach ($siswa as $row)
			{
		
$nama_pegawai=$this->Pegawai_model->get_pegawai_by_id($row->id_pegawai)->nama;


				$this->table->add_row( $row->tanggal,  $nama_pegawai,$row->keterangan, format_uang($row->pemasukan),format_uang($row->pengeluaran)
									
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data kasbon!';
		}		

		
		// Load view
		$this->load->view('template', $data);
	}




}
// END Konsumen Class

/* End of file konsumen.php */
/* Location: ./system/application/controllers/konsumen.php */
