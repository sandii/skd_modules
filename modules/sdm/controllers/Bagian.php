<?php
/**
 * Marketing Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bagian extends  CI_Controller {
	/**
	 * Constructor
	 */
	
  var $title = 'bagian';
  	var $alamat = 'sdm/bagian';

	
  
  function Bagian()
	{
		parent::__construct();
		$this->load->model('Bagian_model', '', TRUE);
	
  // content yang fix, ada terus di web
    $this->data['nama']=$this->session->userdata('nama');
    $this->data['title']=$this->title;   
	  $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());	
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman bagian,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index()
	{
			$this->get_all();
		

	}
	
	/**
	 * Tampilkan semua data bagian
	 */
	function get_all()
	{
		$data = $this->data;
		$data['h2_title'] = $this->title;
		
		
		// Load data
		$query = $this->Bagian_model->get_all();
		$bagian = $query->result();
		$num_rows = $query->num_rows();
		
		if ($num_rows > 0)
		{
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class=table>',
						  'row_alt_start'  => '<tr class="zebra">',
							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);

			/*Set table heading */
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'Bagian', 'Grade', 'Actions');
			$i = 0;
			
			foreach ($bagian as $row)
			{
				$this->table->add_row(++$i,  $row->nama,$row->grade,
										anchor($this->alamat.'/update/'.$row->id_bagian,'<span class="glyphicon glyphicon-pencil"></span>',array('class' => 'btn btn-warning btn-xs'))
										
										);
			}
			$data['table'] = $this->table->generate();
		}
		else
		{
			$data['message'] = 'Tidak ditemukan satupun data bagian!';
		}		
		

		
		// Load view
		$this->load->view('template', $data);
	}
		
	/**
	 * Hapus data bagian
	 */
	function delete($id_bagian)
	{
		$this->Bagian_model->delete($id_bagian);
		$this->session->set_flashdata('message', '1 data bagian berhasil dihapus');
		
		redirect($this->alamat.'');
	}
	
	/**
	 * Pindah ke halaman tambah bagian
	 */
	
	
	/**
	 * Proses tambah data bagian
	 */
	
	
	/**
	 * Pindah ke halaman update bagian
	 */
	function update($id_bagian)
	{
		$data			= $this->data;
		$data['h2_title'] 		= 'update '.$this->title;
		$data['custom_view'] 		= 'bagian_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_bagian);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
	
		// cari data dari database
		$bagian = $this->Bagian_model->get_bagian_by_id($id_bagian);
				
	
		
		
		$data['default']['marketing']		= $bagian->nama;
		$data['default']['grade']		= $bagian->grade;
				
		$this->load->view('template', $data);
	}
	
	/**
	 * Proses update data bagian
	 */
	function update_process($id_bagian)
	{
		$data 			= $this->data;
		$data['h2_title'] 		= $this->title.' > Update Proses';
		$data['custom_view'] 		= 'bagian_form';
		$data['form_action']	= site_url($this->alamat.'/update_process/'.$id_bagian);
		$data['link'] 			= array('link_back' => anchor($this->alamat.'','<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>', array('class' => 'btn btn-success btn-lg','role'=> 'button'))
										);
										
		// Set validation rules
		$this->form_validation->set_rules('marketing', 'Marketing', 'required|max_length[32]');
		
		if ($this->form_validation->run() == TRUE)
		{
			// save data
			$bagian = array(
							'nama'		=> $this->input->post('marketing'),
							'grade'		=> $this->input->post('grade')

						);
			$this->Bagian_model->update($id_bagian,$bagian);
			
			$this->session->set_flashdata('message', 'Satu data bagian berhasil diupdate!');
			redirect($this->alamat.'');
		}
		else
		{		
			$this->load->view('template', $data);
		}
	}
	
	/**
	 * Cek apakah $id_bagian valid, agar tidak ganda
	 */

}
// END Bagian Class

/* End of file bagian.php */
/* Location: ./system/application/controllers/bagian.php */
