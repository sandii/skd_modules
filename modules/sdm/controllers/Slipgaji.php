<?php
/**
 * Orderdetil Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Slipgaji extends  CI_Controller {
	/**
	 * Constructor
	 */
	function Slipgaji()
	{
		parent::__construct();
		$this->load->model('Pegawai_model', '', TRUE);
		$this->load->model('Penggajian_model', '', TRUE);
	
        $this->load->library('cekker');
    $this->cekker->cek($this->router->fetch_class());
  
  }
	
	/**
	 * Inisialisasi variabel untuk $title(untuk id element <body>)
	 */
	var $title = 'slipgaji';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman orderdetil,
	 * jika tidak akan meredirect ke halaman login
	 */
	function index($id_penggajian)
	{
		
		$data['title'] = $this->title;
		//$data['h2_title'] = 'SLIP GAJI';
		$data['alamat'] = 'slipgaji_'.$_SESSION['perusahaan'];
  



 	$penggajian = $this->Penggajian_model->get_by_id($id_penggajian);
  
 	$pegawai = $this->Pegawai_model->get_pegawai_by_id($penggajian->id_pegawai);
 
  
  
  $nama="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
 
  
$data['ttd'] = "Hormat kami <br><br><br><br><br><br><br>(&nbsp;&nbsp;".$nama."&nbsp;&nbsp;&nbsp;)";
 
 
$data['link2'] =  "<img src=".base_url()."gambar/corporate/".$_SESSION['perusahaan'].".png>";
  
  
  	// Load data
			// Table
			/*Set table template for alternating row 'zebra'*/
			$tmpl = array( 'table_open'    => '<table border="0"  cellpadding="3" cellspacing="0" width=100% class=tablex>',

							'row_alt_end'    => '</tr>'
						  );
			$this->table->set_template($tmpl);



			/*Set table heading */
			$this->table->set_empty("&nbsp;");
				
		
		
	  	$this->table->set_heading(array("data"=>"jenis", "width"=>"50%"),
      array("data"=>"jumlah", "width"=>"50%")
     
    );	
					
		
				$this->table->add_row("gaji pokok",  "<div align=right>".number_format($penggajian->pokok, 0, ',', '.'));
				

if(!empty($penggajian->lama_kerja))
				$this->table->add_row("tunjangan lama kerja",   "<div align=right>".number_format($penggajian->lama_kerja, 0, ',', '.'));			
if(!empty($penggajian->bagian))
				$this->table->add_row("tunjangan bagian",   "<div align=right>".number_format($penggajian->bagian, 0, ',', '.'));			
if(!empty($penggajian->performance))
				$this->table->add_row("tunjangan performance",   "<div align=right>".number_format($penggajian->performance, 0, ',', '.'));			
if(!empty($penggajian->transportasi))
				$this->table->add_row("tunjangan transportasi",   "<div align=right>".number_format($penggajian->transportasi, 0, ',', '.'));			
if(!empty($penggajian->komunikasi))
				$this->table->add_row("tunjangan komunikasi",   "<div align=right>".number_format($penggajian->komunikasi, 0, ',', '.'));			
if(!empty($penggajian->kehadiran))
				$this->table->add_row("tunjangan kehadiran",   "<div align=right>".number_format($penggajian->kehadiran, 0, ',', '.'));			
if(!empty($penggajian->lain2))
				$this->table->add_row("tunjangan ".$penggajian->lain2,   "<div align=right>".number_format($penggajian->jumlah_lain2, 0, ',', '.'));			








				if($penggajian->jam_lembur==0)
				$lemburx="";
				else
				$lemburx=$penggajian->jam_lembur. " jam";
				
if(!empty($penggajian->lembur))
				$this->table->add_row("lembur". $lemburx,   "<div align=right>".number_format($penggajian->lembur, 0, ',', '.'));			
				
if(!empty($penggajian->bonus))
				$this->table->add_row("bonus",   "<div align=right>".number_format($penggajian->bonus, 0, ',', '.'));			
		

if(!empty($penggajian->kasbon))
				$this->table->add_row("potong kasbon",  "<div align=right>-".number_format($penggajian->kasbon, 0, ',', '.'));
    
    	$data['tablex'] = $this->table->generate();
			
		
	 
	
		$tanggalx = date('d/m/Y', strtotime($penggajian->tanggal));
	
	$data['total']=number_format($penggajian->total, 0, ',', '.');

	$data['konsumen']=$pegawai->nama;
			
 	$data['tanggal']=$tanggalx;
			
 	$data['nomor']=$id_penggajian;
      	   
		
		
		// Load view
		$this->load->view('template_slip', $data);
		
	}
		
	/**
	 * Hapus data orderdetil
	 */


	

}
// END Orderdetil Class

/* End of file orderdetil.php */
/* Location: ./system/application/controllers/orderdetil.php */
