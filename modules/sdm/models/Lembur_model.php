<?php
/**
 * Menu_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Lembur_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Lembur_model()
	{
		parent::__construct();
	}
	
	// Iid_konsumenialisasi perusahaan tabel yang digunakan
	var $table = 'lembur';
	
	/**
	 * Menghitung jumlah baris dalam sebuah tabel, ada kaitannya dengan pagination
	 */
	
  
  	function get_all($id_user,$tahun)
	{
		$this->db->select('*');
				$this->db->where('tahun', $tahun);
						$this->db->where('id_pegawai', $id_user);
		$this->db->from($this->table);	
	
				$this->db->order_by('bulan', 'asc');	
		return $this->db->get()->result();
	}
	
	
  
  	function check_bulan($id_pegawai,$tahun,$bulan)
	{
		$this->db->select('*');
				$this->db->where('tahun', $tahun);
			$this->db->where('id_pegawai', $id_pegawai);
	  			$this->db->where('bulan', $bulan);
    	$this->db->from($this->table);	
	

			return $this->db->get()->num_rows();
	}
  	
	
  
	function total_lembur($tahun,$id_pegawai)
	{
	
  
  	$this->db->select('sum(jam) as total');
		
		$this->db->where("tahun",$tahun);
	
		
	  $this->db->where('id_pegawai', $id_pegawai);
		return $this->db->get($this->table)->row();
	}
  
  function get_lembur($id_pegawai)
	{
	
  
  	$this->db->select('sum(jam) as total');
		
		$this->db->where("dibayar","belum");
	
		
	  $this->db->where('id_pegawai', $id_pegawai);
		return $this->db->get($this->table)->row();
	}
  
  
  
  	function count_all()
	{
		return $this->db->count_all($this->table);
	}

	
	
	/**
	 * Tampilkan 10 baris menu terkini, diurutkan berdasarkan tanggal (Descending)
	 */

	
	
	function get_lembur_by_id($id_lembur)
	{

		
		$this->db->select ('*');
		$this->db->from($this->table);
			  			$this->db->where('id_lembur', $id_lembur);

	
  	return $this->db->get();
	}
	
	
	/**
	 * Menghapus sebuah entry data menu
	 */
	function delete($id_menu)
	{
		$this->db->where('id_menu', $id_menu);
		$this->db->delete($this->table);
	}
	
	/**
	 * Menambahkan sebuah data ke tabel menu
	 */
	function add($menu)
	{
		$this->db->insert($this->table, $menu);
	}
	
	/**
	 * Dapatkan data menu dengan id_menu tertentu, untuk proses update
	 */
	
	function get_terakhir()
	{
			$this->db->select ('*');
		$this->db->from($this->table);
		$this->db->limit(1,0);
		$this->db->order_by('id_keuangan', 'desc');	

  	return $this->db->get()->row();
		
		
	}
	
	
	function get_menu()
	{
		$this->db->select('*');
		return $this->db->get($this->table);
			
		
	}
		
	function update_bayar($id_pegawai)
	{
	$sql= " update `lembur` set dibayar='sudah' where id_pegawai =".$id_pegawai;
	$this->db->query($sql);	
		
	}
	
	
	
	
	
	function get_last_id_menu()
	{
		$this->db->select('id_menu');
		$this->db->order_by('id_menu', 'desc');
		$this->db->limit(1,0);
		return $this->db->get($this->table);
	}
	
	/**
	 * Update data menusi
	 */
	function update($id_lembur, $menu)
	{
		$this->db->where('id_lembur', $id_lembur);
		$this->db->update($this->table, $menu);
	}
	function update_comment($id_menu, $comment)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->komen;
		
		$komen.=$comment;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set komen='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	function update_pembayaran_log($id_menu, $pembayaran_log)
	{
		$this->db->select('*');
		$this->db->where('id_menu', $id_menu);
		$hasil=$this->db->get($this->table);	
		
		$komen=$hasil->row()->pembayaran_log;
		
		$komen.=$pembayaran_log;
		//$this->db->where('id_menu', $id_menu);
		//$this->db->update($this->table, $comment);
		
	$sql= " update `menu` set pembayaran_log='".$komen."' where id_menu =".$id_menu;
	$this->db->query($sql);
			
		
	}
	
	
	/**
	 * Cek apakah ada entry data yang sama pada tanggal tertentu untuk konsumen dengan NIS tertentu pula
	 */
	function valid_entry($id_konsumen, $tanggal)
	{
		$this->db->where('id_konsumen', $id_konsumen);
		$this->db->where('tanggal', $tanggal);
		$query = $this->db->get($this->table)->num_rows();
						
		if($query > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}	
}
// END Menu_model Class

/* End of file menu_model.php */
/* Location: ./system/application/models/menu_model.php */
