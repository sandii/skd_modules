<?php
/**
 * User_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Jabatan_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Jabatan_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'jabatan';
	
	/**
	 * Mendapatkan semua data user, diurutkan berdasarkan id_user
	 */
	function get_all()
	{
		$this->db->order_by('id');
		return $this->db->get($this->table);
	}
	
		function get_jabatan_by_id($id_jabatan)
	{
		return $this->db->get_where($this->table, array('id' => $id_jabatan))->row();
	}
	
		function update($id_marketing, $marketing)
	{
		$this->db->where('id', $id_marketing);
		$this->db->update($this->table, $marketing);
	}
	
	
	
		function add($marketing)
	{
		$this->db->insert($this->table, $marketing);
	}
	
		function delete($id_marketing)
	{
		$this->db->delete($this->table, array('id' => $id_marketing));
	}
	
	
		function get_jabatan()
	{
		$this->db->select('*');
				$this->db->order_by('id');
		return $this->db->get($this->table);
		
		
		
	}

}
// END Siswa_model Class

/* End of file user_model.php */
/* Location: ./system/application/models/marketing_model.php */
