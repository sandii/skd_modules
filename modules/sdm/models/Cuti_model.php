<?php
/**
 * Cuti_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Cuti_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Cuti_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'cuti';
	
	/**
	 * Mendapatkan semua data cuti, diurutkan berdasarkan id_cuti
	 */
	function get_cuti()
	{
		$this->db->order_by('id_cuti');
		return $this->db->get('cuti');
	}
	
	/**
	 * Mendapatkan data sebuah cuti
	 */
	function get_cuti_by_id($id_cuti)
	{
		return $this->db->get_where($this->table, array('id_cuti' => $id_cuti), 1)->row();
	}
	
	function get_all($tahun,$id_pegawai,$jenis)
	{
		$this->db->order_by('id_cuti');
		
		$this->db->where("tanggal >=",$tahun."-01-01");
			$this->db->where("tanggal <=",$tahun."-12-31");
		$this->db->where('jenis', $jenis);
	  $this->db->where('id_pegawai', $id_pegawai);
		return $this->db->get($this->table);
	}
	
	
	function jumlah_cuti($tahun,$id_pegawai,$jenis)
	{
	
  
  	$this->db->select('count(*)as total');
		
		$this->db->where("tanggal >=",$tahun."-01-01");
			$this->db->where("tanggal <=",$tahun."-12-31");
		
	  $this->db->where('id_pegawai', $id_pegawai);
	  $this->db->where('jenis', $jenis);
		return $this->db->get($this->table)->row();
	}
	
	
	
	
	
	
	
	
	/**
	 * Menghapus sebuah data cuti
	 */
	function delete($id_cuti)
	{
		$this->db->delete($this->table, array('id_cuti' => $id_cuti));
	}
	
	/**
	 * Tambah data cuti
	 */
	function add($cuti)
	{
		$this->db->insert($this->table, $cuti);
	}
	
	/**
	 * Update data cuti
	 */
	function update($id_cuti, $cuti)
	{
		$this->db->where('id_cuti', $id_cuti);
		$this->db->update($this->table, $cuti);
	}
	
	/**
	 * Validasi agar tidak ada cutid dengan id ganda
	 */
	function valid_id($id_cuti)
	{
		$query = $this->db->get_where($this->table, array('id_cuti' => $id_cuti));
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
// END Siswa_model Class

/* End of file cuti_model.php */
/* Location: ./system/application/models/cuti_model.php */
