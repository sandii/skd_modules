<?php
/**
 * Marketing_model Class
 *
 * @author    Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pegawai_model extends CI_Model
{
    /**
     * Constructor
     */
    public function Pegawai_model()
    {
        parent::__construct();
    }

    // Inisialisasi nama tabel yang digunakan
    public $table = 'pegawai';

    /**
     * Mendapatkan semua data marketing, diurutkan berdasarkan id_marketing
     */

    /**
     * Mendapatkan data sebuah marketing
     */
    public function get_pegawai_by_id($id_marketing)
    {

        $this->db->select('*, PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM NOW()), EXTRACT(YEAR_MONTH FROM tgl_masuk)) AS lama_bekerja');
        $this->db->where('id_pegawai', $id_marketing);
        return $this->db->get($this->table)->row();
    }

    public function get_pegawai_by_kode($id_marketing)
    {

        return $this->db->get_where($this->table, array('kode' => $id_marketing))->row();

    }

    public function get_all()
    {
        $this->db->order_by('id_pegawai');
        return $this->db->get($this->table);
    }

    public function get_gaji($query)
    {
        $this->db->where('tgl_keluar is null');
        $this->db->where($query);
        $this->db->where("tgl_masuk is not NULL ");
        return $this->db->get($this->table);
    }

    public function get_aktif()
    {

        $this->db->select('*, PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM NOW()), EXTRACT(YEAR_MONTH FROM tgl_masuk)) AS lama_bekerja');

        $this->db->order_by('id_pegawai');
        $this->db->where('tgl_keluar is NULL');
        $this->db->where("tgl_masuk is not NULL ");

        return $this->db->get($this->table);
    }

    public function get_ar()
    {

        $this->db->order_by('id_pegawai');
        $this->db->where("kode is not NULL ");
        $this->db->where("kode !='' ");
        $this->db->where("kode !='0' ");
        $this->db->where('tgl_keluar is NULL');

        return $this->db->get($this->table);
    }

    public function get_nonaktif()
    {
        $this->db->order_by('id_pegawai');
        $this->db->where('tgl_keluar is not NULL');

        return $this->db->get($this->table);
    }

    /**
     * Menghapus sebuah data marketing
     */
    public function delete($id_marketing)
    {
        $this->db->delete($this->table, array('id_pegawai' => $id_marketing));
    }

    /**
     * Tambah data marketing
     */
    public function add($marketing)
    {
        $this->db->insert($this->table, $marketing);
    }

    /**
     * Update data marketing
     */
    public function update($id_marketing, $marketing)
    {
        $this->db->where('id_pegawai', $id_marketing);
        $this->db->update($this->table, $marketing);
    }

    /**
     * Validasi agar tidak ada marketingd dengan id ganda
     */
    public function valid_id($id_marketing)
    {
        $query = $this->db->get_where($this->table, array('id_marketing' => $id_marketing));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

	public function firstPegawai()
    {
        $this->db->from('pegawai');
        $this->db->where('tgl_masuk IS NOT NULL', null, false);
        $this->db->order_by("tgl_masuk", "desc");
        return $this->db->get()->result();
    }
}
// END Siswa_model Class

/* End of file marketing_model.php */
/* Location: ./system/application/models/marketing_model.php */
