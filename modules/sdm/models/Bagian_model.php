<?php
/**
 * User_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Bagian_model extends CI_Model {
	/**
	 * Constructor
	 */
	function Bagian_model()
	{
		parent::__construct();
	}
	
	// Inisialisasi nama tabel yang digunakan
	var $table = 'bagian';
	
	/**
	 * Mendapatkan semua data user, diurutkan berdasarkan id_user
	 */
	function get_all()
	{
		$this->db->order_by('id_bagian');
		return $this->db->get($this->table);
	}
	
		function get_bagian_by_id($id_bagian)
	{
		return $this->db->get_where($this->table, array('id_bagian' => $id_bagian))->row();
	}
	
		function update($id_marketing, $marketing)
	{
		$this->db->where('id_bagian', $id_marketing);
		$this->db->update($this->table, $marketing);
	}
	
	
	
		function add($marketing)
	{
		$this->db->insert($this->table, $marketing);
	}
	
		function delete($id_marketing)
	{
		$this->db->delete($this->table, array('id' => $id_marketing));
	}
	
	
		function get_bagian()
	{
		$this->db->select('*');
				$this->db->order_by('id_bagian');
		return $this->db->get($this->table);
		
		
		
	}

}
// END Siswa_model Class

/* End of file user_model.php */
/* Location: ./system/application/models/marketing_model.php */
